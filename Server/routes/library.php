<?php
Route::post('/login', 'LoginController@login');
Route::middleware(['auth:api'])->group(function () {

    Route::post('/add-book', 'BookController@addBook');
    Route::post('/add-book-quantity', 'BookController@addBookQuantity');

    Route::post('/edit-book', 'BookController@editBook');
    Route::post('/edit-book-block', 'BookController@editBookBlock');

    Route::post('/search-user', 'UserController@searchUser');
    Route::post('/search-book', 'BookController@searchBook');

    Route::post('/get-user-info', 'UserController@getUserInfo');

    Route::post('/search-book-for-ir', 'BookController@searchBookForIR');
    Route::post('/search-user-for-ir', 'UserController@searchUserForIR');

    Route::post('/return-book', 'BookController@returnBook');
    Route::post('/issue-book', 'BookController@issueBook');
    Route::post('/delete-lost-book', 'BookController@deleteLostBook');

    Route::get('/ir-history', 'BookController@irHistory');
    Route::get('/get-dashboard-info', 'LoginController@getDashboard');

    Route::get('/get-faculty-list', 'UserController@getFacultyList');
    Route::get('/get-batch-list', 'UserController@getBatchList');

    Route::post('/get-book-detail', 'BookController@getBookDetail');
    Route::post('/get-sub-book-detail', 'BookController@getSubBookDetail');

    Route::post('/delete-book-block', 'BookController@deleteBookBlock');
});