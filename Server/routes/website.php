<?php
 
Route::group(['domain' => 'hcc.edu.np'], function(){

    Route::get('/refresh',function(){
        Artisan::call('cache:clear');
        Artisan::call('clear-compiled');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
       // Artisan::call('config:cache');
    });

	Route::get('/','PagesController@home');
//
//    Route::get('/about','PagesController@about');
//
//    Route::get('/contact','PagesController@contact');
//
//    Route::get('/gallery','PagesController@gallery');
//
//    Route::get('/bsccsit','PagesController@bsccsit');
//
//    Route::get('/syllabus','PagesController@syllabus');
//
//    Route::get('/science','PagesController@science');
//
//    Route::get('/management','PagesController@management');
//    Route::get('/work/{id}','PagesController@work');
//    Route::get('/allwork','PagesController@allwork');
//
//    Route::get('{value}','PagesController@customMenu');
    
});