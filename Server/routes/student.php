<?php

Route::group(['domain' =>'student.hcc.edu.np'], function(){

    Route::get('auth-from-admin/{api_token}/{id}/{remember_token}','AuthFromAdminController@Auth');   

	Route::get('/',function(){
		return redirect('/profile');
	});
	/*
	**		New Student
	*/
    Route::get('/signup',  function () {return 'Registration has been closed.';});
    //Route::post('/signup', 'SignUpController@signup');
    Route::get('/{token}/{anythings}/token', 'SignUpController@verifyemail');

//	Route::Auth();

	Route::get('/upload-profile-pic','RegisterNewController@profilePicForm');
    Route::get('/registration-form','RegisterNewController@form');
    Route::post('/registration-form','RegisterNewController@create');

    Route::post('/update','RegisterNewController@update');  

    Route::post('/upload-profile-pic','RegisterNewController@uploadProfilePic');

    /*
    **		Student
    */   
    Route::get('/dashboard','DashboardController@dashboard');
    
    Route::get('/profile','ProfileController@profile');

    Route::post('/post','ProfileController@post');
    Route::delete('/post','ProfileController@deletePost');

    Route::post('/comment','ProfileController@comment');
    Route::delete('/comment','ProfileController@deleteComment');

    Route::get('/other-users','OtherUserController@users');    

    Route::get('/storage','StorageController@drive');
    Route::post('/storage','StorageController@directory');
    Route::post('/up-one-level','StorageController@oneLevelUp');
    Route::post('/create-file','StorageController@createFile');
    Route::post('/make-directory','StorageController@makeDirectory');
    Route::delete('/delete-files-and-folder','StorageController@deleteFilesAndFolder');
    Route::post('/copy-files-and-folder','StorageController@copyFilesAndFolder');
    Route::post('/move-files-and-folder','StorageController@moveFilesAndFolder');
    Route::post('/rename','StorageController@rename');
    Route::post('/view','StorageController@view');
    Route::post('/download','StorageController@download');
    Route::post('/save','StorageController@save');
    Route::post('/edit','StorageController@edit');    
    Route::post('/upload','StorageController@upload');
        
    Route::post('/others-storage','OthersStorageController@directory');
    Route::post('/others-up-one-level','OthersStorageController@oneLevelUp');    
    Route::post('/others-view','OthersStorageController@view');
    Route::post('/others-download','OthersStorageController@download');

    Route::get('/web-hosting','WebStorageController@drive');
    Route::post('/web-storage','WebStorageController@directory');
    Route::post('/web-up-one-level','WebStorageController@oneLevelUp');
    Route::post('/web-create-file','WebStorageController@createFile');
    Route::post('/web-make-directory','WebStorageController@makeDirectory');
    Route::delete('/web-delete-files-and-folder','WebStorageController@deleteFilesAndFolder');
    Route::post('/web-copy-files-and-folder','WebStorageController@copyFilesAndFolder');
    Route::post('/web-move-files-and-folder','WebStorageController@moveFilesAndFolder');
    Route::post('/web-rename','WebStorageController@rename');
    Route::post('/web-view','WebStorageController@view');
    Route::post('/web-download','WebStorageController@download');
    Route::post('/web-save','WebStorageController@save');
    Route::post('/web-edit','WebStorageController@edit');    
    Route::post('/web-upload','WebStorageController@upload');
    
    
    Route::post('/url','ProfileController@url');
    Route::delete('/url','UrlController@deleteUrl');
    Route::get('/url','UrlController@url');
    
    Route::post('/feedback','FeedbackController@addFeedback');
    Route::delete('/feedback','FeedbackController@deleteFeedback');
    Route::get('/feedback','FeedbackController@getFeedback');

    Route::get('/newsfeed','NewsFeedController@newsfeed');
    Route::get('/privacy-policy',function(){return view('student.privacy-policy',['title'=>'Privacy policy']);});

    Route::get('routine', 'RoutineController@routine');
    Route::get('/attendance','AttendanceController@attendance'); 
    Route::post('/attendance','AttendanceController@customAttendance'); 


});