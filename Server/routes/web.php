<?php
//
///*
//|--------------------------------------------------------------------------
//| Web Routes
//|--------------------------------------------------------------------------
//|
//| Here is where you can register web routes for your application. These
//| routes are loaded by the RouteServiceProvider within a group which
//| contains the "web" middleware group. Now create something great!
//|
//*/
//
//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::get('/create-excel', function () {
//    Excel::create('Filename', function($excel) {
//
//        $excel->sheet('First sheet', function($sheet) {
//            $sheet->setOrientation('landscape');
//
//            $sheet
////                ->fromArray(array(
////                array('data1', 'data2'),
////                array('data3', 'data4')
////            ));
//
//            ->fromModel(\HCCNetwork\User::all());
//
//        });
//
//    })
////        ->export('xls');
//
//// or
//    ->store('xlsx');
//    return 'success';
//});
//
//
//
//
//
//Route::get('/push-notification', function () {
//
//
//#API access key from Google API's Console
//    define('API_ACCESS_KEY', 'AAAA-dixc38:APA91bEGp3ZU5RemDpmdWhVVYBQHQ1kecB1WmMScYY2pMVOuOnzDSkav1pQxNUNQbFkSEQR-5ILWAx9ovSsPtdkkIcmxn2xc6wTFwYSVK4VgE0640rv1McdOUWHghXbb5RtNBshn1roo');
//    $registrationIds = "cEJEIc4GewA:APA91bGJ6zTeFUP7je2DFsUJm8M9gFAX7Hdg8BVlfe1PPL-1Wuo1wwJLMNtBXWZNClIhP5BgD1XSva33q5cpVIf6xaASLgEmdtGIQKl8khwhEABIdMioAKfYnvQ0IGFOI8EeT69I7RGC";
//#prep the bundle
//    $msg = array
//    (
//        'body' => 'Body  Of Notification',
//        'title' => 'Title Of Notification',
//        'icon' => 'myicon',/*Default Icon*/
//        'sound' => 'mySound'/*Default sound*/
//    );
//    $fields = array
//    (
//        'to' => $registrationIds,
//        'notification' => $msg,
//        'data' => $msg
//    );
//
//
//    $headers = array
//    (
//        'Authorization: key=' . API_ACCESS_KEY,
//        'Content-Type: application/json'
//    );
//#Send Reponse To FireBase Server
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
//    curl_setopt($ch, CURLOPT_POST, true);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//    $result = curl_exec($ch);
//    curl_close($ch);
//#Echo Result Of FireBase Server
//    echo $result;
//});
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

//Auth::routes();

// Authentication Routes...
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

// Password Reset Routes...
Route::post('password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
    'as' => '',
    'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm'
]);

// Registration Routes...
Route::get('register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
    'as' => '',
    'uses' => 'Auth\RegisterController@register'
]);

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

