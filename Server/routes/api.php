<?php

use Illuminate\Http\Request;
use HCCNetwork\NotificationUser;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'ApiController@login');
Route::get('/calendar-event', 'ApiController@getCalendarEvent');
Route::get('/get-routine', 'ApiController@getRoutine');

Route::middleware('auth:api')->get('/get-notification', function () {
    return NotificationUser::join('notifications', 'notification_user.notification_id', '=', 'notifications.id')
        ->leftJoin('users','notifications.sender_id','=','users.id')
        ->leftJoin('teachers','users.id','=','teachers.id')
        ->where('notification_user.user_id',Auth::id())
        ->orderBy('notifications.schedule_date','desc')
        ->select('notifications.id as id',
            'notifications.title as title',
            'notifications.description as description',
            'notifications.schedule_date as date',
            'notifications.time as time',
            'notifications.send as send',
            'teachers.first_name as first_name',
            'teachers.last_name as last_name',
            'users.user_type_id as user_type'
        )
        ->groupBy('notifications.id')
        ->get();
});

Route::middleware('auth:api')->post('/reset-password', 'ApiController@resetPassword');
Route::middleware('auth:api')->get('/get-course-plan', 'ApiController@getCoursePlan');


Route::middleware('auth:api')->get('/get-library-info', 'ApiController@getLibraryInfo');

Route::middleware('auth:api')->post('/search-available-book', 'ApiController@searchAvailableBook');

Route::middleware('auth:api')->get('/teacher/getsubjectlist', 'TeacherApiController@getSubjectList');
Route::middleware('auth:api')->get('/teacher/get_subject', 'TeacherApiController@getSubject');

Route::middleware('auth:api')->get('/student/getsubjectlist', 'StudentApiController@getSubjectList');

Route::middleware('auth:api')->post('/teacher/getstudentlist', 'TeacherApiController@getStudentList');
Route::middleware('auth:api')->post('/teacher/getstudentlistofschool', 'TeacherApiController@getStudentListOfSchool');

Route::middleware('auth:api')->post('/teacher/getstudentlistofdate', 'TeacherApiController@getStudentListOfDate');

Route::middleware('auth:api')->post('/teacher/takeattendance', 'TeacherApiController@takeAttendance');


Route::middleware('auth:api')->post('/teacher/updateattendance', 'TeacherApiController@updateAttendance');
Route::middleware('auth:api')->post('/teacher/get_subject_for_replace', 'TeacherApiController@getSubjectForReplace');

Route::middleware('auth:api')->get('/teacher/get_subject_of_school', 'TeacherApiController@getSubjectOfSchool');
Route::middleware('auth:api')->post('/teacher/get_all_subject_of_school', 'TeacherApiController@getAllSubjectOfSchool');


Route::middleware('auth:api')->post('/teacher/getstudentattendance', 'TeacherApiController@getStudentAttendance');
Route::middleware('auth:api')->post('/teacher/push-notification', 'TeacherApiController@pushNotification');
Route::middleware('auth:api')->get('/teacher/get-notification-history', 'TeacherApiController@getNotificationHistory');

Route::middleware('auth:api')->post('/teacher/add-course-plan', 'TeacherApiController@addCoursePlan');
Route::middleware('auth:api')->post('/teacher/delete-course-plan', 'TeacherApiController@deleteCoursePlan');
Route::middleware('auth:api')->post('/teacher/start-course-plan', 'TeacherApiController@startCoursePlan');
Route::middleware('auth:api')->post('/teacher/end-course-plan', 'TeacherApiController@endCoursePlan');
Route::middleware('auth:api')->post('/teacher/edit-course-plan', 'TeacherApiController@editCoursePlan');
Route::middleware('auth:api')->get('/teacher/get-my-classes', 'TeacherApiController@getMyClasses');

Route::middleware('auth:api')->post('/send-feedback', 'ApiController@sendFeedback');

Route::
middleware('auth:api')->
get('/teacher/routine', 'TeacherApiController@routine');


