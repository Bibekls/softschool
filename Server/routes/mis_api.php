<?php
//Guest
Route::post('/post_guest','guestController@postGuest');


//Student
Route::post('/post_student','studentController@postStudent');
Route::get('/get_parent','studentController@getParent');
Route::get('/email/{id}','studentController@postEmail');



//Teacher
Route::get('/faculties','teacherController@getFaculties');
Route::post('/post_teacher','teacherController@postTeacher');


//Subject
Route::post('/clone_subject','subjectController@cloneSubject');
Route::post('/store_subject','subjectController@storeSubject');
Route::get('/get_level/{id}','subjectController@getLevel');
Route::get('/get_subject','subjectController@getSubject');
Route::post('/edit_subject','subjectController@editSubject');
Route::get('/delete_subject/{id}','subjectController@deleteSubject');
Route::post('/delete_in_block','subjectController@deleteInBlock');


//Event
Route::post('/post_event','eventController@postEvent');
Route::get('/get_event','eventController@getEvent');
Route::get('/future_event','eventController@futureEvent');
Route::get('/past_event','eventController@pastEvent');
Route::get('/delete_event/{id}','eventController@deleteEvent');


//Configuration
Route::post('/post_faculty','configurationController@postFaculty');
Route::get('/get_faculty','configurationController@getFaculty');
Route::get('/delete_faculty/{id}','configurationController@deleteFaculty');


//Routine
Route::get('/get_teacher_form_routine/{id}','routineController@getTeachers');
Route::get('/get_batch','routineController@getBatch');
Route::post('/get_subject_form_routine','routineController@getSubject');
Route::post('/add_routine','routineController@addRoutine');
Route::get('/get_routine','routineController@getRoutine');
Route::post('/delete_routine','routineController@deleteRoutine');
Route::post('/delete_block_routine','routineController@deleteBlockRoutine');


//Attendence
Route::post('/get_student','attendenceController@getStudent');
Route::get('/get_attendence','attendenceController@getAttendence');
Route::post('/student_wise','attendenceController@studentWise');
Route::post('/day_wise','attendenceController@dayWise');
Route::post('/get_attendence_of_school','attendenceController@getAttendenceOfSchool');



// Batch Level Mapping
Route::post('/get_student_form_batch_level_mapping','BLMController@getStudent');
Route::post('/promoted_student','BLMController@promotedStudent');
Route::get('/get_BLM','BLMController@getBLM');
Route::post('/batch_level','BLMController@batchLevel');
Route::post('/get_year','BLMController@getYear');
Route::get('/getmap','BLMController@getmap');


//Notification
Route::get('/get_batch_faculty','notificationController@getBatchAndFaculty');
Route::get('/faculties_form_notification','notificationController@getFacultyNotification');
Route::post('/post_notication','notificationController@postNotification');
Route::get('/get_notification','notificationController@getNotification');


// UserLists
Route::get('/users','usersController@users');
Route::get('/get_teacher','usersController@getTeacher');
Route::get('/get_guest','usersController@getGuest');
Route::get('/get_school_student','usersController@getSchoolStudent');
Route::post('/get_student_for_update','usersController@getStudentForUpdate');
Route::post('/update_student','usersController@updateStudent');
Route::get('/get_school_student_for_update/{id}','usersController@getSchoolStudentForUpdate');
Route::get('/get_teacher_for_update/{id}','usersController@getTeacherUpdate');
Route::post('/update_school_student','usersController@updateSchoolStudent');
Route::post('/update_teacher','usersController@updateTeacher');
Route::get('/get_guest_for_update/{id}','usersController@getGuestUpdate');
Route::post('/update_guest','usersController@updateGuest');





//Individual Teacher
Route::get('/get_subject_of_particular_teacher','subjectController@getSubjectOfParticularTeacher');
Route::post('/get_student_of_particular_subject','attendenceController@gerStudentOfParticularSubject');



Route::post('/day_wise_of_school','attendenceController@getDatWiseAttendenceOfSchool');

