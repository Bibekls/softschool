<?php

Route::group(['domain' => 'admin.hcc.edu.np'], function(){

    Route::get('/refresh',function(){
        Artisan::call('cache:clear');
        Artisan::call('clear-compiled');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');

    });

    Route::post('/post','ProfileController@post');
    Route::delete('/post','ProfileController@deletePost');

    Route::post('/comment','ProfileController@comment');
    Route::delete('/comment','ProfileController@deleteComment');

    Route::get('/other-users','OtherUserController@users');
    Route::delete('/other-users/delete/{id}','OtherUserController@deleteUser');
    Route::put('/other-users/restore/{id}','OtherUserController@restoreUser');
    Route::get('/update-user-profile/{id}','OtherUserController@updateUserProfile');
    Route::put('/password-reset','OtherUserController@passwordReset');
    
    Route::put('/update-teacher','OtherUserController@updateTeacher');
    Route::put('/update-student','OtherUserController@updateStudent');
    Route::put('/update-guest','OtherUserController@updateGuest');

    Route::put('/update-user-profile-pic','OtherUserController@updateUserProfilePic');

    Route::get('/add-teacher','OtherUserController@addTeacherForm');
    Route::post('/add-teacher','OtherUserController@addTeacher');

    Route::get('/add-student','OtherUserController@addStudentForm');
    Route::post('/add-student','OtherUserController@addStudent');

    Route::get('/add-guest','OtherUserController@addGuestForm');
    Route::post('/add-guest','OtherUserController@addGuest');

    Route::get('/login-to-account/{id}','OtherUserController@loginOtherUser');
    
    Route::get('/storage','StorageController@drive');
    Route::post('/storage','StorageController@directory');
    Route::post('/up-one-level','StorageController@oneLevelUp');
    Route::post('/create-file','StorageController@createFile');
    Route::post('/make-directory','StorageController@makeDirectory');
    Route::delete('/delete-files-and-folder','StorageController@deleteFilesAndFolder');
    Route::post('/copy-files-and-folder','StorageController@copyFilesAndFolder');
    Route::post('/move-files-and-folder','StorageController@moveFilesAndFolder');
    Route::post('/rename','StorageController@rename');
    Route::post('/view','StorageController@view');
    Route::post('/download','StorageController@download');
    Route::post('/save','StorageController@save');
    Route::post('/edit','StorageController@edit');    
    Route::post('/upload','StorageController@upload');
        
    Route::post('/others-storage','OthersStorageController@directory');
    Route::post('/others-up-one-level','OthersStorageController@oneLevelUp');    
    Route::post('/others-view','OthersStorageController@view');
    Route::post('/others-download','OthersStorageController@download');
    
    Route::post('/url','ProfileController@url');
    Route::delete('/url','UrlController@deleteUrl');
    Route::get('/url','UrlController@url');
    
    Route::post('/feedback','FeedbackController@addFeedback');
    Route::delete('/feedback','FeedbackController@deleteFeedback');
    Route::get('/feedback','FeedbackController@getFeedback');

    Route::get('/newsfeed','NewsFeedController@newsfeed');

    /*Route::post('/login', 'LoginController@login');  	
	Route::get('/dashboard','UserController@dashboard'); */

    //Pending User ////////////////////////////////////////////////////////////////////////

    Route::get('/pending-student-form','UserController@pendingStudentForm'); 
    Route::get('/pending-teacher-form','UserController@pendingTeacherForm'); 
    Route::get('/user-request','UserController@userRequest');
    Route::delete('/delete-unverified-email','UserController@deleteUnverifiedEmail');

    Route::put('/accept-pending-student-form','UserController@acceptPendingStudentForm');
    Route::delete('/delete-pending-student-form','UserController@deletePendingStudentForm');

    Route::put('/accept-pending-teacher-form','UserController@acceptPendingTeacherForm');
    Route::delete('/delete-pending-teacher-form','UserController@deletePendingTeacherForm');

	/*
	**
	*/

//    Route::Auth();

//    Route::post('/login',function (\Illuminate\Http\Request $request){
////        return $request->all();
//        if (Auth::attempt(['name' => $request->input('name'), 'password' => $request->input('password')])) {
//            // Authentication passed...
//            return redirect()->intended('dashboard');
//        }
//    });

    Route::get('/',function(){
        return redirect('/dashboard');
    });


    Route::get('/dashboard','DashboardController@dashboard'); 
    Route::resource('menu', 'MenuController');
    Route::resource('page', 'PageController');
    Route::resource('pagecategory', 'PageCategoryController');
    Route::resource('gallery', 'GalleryController');
    Route::resource('image', 'ImageController');
    Route::resource('work', 'WorkController');
    Route::resource('workcategory', 'WorkCategoryController');
    Route::resource('subject', 'SubjectController');

    Route::post('clone-subject', 'SubjectController@cloneSubject');
    Route::put('update-subject', 'SubjectController@updateSubject');


    Route::resource('event', 'EventController');
    Route::resource('notification', 'NotificationController');

    Route::get('resume-pending-notification', 'NotificationController@sendPendingNotification');

    Route::resource('routine', 'RoutineController');

    Route::post('get-teacher', 'RoutineController@getTeacher');
    Route::post('get-subject', 'RoutineController@getSubject');
    Route::post('get-student', 'StudentAttendanceController@getStudent');

    Route::resource('student-attendance', 'StudentAttendanceController');
    Route::post('/student-attendance/day-wise','StudentAttendanceController@dayWise');
    Route::post('/student-attendance/student-wise','StudentAttendanceController@studentWise');

    Route::post('/change-password','DashboardController@changePassword');

    Route::get('/batch-level-mapping','BLMController@getmap');
    Route::post('/batch-level-mapping','BLMController@setmap');

    Route::get('/privacy-policy',function(){return view('mis.privacy-policy',['title'=>'Privacy policy']);});

    Route::get('{value}', function($value) {
        return $value;
    });

    //download excel file

    Route::post('download-teacher-lists','OtherUserController@downloadTeacherLists');
    Route::post('download-student-lists','OtherUserController@downloadStudentLists');
    Route::post('download-student-attendance','StudentAttendanceController@downloadStudentAttendance');

});