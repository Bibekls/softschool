<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title}}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/select2/select2.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/dist/css/AdminLTE.min.css">

    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/fullcalendar/fullcalendar.print.css"
          media="print">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/iCheck/all.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/timepicker/bootstrap-timepicker.min.css">

    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/dropzone.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/lightbox.min.css">
    <link rel="stylesheet" href="/plugins/bootstrap-tree/css/bootstrap-treenav.min.css">

    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/datatables/dataTables.bootstrap.css">

    @yield('style')
    <style type="text/css">
        ::-webkit-scrollbar {
            /*display: none;*/
        }
    </style>
</head>
<body class="hold-transition skin-green fixed">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>M</b>IS</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>HCC</b> MIS</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>


            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- Tasks: style can be found in dropdown.less -->

                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}"
                                 class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"> {{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img id="profile_pic" class="profile-user-img img-responsive img-circle"
                                     src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}"
                                     alt="User profile picture">

                                <p>
                                    {{Auth::user()->name}}
                                    <small>
                                        {{Auth::user()->email}}
                                    </small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer box1">
                                <div class="pull-left">
                                    <a href="#" data-toggle="modal" data-target="#changePassword"
                                       class="btn btn-default btn-flat">Change password</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->


    <div id="changePassword" class="modal fade modal-success" role="dialog">
        <div class="modal-dialog modal-sm">
            <form id="changePasswordForm" method="post" action="/change-password">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Change Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Old Password:</label>
                            <input type="password" name="oldPassword" class="form-control" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>New Password:</label>
                            <input type="password" name="newPassword" class="form-control" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Confirm Password:</label>
                            <input type="password" name="confirmPassword" class="form-control" autocomplete="off">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="pull-left">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-default">Change Password</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}"
                         class="img-circle" alt="User Image" style="max-height: 50px;">
                </div>
                <div class="pull-left info">
                    <p>{{Auth::user()->name}}</p>
                </div>
            </div>
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                    class="fa fa-search"></i>
                        </button>
                      </span>
                </div>
            </form>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>

                <li id="dashboard"><a href="{{ url('dashboard') }}" class="faa-parent animated-hover"><i
                                class="fa fa-dashboard faa-horizontal faa-fast"></i> <span> Dashboard</span></a></li>


                {{--<li class="treeview" id="website">--}}
                {{--<a href="#" class="faa-parent animated-hover">--}}
                {{--<i class="fa fa-code faa-horizontal faa-fast"></i> <span> Website</span>--}}
                {{--<span class="pull-right-container">--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="display: none;">--}}
                {{--<li class="">--}}
                {{--<a href="#"><i class="fa fa-file-code-o"></i> Content--}}
                {{--<span class="pull-right-container">--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="display: none;">--}}
                {{--<li><a href="/menu"><i class="fa fa-bars"></i> Menu</a></li>--}}
                {{--<li><a href="/page"><i class="fa fa-file-o"></i> Pages</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                {{--<li><a href="/gallery"><i class="fa fa-picture-o"></i> Photo Galleries</a></li>--}}
                {{--<li><a href="/work"><i class="fa fa-briefcase"></i> Add Work</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}

                <li class="treeview" id="manageUser">
                    <a href="#" class="faa-parent animated-hover">
                        <i class="fa fa-users faa-horizontal faa-fast"></i> <span> Manage Users </span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li><a href="/other-users"><i class="fa fa-user"></i> User List</a></li>
                        <li><a href="/add-teacher"><i class="fa fa-user"></i> Add Teacher </a></li>
                        <li><a href="/add-student"><i class="fa fa-user"></i> Add Student </a></li>
                        <li><a href="/add-guest"><i class="fa fa-user"></i> Add Guest </a></li>
                    </ul>
                </li>

                {{--<li class="treeview" id="pendingUser">--}}
                {{--<a href="#" class="faa-parent animated-hover">--}}
                {{--<i class="fa fa-users faa-horizontal faa-fast"></i> <span> Pending Users</span>--}}
                {{--<span class="pull-right-container">--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="display: none;">--}}
                {{--<li><a href="/pending-student-form"><i class="fa fa-user"></i> Student Form</a></li>--}}
                {{--<li><a href="/pending-teacher-form"><i class="fa fa-user"></i> Teacher Form</a></li>--}}
                {{--<li><a href="/user-request"><i class="fa fa-user"></i> User Request</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}

                <li id="subject"><a href="{{ url('subject') }}" class="faa-parent animated-hover"><i
                                class="fa fa-folder-open faa-horizontal faa-fast"></i><span> Subject Manager</span>
                    </a>
                </li>

                <li id="routine"><a href="{{ url('routine') }}" class="faa-parent animated-hover"><i
                                class="fa fa-clock-o faa-horizontal faa-fast"></i><span> Routine Manager</span>
                    </a>
                </li>

                <li id="event"><a href="{{ url('event') }}" class="faa-parent animated-hover"><i
                                class="fa fa-calendar-o faa-horizontal faa-fast"></i><span> Event Manager</span>
                    </a>
                </li>

                <li id="notification"><a href="{{ url('notification') }}" class="faa-parent animated-hover"><i
                                class="fa fa-bell-o faa-horizontal faa-fast"></i><span> Notification Manager</span>
                    </a>
                </li>

                <li id="student-attendance"><a href="{{ url('student-attendance') }}" class="faa-parent animated-hover"><i
                                class="fa fa-toggle-on faa-horizontal faa-fast"></i><span> Student Attendance</span>
                    </a>
                </li>

                <li id="batch-level-mapping"><a href="{{ url('/batch-level-mapping') }}"
                                                class="faa-parent animated-hover"><i
                                class="fa fa-plug faa-horizontal faa-fast"></i><span> Batch-Level Mapping</span>
                    </a>
                </li>

                {{--<li id="newsfeed"><a href="{{ url('newsfeed') }}" class="faa-parent animated-hover"><i--}}
                {{--class="fa fa-newspaper-o faa-horizontal faa-fast"></i><span> News Feed</span>--}}
                {{--</a>--}}
                {{--</li>--}}


                {{--<li id="storage"><a href="{{ url('storage') }}" class="faa-parent animated-hover"><i--}}
                {{--class="fa fa-database faa-horizontal faa-fast"></i> <span> Storage</span>--}}
                {{--<span class="pull-right-container">--}}
                {{--</span>--}}
                {{--</a></li>--}}

                {{--<li id="url"><a href="{{ url('url') }}" class="faa-parent animated-hover"><i--}}
                {{--class="fa fa-link faa-horizontal faa-fast"></i> <span> Important URL</span></a></li>--}}

                <li id="feedback">
                    <a href="{{ url('feedback') }}" class="faa-parent animated-hover">
                        <i class="fa fa-pencil-square faa-horizontal faa-fast"></i>
                        <span> Feedback</span>
                        @if($unreadFeedback>0)
                            <span class="pull-right-container">
                                <small class="label pull-right bg-red">{{$unreadFeedback}}</small>
                            </span>
                        @endif
                    </a>
                </li>

                {{--<li id="privacy-policy"><a href="/privacy-policy" class="faa-parent animated-hover"><i--}}
                                {{--class="fa fa-shield faa-horizontal faa-fast"></i> <span> Privacy Policy</span></a></li>--}}

                <li><a href="/logout" class="faa-parent animated-hover"><i
                                class="fa fa-sign-out faa-horizontal faa-fast"></i> <span> Logout</span></a></li>

            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height:1080px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('header')
        </section>

        <!-- Main content -->
        <section class="content">

            @yield('content')

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    @extends('layouts.footer')
    <div class="control-sidebar-bg"></div>
    <!-- /.control-sidebar -->

</div><!-- ./wrapper -->


<!-- jQuery 2.2.3 -->
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/d8532142d4.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="http://hetaudacitycollege.edu.np/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Slimscroll -->
<script src="http://hetaudacitycollege.edu.np/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="http://hetaudacitycollege.edu.np/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="http://hetaudacitycollege.edu.np/dist/js/app.min.js"></script>

<script src="http://hetaudacitycollege.edu.np/plugins/iCheck/icheck.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/select2/select2.full.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/bootstrap/js/dropzone.js"></script>
<script src="/plugins/bootstrap-tree/js/bootstrap-treenav.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/bootstrap/js/lightbox.min.js"></script>

<script src="http://hetaudacitycollege.edu.np/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/datatables/dataTables.bootstrap.min.js"></script>

@yield('script')
</body>
</html>

<script>
    $('form').submit(function () {
        $(this).find(':input[type=submit]').prop('disabled', true);
        $(this).find(':button').prop('disabled', true);
    });

</script>