<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>@yield('title')</title>
        <meta name="description" content="@yield('meta_description')">
        <meta name="keywords" content="@yield('meta_keywords')">
        <meta name="author" content="@yield('meta_author')">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="http://asset.jeleaf.com//bootstrap/css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="http://asset.jeleaf.com//bootstrap/css/slider.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="http://asset.jeleaf.com//bootstrap/css/owl.carousel.css">
        <link rel="stylesheet" href="http://asset.jeleaf.com//bootstrap/css/owl.theme.css">
        <link rel="stylesheet" href="http://asset.jeleaf.com//bootstrap/css/jquery.fancybox.css">
        <!-- template main css file -->
        <link rel="stylesheet" href="http://asset.jeleaf.com//bootstrap/css/main.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="http://asset.jeleaf.com//bootstrap/css/responsive.css">
        
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="http://asset.jeleaf.com//bootstrap/js/vendor/modernizr-2.6.2.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <!-- owl carouserl js -->
        <script src="http://asset.jeleaf.com//bootstrap/js/owl.carousel.min.js"></script>
        <!-- bootstrap js -->

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- wow js -->
        <script src="http://asset.jeleaf.com//bootstrap/js/wow.min.js"></script>
        <!-- slider js -->
        <script src="http://asset.jeleaf.com//bootstrap/js/slider.js"></script>
        <script src="http://asset.jeleaf.com//bootstrap/js/jquery.fancybox.js"></script>
        <!-- template main js -->
        <script src="http://asset.jeleaf.com//bootstrap/js/main.js"></script>
        <style>
            td{
                color: #00A6BB;
            }
        </style>
        @yield('style')
    </head>
    <body>
        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->
                    
                    <!-- logo -->
                    <div class="navbar-brand">
                        <a href="index.html" >
                            <img src="/img/logo.jpg" alt="">
                        </a>
                    </div>
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="/" >Home</a>
                            </li>
                            <li><a href="/about">About</a></li>                            
                            <li><a href="/gallery">Gallery</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Faculties <span class="caret"></span></a>
                                <div class="dropdown-menu">
                                    <ul>
                                        <li><a href="bsccsit">BSc.CSIT</a></li>
                                        <li><a href="science">+2 Science</a></li>
                                        <li><a href="management">+2 Management</a></li>
                                    </ul>
                                </div>
                            </li>
                            <?php $customMenu=Session::get('customMenu');?>

                            {{--@foreach($customMenu as $menu)--}}
                                {{--@if($menu['no_of_submenu']!="0")--}}
                                    {{--<li class="dropdown">--}}
                                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$menu['menu']->name}}--}}
                                            {{--<span class="caret"></span>--}}
                                        {{--</a>--}}
                                        {{--<div class="dropdown-menu">--}}
                                            {{--<ul>--}}
                                                {{--@foreach($menu['submenu'] as $submenu)--}}
                                                    {{--<li><a href="/{{$submenu->url}}">{{$submenu->name}}</a></li>--}}
                                                {{--@endforeach--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                {{--@else--}}
                                    {{--<li><a href="/{{$menu['menu']->url}}">{{$menu['menu']->name}}</a></li>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}

                            <li><a href="/contact">Contact</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
        </header>
        @yield('content')
            <footer id="footer">
                <div class="container">
                    <div class="col-md-8">
                        <p class="copyright">Copyright: <span>2016</span> . Design and Developed by <a href="http://www.jeleaf.com" target="_blank">Jeleaf Technology</a></p>
                    </div>
                    <div class="col-md-4">
                        <!-- Social Media -->
                        <ul class="social">
                            <li>
                                <a href="https://www.facebook.com/hetaudacitycollege.edu.np" target="_blank" class="Facebook">
                                    <i class="ion-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/City_Of_Hetauda" target="_blank" class="Twitter">
                                    <i class="ion-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCZURtW9KwhVzCfyojvMz67g" target="_blank" class="youtube">
                                    <i class="ion-social-youtube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/100730684180848325746" target="_blank"class="Google Plus">
                                    <i class="ion-social-googleplus"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer> <!-- /#footer -->
                
        </body>
    </html>
    