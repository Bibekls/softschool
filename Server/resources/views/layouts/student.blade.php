<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title}}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/font-awesome-animation.min.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/dist/css/skins/skin-red.min.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/dropzone.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/lightbox.min.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/box.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    @yield('style')
    <style type="text/css">
        ::-webkit-scrollbar {
            display: none;
        }   
    </style>
</head>
<body class="hold-transition skin-red fixed">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b>tudent</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>HCC</b> Student</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>


            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">                  
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"> {{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                    <img id="profile_pic" class="profile-user-img img-responsive img-circle"
                                         src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}" alt="User profile picture">

                                <p>
                                    {{Auth::user()->name}}
                                    <small>
                                        {{Auth::user()->email}}
                                    </small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer box1">
                                <div class="pull-left">
                                    <a href="/profile" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{Auth::user()->name}}</p>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>

                <li id="dashboard"><a href="{{ url('dashboard') }}" class="faa-parent animated-hover"><i class="fa fa-dashboard faa-horizontal faa-fast"></i> <span> Dashboard</span></a></li>
                
                <li id="routine"><a href="{{ url('routine') }}" class="faa-parent animated-hover"><i class="fa fa-clock-o faa-horizontal faa-fast"></i><span> Subject Routine </span>
                </a>
                </li>

                <li id="attendance"><a href="{{ url('attendance') }}" class="faa-parent animated-hover"><i class="fa fa-toggle-on faa-horizontal faa-fast"></i><span> My Attendance</span>
                </a>
                </li>

                <li id="newsfeed"><a href="{{ url('newsfeed') }}" class="faa-parent animated-hover"><i class="fa fa-newspaper-o faa-horizontal faa-fast"></i><span> News Feed</span>
                </a>
                </li>

                <li id="profile"><a href="{{ url('profile') }}" class="faa-parent animated-hover"><i class="fa fa-user faa-horizontal faa-fast"></i> <span>Profile</span></a></li>

                {{--<li id="other-users"><a href="{{ url('other-users') }}" class="faa-parent animated-hover"><i class="fa fa-users faa-horizontal faa-fast"></i> <span> Other Users</span></a></li>--}}

                {{--<li id="storage"><a href="{{ url('storage') }}" class="faa-parent animated-hover"><i class="fa fa-database faa-horizontal faa-fast"></i> <span> Storage</span>--}}
                {{--</a></li>--}}

                {{--<li id="web-hosting"><a href="{{ url('web-hosting') }}" class="faa-parent animated-hover"><i class="fa fa-globe faa-horizontal faa-fast"></i> <span> Web Hosting</span>  --}}
                {{--</a></li>--}}

                <li id="url"><a href="{{ url('url') }}" class="faa-parent animated-hover"><i class="fa fa-link faa-horizontal faa-fast"></i> <span> Important URL</span></a></li> 

                <li id="feedback"><a href="{{ url('feedback') }}" class="faa-parent animated-hover"><i class="fa fa-pencil-square faa-horizontal faa-fast"></i> <span> Feedback</span>
                </a></li>
                
                <li id="privacy-policy"><a href="/privacy-policy" class="faa-parent animated-hover"><i class="fa fa-shield faa-horizontal faa-fast"></i> <span> Privacy Policy</span></a></li>

                <li><a href="/logout" class="faa-parent animated-hover"><i class="fa fa-sign-out faa-horizontal faa-fast"></i> <span> Logout</span></a></li>


            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height:1080px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('header')
        </section>

        <!-- Main content -->
        <section class="content">

            @yield('content')

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    @extends('layouts.footer')
</div><!-- ./wrapper -->

<script src="https://code.jquery.com/jquery-3.1.1.min.js" 
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous">
  </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/d8532142d4.js"></script>
<script src="http://hetaudacitycollege.edu.np/dist/js/app.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/daterangepicker/daterangepicker.js"></script>

<script src="http://hetaudacitycollege.edu.np/plugins/datepicker/bootstrap-datepicker.js"></script>

<script src="http://hetaudacitycollege.edu.np/plugins/fastclick/Fastclick.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/bootstrap/js/dropzone.js"></script>
<script src="http://hetaudacitycollege.edu.np/bootstrap/js/lightbox.min.js"></script>

@yield('script')
</body>
</html>
