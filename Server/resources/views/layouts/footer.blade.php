<footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Powered by <a href="http://softwebdevelopers.com/" target="_blank">Softwebdevelopers</a></div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2017-2018 <a href="http://www.hetaudacitycollege.edu.np" target="_blank">Hetauda City College</a>.</strong>
        [  Version 2.0.0 ]
</footer> 