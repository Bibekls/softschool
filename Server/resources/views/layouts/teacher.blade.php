<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title}}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/select2/select2.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/dist/css/AdminLTE.min.css">

    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/fullcalendar/fullcalendar.print.css" media="print">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/iCheck/all.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/timepicker/bootstrap-timepicker.min.css">

    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/dropzone.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/lightbox.min.css">
    <link rel="stylesheet" href="/plugins/bootstrap-tree/css/bootstrap-treenav.min.css">

    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/datatables/dataTables.bootstrap.css">

    @yield('style')
    <style type="text/css">
        ::-webkit-scrollbar {
            /*display: none;*/
        }   
    </style>
</head>
<body class="hold-transition skin-yellow fixed">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>T</b>eacher</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>HCC</b> Teacher</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"> {{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                    <img id="profile_pic" class="profile-user-img img-responsive img-circle"
                                         src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}" alt="User profile picture">

                                <p>
                                    {{Auth::user()->name}}
                                    <small>
                                        {{Auth::user()->email}}
                                    </small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/profile" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                  </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{url('images/thumbnails')}}/{{Auth::user()->profile_picture->file_name}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{Auth::user()->name}}</p>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>

                <li id="dashboard"><a href="{{ url('dashboard') }}" class="faa-parent animated-hover"><i class="fa fa-dashboard faa-horizontal faa-fast"></i> <span>Dashboard</span></a></li>
                
                <li id="routine"><a href="{{ url('routine') }}" class="faa-parent animated-hover"><i class="fa fa-clock-o faa-horizontal faa-fast"></i><span> My Routine </span>
                </a>
                </li>

                <li id="attendance"><a href="{{ url('take-attendance-view') }}" class="faa-parent animated-hover"><i class="fa fa-toggle-on faa-horizontal faa-fast"></i><span> Take Attendance </span>
                    </a>
                </li>

                <li id="student-attendance"><a href="{{ url('student-attendance') }}" class="faa-parent animated-hover"><i
                                class="fa fa-toggle-on faa-horizontal faa-fast"></i><span> Student Attendance</span>
                    </a>
                </li>

                {{--<li id="newsfeed"><a href="{{ url('newsfeed') }}" class="faa-parent animated-hover"><i class="fa fa-newspaper-o faa-horizontal faa-fast"></i> <span>News Feed</span></a></li>                --}}

                <li id="profile"><a href="{{ url('profile') }}" class="faa-parent animated-hover"><i class="fa fa-user faa-horizontal faa-fast"></i> <span>Profile</span></a></li>
                
                 {{--<li id="other-users"><a href="{{ url('other-users') }}" class="faa-parent animated-hover"><i class="fa fa-users faa-horizontal faa-fast"></i> <span>Other Users</span></a></li>                 --}}

                {{--<li id="storage"><a href="{{ url('storage') }}" class="faa-parent animated-hover"><i class="fa fa-database faa-horizontal faa-fast"></i> <span>Storage</span></a></li>--}}

                {{--<li id="web-hosting"><a href="{{ url('web-hosting') }}" class="faa-parent animated-hover"><i class="fa fa-globe faa-horizontal faa-fast"></i> <span>Web Hosting</span></a></li>--}}

                {{--<li id="url"><a href="{{ url('url') }}" class="faa-parent animated-hover"><i class="fa fa-link faa-horizontal faa-fast"></i> <span>Important URL</span></a></li> --}}

                <li id="feedback"><a href="{{ url('feedback') }}" class="faa-parent animated-hover"><i class="fa fa-pencil-square faa-horizontal faa-fast"></i> <span>Feedback</span></a></li>
                
                {{--<li id="privacy-policy"><a href="/privacy-policy" class="faa-parent animated-hover"><i class="fa fa-shield faa-horizontal faa-fast"></i> <span> Privacy Policy</span></a></li>--}}

                <li><a href="/logout" class="faa-parent animated-hover"><i class="fa fa-sign-out faa-horizontal faa-fast"></i> <span>Logout</span></a></li>


            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height:1080px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('header')
        </section>

        <!-- Main content -->
        <section class="content">

            @yield('content')

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
  @extends('layouts.footer')
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab" aria-expanded=true><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>          
        </ul>  

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>       
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>    
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->


<!-- jQuery 2.2.3 -->
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/d8532142d4.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="http://hetaudacitycollege.edu.np/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Slimscroll -->
<script src="http://hetaudacitycollege.edu.np/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="http://hetaudacitycollege.edu.np/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="http://hetaudacitycollege.edu.np/dist/js/app.min.js"></script>

<script src="http://hetaudacitycollege.edu.np/plugins/iCheck/icheck.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/select2/select2.full.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/bootstrap/js/dropzone.js"></script>
<script src="/plugins/bootstrap-tree/js/bootstrap-treenav.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/bootstrap/js/lightbox.min.js"></script>

<script src="http://hetaudacitycollege.edu.np/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="http://hetaudacitycollege.edu.np/plugins/datatables/dataTables.bootstrap.min.js"></script>

@yield('script')
</body>
</html>
