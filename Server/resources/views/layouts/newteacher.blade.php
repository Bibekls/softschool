<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title}}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="http://asset.jeleaf.com/bootstrap/css/font-awesome-animation.min.css">
    <link rel="stylesheet" href="http://asset.jeleaf.com/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="http://asset.jeleaf.com/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="http://asset.jeleaf.com/dist/css/skins/skin-yellow.min.css">
    <link rel="stylesheet" href="http://asset.jeleaf.com/bootstrap/css/dropzone.css">
    <link rel="stylesheet" href="http://asset.jeleaf.com/bootstrap/css/lightbox.min.css">    
    <link rel="stylesheet" href="http://asset.jeleaf.com/bootstrap/css/box.css">
    

    @yield('style')
</head>
<body class="hold-transition skin-yellow fixed">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="http:\\www.hetaudacitycollege.edu.np" target="_blank" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>T</b>eacher</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>HCC</b> Teacher</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="/img/male.jpg" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"> {{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/img/male.jpg" class="img-circle" alt="User Image">
                                <p>
                                    {{Auth::user()->name}}
                                    <small>
                                        {{Auth::user()->email}}
                                    </small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">

                                <div class="pull-right">
                                    <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img
                            src="/img/male.jpg"
                            class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{Auth::user()->name}}</p>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
             
                <li><a href="/logout" class="faa-parent animated-hover"><i class="fa fa-sign-out faa-horizontal faa-fast"></i> <span>Logout</span></a></li>


            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('header')
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    @extends('layouts.footer')
    
</div><!-- ./wrapper -->

<script src="https://code.jquery.com/jquery-3.1.1.min.js" 
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous">
  </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/d8532142d4.js"></script>
<script src="http://asset.jeleaf.com/dist/js/app.min.js"></script>
<script src="http://asset.jeleaf.com/plugins/fastclick/Fastclick.min.js"></script>
<script src="http://asset.jeleaf.com/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="http://asset.jeleaf.com/bootstrap/js/dropzone.js"></script>
<script src="/plugins/iCheck/icheck.min.js"></script>
<script src="http://asset.jeleaf.com/bootstrap/js/lightbox.min.js"></script>


@yield('script')
<script>
    Dropzone.options.addImages={
        maxFileSize: 1,
        acceptedFiles : 'image/*',
        success : function(file,response)
        {
            if(file.status=='success')
            {
                handleDropzoneFileUpload.handleSuccess(response);
            }
            else {
                handleDropzoneFileUpload.handleError(response);
            }
            console.log(file);
            console.log(response);

        }
    };

    var handleDropzoneFileUpload={
        handleError:function(response)
        {
            console.log(response);
        },
        handleSuccess:function(response)
        {
            window.location = "http://teacher.hcc.edu.np/registration-form";         
        }
    }

</script>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

</body>
</html>