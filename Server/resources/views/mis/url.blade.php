@extends('layouts.mis')
@section('header')
    <h1>
        Important URLs
        <small>List of url</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">URL</li>
    </ol>
@endsection
@section('content') 
 <div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Add URLs</h3>
    </div>
    <div class="box-body">
        <div class="col-lg-10 col-lg-offset-1">
            @if(Session::has('message'))
                <div class="alert alert-info">
                    <strong>Info!</strong>
                    {{Session::get('message')}}
                    {{Session::forget('message')}}
                </div>
            @endif
            <form class="form-horizontal" role="form" method="POST" 
                action="{{ url('/url') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                 <div class="row">
                    <!--Name of book -->
                    <div class="col-md-12">
                        <div class="input-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <span class="input-group-addon">
                                 <i class="fa fa-pencil"></i>
                            </span>
                            <input type="text" class="form-control" 
                                name="title" value="{{ old('title') }}" 
                                    placeholder="Title">
                             <span class="input-group-addon">
                                <i class="fa fa-asterisk"></i>
                            </span>
                        </div>
                        @if ($errors->has('title'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError">
                                    <i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('title') }}</strong>
                                </label>
                            </div>
                        @endif
                    </div>
                    <!--Name of author -->                                          
                </div><br>
                <div class="row">
                      <div class="col-md-12">
                        <div class="input-group{{ $errors->has('url') ? ' has-error' : '' }}">
                            <span class="input-group-addon">
                                <i class="fa fa-link"></i>
                            </span>
                            <input type="text" class="form-control" 
                                name="url" value="{{ old('url') }}" 
                                placeholder="http://">
                            <span class="input-group-addon">
                                <i class="fa fa-asterisk"></i>
                            </span>
                        </div>
                        @if ($errors->has('url'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError">
                                    <i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('url') }}</strong>
                                </label>
                            </div>
                        @endif
                    </div>
                </div><br>

                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                             <textarea class="form-control" rows="5" name="description" placeholder="Write description here.....">{{ old('description') }}</textarea>
                        </div>
                        @if ($errors->has('description'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('description') }}</strong>
                                </label>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="pull-right">
                        <div class="input-group"><br>
                            <button type="submit" id="searchstudent" class="btn btn-primary btn-flat">
                                <i class="fa fa-plus"></i> Add Urls &nbsp
                                <i id="addStudent" class="fa fa-circle-o-notch fa-spin hidden"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    <div class="row">    
    @foreach($urls as $url)
    <div class="col-md-4">
              <!-- Box Comment -->
              <div class="box box1 box-widget">
                <div class='box-header with-border'>
                  <div class='user-block'>
                    <img class='img-circle' src="/images/thumbnails/{{$url->profile_pic}}" alt='user image'>
                    <span class='username'><a href="#">{{$url->user->name}}</a></span>                    
                    <br>
                    <span class='description'>
                        {{Carbon::createFromTimeStamp(strtotime($url->created_at))->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime($url->created_at))))->diffForHumans() }}
                    </span>
                  </div><!-- /.user-block -->
                  <div class='box-tools'>                   
                    
                      <div class="pull-right">
                         <form class="form-horizontal" role="form" method="POST" 
                              action="{{ url('/url') }}">
                              
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <input type="hidden" name="_method" value="delete">
                              <input type="hidden" name="url_id" value="{{$url->id}}">
                              <button type="submit" class='btn btn-box-tool'><i class='fa fa-2x fa-trash'></i>
                              </button>
                          </form>
                      </div>
                    
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class='box-body'>                  
                  <h4 class="text-center"><a href="{{$url->url}}" target="_blank">{{$url->title}}</a></h4>
                  <p class="text-justify">{{$url->description}}</p>
                  <p class="text-center"><a href="{{$url->url}}" target="_blank">{{$url->url}}</a></p>
                </div><!-- /.box-body -->                           
              </div><!-- /.box -->
            </div><!-- /.col -->
        @endforeach
    </div>  

    <div class="row">
        <div class="container">    
            <div class="pagination"> {{$urls->links() }} </div>  
        </div>
    </div>        


@endsection

@section('script')
    <script>
        $( "#url" ).addClass( "active" );
    </script>
@endsection