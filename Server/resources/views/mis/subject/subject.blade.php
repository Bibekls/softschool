@extends('layouts.mis')
@section('header')
    <h1>
        Subject Manager
        <small>Add Subject</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">subject-manager</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <form method="POST" action="{{ url('/clone-subject') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Clone Subject</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">

                        <div class="input-form" style="margin:0">
                            <label>Past Year</label>
                            <select class="form-control select2" style="width: 100%;" name="faculty"
                                    value="{{ old('faculty') }}">
                                @foreach($faculties as $fac)
                                    <option value="{{$fac->id}}">{{$fac->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>

                        <div class="input-form" style="margin:0">
                            <label>Past Year</label>
                            <select class="form-control select2" style="width: 100%;" name="past_year"
                                    value="{{ old('past_year') }}">
                                @foreach($batches as $batch)
                                    <option>{{$batch}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>

                        <div class="input-group{{ $errors->has('year') ? ' has-error' : '' }} col-md-12">
                            <label>Enter year</label>
                            <input type="number" class="form-control input-sm" name="year" min="2070" max="2090"
                                   value="{{ old('year') }}" placeholder="Subject Name">
                        </div>
                        <br>
                        @if ($errors->has('year'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('year') }}</strong>
                                </label>
                            </div><br>
                        @endif


                        <div class="input-group col-md-12">
                            <button class="btn btn-primary btn-flat btn-block">Clone Subject</button>
                        </div>
                    </div>
                </div>
            </form>

            <form method="POST" action="{{ url('/subject') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Subject</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="input-group{{ $errors->has('subject_name') ? ' has-error' : '' }} col-md-12">
                            <label>Subject Name</label>

                            <input type="text" class="form-control input-sm" name="subject_name"
                                   value="{{ old('subject_name') }}" placeholder="Subject Name">
                        </div>
                        <br>
                        @if ($errors->has('subject_name'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('subject_name') }}</strong>
                                </label>
                            </div><br>
                        @endif

                        <div class="input-group col-md-12">
                            <label>Faculty</label>
                            <select class="form-control select2" style="width: 100%;" name="faculty">
                                @foreach($faculties as $faculty)
                                    <option @if($faculty->id==1) selected @endif>{{$faculty->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>

                        <div class="form-group" style="margin: 0">
                            <label>Level</label>

                            <select class="form-control select2" style="width: 100%;" name="level"
                                    value="{{ old('level') }}">
                                <option value="1">1st</option>
                                <option value="2">2nd</option>
                                <option value="3">3rd</option>
                                <option value="4">4th</option>
                                <option value="5">5th</option>
                                <option value="6">6th</option>
                                <option value="7">7th</option>
                                <option value="8">8th</option>
                            </select>
                        </div>
                        <br>

                        <div class="input-group{{ $errors->has('year') ? ' has-error' : '' }} col-md-12">
                            <label>Year</label>

                            <select name="year" id="" class="form-control select2" style="width: 100%;"
                                    placeholder="Batch [year]">
                                <option value="">Select-Batch</option>
                                <?php for($i = 2070;$i <= 2090;$i++){ ?>
                                <option value="{{$i}}" @if(old('year')==$i) selected @endif >{{$i}}</option>
                                <?php } ?>
                            </select>

                        </div>
                        <br>
                        @if ($errors->has('year'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('year') }}</strong>
                                </label>
                            </div><br>
                        @endif

                        <div class="input-group col-md-12">
                            <button class="btn btn-primary btn-flat btn-block">Add Subject</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    @foreach($routines as $routine)
                        <li @if($routine['faculty']->id==1) class="active @endif">
                            <a href="#{{$routine['faculty']->name}}" data-toggle="tab">{{$routine['faculty']->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($routines as $routine)
                        <div class="
                      @if($routine['faculty']->id==1)
                                active
                              @endif
                                tab-pane" id="{{$routine['faculty']->name}}">


                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    @foreach($routine['batch'] as $batch)
                                        <li @if($batch['name']=='2070') class="active @endif">
                                            <a href="#{{$batch['name']}}" data-toggle="tab">{{$batch['name']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content">
                                    @foreach($routine['batch'] as $batch)
                                        <div class="
                      @if($batch['name']=='2070')
                                                active
                                              @endif
                                                tab-pane" id="{{$batch['name']}}">

                                            <ul class="nav nav-pills nav-stacked nav-tree"
                                                id="{{$batch['name']}}-tree" data-toggle="nav-tree">
                                                @foreach($batch['levels'] as $level)
                                                    <li>
                                                        <a href="#">Level-{{$level['name']}}</a>
                                                        <ul class="nav nav-pills nav-stacked nav-tree">
                                                            @foreach($level['subjects'] as $subject)
                                                                <li>
                                                                    <div class="pull-right">
                                                                        <button data-subject-id="{{$subject->id}}"
                                                                                data-subject-name="{{$subject->name}}"
                                                                                class='btn btn-box-tool edit-subject'><i
                                                                                    class='fa fa-pencil'></i>
                                                                        </button>

                                                                        <form class="form-horizontal" role="form"
                                                                              method="POST"
                                                                              action="{{ url('/subject/'.$subject->id)}}"
                                                                              style="display: inline; float: right;"
                                                                              onsubmit="return confirm('Do you to Delete?');">
                                                                            <input type="hidden" name="_token"
                                                                                   value="{{ csrf_token() }}">
                                                                            <input type="hidden" name="_method"
                                                                                   value="delete">
                                                                            <button type="submit"
                                                                                    class='btn btn-box-tool'><i
                                                                                        class='fa fa-trash-o'></i>
                                                                            </button>
                                                                        </form>
                                                                    </div>
                                                                    <div>
                                                                        <h4 style="margin-left: 25px;">
                                                                            <i>{{$subject->name}}</i>
                                                                        </h4>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @endforeach
                                            </ul>


                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>


    <div id="editSubject" class="modal fade" role="dialog">
        <div class="modal-dialog ">
            <form method="POST"
                  action="{{ url('/update-subject/')}}">

                <input type="hidden" name="_token"
                       value="{{ csrf_token() }}">
                <input type="hidden" name="_method"
                       value="put">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit subject</h4>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <label>Subject name</label>
                            <input type="text" class="form-control" id="subjectName" name="subject-name" value="">
                            <input type="hidden" class="form-control" id="subjectId" name="subject-id" value="">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancle</button>
                        <button type="submit" class="btn btn-default">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $("#subject").addClass("active");
    </script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

        });
    </script>

    <script>
        $('.edit-subject').on('click', function (event) {
            var subjectId = event.delegateTarget.attributes["data-subject-id"].nodeValue;
            var subjectName = event.delegateTarget.attributes["data-subject-name"].nodeValue;

            $('#subjectName').val(subjectName);
            $('#subjectId').val(subjectId);

            $('#editSubject').modal();
        })
    </script>
@endsection
