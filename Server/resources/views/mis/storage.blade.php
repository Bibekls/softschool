@extends('layouts.mis')
@section('style')
    <style type="text/css">
    .file-manager>li{
        margin-left:5%;
    }
    .modal-dialog-center {
    padding-top: 10%;
    }

    </style>
@endsection
@section('header')
    <h1>
        Storage
        <small>Cloud Store</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">Cloud Store</li>
    </ol>
@endsection
@section('content') 

<div class="row">        
        <div class="col-xs-12">          
          <div class="box box-success">
          <?php if($view){ ?>
          <div> 
            <object type="text/html" data="{{url($directory)}}" width="100%" height="720px">
            </object>
          </div>
          <?php } else { ?>

            <div class="box-header">
            <div>
                <form class="form-horizontal" action="storage" method="post"> 
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="_method" value="post">                 
                  <div class="form-group form-group-sm">
                    <div class="input-group col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                          <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                          <input class="form-control" type="text" id="directory" name="directory" value="{{$directory}}">
                          <span class="input-group-addon"><a href="#" onClick="this.parentElement.parentElement.parentElement.parentElement.parentElement.childNodes[1].submit();">Go</a></span>
                    </div>
                  </div>
                </form>                
            </div>
            <hr>
            <div>
                <ul class="file-manager list-inline">
                    <li>
                        <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/up-one-level') }}">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="directory" value="{{$directory}}">                               
                        </form>
                        <a href="#" onClick="this.parentElement.childNodes[1].submit();">
                          <div>
                            <b><i class="fa fa-level-up"></i> Up-One-Level</b>                        
                          </div>
                        </a>
                    </li>
                    <li>
                        <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/create-file') }}">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="directory" value="{{$directory}}">                               
                                <input type="hidden" name="fileName" value="untitle.html"> 
                        </form>
                        <a href="#" onClick="this.parentElement.childNodes[1].submit();">
                          <div>
                            <b><i class="fa fa-plus"></i> File</b>                       
                          </div>
                        </a>
                    </li>
                    <li>
                        <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/make-directory') }}">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="directory" value="{{$directory}}"> 
                                <input type="hidden" name="folderName" value="newfolder">                               
                        </form>
                        <a href="#" onClick="this.parentElement.childNodes[1].submit();">
                          <div>
                            <b><i class="fa fa-plus"></i> Folder</b>                       
                          </div>
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="#" class="faa-parent animated-hover" data-toggle="modal" data-target="#uploadDialog">
                            <div>
                                <b><i class="fa fa-upload"></i>Upload</b>
                            </div>
                        </a>   
                        <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/upload') }}">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="directory" value="{{$directory}}">                               
                        </form>
                        <a href="#" onClick="this.parentElement.childNodes[1].submit();">                          
                        </a>
                    </li>
                </ul>
            </div>            
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="myTable" class="table table-hover">
                    <thead>                        
                          <th class="text-center">Icon</th>
                          <th>Name</th>
                          <th>Format</th>
                          <th>Size</th>
                          <th>Last Modified</th>                                          
                    </thead>
                    <tbody>
                        @foreach($folders as $folder)
                        <tr>
                          <td class="text-center"><i class="fa fa-2x fa-folder-o"></i></td>
                          <td>
                            <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/storage') }}">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="directory" value="{{$directory}}">
                                <input type="hidden" name="folderName" value="{{$folder->folderName}}">            
                            </form>
                            <a href="#" onClick="this.parentElement.childNodes[1].submit();"><div>{{$folder->folderName}}</div></a>
                          </td>
                          <td>Folder</td>
                          <td>---</td>
                          <td>---</td>
                        </tr>  
                        @endforeach

                        @foreach($files as $file)
                            <tr>
                              <td class="text-center"><i class="fa fa-2x 
                                @if($file->extension=='txt')fa-file-text-o

                                @elseif($file->extension=='doc')fa-file-word-o
                                @elseif($file->extension=='dot')fa-file-word-o
                                @elseif($file->extension=='docx')fa-file-word-o

                                @elseif($file->extension=='xls')fa-file-excel-o
                                @elseif($file->extension=='xlsx')fa-file-excel-o
                                @elseif($file->extension=='xlm')fa-file-excel-o 

                                @elseif($file->extension=='ppt')fa-file-powerpoint-o                         
                                @elseif($file->extension=='pot')fa-file-powerpoint-o
                                @elseif($file->extension=='pps')fa-file-powerpoint-o

                                @elseif($file->extension=='pdf')fa-file-pdf-o
                                
                                @elseif($file->extension=='jpg')fa-file-image-o
                                @elseif($file->extension=='jpeg')fa-file-image-o
                                @elseif($file->extension=='tif')fa-file-image-o
                                @elseif($file->extension=='gif')fa-file-image-o
                                @elseif($file->extension=='png')fa-file-image-o                                
                                @elseif($file->extension=='bmp')fa-file-image-o

                                @elseif($file->extension=='mp4')fa-file-video-o                            
                                @elseif($file->extension=='html')fa-html5
                                @elseif($file->extension=='htm')fa-html5
                                @elseif($file->extension=='css')fa-css3
                                @elseif($file->extension=='js')fa-file-code-o                            
                                @elseif($file->extension=='zip')fa-file-archive-o 

                                @else fa-exclamation-triangle
                                @endif
                              "></i></td>
                              <td>
                                <form class="form-horizontal" role="form" method="POST" 
                                    action='
                                    <?php if($file->extension=='html' || $file->extension=='htm' ) { ?>
                                    {{ url('/edit') }}
                                    <?php }else{ ?>
                                      {{ url('/storage') }}
                                    <?php }?>
                                    '>

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_method" value="post">
                                    <input type="hidden" name="directory" value="{{$directory}}">
                                    <input type="hidden" name="fileName" value="{{$file->fileName}}">
                                </form>
                                <a href="#" onClick="this.parentElement.childNodes[1].submit();"><div>{{$file->fileName}}</div></a>
                              </td>  
                              <td>{{$file->extension}}</td>                
                              <td>{{(int)((int)$file->size/1024)}} KB</td>
                              <td>
                                  {{Carbon::createFromTimeStamp($file->lastModifiedDate)->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp($file->lastModifiedDate)))->diffForHumans() }}
                              </td>
                            </tr>  
                           
                        @endforeach
                    </tbody>
              </table>
            </div>
            <?php } ?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
</div>  

<!-- Trigger the modal with a button -->

<ul id="contextMenu" class="box1 dropdown-menu" role="menu" style="display:none" >
    <li>
        <a tabindex="-1" href="#" class="faa-parent animated-hover" data-toggle="modal" data-target="#copyDialog">
            <i class="fa fa-clone faa-horizontal faa-fast"></i>
            Copy
        </a>        
    </li>
    <li>        
        <a tabindex="-1" href="#" class="faa-parent animated-hover" data-toggle="modal" data-target="#moveDialog">
                <i class="fa fa-arrows faa-horizontal faa-fast"></i>
                Move
        </a>        
    </li>
    <li>
        <a tabindex="-1" href="#" class="faa-parent animated-hover" data-toggle="modal" data-target="#renameDialog">
                <i class="fa fa-i-cursor faa-horizontal faa-fast"></i>
                Rename
        </a>         
    </li>
    <li>
        <form class="form-horizontal" role="form" method="POST"
            action="{{ url('/view') }}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="post">
            <input type="hidden" id="view" name="fileAndFolder"  value="">  
            <input type="hidden" name="directory" value="{{$directory}}">          
        </form>
        <a tabindex="-1" href="#" class="faa-parent animated-hover" onClick="this.parentElement.childNodes[1].submit();">
                <i class="fa fa-eye faa-horizontal faa-fast"></i>
                View                
        </a>        
    </li>
     <li>
        <form class="form-horizontal" role="form" method="POST" target="_blank"
            action="{{ url('/download') }}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="post">
            <input type="hidden" id="download" name="fileAndFolder"  value="">  
            <input type="hidden" name="directory" value="{{$directory}}">          
        </form>
        <a tabindex="-1" href="#" class="faa-parent animated-hover" onClick="this.parentElement.childNodes[1].submit();">
                <i class="fa fa-download faa-horizontal faa-fast"></i>
                Download
        </a>        
    </li>
    <li class="divider"></li>
    <li>
        <a tabindex="-1" href="#" class="faa-parent animated-hover" data-toggle="modal" data-target="#deleteDialog">
                <i class="fa fa-i-cursor faa-horizontal faa-fast"></i>
                Delete
        </a>         
    </li>     
</ul>
<!-- Modal -->
<div id="copyDialog" class="modal fade modal-primary" role="dialog">
  <div class="modal-dialog modal-dialog-center">
    <form class="form-horizontal" action="/copy-files-and-folder" method="post"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title text-center">Copy</h4>
          </div>
          <div class="modal-body">
            
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="post">    
              <input type="hidden" id="copy" name="fileAndFolder" value="">   
              <input class="form-control" type="hidden" id="directory" name="directory" value="{{$directory}}">

              <div class="form-group form-group-sm">
                    <div class="input-group col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                          <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                          <input class="form-control" type="text" id="targetDirectory" name="targetDirectory" value="{{$directory}}">                          
                    </div>
              </div>                                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-outline">Copy</button>
          </div>
        </div>
    </form>
  </div>
</div>
<div id="moveDialog" class="modal fade modal-primary" role="dialog">
  <div class="modal-dialog modal-dialog-center">
    <form class="form-horizontal" action="/move-files-and-folder" method="post"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title text-center">Move</h4>
          </div>
          <div class="modal-body">
            
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="post">    
              <input type="hidden" id="move" name="fileAndFolder" value=""> 
              <input class="form-control" type="hidden" id="directory" name="directory" value="{{$directory}}">  
              <div class="form-group form-group-sm">
                    <div class="input-group col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                          <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                          <input class="form-control" type="text" id="targetDirectory" name="targetDirectory" value="{{$directory}}">
                    </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-outline">Move</button>
          </div>
        </div>
    </form>
  </div>
</div>
<div id="renameDialog" class="modal fade modal-primary" role="dialog">
  <div class="modal-dialog modal-dialog-center">
    <form class="form-horizontal" action="/rename" method="post"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title text-center">Rename</h4>
          </div>
          <div class="modal-body">
            
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="post">    
              <input type="hidden" id="rename" name="fileAndFolder" value=""> 
              <input class="form-control" type="hidden" id="directory" name="directory" value="{{$directory}}">  
              <div class="form-group form-group-sm">
                    <div class="input-group col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                          <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                          <input class="form-control" type="text" name="rename" value="untitled">
                    </div>
              </div>    
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-outline">Rename</button>
          </div>
        </div>
    </form>
  </div>
</div>
<div id="deleteDialog" class="modal modal-danger" role="dialog">
  <div class="modal-dialog modal-dialog-center">
    <form class="form-horizontal" action="/delete-files-and-folder" method="post"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title text-center">Delete</h4>
          </div>
          <div class="modal-body">
            
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="delete">

            <input type="hidden" id="delete" name="fileAndFolder" value="">            
            <input type="hidden" name="directory" value="{{$directory}}">
              <p class="text-center">Do you want to delete this file/folder from cloud. This action will permanently delete from server.</p>   
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-outline">Delete</button>
          </div>
        </div>
    </form>
  </div>
</div>
<div id="uploadDialog" class="modal fade modal-primary" role="dialog">
  <div class="modal-dialog modal-dialog-center">
    <form class="form-horizontal" action="/upload" method="post" enctype="multipart/form-data"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title text-center">File Upload</h4>
          </div>
          <div class="modal-body">
            
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="post">    
              <input type="hidden" id="moveDialog" name="fileAndFolder" value="">   
              <input class="form-control" type="hidden" id="directory" name="directory" value="{{$directory}}">

              <div class="form-group form-group-sm">
                <div class="row">
                    <label class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2 control-label btn btn-primary" for="my-file-selector">
                        <input class="form-control"  id="my-file-selector" type="file" name="uploadFile" style="display:none;" onchange="$('#upload-file-info').html($(this).val());">
                        Select File
                    </label>
                    <span class='label label-info' id="upload-file-info"></span>             
                </div>
              </div>                      
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-outline">Upload</button>
          </div>
        </div>
    </form>
  </div>
</div>

<div id="errorDialog" class="modal fade modal-danger" role="dialog">
  <div class="modal-dialog modal-dialog-center">
    <form class="form-horizontal" action="/upload" method="post" enctype="multipart/form-data"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title text-center">Error</h4>
          </div>
          <div class="modal-body">
              <p class="text-center" id="fileManagerError" val>
                
              </p>                 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>            
          </div>
        </div>
    </form>
  </div>
</div>

@endsection

@section('script')
    <script>
        $( "#storage" ).addClass( "active" );
    </script>
    <script type="text/javascript">

        (function ($, window) {

          $.fn.contextMenu = function (settings) {

              return this.each(function () {

                  // Open context menu
                  $(this).on("contextmenu", function (e) {
                      // return native menu if pressing control
                      if (e.ctrlKey) return;                    
                      //open menu
                      /*seeding the menu data*/
                      $("#copy").val($(e.target).text());
                      $("#move").val($(e.target).text());
                      $("#rename").val($(e.target).text());
                      $("#view").val($(e.target).text());
                      $("#download").val($(e.target).text());
                      $("#delete").val($(e.target).text());

                      

                      var $menu = $(settings.menuSelector)
                          .data("invokedOn", $(e.target))
                          .show()
                          .css({
                              position: "absolute",
                              left: getMenuPosition(e.clientX, 'width', 'scrollLeft'),
                              top: getMenuPosition(e.clientY, 'height', 'scrollTop')
                          })
                          .off('click')
                          .on('click', 'a', function (e) {
                              $menu.hide();                    
                              var $invokedOn = $menu.data("invokedOn");                            
                              var $selectedMenu = $(e.target);
                              
                              settings.menuSelected.call(this, $invokedOn, $selectedMenu);
                          });
                      
                      return false;
                  });

                  //make sure menu closes on any click
                  $('body').click(function () {
                      $(settings.menuSelector).hide();
                  });
              });
              
              function getMenuPosition(mouse, direction, scrollDir) {
                  var win = $(window)[direction](),
                      scroll = $(window)[scrollDir](),
                      menu = $(settings.menuSelector)[direction](),
                      position = mouse + scroll;
                              
                  // opening menu would pass the side of the page
                  if (mouse + menu > win && menu < mouse) 
                      position -= menu;
                  
                  return position;
              }    

          };

        })(jQuery, window);

        $("#myTable td").contextMenu({            
            menuSelector: "#contextMenu",

           /* menuSelected: function (invokedOn, selectedMenu) {
               // var msg = "You selected the menu item '" + selectedMenu.text() +
                 //   "' on the value '" + invokedOn.text() + "'";
               // alert(msg);
            }*/
        });

       @if(session('fileManagerError'))
          document.getElementById("fileManagerError").innerHTML = "{{ session('fileManagerError') }}";          
          $('#errorDialog').modal();   
          {{session()->forget('fileManagerError')}}
       @endif

    </script>
@endsection