@extends('layouts.mis')
@section('header')
    <h1>
        Privacy Policy
        <small>Privacy policy information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">Privacy Policy</li>
    </ol>
@endsection
@section('content') 
 
 <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Privacy policy Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="box-group" id="accordion">
            
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#cookies-policy">
                    Cookies Policy
                  </a>
                </h4>
              </div>
              <div id="cookies-policy" class="panel-collapse collapse in">
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="callout callout-info">
                            <h4>Cookies & Other Storage Technologies</h4>
                        </div>
                        <div class="col-md-12 text-justify">
                          <p>
                            Cookies are small pieces of text used to store information on web browsers. Cookies are used to store and receive identifiers and other information on computers, phones, and other devices. Other technologies, including data we store on your web browser or device, identifiers associated with your device, and other software, are used for similar purposes. In this policy, we refer to all of these technologies as “cookies.” 
                          </p>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="callout callout-info">
                            <h4>Why do we use cookies?</h4>
                        </div>
                        <div class="col-md-12 text-justify">
                          <p>
                            <ol>
                                <li>Authentication</li>
                                <li>Security</li>
                                <li>Performance</li>                                
                            </ol>
                          </p>
                        </div>
                    </div>

                </div>
              </div>
            </div>
            

            <div class="panel box box-danger">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    Statement of Rights and Responsibilities
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse">
                <div class="box-body">
                    Empty
                </div>
              </div>
            </div>
            

           
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>        
  </div>
      

@endsection

@section('script')
    <script>
        $( "#privacy-policy" ).addClass( "active" );
    </script>
@endsection