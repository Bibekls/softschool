@extends('layouts.mis')
@section('header')
    <h1>
        Works
        <small>Edit Work</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">work</li>
    </ol>
@endsection

@section('content') 

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">                    
                    <h3 class="box-title">Work Category Setting</h3>     
            </div>
            <div class="box-body">
                <div class="col-md-6" style="padding: 1%;padding-bottom:0;">
                    <form class="form-inline" role="form" method="POST" action="{{ url('/workcategory') }}">
                          <div class="form-group">
                            <label for="addcategory">Add Category</label>
                            <input type="text" name="name" placeholder="Add Category" class="form-control" id="addcategory">
                          </div>                    
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">    
                        <button type="submit" class='btn btn-success btn-flat' style="color: white;">
                                &nbsp&nbsp&nbsp<i class='fa fa-plus'></i> &nbsp&nbsp&nbsp
                        </button>                     
                    </form>
                </div>               
                
                <div class="col-md-6" style="padding: 1%;padding-bottom:0;">
                    <form class="form-inline" role="form" method="POST" action="{{ url('/workcategory/0') }}" onsubmit="return confirm('Do you to Delete?');">
                        <div class="form-group">
                            <label for="deletecategory">Delete Category</label>
                            
                            <select class="form-control" id="deletecategory" name="category">                                  
                              @foreach($works as $workCategory)
                                <option 
                                    @if($workCategory->id==1)
                                        selected
                                    @endif
                                >{{$workCategory->name}}</option>
                              @endforeach
                            </select>                            
                        </div> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="delete">

                        <button type="submit" class='btn btn-danger btn-flat' style="color: white;">
                                &nbsp&nbsp&nbsp<i class='fa fa-trash-o'></i> &nbsp&nbsp&nbsp
                        </button> 
                    </form>
                </div>
            </div>
        </div>            
    </div>
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach($works as $WorkCategory)
                <li 
                @if($WorkCategory->id==1)
                    class="active"
                @endif
                ><a href="#{{$WorkCategory->id}}" data-toggle="tab">{{$WorkCategory->name}}</a></li>
            @endforeach            
        </ul>
        <div class="tab-content">
            @foreach($works as $WorkCategory)
              <div class="
                @if($WorkCategory->id==1)
                      active
                @endif
              tab-pane" id="{{$WorkCategory->id}}">
                <ul class="list-group list-inline">        
                    @foreach($WorkCategory->works as $work) 
                        <li class="list-group-item"> 
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/work/'.$work->id) }}" onsubmit="return confirm('Do you to Delete?');">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="delete">   
                                <a href="http://hcc.edu.np/work/{{$work->id}}" target="_blank">{{$work->title}}</a> &nbsp &nbsp&nbsp&nbsp
                                
                                <a href="/work/{{$work->id}}" class='btn btn-box-tool'>
                                    <i class='fa fa-pencil'></i>
                                </a>                
                                <button type="submit" class='btn btn-box-tool'><i class='fa fa-trash-o'></i>
                                </button>                  
                            </form>  
                        </li>                                  
                    @endforeach        
                </ul>
              </div>
            @endforeach
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
</div>
<form class="form-horizontal" role="form" method="POST" action="{{ url('/work/'.$editwork->id) }}">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Work</h3>
                </div>
                <div class="box-body">
                    <div class="col-lg-10 col-lg-offset-1">
                        @if(Session::has('message'))
                            <div class="alert alert-info">
                                <strong>Info!</strong>
                                {{Session::get('message')}}
                                {{Session::forget('message')}}
                            </div>
                        @endif
                        
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PUT">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="fa fa-link"></i>
                                        </span>
                                        <input type="text" class="form-control" 
                                            name="title" value="{{$editwork->title}}" 
                                            placeholder="Title of Work">
                                        <span class="input-group-addon">
                                            <i class="fa fa-asterisk"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('title'))
                                        <div class="input-group has-error">
                                            <label class="control-label" for="inputError">
                                                <i class="fa fa-times-circle-o"></i>
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </label>
                                        </div>
                                    @endif
                                </div>
                            </div><br>


                             <div class="row">
                                <!--Name of book -->
                                <div class="col-md-12">
                                    <div class="input-group{{ $errors->has('summary') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                             <i class="fa fa-pencil"></i>
                                        </span>
                                        <input type="text" class="form-control" 
                                            name="summary" value="{{ $editwork->summary }}" 
                                                placeholder="Summary of work">
                                         <span class="input-group-addon">
                                            <i class="fa fa-asterisk"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('summary'))
                                        <div class="input-group has-error">
                                            <label class="control-label" for="inputError">
                                                <i class="fa fa-times-circle-o"></i>
                                                <strong>{{ $errors->first('summary') }}</strong>
                                            </label>
                                        </div>
                                    @endif
                                </div>
                                <!--Name of author -->                                          
                            </div><br>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="input-group">
                                  <label for="category">Category</label>
                                    <select class="form-control" id="category" name="category">
                                      @foreach($works as $WorkCategory)
                                        <option 
                                            @if($editwork->workcategory->name==$WorkCategory->name)
                                                selected
                                            @endif
                                        >{{$WorkCategory->name}}</option>
                                      @endforeach
                                    </select>                                    
                                </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="input-group{{ $errors->has('banner') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                             <i class="fa fa-pencil"></i>
                                        </span>
                                        <input type="text" class="form-control" 
                                            name="banner" value="{{ $editwork->banner }}" 
                                                placeholder="Banner Image Link [ 600 x 500 ]">
                                         <span class="input-group-addon">
                                            <i class="fa fa-asterisk"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('banner'))
                                        <div class="input-group has-error">
                                            <label class="control-label" for="inputError">
                                                <i class="fa fa-times-circle-o"></i>
                                                <strong>{{ $errors->first('banner') }}</strong>
                                            </label>
                                        </div>
                                    @endif
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="pull-right">
                                    <div class="input-group"><br>
                                        <button type="submit" id="searchstudent" class="btn btn-primary btn-flat">
                                            <i class="fa fa-plus"></i> Update Work &nbsp                                        
                                        </button>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">            
                <textarea id="data" name="content" rows="50" cols="80">{{ $editwork->content }}</textarea>    
            </div>            
        </div>
    </div>
</form>
@endsection

@section('script')
    <script>
        $( "#website" ).addClass( "active" );
    </script>
    <script src="/plugins/ckeditor/ckeditor.js"></script>
    <script>
      $(function () {
        CKEDITOR.replace('data');
        CKEDITOR.config.height = window.innerHeight-265;
        CKEDITOR.config.skin = 'flat';
        CKEDITOR.on('instanceReady',
       function( evt )
         {
        var editor = evt.editor;
        //editor.execCommand('maximize');
         });    
      });
  
</script>
@endsection

