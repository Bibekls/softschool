@extends('layouts.mis')
@section('header')
    <h1>
        Feedback
        <small>Feedback</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">feedback</li>
    </ol>
@endsection
@section('content')
        
        {{--<div class="box box-success">--}}
            {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">Give your feedback to make our system more stable.</h3>--}}
            {{--</div>--}}
            {{--<div class="box-body">--}}
                {{--<div class="col-lg-10 col-lg-offset-1">                                       --}}
                    {{--<form class="form-horizontal" role="form" method="POST" --}}
                        {{--action="{{ url('/feedback') }}">--}}

                        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="input-group{{ $errors->has('feedback') ? ' has-error' : '' }}">--}}
                                    {{--<span class="input-group-addon">--}}
                                        {{--<i class="fa fa-newspaper-o"></i>--}}
                                    {{--</span>--}}
                                    {{--<textarea class="form-control" rows="5" name="feedback" placeholder="Write your Feedback here.....">{{ old('feedback') }}</textarea>--}}
                                {{--</div>--}}

                                {{--@if ($errors->has('feedback'))--}}
                                    {{--<div class="input-group has-error">--}}
                                        {{--<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>--}}
                                            {{--<strong>{{ $errors->first('feedback') }}</strong>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row">--}}
                            {{--<div class="pull-right">--}}
                                {{--<div class="input-group"><br>--}}
                                    {{--<button type="submit" id="searchstudent" class="btn btn-primary btn-flat">--}}
                                        {{--<i class="fa fa-plus"></i> Send --}}
                                        {{--<i id="addStudent" class="fa fa-circle-o-notch fa-spin hidden"></i>--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div> --}}
        
        <div class="row">
            @foreach($feedbacks as $feedback)
           
                <div class="col-md-10 col-md-offset-1">
                  <!-- Box Comment -->
                  <div class="box box-widget">
                    <div class='box-header with-border'>
                      <div class='user-block'>
                        <img class='img-circle' src="/images/thumbnails/{{$feedback->profile_pic}}" alt='user image'>
                        <span class='username'><a href="#">{{$feedback->user->name}}</a></span>
                        <span class='description'>
                            {{Carbon::createFromTimeStamp(strtotime($feedback->created_at))->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime($feedback->created_at))))->diffForHumans() }}
                        </span>
                      </div><!-- /.user-block -->                  
                      <div class='box-tools'> 
                        
                          <form class="form-horizontal" role="form" method="POST" 
                          action="{{ url('/feedback') }}">                    
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="feedback_id" value="{{$feedback->id}}">                            
                            <button type="submit" class='btn btn-box-tool'><i class='fa fa-2x fa-trash'></i>
                            </button>
                          
                        </form>                                                  
                      </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body'>
                      <p class="text-justify"><a>{{$feedback->feedback}}</a></p>  
                    </div><!-- /.box-body -->             
                  </div><!-- /.box -->
                </div><!-- /.col -->     
            @endforeach
        </div>
        <div class="row">
            <div class="container">    
                <div class="pagination"> {{$feedbacks->links() }} </div>  
            </div>
        </div>


@endsection

@section('script')
    <script>
        $( "#feedback" ).addClass( "active" );
    </script>
@endsection