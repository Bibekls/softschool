@extends('layouts.mis')
@section('header')
    <h1>
        Event Manager
        <small>Add Event</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">subject-manager</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <form method="POST" action="{{ url('/event') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Event</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="input-group{{ $errors->has('title') ? ' has-error' : '' }} col-md-12">
                            <label>Title</label>

                            <input type="text" class="form-control input-sm" name="title"
                                   value="{{ old('title') }}" placeholder="Title">
                        </div>
                        <br>
                        @if ($errors->has('title'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('title') }}</strong>
                                </label>
                            </div><br>
                        @endif

                        <div class="input-group{{ $errors->has('description') ? ' has-error' : '' }} col-md-12">
                            <label>Description</label>
                            <textarea name="description" class="form-control" rows="5">{{old('description')}}</textarea>
                        </div>
                        <br>
                        @if ($errors->has('description'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('description') }}</strong>
                                </label>
                            </div><br>
                        @endif


                        <div class="input-group{{ $errors->has('date') ? ' has-error' : '' }} col-md-12">
                            <label>Date</label>

                            <input type="text" class="form-control input-sm datePicker" name="date"
                                   value="{{ old('date') }}" placeholder="Date">
                        </div>
                        <br>
                        @if ($errors->has('date'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('date') }}</strong>
                                </label>
                            </div><br>
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <label>
                                    <input type="checkbox" class="flat-red" id="timeInterval"
                                           name="has_time_interval">
                                    Has Time Interval
                                </label>
                                <hr>
                            </div>
                        </div>

                        <div class="row" id="scheduleRow" style="display: none">
                            <div class="col-md-12">
                                If this event has time interval then please enter below.
                            </div>
                            <br>
                            <div class="col-md-6">
                                <div class="input-group bootstrap-timepicker  {{ $errors->has('start_at') ? ' has-error' : '' }} col-md-12">
                                    <label>Start At</label>

                                    <input type="text" class="form-control timepicker" name="start_at"
                                           value="{{ old('start_at') }}" placeholder="start_at">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group bootstrap-timepicker {{ $errors->has('end_at') ? ' has-error' : '' }} col-md-12">
                                    <label>End At</label>

                                    <input type="text" class="form-control timepicker" name="end_at"
                                           value="{{ old('end_at') }}" placeholder="end_at">
                                </div>
                            </div>

                        </div>

                        <br>


                        <div class="input-group col-md-12">
                            <button class="btn btn-primary btn-flat btn-block">Add Event</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">

                    <li class="active">
                        <a href="#upcomming" data-toggle="tab">Up comming</a>
                    </li>

                    <li>
                        <a href="#completed" data-toggle="tab">Completed</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="upcomming">
                        <table id="futureEventTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Start time</th>
                                <th>End time</th>
                                <th>Controls</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($futureEvents as $event)
                                <tr>
                                    <td>
                                        {{Carbon::createFromTimeStamp(strtotime($event->date))->diffForHumans()}}
                                        {{--{{Carbon::now()->subSeconds( ))->diffForHumans() }}--}}
                                    </td>
                                    <td>
                                        <b>{{$event->title}}</b> <br>
                                        <p>{{$event->description}}</p>
                                    </td>
                                    <td>@if($event->start_at) {{$event->start_at}} @else --:--:-- @endif</td>
                                    <td>@if($event->start_at) {{$event->end_at}} @else --:--:-- @endif</td>
                                    <td>
                                        <form class="form-horizontal" role="form"
                                              method="POST"
                                              action="{{ url('/event/'.$event->id)}}"
                                              onsubmit="return confirm('Do you to Delete?');">
                                            <input type="hidden" name="_token"
                                                   value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method"
                                                   value="delete">
                                            <button type="submit"
                                                    class='btn btn-danger'><i
                                                        class='fa fa-trash-o'></i>
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Start time</th>
                                <th>End time</th>
                                <th>Controls</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="tab-pane" id="completed">
                        <table id="pastEventTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Start time</th>
                                <th>End time</th>
                                <th>Controls</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pastEvents as $event)
                                <tr>
                                    <td>
                                        {{$event->date}}
                                    </td>
                                    <td>
                                        <b>{{$event->title}}</b> <br>
                                        <p>{{$event->description}}</p>
                                    </td>
                                    <td>@if($event->start_at) {{$event->start_at}} @else --:--:-- @endif</td>
                                    <td>@if($event->start_at) {{$event->end_at}} @else --:--:-- @endif</td>
                                    <td>
                                        <form class="form-horizontal" role="form"
                                              method="POST"
                                              action="{{ url('/event/'.$event->id)}}"
                                              onsubmit="return confirm('Do you to Delete?');">
                                            <input type="hidden" name="_token"
                                                   value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method"
                                                   value="delete">
                                            <button type="submit"
                                                    class='btn btn-danger'><i
                                                        class='fa fa-trash-o'></i>
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Start time</th>
                                <th>End time</th>
                                <th>Controls</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
    </div>

@endsection

@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="http://hetaudacitycollege.edu.np/plugins/fullcalendar/fullcalendar.min.js"></script>

    <script>
        $("#event").addClass("active");
    </script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
            $(".timepicker").timepicker({
                showInputs: false
            });
        });

        $('.datePicker').datepicker({
            autoclose: true
        });

        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#futureEventTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aaSorting": [[0, 'asc']]
            });
        });
        $(document).ready(function () {
            $('#pastEventTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aaSorting": [[0, 'desc']]
            });
        });
    </script>

    <script>
        $(document.body).on("ifChecked", "#timeInterval", function () {
            $("#scheduleRow").show();
        });
        $(document.body).on("ifUnchecked", "#timeInterval", function () {
            $("#scheduleRow").hide();
        });
    </script>

    <script>
        $(function () {

            /* initialize the external events
             -----------------------------------------------------------------*/
            function ini_events(ele) {
                ele.each(function () {

                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    };

                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject);

                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 1070,
                        revert: false, // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    });

                });
            }

            ini_events($('#external-events div.external-event'));

            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date();
            var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week: 'week',
                    day: 'day'
                },
                //Random default events
                events: [
                        @foreach($events as $event)
                    {
                        title: '{{$event->title}}',
                        backgroundColor: "#f56954",
                        borderColor: "#f56954",
                        @if($event->start_at)
                        start: new Date('{{$event->date}} {{$event->start_at}}'),
                        end: new Date('{{$event->date}} {{$event->end_at}}'),
                        @else
                        start: new Date('{{$event->date}}'),
                        allDay: true,
                        @endif
                    },
                    @endforeach
                ],
                editable: false,
                droppable: false, // this allows things to be dropped onto the calendar !!!
                drop: function (date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.backgroundColor = $(this).css("background-color");
                    copiedEventObject.borderColor = $(this).css("border-color");

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                }
            });

            /* ADDING EVENTS */
            var currColor = "#3c8dbc"; //Red by default
            //Color chooser button
            var colorChooser = $("#color-chooser-btn");
            $("#color-chooser > li > a").click(function (e) {
                e.preventDefault();
                //Save color
                currColor = $(this).css("color");
                //Add color effect to button
                $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
            });
            $("#add-new-event").click(function (e) {
                e.preventDefault();
                //Get value and make sure it is not null
                var val = $("#new-event").val();
                if (val.length == 0) {
                    return;
                }

                //Create events
                var event = $("<div />");
                event.css({
                    "background-color": currColor,
                    "border-color": currColor,
                    "color": "#fff"
                }).addClass("external-event");
                event.html(val);
                $('#external-events').prepend(event);

                //Add draggable funtionality
                ini_events(event);

                //Remove event from text input
                $("#new-event").val("");
            });
        });
    </script>


@endsection
