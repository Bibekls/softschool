@extends('layouts.mis')

@section('header')
    <h1>
        Editor
        <small>Editor</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">Editor</li>
    </ol>
@endsection
@section('content')
<div class="row"> 
  <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header">
            <h3 class="box-title">HTML Editor
              <small>Make your document and share online.</small>
            </h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /. tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body pad">
            <form 
            @if($storageSession)
              action="{{url('save')}}" 
            @else
              action="{{url('web-save')}}" 
            @endif
            method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="_method" value="post">   
                  <input type="hidden" name="directory" value="{{$directory}}">
                  <input type="hidden" name="fileName" value="{{$fileName}}">
                  <textarea id="data" name="data" rows="50" cols="80">
                        <?php echo $data ?>
                  </textarea>
            </form>
          </div>
        </div> 
  </div>
</div>

@endsection

@section('script')
<script src="//cdn.ckeditor.com/4.5.10/full/ckeditor.js"></script>
<script>
  $(function () {
    CKEDITOR.replace('data');
    CKEDITOR.on('instanceReady',
   function( evt )
     {
    var editor = evt.editor;
    editor.execCommand('maximize');
     });    
  });
  
</script>
@endsection