@extends('layouts.mis')
@section('header')
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">dashboard</li>
    </ol>
@endsection

@section('content')
<div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-sign-in"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Last Login</span>
                <span class="info-box-number">                       
                          {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime(Auth::user()->last_login_time))))->diffForHumans() }}
                </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-wifi"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">IP</span>
              <span class="info-box-number">{{Auth::user()->last_login_ip}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green">
            <i class="ion ion-ios-paper-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Post</span>
              <span class="info-box-number">{{$totalPost}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-android-textsms"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Comment</span>
              <span class="info-box-number">{{$totalComment}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
</div>


<div class="row">
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-image"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Images</span>
              <span class="info-box-number">{{$totalImage}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green">
            <i class="ion ion-link"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Url</span>
              <span class="info-box-number">{{$totalUrl}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Feedback</span>
              <span class="info-box-number">{{$totalFeedback}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
</div>

<h3>
    Library    
</h3>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-book"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Books</span>
          <span class="info-box-number">{{$books}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-building"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Publications</span>
          <span class="info-box-number">{{$publications}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-social-usd"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Total Price</span>
          <span class="info-box-number">{{$price}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-book"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Available books</span>
          <span class="info-box-number">{{$bookAvailable}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>


@endsection

@section('script')
    <script>
        $( "#dashboard" ).addClass( "active" );
    </script>
@endsection
