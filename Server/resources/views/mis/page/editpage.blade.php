@extends('layouts.mis')
@section('header')
    <h1>
        Page
        <small>Edit Page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">page</li>
    </ol>
@endsection

@section('content') 
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">                    
                    <h3 class="box-title">Page Category Setting</h3>     
            </div>
            <div class="box-body">
                <div class="col-md-6" style="padding: 1%;padding-bottom:0;">
                    <form class="form-inline" role="form" method="POST" action="{{ url('/pagecategory') }}">
                          <div class="form-group">
                            <label for="addcategory">Add Category</label>
                            <input type="text" name="name" placeholder="Add Category" class="form-control" id="addcategory">
                          </div>                    
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">    
                        <button type="submit" class='btn btn-success btn-flat' style="color: white;">
                                &nbsp&nbsp&nbsp<i class='fa fa-plus'></i> &nbsp&nbsp&nbsp
                        </button>                     
                    </form>
                </div>               
                
                <div class="col-md-6" style="padding: 1%;padding-bottom:0;">
                    <form class="form-inline" role="form" method="POST" action="{{ url('/pagecategory/0') }}" onsubmit="return confirm('Do you to Delete?');">
                        <div class="form-group">
                            <label for="deletecategory">Delete Category</label>
                            
                            <select class="form-control" id="deletecategory" name="category">                                  
                              @foreach($pages as $pageCategory)
                                <option 
                                    @if($pageCategory->id==1)
                                        selected
                                    @endif
                                >{{$pageCategory->name}}</option>
                              @endforeach
                            </select>                            
                        </div> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="delete">

                        <button type="submit" class='btn btn-danger btn-flat' style="color: white;">
                                &nbsp&nbsp&nbsp<i class='fa fa-trash-o'></i> &nbsp&nbsp&nbsp
                        </button> 
                    </form>
                </div>
            </div>
        </div>            
    </div>
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach($pages as $pageCategory)
                <li 
                @if($pageCategory->id==1)
                    class="active"
                @endif
                ><a href="#{{$pageCategory->id}}" data-toggle="tab">{{$pageCategory->name}}</a></li>
            @endforeach
            <div class="pull-right" style="padding: 1%;padding-bottom:0;">
                <form role="form" method="POST" action="{{ url('/pagecategory') }}">
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="text" name="name" placeholder="Add Category" class="form-control">
                    </div>
                </form>
            </div>
        </ul>
        <div class="tab-content">
            @foreach($pages as $pageCategory)
              <div class="
                @if($pageCategory->id==1)
                      active
                @endif
              tab-pane" id="{{$pageCategory->id}}">
                <ul class="list-group list-inline">        
                    @foreach($pageCategory->pages as $page)  
                        <li class="list-group-item"> 
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/page/'.$page->id) }}" onsubmit="return confirm('Do you to Delete?');">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="delete">   
                                <a href="http://hcc.edu.np/{{$page->tag}}" target="_blank">{{$page->name}}</a> &nbsp &nbsp&nbsp&nbsp
                                <span class="badge">{{$page->order}}</span>                                
                                <a href="/page/{{$page->id}}" class='btn btn-box-tool'>
                                    <i class='fa fa-pencil'></i>
                                </a>                
                                <button type="submit" class='btn btn-box-tool'><i class='fa fa-trash-o'></i>
                                </button>                  
                            </form>  
                        </li>  
                    @endforeach        
                </ul>
              </div>
            @endforeach
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
</div>
<form class="form-horizontal" role="form" method="POST" action="{{ url('/page/'.$editpage->id) }}">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Page</h3>
                </div>
                <div class="box-body">
                    <div class="col-lg-10 col-lg-offset-1">
                        @if(Session::has('message'))
                            <div class="alert alert-info">
                                <strong>Info!</strong>
                                {{Session::get('message')}}
                                {{Session::forget('message')}}
                            </div>
                        @endif
                        
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PUT">

                             <div class="row">
                                <!--Name of book -->
                                <div class="col-md-12">
                                    <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                             <i class="fa fa-pencil"></i>
                                        </span>
                                        <input type="text" class="form-control" 
                                            name="name" value="{{ $editpage->name }}" 
                                                placeholder="Name">
                                         <span class="input-group-addon">
                                            <i class="fa fa-asterisk"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="input-group has-error">
                                            <label class="control-label" for="inputError">
                                                <i class="fa fa-times-circle-o"></i>
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </label>
                                        </div>
                                    @endif
                                </div>
                                <!--Name of author -->                                          
                            </div><br>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="fa fa-link"></i>
                                        </span>
                                        <input type="text" class="form-control" 
                                            name="title" value="{{ $editpage->title }}" 
                                            placeholder="Title">
                                        <span class="input-group-addon">
                                            <i class="fa fa-asterisk"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('title'))
                                        <div class="input-group has-error">
                                            <label class="control-label" for="inputError">
                                                <i class="fa fa-times-circle-o"></i>
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </label>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group{{ $errors->has('tag') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="fa fa-link"></i>
                                        </span>
                                        <input type="text" class="form-control" 
                                            name="tag" value="{{ $editpage->tag }}" 
                                            placeholder="Tag">
                                        <span class="input-group-addon">
                                            <i class="fa fa-asterisk"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('tag'))
                                        <div class="input-group has-error">
                                            <label class="control-label" for="inputError">
                                                <i class="fa fa-times-circle-o"></i>
                                                <strong>{{ $errors->first('tag') }}</strong>
                                            </label>
                                        </div>
                                    @endif
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col-md-2 col-xs-4">
                                    <p class="text-right"><b>Category:</b></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                <div class="input-group">
                                    <select class="form-control" id="category" name="category">                                  
                                      @foreach($pages as $pageCategory)
                                        <option 
                                            @if($editpage->pageCategory->name==$pageCategory->name)
                                                selected
                                            @endif
                                        >{{$pageCategory->name}}</option>
                                      @endforeach
                                    </select>
                                </div>
                                </div>
                                <div class="col-md-4">
                                <div class="input-group{{ $errors->has('order') ? ' has-error' : '' }}">
                                    <span class="input-group-addon">
                                         <i class="fa fa-pencil"></i>
                                    </span>
                                    <input type="text" class="form-control" 
                                        name="order" value="{{ $editpage->order }}" 
                                            placeholder="Order">
                                     <span class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </span>
                                </div>
                                @if ($errors->has('order'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError">
                                            <i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('order') }}</strong>
                                        </label>
                                    </div>
                                @endif
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="pull-right">
                                    <div class="input-group"><br>
                                        <button type="submit" id="searchstudent" class="btn btn-primary btn-flat">
                                            <i class="fa fa-plus"></i> Update Page &nbsp                                        
                                        </button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                <textarea id="data" name="content" rows="50" cols="80">{{ $editpage->content }}</textarea>
        </div>
    </div>
</form>
@endsection

@section('script')
    <script>
        $( "#website" ).addClass( "active" );
    </script>
    <script src="/../../plugins/ckeditor/ckeditor.js"></script>
    <script>
      $(function () {
        CKEDITOR.replace('data');
        CKEDITOR.config.height = window.innerHeight-265; 
        CKEDITOR.config.skin = 'flat';
        CKEDITOR.on('instanceReady',
       function( evt )
         {
        var editor = evt.editor;
        //editor.execCommand('maximize');
         });    
      });
  
</script>
@endsection

