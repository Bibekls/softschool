@extends('layouts.mis')
@section('header')
    <h1>
        Routine Manager
        <small>Add Routine</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">routine-manager</li>
    </ol>
@endsection
@section('style')
    <style>
        .progress-block {
            height: 60px !important;
        }
    </style>
@endsection
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Routine</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    @if( count( $errors ) > 0 )
                        <div class="callout callout-danger">
                            <h4>Transaction Error !!!</h4>
                            <ol>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ol>
                        </div>
                    @endif
                    <form class="form-inline" method="POST" action="{{ url('/routine') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group col-md-4">
                            <label>Faculty</label>
                            <select class="form-control select2" id="faculty" style="width: 100%;" name="faculty">
                                <option selected="selected">Select-Faculty</option>
                                @foreach($faculties as $faculty)
                                    <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Batch</label>
                            <select class="form-control select2" id="batch" style="width: 100%;" name="batch">
                                <option selected="selected" value="0">Select-Batch</option>
                                @foreach($batches as $batch)
                                    <option>{{$batch}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Level</label>
                            <select class="form-control select2" id="level" style="width: 100%;" name="level">
                                <option selected="selected" value="0">Select-level</option>
                                <option value="1">First</option>
                                <option value="2">Second</option>
                                <option value="3">Third</option>
                                <option value="4">Fourth</option>
                                <option value="5">Fifth</option>
                                <option value="6">Sixth</option>
                                <option value="7">Seventh</option>
                                <option value="8">Eighth</option>

                            </select>
                        </div>


                        <div class="form-group col-md-6">
                            <label>Teacher</label>
                            <select class="form-control select2" style="width: 100%;" name="teacher" id="teacherList">
                                <option selected="selected">Select-Teacher</option>
                                @foreach($teachers as $teacher)
                                    <option value="{{$teacher->id}}">{{$teacher->first_name}} {{$teacher->last_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Subject</label>
                            <select class="form-control select2" style="width: 100%;" name="subject" id="subjectList">
                                <option selected="selected">Select-Subject</option>
                                @foreach($subjects as $subject)
                                    <option value="{{$subject->id}}">{{$subject->name}} ( {{$subject->batch}} )</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Day</label>
                            <select class="form-control select2" multiple="multiple" style="width: 100%;" name="day[]">
                                <option selected="selected">Sunday</option>
                                <option>Monday</option>
                                <option>Tuesday</option>
                                <option>Wednesday</option>
                                <option>Thursday</option>
                                <option>Friday</option>
                                <option>Saturday</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2 bootstrap-timepicker">
                            <label>Start At</label>
                            <input type="text" class="form-control timepicker" style="width: 100%;" name="start_at"
                                   value="06:00 AM">
                        </div>
                        <div class="form-group col-md-2 bootstrap-timepicker">
                            <label>End At</label>
                            <input type="text" class="form-control timepicker" style="width: 100%;" name="end_at"
                                   value="07:00 AM">
                        </div>
                        <div class="form-group col-md-2">
                            <br>
                            <button class="btn btn-primary btn-flat btn-block">Add To Routine</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    @foreach($routines as $routine)
                        <li @if($routine['faculty']->id==1) class="active @endif">
                            <a href="#{{$routine['faculty']->name}}" id="{{$routine['faculty']->name}}-tab"
                               data-toggle="tab">{{$routine['faculty']->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($routines as $routine)
                        <div class="
                            @if($routine['faculty']->id==1)
                                active
                              @endif
                                tab-pane"
                             id="{{$routine['faculty']->name}}">
                            <div class="nav-tabs-custom dark-box">
                                <ul class="nav nav-tabs">
                                    @foreach($routine['level-list'] as $levelList)
                                        <li @if($levelList['level']==1) class="active @endif">
                                            <a href="#{{$levelList['level']}}-{{$routine['faculty']->name}}"
                                               id="{{$levelList['level']}}-{{$routine['faculty']->name}}-tab"
                                               data-toggle="tab">Semester-({{$levelList['level']}})</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content">
                                    @foreach($routine['levels'] as $level)
                                        <div class="    @if($level['name']==1)
                                                active
                                              @endif
                                                tab-pane"
                                             id="{{$level['name']}}-{{$routine['faculty']->name}}">

                                            <table class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Day</th>
                                                    <th>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 style="width:11.11%">
                                                                06:00 - 06:30
                                                            </div>
                                                            <div class="progress-bar  progress-bar-info"
                                                                 role="progressbar" style="width:11.11%">
                                                                06:30 - 07:00
                                                            </div>
                                                            <div class="progress-bar" role="progressbar"
                                                                 style="width:11.11%">
                                                                07:00 - 07:30
                                                            </div>
                                                            <div class="progress-bar  progress-bar-info"
                                                                 role="progressbar" style="width:11.11%">
                                                                07:30 - 08:00
                                                            </div>
                                                            <div class="progress-bar" role="progressbar"
                                                                 style="width:11.11%">
                                                                08:00 - 08:30
                                                            </div>
                                                            <div class="progress-bar  progress-bar-info"
                                                                 role="progressbar" style="width:11.11%">
                                                                08:30 - 09:00
                                                            </div>
                                                            <div class="progress-bar" role="progressbar"
                                                                 style="width:11.11%">
                                                                09:00 - 09:30
                                                            </div>
                                                            <div class="progress-bar  progress-bar-info"
                                                                 role="progressbar" style="width:11.11%">
                                                                09:30 - 10:00
                                                            </div>
                                                            <div class="progress-bar" role="progressbar"
                                                                 style="width:11.11%">
                                                                10:00 - 10:30
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($level['days'] as $day)
                                                    <tr>

                                                        <td>@if($day['day']==1) Sunday
                                                            @elseif($day['day']==2) Monday
                                                            @elseif($day['day']==3) Tuesday
                                                            @elseif($day['day']==4) Wednesday
                                                            @elseif($day['day']==5) Thursday
                                                            @elseif($day['day']==6) Friday
                                                            @elseif($day['day']==7) Saturday
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="progress progress-block ">
                                                                <?php $counterTR = 0; $previousPeriod = 21600; $break = false;?>
                                                                @foreach($day['period'] as $period)
                                                                    <?php
                                                                    //6:00 - 11:30
                                                                    if ($period->start_at - $previousPeriod > 300)
                                                                        $break = true;
                                                                    else
                                                                        $break = false;

                                                                    $counterTR++;
                                                                    $totalTime = 16200;
                                                                    $barwidth = ($period->end_at - $period->start_at) / $totalTime;
                                                                    $breakWidht = ($period->start_at - $previousPeriod) / $totalTime;
                                                                    $previousPeriod = $period->end_at;
                                                                    ?>
                                                                    @if($break)
                                                                        <div class="progress-bar" role="progressbar"
                                                                             style="width:{{$breakWidht*100}}%"> --
                                                                        </div>
                                                                    @endif

                                                                    <div class="progress-bar
                                                    @if($counterTR%4==0) progress-bar-success
                                                    @elseif($counterTR%4==1) progress-bar-warning
                                                    @elseif($counterTR%4==2) progress-bar-success
                                                    @elseif($counterTR%4==3) progress-bar-warning
                                                    @endif"
                                                                         role="progressbar"
                                                                         style="width:{{$barwidth*100}}%">
                                                                        <div style="position: relative;">
                                                                            {{$period->subject}}<br>{{$period->fname}}
                                                                            -{{$period->lname}}
                                                                            <div style="position: absolute; top:0px;left:-15px;">
                                                                                <form action="/routine/{{$period->id}}"
                                                                                      method="POST"
                                                                                      onsubmit="return confirm('Do you to Delete?');">
                                                                                    <input type="hidden" name="_token"
                                                                                           value="{{ csrf_token() }}">
                                                                                    <input type="hidden" name="_method"
                                                                                           value="DELETE">
                                                                                    <div class="form-group col-md-2">
                                                                                        <button type="submit"
                                                                                                class="btn btn-danger btn-sm btn-flat"
                                                                                                style="padding: 3px;"><i
                                                                                                    class="fa fa-times"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                @endforeach
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @endforeach
                                </div>
                                <!-- /.tab-content -->
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>

@endsection

@section('script')
    <script>
        $("#routine").addClass("active");
    </script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
            $(".timepicker").timepicker({
                showInputs: false
            });
        });

    </script>

    <script>
        $(document.body).on("change", "#batch", function () {
            getSubject()
        });
        $(document.body).on("change", "#level", function () {
            getSubject()

            if (document.getElementById($('#level').val() + '-' + $("#faculty option:selected").text() + '-tab'))
                document.getElementById($('#level').val() + '-' + $("#faculty option:selected").text() + '-tab').click();
        });
        $(document.body).on("change", "#faculty", function () {
            getTeacher(this.value)
            getSubject()

            if (document.getElementById($("#faculty option:selected").text() + '-tab'))
                document.getElementById($("#faculty option:selected").text() + '-tab').click();

            if (document.getElementById($('#level').val() + '-' + $("#faculty option:selected").text() + '-tab'))
                document.getElementById($('#level').val() + '-' + $("#faculty option:selected").text() + '-tab').click();

        });

        function getTeacher(data) {
            $.post("/get-teacher",
                {
                    _token: '{{csrf_token()}}',
                    faculty: data
                },
                function (data, status) {
                    optionList = '<option selected="selected">Select-Teacher</option>'
                    for (i in data) {
                        optionList = optionList + '<option value="' + data[i].id + '">' + data[i].first_name + ' ' + data[i].last_name + '</option>'
                    }
                    $("#teacherList").html(optionList);
                    $("#teacherList").trigger('change');
                });
        }
        function getSubject() {
            $.post("get-subject",
                {
                    _token: '{{csrf_token()}}',
                    batch: $('#batch').val(),
                    level: $('#level').val(),
                    faculty: $('#faculty').val()
                },
                function (data, status) {
                    optionList = '<option selected="selected">Select-Subject</option>'
                    for (i in data) {
                        optionList = optionList + '<option value="' + data[i].id + '">' + data[i].name + '</option>'
                    }
                    $("#subjectList").html(optionList);
                    $("#subjectList").trigger('change');
                });
        }
    </script>
@endsection