@extends('layouts.mis')
@section('header')
    <h1>
        Menu
        <small>Add Menu</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">menu</li>
    </ol>
@endsection

@section('content') 
<div class="row"> 
    <div class="col-md-4">
      <div class="box box-success">
          <div class="box-header with-border">
              <h3 class="box-title">Menu</h3>
          </div>
          <div class="box-body">
            <ul class="list-group">
                @foreach($menus as $menu)
                  <li class="list-group-item">
                    <div>
                        <a href="http://hcc.edu.np/{{$menu['menu']->url}}" target="{{$menu['menu']->target}}">{{$menu['menu']->name}} &nbsp&nbsp</a>
                        <span class="badge">{{$menu['menu']->order}}</span>

                        <form class="form-horizontal" role="form" method="POST" 
                          action="{{ url('/menu/'.$menu['menu']->id) }}" style="display: inline; float: right;">                    
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="delete">                    
                            <a href="/menu/{{$menu['menu']->id}}" class='btn btn-box-tool'><i class='fa fa-pencil'></i>
                            </a>
                            <button type="submit" class='btn btn-box-tool'><i class='fa fa-trash-o'></i>
                            </button>                  
                        </form> 
                    </div><br>
                    @if($menu['submenu']!=null)
                        <ul class="list-group">
                          @foreach($menu['submenu'] as $submenu)                          
                            <li class="list-group-item">
                                <div>
                                  <a href="http://hcc.edu.np/{{$submenu->url}}" target="{{$submenu->target}}">{{$submenu->name}} &nbsp&nbsp&nbsp</a>
                                    <span class="badge">{{$submenu->order}}</span>
                                    <form class="form-horizontal" role="form" method="POST" 
                                      action="{{ url('/menu/'.$submenu->id) }}" style="display: inline; float: right;"> 
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="delete">
                                        <a href="/menu/{{$submenu->id}}" class='btn btn-box-tool'><i class='fa fa-pencil'></i>
                                        </a>
                                        <button type="submit" class='btn btn-box-tool'><i class='fa fa-trash-o'></i>
                                        </button>                  
                                    </form>
                                </div><br>
                            </li>                          
                          @endforeach
                      </ul>
                    @endif
                  </li>            
                @endforeach
            </ul>
          </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Add Menu</h3>
            </div>
            <div class="box-body">
                <div class="col-lg-10 col-lg-offset-1">
                    @if(Session::has('message'))
                        <div class="alert alert-info">
                            <strong>Info!</strong>
                            {{Session::get('message')}}
                            {{Session::forget('message')}}
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/menu') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                         <div class="row">
                            <!--Name of book -->
                            <div class="col-md-12">
                                <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <span class="input-group-addon">
                                         <i class="fa fa-pencil"></i>
                                    </span>
                                    <input type="text" class="form-control" 
                                        name="name" value="{{ old('name') }}" 
                                            placeholder="Name">
                                     <span class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </span>
                                </div>
                                @if ($errors->has('name'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError">
                                            <i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <!--Name of author -->                                          
                        </div><br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                    <span class="input-group-addon">
                                        <i class="fa fa-link"></i>
                                    </span>
                                    <input type="text" class="form-control" 
                                        name="url" value="{{ old('url') }}" 
                                        placeholder="Tag value of page">
                                    <span class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </span>
                                </div>
                                @if ($errors->has('url'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError">
                                            <i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('url') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div><br>

                        <div class="row">
                          <div class="col-md-4">
                            <div class="input-group">
                                <select class="form-control" id="parent" name="parent">
                                  <option selected>Select Parent</option>
                                  @foreach($menus as $menu)
                                    <option>{{$menu['menu']->name}}</option>
                                  @endforeach
                                </select>
                            </div>
                          </div>
                          <div class="col-md-4">
                                <div class="input-group{{ $errors->has('order') ? ' has-error' : '' }}">
                                    <span class="input-group-addon">
                                        <i class="fa fa-link"></i>
                                    </span>
                                    <input type="text" class="form-control" 
                                        name="order" value="{{ old('order') }}" 
                                        placeholder="Order">
                                    <span class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </span>
                                </div>
                                @if ($errors->has('order'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError">
                                            <i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('order') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                          <div class="col-md-4">
                            <label class="checkbox-inline"><input type="checkbox" value="_blank" name="target">Open in New Window</label>
                          </div>
                        </div><br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <span class="input-group-addon">
                                        <i class="fa fa-info"></i>
                                    </span>
                                     <textarea class="form-control" rows="5" name="description" placeholder="Write description here.....">{{ old('description') }}</textarea>
                                </div>
                                @if ($errors->has('description'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="pull-right">
                                <div class="input-group"><br>
                                    <button type="submit" id="searchstudent" class="btn btn-primary btn-flat">
                                        <i class="fa fa-plus"></i> Add Menu 
                                        <i id="addStudent" class="fa fa-circle-o-notch fa-spin hidden"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $( "#website" ).addClass( "active" );
    </script>
@endsection
