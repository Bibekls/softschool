@extends('layouts.mis')
@section('header')
    <h1>
        Student Attendance
        <small>View attendance</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">student-attendance</li>
    </ol>
@endsection
@section('style')
    <style>
        .progress-block {
            height: 60px !important;
        }

        .danger {
            color: #ff4d4d;
        }

        .primary {
            color: #004d99;
        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">View student wise attendance</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <form class="form-inline" method="POST" action="{{ url('/student-attendance/student-wise') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group col-md-4">
                            <label>Batch</label>
                            <select class="form-control select2" id="batch" style="width: 100%;" name="batch">
                                <option selected="selected" value="0">Select-Batch</option>
                                @foreach($batches as $b)
                                    <option>{{$b}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Level</label>
                            <select class="form-control select2" id="level" style="width: 100%;" name="level">
                                <option selected="selected" value="0">Select-level</option>
                                <option value="1">First</option>
                                <option value="2">Second</option>
                                <option value="3">Third</option>
                                <option value="4">Fourth</option>
                                <option value="5">Fifth</option>
                                <option value="6">Sixth</option>
                                <option value="7">Seventh</option>
                                <option value="8">Eighth</option>

                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Faculty</label>
                            <select class="form-control select2" id="faculty" style="width: 100%;" name="faculty">
                                <option selected="selected">Select-Faculty</option>
                                @foreach($faculties as $f)
                                    <option value="{{$f->id}}">{{$f->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group col-md-6">
                            <label>Student</label>
                            <select class="form-control select2" style="width: 100%;" name="student" id="studentList">
                                <option selected="selected">Select-Student</option>
                                @foreach($students as $s)
                                    <option value="{{$s->id}}">{{$s->first_name}} {{$s->last_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Date range </label>

                            <div class="input-group" style="width: 100%">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right reservation" name="date-range">
                            </div>
                            <!-- /.input group -->
                        </div>

                        <div class="form-group col-md-2">
                            <br>
                            <button class="btn btn-primary btn-flat btn-block">Done</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php $facultyCounter = 1; ?>
                    @foreach($attendances as $attendance)
                        <li onclick="localStorage.setItem('today-attendance-tab1','{{$attendance['faculty']->name}}')">
                            <a href="#{{$attendance['faculty']->name}}"
                               id="{{$attendance['faculty']->name}}-menu"
                               data-toggle="tab">{{$attendance['faculty']->name}}</a>
                        </li>
                        <?php $facultyCounter++; ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    <?php $facultyCounter = 1; ?>
                    @foreach($attendances as $attendance)
                        <div class="@if($facultyCounter==1) active @endif tab-pane"
                             id="{{$attendance['faculty']->name}}">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <?php $batchCounter = 1; ?>
                                    @foreach($attendance['batch-list'] as $batch)
                                        <li onclick="localStorage.setItem('today-attendance-tab2','{{$batch['batch']}}{{$attendance['faculty']->name}}')">
                                            <a href="#{{$batch['batch']}}{{$attendance['faculty']->name}}"
                                               id="{{$batch['batch']}}{{$attendance['faculty']->name}}-menu"
                                               data-toggle="tab"></sup> Batch
                                                ( {{$batch['batch']}} )</a>
                                        </li>
                                        <?php $batchCounter++; ?>
                                    @endforeach
                                </ul>
                                <div class="tab-content">
                                    <?php $batchCounter = 1; ?>
                                    @foreach($attendance['batch'] as $batch)
                                        <div class="@if($batchCounter==1) active @endif tab-pane"
                                             id="{{$batch['batch']}}{{$attendance['faculty']->name}}">


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <form class="form-inline" method="POST"
                                                          action="{{ url('/student-attendance/day-wise') }}">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="batch" value="{{ $batch['batch'] }}">
                                                        <input type="hidden" name="faculty"
                                                               value="{{ $attendance['faculty']->name }}">

                                                        <div class="form-group col-md-5">
                                                            <label>Date</label>
                                                            <input type="text" class="form-control datepicker"
                                                                   name="date"
                                                                   value="{{ $batch['attendance-date'] }}">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <button class="btn btn-primary btn-flat btn-block">View
                                                                attendance
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-5 pull-right">
                                                    @if(count($batch['students'] )> 0)
                                                        <form class="form-inline" role="form" method="post"
                                                              onsubmit="return confirm('Do you really want to download file?');"
                                                              action="download-student-attendance">
                                                            <input type="hidden" name="_token"
                                                                   value="{{csrf_token()}}">
                                                            <input type="hidden" name="batch_name"
                                                                   value="{{$batch['batch']}}">
                                                            <input type="hidden" name="faculty_id"
                                                                   value="{{$attendance['faculty']->id}}">

                                                            <div class="form-group">
                                                                <label>Select the level </label>
                                                                <select class="form-control" name="level" id="">
                                                                    <option value="1" selected>First</option>
                                                                    <option value="2">Second</option>
                                                                    <option value="3">Third</option>
                                                                    <option value="4">Fourth</option>
                                                                    <option value="5">Fifth</option>
                                                                    <option value="6">Sixth</option>
                                                                    <option value="7">Seventh</option>
                                                                    <option value="8">Eights</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group ">
                                                                and
                                                                <a onclick="$(this).closest('form').submit();"
                                                                   style="cursor: pointer"> click me to download excel
                                                                    file <span>
                                                            <i class="fa fa-file-excel-o"></i></span>
                                                                </a>
                                                            </div>

                                                        </form>
                                                    @endif
                                                </div>
                                            </div>
                                            <br>
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Student Name</th>
                                                    <th>Contact</th>
                                                    @foreach($batch['subject-list'] as $subject)
                                                        <th class="text-center">
                                                            {{$subject['subject_name']}}
                                                            <div style="display: flex; text-align: center">
                                                                <div style="flex: 1">
                                                                    REG
                                                                </div>
                                                                <div style="flex: 1">
                                                                    REP
                                                                </div>
                                                                <div style="flex:1">
                                                                    EXT
                                                                </div>
                                                            </div>
                                                        </th>
                                                    @endforeach
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($batch['students'] as $student)
                                                    <tr>
                                                        <td>
                                                            {{$student['name']}}
                                                        </td>
                                                        <td>
                                                            Student : {{$student['contact']}}<br>
                                                            Parent : {{$student['parent_contact']}}
                                                        </td>

                                                        @foreach($student['subjects'] as $subAttendance)
                                                            <td>
                                                                <div style="display:flex ;text-align: center">
                                                                    @foreach($subAttendance as $attendanceType)

                                                                        {{--<pre>{{print_r($attendanceType)}}</pre>--}}

                                                                        <div style="flex: 1">

                                                                            <div style="display: block">
                                                                                @if($attendanceType[0]->attendance==1)
                                                                                    <i class="fa fa-check-circle fa-2x primary"></i>
                                                                                @elseif($attendanceType[0]->attendance==0)
                                                                                    <i class="fa fa-times-circle fa-2x danger"></i>
                                                                                @else
                                                                                    <i class="fa fa-times-circle fa-2x danger" style="visibility:hidden"></i>
                                                                                @endif
                                                                            </div>
                                                                            <div style="display: block">
                                                                                [ @if(!$attendanceType[0]->total_attendance)
                                                                                    0
                                                                                     @else 
                                                                                         {{$attendanceType[0]->total_attendance}} 
                                                                                     @endif
                                                                                / {{$attendanceType[1]}} ]
                                                                            </div>
                                                                        </div>

                                                                    @endforeach
                                                                </div>
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php $batchCounter++; ?>
                                    @endforeach
                                </div>
                                <!-- /.tab-content -->
                            </div>
                        </div>
                        <?php $facultyCounter++; ?>
                    @endforeach
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>

@endsection

@section('script')
    <script>
        $("#student-attendance").addClass("active");
    </script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
            $(".timepicker").timepicker({
                showInputs: false
            });
            $('.reservation').daterangepicker();
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true
            });
        });

    </script>

    <script>
        $(document.body).on("change", "#batch", function () {
            getStudent()
        });
        $(document.body).on("change", "#level", function () {
            getStudent()
        });
        $(document.body).on("change", "#faculty", function () {
            getStudent()
        });

        function getStudent(data) {
            $.post("/get-student",
                {
                    _token: '{{csrf_token()}}',
                    batch: $('#batch').val(),
                    level: $('#level').val(),
                    faculty: $('#faculty').val()
                },
                function (data, status) {
                    optionList = '<option selected="selected">Select-Student</option>'
                    for (i in data) {
                        optionList = optionList + '<option value="' + data[i].id + '">' + data[i].first_name + ' ' + data[i].last_name + '</option>'
                    }
                    $("#studentList").html(optionList);
                    $("#studentList").trigger('change');
                });
        }

        document.getElementById(localStorage.getItem('today-attendance-tab1').trim() + '-menu').click();
        document.getElementById(localStorage.getItem('today-attendance-tab2').trim() + '-menu').click();

    </script>

@endsection