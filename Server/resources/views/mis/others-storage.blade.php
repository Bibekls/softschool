@extends('layouts.mis')
@section('style')
    <style type="text/css">
    .file-manager>li{
        margin-left:5%;
    }
    .modal-dialog-center {
    padding-top: 10%;
    }

    </style>
@endsection
@section('header')
    <h1>
        Storage
        <small>Others Store</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">Cloud Store</li>
    </ol>
@endsection
@section('content') 

<div class="row">        
        <div class="col-xs-12">          
          <div class="box box-success">
          <?php if($view){ ?>
          <div> 
            <object type="text/html" data="http://student.hcc.edu.np/{{$directory}}" width="100%" height="720px">
            </object>
          </div>
          <?php } else { ?>

            <div class="box-header">
            <div>
                <form class="form-horizontal" action="others-storage" method="post"> 
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="_method" value="post">   
                  <input type="hidden" name="user_id" value="{{$user_id}}">              
                  <div class="form-group form-group-sm">
                    <div class="input-group col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                          <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                          <input class="form-control" type="text" id="directory" name="directory" value="{{$directory}}">
                          <span class="input-group-addon"><a href="#" onClick="this.parentElement.parentElement.parentElement.parentElement.parentElement.childNodes[1].submit();">Go</a></span>
                    </div>
                  </div>
                </form>                
            </div>
            <hr>
            <div>
                <ul class="file-manager list-inline">
                    <li>
                        <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/others-up-one-level') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="directory" value="{{$directory}}">
                                <input type="hidden" name="user_id" value="{{$user_id}}">
                        </form>
                        <a href="#" onClick="this.parentElement.childNodes[1].submit();">
                          <div>
                            <b><i class="fa fa-level-up"></i> Up-One-Level</b>                        
                          </div>
                        </a>
                    </li>                    
                </ul>
            </div>            
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="myTable" class="table table-hover">
                    <thead>                        
                          <th class="text-center">Icon</th>
                          <th>Name</th>
                          <th>Format</th>
                          <th>Size</th>
                          <th>Last Modified</th>                                          
                    </thead>
                    <tbody>
                        @foreach($folders as $folder)
                        <tr>
                          <td class="text-center"><i class="fa fa-2x fa-folder-o"></i></td>
                          <td>
                            <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/others-storage') }}">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="directory" value="{{$directory}}">
                                <input type="hidden" name="folderName" value="{{$folder->folderName}}"> 
                                <input type="hidden" name="user_id" value="{{$user_id}}">           
                            </form>
                            <a href="#" onClick="this.parentElement.childNodes[1].submit();"><div>{{$folder->folderName}}</div></a>
                          </td>
                          <td>Folder</td>
                          <td>---</td>
                          <td>---</td>
                        </tr>  
                        @endforeach

                        @foreach($files as $file)
                            <tr>
                              <td class="text-center"><i class="fa fa-2x 
                                @if($file->extension=='txt')fa-file-text-o

                                @elseif($file->extension=='doc')fa-file-word-o
                                @elseif($file->extension=='dot')fa-file-word-o
                                @elseif($file->extension=='docx')fa-file-word-o

                                @elseif($file->extension=='xls')fa-file-excel-o
                                @elseif($file->extension=='xlsx')fa-file-excel-o
                                @elseif($file->extension=='xlm')fa-file-excel-o 

                                @elseif($file->extension=='ppt')fa-file-powerpoint-o                         
                                @elseif($file->extension=='pot')fa-file-powerpoint-o
                                @elseif($file->extension=='pps')fa-file-powerpoint-o

                                @elseif($file->extension=='pdf')fa-file-pdf-o
                                
                                @elseif($file->extension=='jpg')fa-file-image-o
                                @elseif($file->extension=='jpeg')fa-file-image-o
                                @elseif($file->extension=='tif')fa-file-image-o
                                @elseif($file->extension=='gif')fa-file-image-o
                                @elseif($file->extension=='png')fa-file-image-o                                
                                @elseif($file->extension=='bmp')fa-file-image-o

                                @elseif($file->extension=='mp4')fa-file-video-o                            
                                @elseif($file->extension=='html')fa-html5
                                @elseif($file->extension=='htm')fa-html5
                                @elseif($file->extension=='css')fa-css3
                                @elseif($file->extension=='js')fa-file-code-o                            
                                @elseif($file->extension=='zip')fa-file-archive-o 

                                @else fa-exclamation-triangle
                                @endif
                              "></i></td>
                              <td>
                                <form class="form-horizontal" role="form" method="POST" 
                                    action="{{ url('/others-storage') }}">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_method" value="post">
                                    <input type="hidden" name="directory" value="{{$directory}}">
                                    <input type="hidden" name="fileName" value="{{$file->fileName}}">
                                    <input type="hidden" name="user_id" value="{{$user_id}}">
                                </form>
                                <a href="#" onClick="this.parentElement.childNodes[1].submit();"><div>{{$file->fileName}}</div></a>
                              </td>  
                              <td>{{$file->extension}}</td>                
                              <td>{{(int)((int)$file->size/1024)}} KB</td>
                              <td>
                                  {{Carbon::createFromTimeStamp($file->lastModifiedDate)->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp($file->lastModifiedDate)))->diffForHumans() }}
                              </td>
                            </tr>  
                           
                        @endforeach
                    </tbody>
              </table>
            </div>
            <?php } ?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
  </div>  

<!-- Trigger the modal with a button -->

<ul id="contextMenu" class="box1 dropdown-menu" role="menu" style="display:none" >
    
    <li>
        <form class="form-horizontal" role="form" method="POST"
            action="{{ url('/others-view') }}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="post">
            <input type="hidden" id="view" name="fileAndFolder"  value="">  
            <input type="hidden" name="directory" value="{{$directory}}"> 
            <input type="hidden" name="user_id" value="{{$user_id}}">         
        </form>
        <a tabindex="-1" href="#" class="faa-parent animated-hover" onClick="this.parentElement.childNodes[1].submit();">
                <i class="fa fa-eye faa-horizontal faa-fast"></i>
                View                
        </a>        
    </li>
     <li>
        <form class="form-horizontal" role="form" method="POST" target="_blank"
            action="{{ url('/others-download') }}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="post">
            <input type="hidden" id="download" name="fileAndFolder"  value="">  
            <input type="hidden" name="directory" value="{{$directory}}"> 
            <input type="hidden" name="user_id" value="{{$user_id}}">         
        </form>
        <a tabindex="-1" href="#" class="faa-parent animated-hover" onClick="this.parentElement.childNodes[1].submit();">
                <i class="fa fa-download faa-horizontal faa-fast"></i>
                Download
        </a>        
    </li>    
</ul>
@endsection

@section('script')
    <script>
        $( "#storage" ).addClass( "active" );
    </script>
    <script type="text/javascript">
        (function ($, window) {

        $.fn.contextMenu = function (settings) {

            return this.each(function () {

                // Open context menu
                $(this).on("contextmenu", function (e) {
                    // return native menu if pressing control
                    if (e.ctrlKey) return;                    
                    //open menu
                    /*seeding the menu data*/
                   
                    $("#view").val($(e.target).text());
                    $("#download").val($(e.target).text());
                    

                    

                    var $menu = $(settings.menuSelector)
                        .data("invokedOn", $(e.target))
                        .show()
                        .css({
                            position: "absolute",
                            left: getMenuPosition(e.clientX, 'width', 'scrollLeft'),
                            top: getMenuPosition(e.clientY, 'height', 'scrollTop')
                        })
                        .off('click')
                        .on('click', 'a', function (e) {
                            $menu.hide();                    
                            var $invokedOn = $menu.data("invokedOn");                            
                            var $selectedMenu = $(e.target);
                            
                            settings.menuSelected.call(this, $invokedOn, $selectedMenu);
                        });
                    
                    return false;
                });

                //make sure menu closes on any click
                $('body').click(function () {
                    $(settings.menuSelector).hide();
                });
            });
            
            function getMenuPosition(mouse, direction, scrollDir) {
                var win = $(window)[direction](),
                    scroll = $(window)[scrollDir](),
                    menu = $(settings.menuSelector)[direction](),
                    position = mouse + scroll;
                            
                // opening menu would pass the side of the page
                if (mouse + menu > win && menu < mouse) 
                    position -= menu;
                
                return position;
            }    

        };
        })(jQuery, window);

        $("#myTable td").contextMenu({            
            menuSelector: "#contextMenu",

           /* menuSelected: function (invokedOn, selectedMenu) {
               // var msg = "You selected the menu item '" + selectedMenu.text() +
                 //   "' on the value '" + invokedOn.text() + "'";
               // alert(msg);
            }*/
        });
    </script>
@endsection
