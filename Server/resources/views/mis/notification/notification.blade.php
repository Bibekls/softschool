@extends('layouts.mis')
@section('header')
    <h1>
        Notification Manager
        <small>Push notification</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">notification-manager</li>
    </ol>
@endsection
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Notification</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    @if( count( $errors ) > 0 )
                        <div class="callout callout-danger">
                            <h4>Transaction Error !!!</h4>
                            <ol>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ol>
                        </div>
                    @endif
                    <form class="form-inline" method="POST" action="{{ url('/notification') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-md-4">

                            <div class="input-group{{ $errors->has('title') ? ' has-error' : '' }} col-md-12">
                                <label>Title</label>

                                <input type="text" class="form-control input-sm" name="title"
                                       value="{{ old('title') }}" placeholder="Title">
                            </div>
                            <br>
                            @if ($errors->has('title'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                            <br>

                            <div class="input-group{{ $errors->has('description') ? ' has-error' : '' }} col-md-12">
                                <label>Description</label>

                                <textarea type="text" class="form-control input-sm" name="description" rows="10"
                                          placeholder="Description">{{ old('description') }}</textarea>
                            </div>
                            <br>
                            @if ($errors->has('description'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                            <br>

                            <div class="input-group col-md-12">
                                <label>
                                    <input type="checkbox" class="flat-red"
                                           name="schedule" id="scheduleNotification">
                                    Schedule Notification
                                </label>
                                <hr>
                            </div>

                            <div class="row" id="scheduleRow" style="display: none">
                                <div class="col-md-7">
                                    <div class="input-group{{ $errors->has('date') ? ' has-error' : '' }} col-md-12">
                                        <label>Schedule Date</label>

                                        <input type="text" class="form-control input-sm datePicker" name="date"
                                               value="{{ old('date') }}" placeholder="Date">
                                    </div>
                                    <br>
                                    @if ($errors->has('date'))
                                        <div class="input-group has-error">
                                            <label class="control-label" for="inputError"><i
                                                        class="fa fa-times-circle-o"></i>
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </label>
                                        </div><br>
                                    @endif

                                </div>
                                <div class="col-md-5">
                                    <div class="input-group bootstrap-timepicker {{ $errors->has('time') ? ' has-error' : '' }} col-md-12">
                                        <label>Schedule Time</label>

                                        <input type="text" class="form-control input-sm timepicker" name="time"
                                               value="{{ old('time') }}" placeholder="Time">
                                    </div>
                                    <br>
                                    @if ($errors->has('time'))
                                        <div class="input-group has-error">
                                            <label class="control-label" for="inputError"><i
                                                        class="fa fa-times-circle-o"></i>
                                                <strong>{{ $errors->first('time') }}</strong>
                                            </label>
                                        </div><br>
                                    @endif
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-12">
                                    <br>
                                    <button class="btn btn-primary btn-flat btn-block">Push Notification</button>
                                </div>
                            </div>

                        </div>

                        <div class="form-group col-md-8">

                            <div class="col-md-12">
                                <h3>Select the target member</h3>
                                <br>
                            </div>

                            <div class="col-md-12">
                                <label>
                                    <input type="checkbox" class="flat-red" value="all" name="target[]">
                                    Push notification to all member of Hetauda city college
                                </label>
                                <hr>
                            </div>

                            <div class="col-md-12">
                                <label>
                                    <input type="checkbox" class="flat-red" value="teacher" name="target[]">
                                    Push notification to all teacher Hetauda city college
                                </label>
                                <div class="col-md-12 col-md-offset-1">

                                    @foreach($faculties as $singleFaculty)
                                        <div class="col-md-3">
                                            <label>
                                                <input type="checkbox" class="flat-red"
                                                       value="teacher#{{$singleFaculty->name}}"
                                                       name="target[]">
                                                {{$singleFaculty->name}}
                                            </label>
                                        </div>
                                    @endforeach

                                </div>

                            </div>

                            <div class="col-md-12">
                                <hr>
                                <label>
                                    <input type="checkbox" class="flat-red" value="student" name="target[]">
                                    Push notification to all student of Hetauda city college
                                    <div class="col-md-12 col-md-offset-1">
                                        <?php $counter = 1; ?>
                                        @foreach($faculties as $singleFaculty)
                                            <div class="col-md-3">
                                                <label>
                                                    <input type="checkbox" class="flat-red"
                                                           value="student#{{$singleFaculty->name}}"
                                                           name="target[]">
                                                    {{$singleFaculty->name}}
                                                </label>
                                                <ul style="list-style: none">
                                                    @foreach($singleFaculty->batch as $batch)
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" class="flat-red"
                                                                       value="student#{{$singleFaculty->name}}#{{$batch->batch}}"
                                                                       name="target[]">
                                                                {{$batch->batch}}
                                                            </label>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @if($counter%4==0)
                                                <div class="clearfix"></div> @endif
                                            <?php $counter++; ?>
                                        @endforeach

                                    </div>
                                </label>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Notification Table</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="notificationTable" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Schedule Date</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Sender</th>
                            <th>Receiver</th>
                            <th>Controls</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($notifications as $notification)
                            <tr>
                                <td>{{ date_format(date_create($notification->schedule_date.' '.$notification->time),'m/d/Y h:i:s a') }}</td>
                                <td>
                                    <b>{{$notification->title}}</b> <br>
                                    <p>{{$notification->description}}</p>
                                </td>
                                <td>@if($notification->send==true) Success @else Pending  @endif</td>

                                <td>@if($notification->user_type_id==1)
                                        Admin @else {{$notification->first_name}} {{$notification->last_name}}  @endif</td>
                                <td>
                                    <?php
                                    $targetArray = unserialize($notification->target_user);
                                    if ($notification->sender_id == "1") {

                                        $sendToAll = true;

                                        if (array_key_exists('student-data', $targetArray)) {
                                            $sendToAll = false;
                                            if (array_key_exists('student', $targetArray)) {
                                                echo " All student ";
                                            } else {
                                                foreach ($faculties as $faculty) {
                                                    if (array_key_exists($faculty->name . '-data', $targetArray['student-data'])) {

                                                        if (array_key_exists($faculty->name, $targetArray['student-data'])) {
                                                            echo " All Student of " . $faculty->name;
                                                        } else {
                                                            foreach ($faculty->batch as $batch) {
                                                                if (array_key_exists($batch->batch . '-data', $targetArray['student-data'][$faculty->name . '-data'])) {
                                                                    echo " All Student of " . $faculty->name . ', Batch ' . $batch->batch;
                                                                }
                                                            }

                                                        }

                                                    }
                                                }

                                            }
                                        }

                                        if (array_key_exists('teacher-data', $targetArray)) {
                                            $sendToAll = false;
                                            if (array_key_exists('teacher', $targetArray)) {
                                                $this->sendToAllTeacher($notification->id);
                                                echo "All teacher";
                                            } else {
                                                foreach ($faculties as $faculty) {
                                                    if (array_key_exists($faculty->name, $targetArray['teacher-data'])) {
                                                        echo " All teacher of faculty " . $faculty->name;
                                                    }
                                                }
                                            }
                                        }

                                        if($sendToAll)
                                            echo "All member of Hetauda city college";

                                    } else {
                                        echo " All student of faculty " . $targetArray->faculty. ' , Batch  '. $targetArray->batch;
                                    }

                                    ?>
                                </td>

                                <td>
                                    <form class="form-horizontal" role="form"
                                          method="POST"
                                          action="{{ url('/notification/'.$notification->id)}}"
                                          onsubmit="return confirm('Do you to Delete?');">
                                        <input type="hidden" name="_token"
                                               value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method"
                                               value="delete">
                                        <button type="submit"
                                                class='btn btn-danger'><i
                                                    class='fa fa-trash-o'></i>
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Schedule Date</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Sender</th>
                            <th>Receiver</th>
                            <th>Controls</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script>
        $("#notification").addClass("active");
    </script>

    <script>
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
            $(".timepicker").timepicker({
                showInputs: false
            });
        });

        $('.datePicker').datepicker({
            autoclose: true
        });

    </script>

    <script>
        $(document).ready(function () {
            $('#notificationTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aaSorting": [[0, 'des']]
            });
        });
    </script>

    <script>
        $(document.body).on("ifChecked", "#scheduleNotification", function () {
            $("#scheduleRow").show();
        });
        $(document.body).on("ifUnchecked", "#scheduleNotification", function () {
            $("#scheduleRow").hide();
        });
    </script>

@endsection