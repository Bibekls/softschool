 @extends('layouts.mis')
 @section('header')

 <section class="content-header">
     <h1>
        User Request
        <small>user request list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">{{$title}}</li>
    </ol>
</section>
@endsection

@section('content')

<section class="content"> 
  <div class="row">    
    <div class="box box-success">
          <div class="box-header with-border">
              <h3 class="box-title">Un-verified Email [ Step -1 ]</h3>
          </div>
        <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                  <div class="box-body">
                    <table  class="table table-bordered table-striped table-hover">
                      <thead>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th>USER TYPE</th>
                        <th>SUBMITTED DATE</th>
                        <th>ACTION</th>
                      </thead>
                      <tbody>                        
                          @foreach($userrequests as $userrequest)
                          <tr>
                            <td>{{$userrequest->id}}</td>
                            <td>{{$userrequest->name}}</td>
                            <td>{{$userrequest->email}}</td>
                            <td>{{$userrequest->userType->name}}</td>
                            <td>
                            {{Carbon::createFromTimeStamp(strtotime($userrequest->created_at))->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime($userrequest->created_at))))->diffForHumans() }}
                            </td>
                            <td>
                              <form role="form" method="POST" action="/delete-unverified-email">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" name="_method" value="delete">
                                  <input type="hidden" name="id" value="{{$userrequest->id}}">
                                  <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i></button>
                              </form>
                            </td>
                          </tr>
                          @endforeach                        
                      </tbody>                                  
                    </table>
                  </div>
              </div>
            </div>
        </div>
    </div>    
  </div>

    <div class="row">    
    <div class="box box-success">
          <div class="box-header with-border">
              <h3 class="box-title">Verified Email [ Step -2 ]</h3>
          </div>
        <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                  <div class="box-body">
                    <table  class="table table-bordered table-striped table-hover">
                      <thead>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th>USER TYPE</th>
                        <th>SUBMITTED DATE</th>
                      </thead>
                      <tbody>                        
                          @foreach($emailVerifiedUsers as $emailVerifiedUser)
                          <tr>
                            <td>{{$emailVerifiedUser->id}}</td>
                            <td>{{$emailVerifiedUser->name}}</td>
                            <td>{{$emailVerifiedUser->email}}</td>
                            <td>{{$emailVerifiedUser->userType->name}}</td>
                            <td>
                            {{Carbon::createFromTimeStamp(strtotime($emailVerifiedUser->created_at))->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime($emailVerifiedUser->created_at))))->diffForHumans() }}
                            </td>
                          </tr>
                          @endforeach                        
                      </tbody>                                  
                    </table>
                  </div>
              </div>
            </div>
        </div>
    </div>    
  </div>   
</section>

@endsection

@section('script')
    <script>
        $( "#pendingUser" ).addClass( "active" );
    </script>
@endsection
