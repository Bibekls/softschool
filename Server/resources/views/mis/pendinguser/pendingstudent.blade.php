 @extends('layouts.mis')
 @section('header')

 <section class="content-header">
     <h1>
        Pending Student Form
        <small>Pending Student Form</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Pending-StudentForm</li>
    </ol>
</section>
@endsection

@section('content')

<section class="content"> 
  <div class="row">
    @foreach($pendingStudentForms as $pendingStudentForm)
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Student Registration form</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                  <h3 class="text-center">Profile Picture</h3>
                    <img class="img-responsive img img-rounded" src="{{$pendingStudentForm->image}}">
                </div>  
                <div class="col-md-6 pull-right">
                    <h2 class="text-center">{{$pendingStudentForm->first_name}} {{$pendingStudentForm->middle_name}} {{$pendingStudentForm->last_name}}</h2>
                  <div class="box-body">
                    <table  class="table table-bordered table-striped table-hover">
                      
                      <tr>
                        <td>ID</td>
                        <td>{{$pendingStudentForm->id}}</td>
                      </tr> 
                      <tr>
                        <td>First Name</td>
                        <td>{{$pendingStudentForm->first_name}}</td>
                      </tr>  
                      <tr>
                        <td>Middle Name</td>
                        <td>{{$pendingStudentForm->middle_name}}</td>
                      </tr>
                      <tr>
                        <td>Last Name</td>
                        <td>{{$pendingStudentForm->last_name}}</td>
                      </tr> 
                       <tr>
                        <td>Date of Birth</td>
                        <td>{{$pendingStudentForm->date_of_birth}}</td>
                      </tr>
                      <tr>
                        <td>Parent Name</td>                          
                        <td>{{$pendingStudentForm->parent_name}}</td>
                      </tr>                        
                    </table>
                  </div><!-- /.box-body -->
                    
                </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="box-body">
                    <table  class="table table-bordered table-striped table-hover">                       
                      
                      <tr>
                        <td>Faculty</td>
                        <td>{{$pendingStudentForm->faculty}}</td>
                      </tr> 
                      <tr>
                        <td>Gender</td>
                        @if($pendingStudentForm->gender==1)<td>Male</td> @else <td>Female</td> @endif
                      </tr>
                      <tr>
                        <td>Batch</td>
                        <td>{{$pendingStudentForm->batch}}</td>
                      </tr>
                      <tr>
                        <td>Email Address</td>
                        <td>{{$pendingStudentForm->email}}</td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td>{{$pendingStudentForm->phone_number}}</td>
                      </tr>
                      <tr>
                        <td>Mobile Number</td>
                        <td>{{$pendingStudentForm->mobile_number}}</td>
                      </tr>                        
                  </table>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="box-body">
                    <table  class="table table-bordered table-striped table-hover">
                      <tr>
                        <td>Permanent Address</td>
                        <td>{{$pendingStudentForm->permanent_address}}</td>
                      </tr>
                      <tr>
                        <td>Current Address</td>
                        <td>{{$pendingStudentForm->current_address}}</td>
                      </tr>
                      <tr>
                        <td>Religion</td>
                        <td>{{$pendingStudentForm->religion}}</td>
                      </tr>
                      <tr>
                        <td>Symbol Number</td>
                        <td>{{$pendingStudentForm->symbol_number}}</td>
                      </tr>
                      <tr>
                        <td>Registration Number</td>
                        <td>{{$pendingStudentForm->registration_number}}</td>
                      </tr>
                      <tr>
                        <td>Form Submitted Date</td>
                        <td>{{$pendingStudentForm->created_at}}</td>
                      </tr> 
                  </table>
                </div>
              </div>
            </div>
            <div class="col-md-12"><br>
              <div class="col-md-6">
                <form class="form-horizontal" role="form" method="POST" action="{{url('accept-pending-student-form')}}" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="put"> 
                    <input type="hidden" name="id" value="{{$pendingStudentForm->id}}">
                    <button type="submit" class="btn btn-info">Accept</button>
                </form>
              </div>
              <div class="col-md-6 text-right">
                <form class="form-horizontal" role="form" method="POST" action="{{url('delete-pending-student-form')}}" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="delete"> 
                        <input type="hidden" name="id" value="{{$pendingStudentForm->id}}">
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>  
              </div> 
            </div>
        </div>
    </div>
    @endforeach
  </div>  
</section>  


@endsection

@section('script')
    <script>
        $( "#pendingUser" ).addClass( "active" );
    </script>
@endsection
