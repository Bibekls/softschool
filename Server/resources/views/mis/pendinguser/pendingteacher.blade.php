@extends('layouts.mis')
@section('header')
 <section class="content-header">
     <h1>
        Pending Teacher Form
        <small>Pending Teacher Form</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Pending-TeacherForm</li>
    </ol>
</section>

@endsection

@section('content')

<section class="content" ng-controller="pendingTeacherFormController"> 
  <div class="row">
    @foreach($pendingTeacherForms as $pendingTeacherForm)
    <div class="box box-info">
          <div class="box-header with-border">
              <h3 class="box-title">Teacher Registration form</h3>
          </div>
          <div class="box-body">
              <div class="row">
                  <div class="col-md-4">
                    <h3 class="text-center">Profile Picture</h3>
                      <img class="img-responsive img img-rounded" src="{{$pendingTeacherForm->image}}">
                  </div>  
                  <div class="col-md-6 pull-right">
                      <h2 class="text-center">{{$pendingTeacherForm->first_name}} {{$pendingTeacherForm->middle_name}} {{$pendingTeacherForm->last_name}}</h2>
                    <div class="box-body">
                      <table  class="table table-bordered table-striped table-hover">
                        
                        <tr>
                          <td>ID</td>
                          <td>{{$pendingTeacherForm->id}}</td>
                        </tr> 
                        <tr>
                          <td>First Name</td>
                          <td>{{$pendingTeacherForm->first_name}}</td>
                        </tr>  
                        <tr>
                          <td>Middle Name</td>
                          <td>{{$pendingTeacherForm->middle_name}}</td>
                        </tr>
                        <tr>
                          <td>Last Name</td>
                          <td>{{$pendingTeacherForm->last_name}}</td>
                        </tr> 
                         <tr>
                          <td>Date of Birth</td>
                          <td>{{$pendingTeacherForm->date_of_birth}}</td>
                        </tr>
                        <tr>
                          <td>Parent Name</td>
                          <td>{{$pendingTeacherForm->parent_name}}</td>
                        </tr>                        
                      </table>
                    </div><!-- /.box-body -->
                      
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="box-body">
                      <table  class="table table-bordered table-striped table-hover">
                        <tr>
                          <td>Gender</td>
                          @if($pendingTeacherForm->gender==1) <td>Male</td> @else <td>Female</td> @endif
                        </tr>                        
                        <tr>
                          <td>Email Address</td>
                          <td>{{$pendingTeacherForm->email}}</td>
                        </tr>
                        <tr>
                          <td>Phone Number</td>
                          <td>{{$pendingTeacherForm->phone_number}}</td>
                        </tr>
                        <tr>
                          <td>Mobile Number</td>
                          <td>{{$pendingTeacherForm->mobile_number}}</td>
                        </tr>                        
                    </table>
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="box-body">
                      <table  class="table table-bordered table-striped table-hover">
                        <tr>
                          <td>Permanent Address</td>
                          <td>{{$pendingTeacherForm->permanent_address}}</td>
                        </tr>
                        <tr>
                          <td>Current Address</td>
                          <td>{{$pendingTeacherForm->current_address}}</td>
                        </tr>
                        <tr>
                          <td>Religion</td>
                          <td>{{$pendingTeacherForm->religion}}</td>
                        </tr>                       
                        <tr>
                          <td>Form Submitted Date</td>
                          <td>{{$pendingTeacherForm->created_at}}</td>
                        </tr> 
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-md-12"><br>
                <div class="col-md-6">
                  <form class="form-horizontal" role="form" method="POST" action="{{url('accept-pending-teacher-form')}}" >
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="_method" value="put"> 
                      <input type="hidden" name="id" value="{{$pendingTeacherForm->id}}">
                      <button type="submit" class="btn btn-info">Accept</button>
                  </form>
                </div>
                <div class="col-md-6 text-right">
                  <form class="form-horizontal" role="form" method="POST" action="{{url('delete-pending-teacher-form')}}" >
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="_method" value="delete"> 
                          <input type="hidden" name="id" value="{{$pendingTeacherForm->id}}">
                      <button type="submit" class="btn btn-danger">Delete</button>
                  </form>  
                </div> 
              </div>
          </div>
    </div>
    @endforeach
  </div>  
</section>  

@endsection

@section('script')
    <script>
        $( "#pendingUser" ).addClass( "active" );
    </script>
@endsection

