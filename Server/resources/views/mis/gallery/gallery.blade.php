@extends('layouts.mis')
@section('header')
    <h1>
        Gallery
        <small>View Gallery</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">Gallery</li>
    </ol>
@endsection

@section('content')
<div class="row"> 
        <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">                    
                    <h3 class="box-title">Gallery Setting</h3>     
            </div>
            <div class="box-body">
                <div class="col-md-6" style="padding: 1%;padding-bottom:0;">
                    <form class="form-inline" role="form" method="POST" action="{{ url('/gallery') }}">
                          <div class="form-group">
                            <label for="addcategory">Add Category</label>
                            <input type="text" name="name" placeholder="Add Category" class="form-control" id="addcategory">
                          </div>                    
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">    
                        <button type="submit" class='btn btn-success btn-flat' style="color: white;">
                                &nbsp&nbsp&nbsp<i class='fa fa-plus'></i> &nbsp&nbsp&nbsp
                        </button>                     
                    </form>
                </div>               
                
                <div class="col-md-6" style="padding: 1%;padding-bottom:0;">
                    <form class="form-inline" role="form" method="POST" action="{{ url('/gallery/0') }}" onsubmit="return confirm('Do you to Delete?');">
                        <div class="form-group">
                            <label for="deletecategory">Delete Category</label>
                            
                            <select class="form-control" id="deletecategory" name="category">                                  
                              @foreach($galleries as $gallery)
                                <option 
                                    @if($gallery->id==1)
                                        selected
                                    @endif
                                >{{$gallery->title}}</option>
                              @endforeach
                            </select>                            
                        </div> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="delete">

                        <button type="submit" class='btn btn-danger btn-flat' style="color: white;">
                                &nbsp&nbsp&nbsp<i class='fa fa-trash-o'></i> &nbsp&nbsp&nbsp
                        </button> 
                    </form>
                </div>
            </div>
        </div>            
    </div>
</div>

<div class="row">
    @foreach($galleries as $gallery)
        <div class="col-md-2 col-sm-3 col-xs-6">
            <a href="/gallery/{{$gallery->id}}">
                <img src="img/thumbnail.jpg" class="img-thumbnail">
                <h4 class="text-center">{{$gallery->title}}</h4>
            </a>
        </div>
    @endforeach
</div>
@endsection

@section('script')
    <script>
        $( "#website" ).addClass( "active" );
    </script>   
@endsection

