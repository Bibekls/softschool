@extends('layouts.mis')
@section('header')
    <h1>
        Gallery
        <small>Image</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">Gallery</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <form action="/image" class="dropzone " id="addImages">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="gallery" value="{{$gallery->id}}">
        </form>
    </div>
    <div class="col-md-6">        
        <a href="/gallery/{{$gallery->id}}">
            <img src="
            @if($gallery->image==null)
                /img/thumbnail.jpg
            @else
                {{url($gallery->image)}}
            @endif
            " class="img img-responsive img-rounded center-block">
            <h4 class="text-center">{{$gallery->title}}</h4>
        </a>               
    </div>
</div>
<div class="row">
    @foreach($images as $image)
        <div class="col-md-3 col-sm-4 col-xs-12">
            <a href="{{url($image->file_path)}}" data-lightbox="profile_pic">           
                <img src="/images/thumbs/{{$image->file_name}}" class="img-thumbnail">
            </a>            
        </div>
    @endforeach
</div>
@endsection

@section('script')
    <script>
        $( "#website" ).addClass( "active" );
    </script>   
@endsection

