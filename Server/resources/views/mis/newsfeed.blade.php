@extends('layouts.mis')
@section('header')
    <h1>
        News Feed
        <small>view posts from all users</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">newsfeed</li>
    </ol>
@endsection
@section('content') 
<div>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Post to newsfeed</h3>
        </div>
        <div class="box-body">
            <div class="col-lg-10 col-lg-offset-1">                                       
                <form class="form-horizontal" role="form" method="POST" 
                    action="{{ url('/post') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group{{ $errors->has('post') ? ' has-error' : '' }}">
                                <span class="input-group-addon">
                                    <i class="fa fa-newspaper-o"></i>
                                </span>
                                <textarea class="form-control" rows="5" name="post" placeholder="Write your status here.....">{{ old('post') }}</textarea>
                            </div>

                            @if ($errors->has('post'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('post') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="pull-right">
                            <div class="input-group"><br>
                                <button type="submit" id="searchstudent" class="btn btn-primary btn-flat">
                                    <i class="fa fa-plus"></i> Post
                                    <i id="addStudent" class="fa fa-circle-o-notch fa-spin hidden"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>

    @foreach($newsfeed as $post)
        <?php if($counter%3==0){?>
            <div class="row">
        <?php   } ?>

        <div class="col-md-4">
          <!-- Box Comment -->
          <div class="box box1 box-widget">
            <div class='box-header with-border'>
              <div class='user-block'>
                <img class='img-circle' src="/images/thumbnails/{{$post['profile_pic']->profile_pic}}" alt='user image'>
                <span class='username'><a href="#">{{$post['post']->user->name}}</a></span>
                <span class='description'>{{$post['postTime']}}</span>
              </div><!-- /.user-block -->
              <div class='box-tools'> 
                
                  <form class="form-horizontal" role="form" method="POST" 
                  action="{{ url('/post') }}">                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="post_id" value="{{$post['post']->id}}">
                    <button type="submit" class='btn btn-box-tool'><i class='fa fa-2x fa-trash'></i>
                    </button>
                  
                </form>                                                  
              </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class='box-body'>
              <p class="text-justify"><a>{{$post['post']->post}}</a></p>                 
              <span class='pull-right text-muted'>{{ $post['comments']->count()}} comments</span>
            </div><!-- /.box-body -->
            <div class='box-footer box-comments'>
              <div class="box box-success collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Read Comments</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">

            @foreach( $post['comments'] as $comment)
              <div class='box-comment'>
                <!-- User image -->
                <img class='img-circle img-sm' src="/images/thumbnails/{{$comment->profile_pic}}" alt='user image'>
                <div class='comment-text'>
                  <span class="username">
                   {{$comment->username}}

                      
                        <div class="pull-right">
                           <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/comment') }}">
                                
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="comment_id" value="{{$comment->id}}">
                                <button type="submit" class='btn btn-box-tool'><i class='fa fa-trash'></i>
                                </button>
                            </form>
                        </div>
                      
                    <br>
                    <span class='text-muted'>
                        {{Carbon::createFromTimeStamp(strtotime($comment->time))->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime($comment->time))))->diffForHumans() }}
                    </span>
                  </span><!-- /.username -->
                  <br>
                      <p class="text-justify">{{$comment->comment}}</p>
                </div><!-- /.comment-text -->
              </div><!-- /.box-comment -->
              @endforeach 
              </div>
              </div>
                              
            </div><!-- /.box-footer -->
            <div class="box-footer">
              <form action="{{url('/comment')}}" method="POST">
                  <input type="hidden" name="post_id" value="{{$post['post']->id}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <img class="img-responsive img-circle img-sm" src="images/thumbnails/{{$profile_pic->file_name}}" alt="alt text">
                <!-- .img-push is used to add margin to elements next to floating images -->
                <div class="img-push">
                  <input type="text" class="form-control input-sm" placeholder="Press enter to post comment. 140 character only" name="comment">
                </div>
              </form>
            </div><!-- /.box-footer -->
          </div><!-- /.box -->
        </div><!-- /.col -->

        <?php $counter++; if( $counter%3==0) { ?>
          </div>
        <?php   } ?>
     @endforeach

     <?php if( $counter%3!=0) { ?>
          </div>
      <?php   } ?>

    <div class="row">
        <div class="container">    
            <div class="pagination"> {{ $pagelink->links() }} </div>  
        </div>
    </div>


@endsection

@section('script')
    <script>
        $( "#newsfeed" ).addClass( "active" );
    </script>
@endsection