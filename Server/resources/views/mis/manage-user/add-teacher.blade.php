@extends('layouts.mis')


@section('header')
    <h1>
        Teacher Registration
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">teacher-registration</li>
    </ol>
@endsection
@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Teacher Registration Form</h3>
        </div>
        <div class="box-body">
            <div class="col-lg-10 col-lg-offset-1">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/add-teacher') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="img img-responsive img-thumbnail center-block" src="/img/male.jpg"
                                 alt="User profile picture">
                        </div>
                        <!--Fname -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="username" value="{{ old('username') }}"
                                       placeholder="Username">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('username'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </label>
                                </div>
                            @endif<br>

                            <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                                       placeholder="Email">
                            </div>
                            @if ($errors->has('email'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </label>
                                </div>
                            @endif<br>

                            <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" class="form-control" name="password" placeholder="password">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('password'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </label>
                                </div>
                            @endif<br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" class="form-control" name="password_confirmation"
                                       placeholder="confirm-password">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            <br>

                        </div>

                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="first_name"
                                       value="{{ old('first_name') }}" placeholder="First-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('first_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                        <div class="col-md-4">

                            <div class="input-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="middle_name"
                                       value="{{ old('middle_name') }}" placeholder="Middle-Name">
                            </div>
                            @if ($errors->has('middle_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                        <div class="col-md-4">

                            <div class="input-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}"
                                       placeholder="Last-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('last_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('parent_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="parent_name"
                                       value="{{ old('parent_name') }}" placeholder="Parent-Name">
                            </div>
                            @if ($errors->has('parent_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('parent_name') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>


                        <div class="col-md-4 col-md-offset-1">
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="1" checked
                                           @if (old('gender')==="1")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-male"></i>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="0"
                                           @if (old('gender')==="0" )
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-female"></i>
                                </label>
                            </div>
                        </div>


                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <p><b>Joining date</b></p>
                            <div class="input-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" placeholder="Joining Date" class="form-control" name="joining_date"
                                       id="joining_date" value="{{ old('joining_date') }}">

                            </div>
                            @if ($errors->has('joining_date'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('joining_date') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>

                        <div class="col-md-6">
                            <p><b>Leaving date</b></p>
                            <div class="input-group{{ $errors->has('leaving_date') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" placeholder="Leaving Date" class="form-control" name="leaving_date"
                                       id="leaving_date" value="{{ old('leaving_date') }}">
                            </div>
                            @if ($errors->has('leaving_date'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('leaving_date') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>

                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group" style="margin: 0">
                                <label>Select a department</label>
                                <select class="form-control select2" multiple name="department[]" style="width: 100%;">
                                    @foreach($faculties as $faculty)
                                        <option value="{{$faculty->id}}" @if(old('department')) @if(in_array($faculty->id,old('department'))) selected @endif @endif>{{$faculty->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group" style="margin: 0">
                                <label>Select a post</label>
                                <select class="form-control select2" multiple name="teacher_post[]" style="width: 100%;">
                                    <option @if(old('teacher_post')) @if(in_array('Principle',old('teacher_post'))) selected @endif @endif>Principal</option>
                                    <option @if(old('teacher_post')) @if(in_array('Co-ordinator',old('teacher_post'))) selected @endif @endif>Co-ordinator</option>
                                    <option @if(old('teacher_post')) @if(in_array('Teacher',old('teacher_post'))) selected @endif @endif>Teacher</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('permanent_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="permanent_address"
                                       value="{{ old('permanent_address') }}" placeholder="Permanent-Address">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('permanent_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('permanent_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('current_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="current_address"
                                       value="{{ old('current_address') }}" placeholder="Current-Address">
                            </div>
                            @if ($errors->has('current_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('current_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="number" class="form-control" name="mobile_number"
                                       value="{{ old('mobile_number') }}" placeholder="Mobile number">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('mobile_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input type="number" class="form-control" name="phone_number"
                                       value="{{ old('phone_number') }}" placeholder="Phone Number">
                            </div>
                            @if ($errors->has('phone_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-flat pull-right">
                                <i class="fa fa-plus"></i> Submit Form &nbsp
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $(".select2").select2();

    </script>
    <script>
        $("#manageUser").addClass("active");

        //Date picker
        $('#joining_date').datepicker({
            autoclose: true
        });

        //Date picker
        $('#leaving_date').datepicker({
            autoclose: true
        });

    </script>
@endsection