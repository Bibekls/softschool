@extends('layouts.mis')
@section('header')
    <h1>
        Student Registration
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">student-registration</li>
    </ol>
@endsection
@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Student Registration Form</h3>
        </div>
        <div class="box-body">
            <div class="col-lg-10 col-lg-offset-1">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/add-student') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="img img-responsive img-thumbnail center-block" src="/img/male.jpg"
                                 alt="User profile picture">
                        </div>
                        <!--Fname -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="username" value="{{ old('username') }}"
                                       placeholder="Username">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('username'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </label>
                                </div>
                            @endif<br>

                            <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                                       placeholder="Email">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('email'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </label>
                                </div>
                            @endif<br>
                        </div>

                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="first_name"
                                       value="{{ old('first_name') }}" placeholder="First-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('first_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                        <div class="col-md-4">

                            <div class="input-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="middle_name"
                                       value="{{ old('middle_name') }}" placeholder="Middle-Name">
                            </div>
                            @if ($errors->has('middle_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                        <div class="col-md-4">

                            <div class="input-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}"
                                       placeholder="Last-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('last_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('parent_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="parent_name"
                                       value="{{ old('parent_name') }}" placeholder="Parent-Name">

                            </div>
                            @if ($errors->has('parent_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('parent_name') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input id="date_of_birth" type="text" class="form-control" name="date_of_birth"
                                       value="{{ old('date_of_birth') }}">
                            </div>
                            @if ($errors->has('date_of_birth'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-4 col-md-offset-1">
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="1" checked
                                           @if (old('gender')==="1")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-male"></i>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="0"
                                           @if (old('gender')==="0")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-female"></i>
                                </label>
                            </div>
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6 col-md-offset-1">

                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('permanent_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="permanent_address"
                                       value="{{ old('permanent_address') }}" placeholder="Permanent-Address">

                            </div>
                            @if ($errors->has('permanent_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('permanent_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('current_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="current_address"
                                       value="{{ old('current_address') }}" placeholder="Current-Address">
                            </div>
                            @if ($errors->has('current_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('current_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('symbol_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
                                <input type="number" class="form-control" name="symbol_number"
                                       value="{{ old('symbol_number') }}" placeholder="Symbol Number">
                            </div>
                            @if ($errors->has('symbol_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('symbol_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('registration_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                <input type="text" class="form-control" name="registration_number"
                                       value="{{ old('registration_number') }}" placeholder="Registration Number">
                            </div>
                            @if ($errors->has('registration_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('registration_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6" >
                            <div class="form-group{{ $errors->has('batch') ? ' has-error' : '' }}" style="margin: 0">

                                <select name="batch" id="" class="form-control select2" style="width: 100%;" placeholder="Batch [year]">
                                    <option value="">Select-Batch</option>
                                    <?php for($i = 2070;$i <= 2090;$i++){ ?>
                                    <option value="{{$i}}" @if(old('batch')==$i) selected @endif >{{$i}}</option>
                                    <?php } ?>
                                </select>

                            </div>
                            @if ($errors->has('batch'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('batch') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="form-group" style="margin: 0">
                                <select class="form-control select2" name="faculty_id" style="width: 100%;" >
                                    <?php $counter = 1; ?>
                                    @foreach($faculties as $faculty)
                                        <option
                                                @if(old('faculty_id')==$faculty->id)
                                                selected
                                                @endif
                                        >
                                            {{$faculty->name}}
                                            <?php $counter++; ?>
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="number" class="form-control" name="mobile_number"
                                       value="{{ old('mobile_number') }}" placeholder="Mobile Number">
                            </div>
                            @if ($errors->has('mobile_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input type="number" class="form-control" name="phone_number"
                                       value="{{ old('phone_number') }}" placeholder="Phone Number">
                            </div>
                            @if ($errors->has('phone_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>


                    <div class="pull-right">
                        <div class="input-group col-md-4"><br>
                            <button type="submit" class="btn btn-primary btn-flat">
                                <i class="fa fa-plus"></i> Submit Form &nbsp
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('#date_of_birth').datepicker({
            autoclose: true
        });

        $(function () {
            $(".select2").select2();
        });

    </script>
    <script>
        $("#manageUser").addClass("active");
    </script>
@endsection