@extends('layouts.mis')
@section('header')
    <h1>
        Profile
        <small>Profile information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">profile</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li onclick="localStorage.setItem('other-user-tab1','teacher')">
                        <a href="#teacher" data-toggle="tab" id="teacher-menu">Teachers</a>
                    </li>
                    <?php $facultyCounter = 1; ?>
                    @foreach($userLists as $attendance)
                        <li onclick="localStorage.setItem('other-user-tab1',' {{$attendance['faculty']->name}} ')">
                            <a href="#{{$attendance['faculty']->name}}"
                               id="{{$attendance['faculty']->name}}-menu"
                               data-toggle="tab">{{$attendance['faculty']->name}}</a>
                        </li>
                        <?php $facultyCounter++; ?>
                    @endforeach
                    <li onclick="localStorage.setItem('other-user-tab1','guest')">
                        <a href="#guest" data-toggle="tab" id="guest-menu">Guest</a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane" id="teacher">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 10px">
                                <form role="form" method="post"
                                      onsubmit="return confirm('Do you really want to download file?');"
                                      action="download-teacher-lists">
                                    <input type="hidden" name="_token"
                                           value="{{csrf_token()}}">
                                    <a onclick="$(this).closest('form').submit();" class="pull-right"
                                       style="cursor: pointer"> Download excel file <span><i
                                                    class="fa fa-file-excel-o"></i></span> </a>
                                </form>
                            </div>
                            @foreach($teachers as $detail)
                                <div class="col-md-4">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="box box1 box-widget widget-user">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="widget-user-header bg-yellow-active">
                                            <h3 class="widget-user-username">
                                                {{$detail->first_name}} {{$detail->middle_name}} {{$detail->last_name}}
                                            </h3>
                                            {{--<h5 class="widget-user-desc">@if($detail->gender=="0")Female @else Male @endif</h5>--}}
                                        </div>
                                        <a href="/images/{{$detail->file_name}}" data-lightbox="profile_pic">
                                            <div class="widget-user-image">
                                                <img class="img-circle" src="images/thumbnails/{{$detail->file_name}}"
                                                     alt="User Avatar">
                                            </div>
                                        </a>
                                        <div class="box-footer">
                                            <div class="row">
                                                {{--<div class="col-sm-4">--}}
                                                {{--<div class="description-block">--}}
                                                {{--<form class="form-horizontal" role="form" method="POST"--}}
                                                {{--action="{{ url('/others-storage') }}">--}}

                                                {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                                {{--<input type="hidden" name="_method" value="post">--}}
                                                {{--<input type="hidden" name="directory" value="">--}}
                                                {{--<input type="hidden" name="fileName" value="">--}}
                                                {{--<input type="hidden" name="user_id" value="{{$detail->user_id}}">--}}
                                                {{--</form>--}}
                                                {{--<a href="#" class="description-text"--}}
                                                {{--@if($detail->user_type!="Admin")--}}
                                                {{--onClick="this.parentElement.childNodes[1].submit();"--}}
                                                {{--@endif--}}
                                                {{-->--}}
                                                {{--<h5 class="description-header"><i class="fa fa-2x fa-database"></i></h5>--}}
                                                {{--</a>--}}
                                                {{--</div>--}}
                                                {{--<!-- /.description-block -->--}}
                                                {{--</div>--}}

                                                <div class="col-sm-4">
                                                    <div class="description-block">
                                                        <a
                                                                @if($detail->user_type!="Admin" && $detail->deleted_at ==null)
                                                                href="/update-user-profile/{{$detail->user_id}}"
                                                                @endif
                                                                class="description-text">
                                                            <h5 class="description-header"><i
                                                                        class="fa fa-2x fa-pencil"></i></h5>
                                                        </a>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>

                                                {{--<div class="col-sm-4">--}}
                                                {{--<div class="description-block">--}}
                                                {{--<a--}}
                                                {{--@if($detail->user_type!="Admin")--}}
                                                {{--href="/login-to-account/{{$detail->user_id}}"--}}
                                                {{--@endif--}}
                                                {{--class="description-text">--}}
                                                {{--<h5 class="description-header"><i class="fa fa-2x fa-sign-in"></i></h5>--}}
                                                {{--</a>--}}
                                                {{--</div>--}}
                                                {{--<!-- /.description-block -->--}}
                                                {{--</div>--}}

                                                <div class="col-sm-4">
                                                    <div class="description-block">
                                                        <form role="form" method="post">
                                                            <input type="hidden" class="user_id"
                                                                   value="{{$detail->user_id}}">
                                                            <a class="description-text password-reset"
                                                               data-toggle="modal" data-target="#passwordReset">
                                                                <h5 class="description-header"><i
                                                                            class="fa fa-2x fa-lock"></i></h5>
                                                            </a>
                                                        </form>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>

                                                @if($detail->deleted_at)
                                                    <div class="col-sm-4">
                                                        <div class="description-block">
                                                            <form role="form" method="post"
                                                                  onsubmit="return confirm('Do you really want to restore account?');"
                                                                  action="/other-users/restore/{{$detail->user_id}}">
                                                                <input type="hidden" name="_method"
                                                                       value="put">
                                                                <input type="hidden" name="_token"
                                                                       value="{{csrf_token()}}">

                                                                <a class="description-text"
                                                                   onclick="$(this).closest('form').submit();">
                                                                    <h5 class="description-header">
                                                                        <i class="fa fa-2x fa-ban"
                                                                           style="color: red;"></i>
                                                                    </h5>
                                                                </a>
                                                            </form>
                                                        </div>
                                                        <!-- /.description-block -->
                                                    </div>
                                                @else
                                                    <div class="col-sm-4">
                                                        <div class="description-block">
                                                            <form role="form" method="post"
                                                                  onsubmit="return confirm('Do you really want to ban this users?');"
                                                                  action="/other-users/delete/{{$detail->user_id}}">
                                                                <input type="hidden" name="_method"
                                                                       value="delete">
                                                                <input type="hidden" name="_token"
                                                                       value="{{csrf_token()}}">

                                                                <a class="description-text"
                                                                   onclick="$(this).closest('form').submit();">
                                                                    <h5 class="description-header">
                                                                        <i class="fa fa-2x fa-ban"></i>
                                                                    </h5>
                                                                </a>
                                                            </form>
                                                        </div>
                                                        <!-- /.description-block -->
                                                    </div>
                                                @endif

                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <?php $facultyCounter = 1; ?>
                    @foreach($userLists as $attendance)
                        <div class="tab-pane"
                             id="{{$attendance['faculty']->name}}">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <?php $batchCounter = 1; ?>
                                    @foreach($attendance['batch-list'] as $batch)
                                        <li onclick="localStorage.setItem('other-user-tab2',' {{$batch['batch']}}{{$attendance['faculty']->name}} ')">
                                            <a href="#{{$batch['batch']}}{{$attendance['faculty']->name}}"
                                               id="{{$batch['batch']}}{{$attendance['faculty']->name}}-menu"
                                               data-toggle="tab">{{$batchCounter}}<sup>Th</sup> Batch
                                                ( {{$batch['batch']}} )</a>
                                        </li>
                                        <?php $batchCounter++; ?>
                                    @endforeach
                                </ul>
                                <div class="tab-content">

                                    <?php $batchCounter = 1; ?>
                                    @foreach($attendance['batch'] as $batch)
                                        <div class="@if($batchCounter==1) active @endif tab-pane"
                                             id="{{$batch['batch']}}{{$attendance['faculty']->name}}">
                                            <div class="row">

                                                @if(count($batch['students'] )>0)
                                                    <div class="col-md-12" style="margin-bottom: 10px">
                                                        <form role="form" method="post"
                                                              onsubmit="return confirm('Do you really want to download file?');"
                                                              action="download-student-lists">
                                                            <input type="hidden" name="_token"
                                                                   value="{{csrf_token()}}">
                                                            <input type="hidden" name="batch_name" value="{{$batch['batch']}}">
                                                            <input type="hidden" name="faculty_id" value="{{ $attendance['faculty']->id }}">
                                                            <a onclick="$(this).closest('form').submit();" class="pull-right"
                                                               style="cursor: pointer"> Download excel file <span>
                                                        <i class="fa fa-file-excel-o"></i></span>
                                                            </a>
                                                        </form>
                                                    </div>
                                                @endif

                                                @foreach($batch['students'] as $detail)

                                                    <div class="col-md-4">
                                                        <!-- Widget: user widget style 1 -->
                                                        <div class="box box1 box-widget widget-user">
                                                            <!-- Add the bg color to the header using any of the bg-* classes -->
                                                            <div class="widget-user-header bg-red-active">
                                                                <h3 class="widget-user-username">
                                                                    {{$detail->first_name}} {{$detail->middle_name}} {{$detail->last_name}}
                                                                </h3>
                                                                {{--<h5 class="widget-user-desc">@if($detail->gender=="0")Female @else Male @endif</h5>--}}
                                                            </div>
                                                            <a href="/images/{{$detail->file_name}}"
                                                               data-lightbox="profile_pic">
                                                                <div class="widget-user-image">
                                                                    <img class="img-circle"
                                                                         src="images/thumbnails/{{$detail->file_name}}"
                                                                         alt="User Avatar">
                                                                </div>
                                                            </a>
                                                            <div class="box-footer">
                                                                <div class="row">
                                                                    {{--<div class="col-sm-4">--}}
                                                                    {{--<div class="description-block">--}}
                                                                    {{--<form class="form-horizontal" role="form" method="POST"--}}
                                                                    {{--action="{{ url('/others-storage') }}">--}}

                                                                    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                                                    {{--<input type="hidden" name="_method" value="post">--}}
                                                                    {{--<input type="hidden" name="directory" value="">--}}
                                                                    {{--<input type="hidden" name="fileName" value="">--}}
                                                                    {{--<input type="hidden" name="user_id" value="{{$detail->user_id}}">--}}
                                                                    {{--</form>--}}
                                                                    {{--<a href="#" class="description-text"--}}
                                                                    {{--@if($detail->user_type!="Admin")--}}
                                                                    {{--onClick="this.parentElement.childNodes[1].submit();"--}}
                                                                    {{--@endif--}}
                                                                    {{-->--}}
                                                                    {{--<h5 class="description-header"><i class="fa fa-2x fa-database"></i></h5>--}}
                                                                    {{--</a>--}}
                                                                    {{--</div>--}}
                                                                    {{--<!-- /.description-block -->--}}
                                                                    {{--</div>--}}

                                                                    <div class="col-sm-4">
                                                                        <div class="description-block">
                                                                            <a
                                                                                    @if($detail->user_type!="Admin" && $detail->deleted_at ==null)
                                                                                    href="/update-user-profile/{{$detail->user_id}}"
                                                                                    @endif
                                                                                    class="description-text">
                                                                                <h5 class="description-header"><i
                                                                                            class="fa fa-2x fa-pencil"></i>
                                                                                </h5>
                                                                            </a>
                                                                        </div>
                                                                        <!-- /.description-block -->
                                                                    </div>

                                                                    {{--<div class="col-sm-4">--}}
                                                                    {{--<div class="description-block">--}}
                                                                    {{--<a--}}
                                                                    {{--@if($detail->user_type!="Admin")--}}
                                                                    {{--href="/login-to-account/{{$detail->user_id}}"--}}
                                                                    {{--@endif--}}
                                                                    {{--class="description-text">--}}
                                                                    {{--<h5 class="description-header"><i class="fa fa-2x fa-sign-in"></i></h5>--}}
                                                                    {{--</a>--}}
                                                                    {{--</div>--}}
                                                                    {{--<!-- /.description-block -->--}}
                                                                    {{--</div>--}}

                                                                    <div class="col-sm-4">
                                                                        <div class="description-block">
                                                                            <form role="form" method="post">
                                                                                <input type="hidden" class="user_id"
                                                                                       value="{{$detail->user_id}}">
                                                                                <a class="description-text password-reset"
                                                                                   data-toggle="modal"
                                                                                   data-target="#passwordReset">
                                                                                    <h5 class="description-header"><i
                                                                                                class="fa fa-2x fa-lock"></i>
                                                                                    </h5>
                                                                                </a>
                                                                            </form>
                                                                        </div>
                                                                        <!-- /.description-block -->
                                                                    </div>
                                                                    @if($detail->deleted_at)
                                                                        <div class="col-sm-4">
                                                                            <div class="description-block">
                                                                                <form role="form" method="post"
                                                                                      onsubmit="return confirm('Do you really want to restore account?');"
                                                                                      action="/other-users/restore/{{$detail->user_id}}">
                                                                                    <input type="hidden" name="_method"
                                                                                           value="put">
                                                                                    <input type="hidden" name="_token"
                                                                                           value="{{csrf_token()}}">

                                                                                    <a class="description-text"
                                                                                       onclick="$(this).closest('form').submit();">
                                                                                        <h5 class="description-header">
                                                                                            <i class="fa fa-2x fa-ban"
                                                                                               style="color: red;"></i>
                                                                                        </h5>
                                                                                    </a>
                                                                                </form>
                                                                            </div>
                                                                            <!-- /.description-block -->
                                                                        </div>
                                                                    @else
                                                                        <div class="col-sm-4">
                                                                            <div class="description-block">
                                                                                <form role="form" method="post"
                                                                                      onsubmit="return confirm('Do you really want to ban this users?');"
                                                                                      action="/other-users/delete/{{$detail->user_id}}">
                                                                                    <input type="hidden" name="_method"
                                                                                           value="delete">
                                                                                    <input type="hidden" name="_token"
                                                                                           value="{{csrf_token()}}">

                                                                                    <a class="description-text"
                                                                                       onclick="$(this).closest('form').submit();">
                                                                                        <h5 class="description-header">
                                                                                            <i class="fa fa-2x fa-ban"></i>
                                                                                        </h5>
                                                                                    </a>
                                                                                </form>
                                                                            </div>
                                                                            <!-- /.description-block -->
                                                                        </div>
                                                                    @endif

                                                                </div>
                                                                <!-- /.row -->
                                                            </div>
                                                        </div>
                                                        <!-- /.widget-user -->
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <?php $batchCounter++; ?>
                                    @endforeach

                                </div>
                                <!-- /.tab-content -->
                            </div>
                        </div>
                        <?php $facultyCounter++; ?>
                    @endforeach

                    <div class="tab-pane" id="guest">
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 10px">
                                <form role="form" method="post"
                                      onsubmit="return confirm('Do you really want to download file?');"
                                      action="download-teacher-lists">
                                    <input type="hidden" name="_token"
                                           value="{{csrf_token()}}">
                                    <a onclick="$(this).closest('form').submit();" class="pull-right"
                                       style="cursor: pointer"> Download excel file <span><i
                                                    class="fa fa-file-excel-o"></i></span> </a>
                                </form>
                            </div>
                            @foreach($guests as $detail)
                                <div class="col-md-4">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="box box1 box-widget widget-user">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="widget-user-header bg-yellow-active">
                                            <h3 class="widget-user-username">
                                                {{$detail->first_name}} {{$detail->middle_name}} {{$detail->last_name}}
                                            </h3>
                                            {{--<h5 class="widget-user-desc">@if($detail->gender=="0")Female @else Male @endif</h5>--}}
                                        </div>
                                        <a href="/images/{{$detail->file_name}}" data-lightbox="profile_pic">
                                            <div class="widget-user-image">
                                                <img class="img-circle" src="images/thumbnails/{{$detail->file_name}}"
                                                     alt="User Avatar">
                                            </div>
                                        </a>
                                        <div class="box-footer">
                                            <div class="row">
                                                {{--<div class="col-sm-4">--}}
                                                {{--<div class="description-block">--}}
                                                {{--<form class="form-horizontal" role="form" method="POST"--}}
                                                {{--action="{{ url('/others-storage') }}">--}}

                                                {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                                {{--<input type="hidden" name="_method" value="post">--}}
                                                {{--<input type="hidden" name="directory" value="">--}}
                                                {{--<input type="hidden" name="fileName" value="">--}}
                                                {{--<input type="hidden" name="user_id" value="{{$detail->user_id}}">--}}
                                                {{--</form>--}}
                                                {{--<a href="#" class="description-text"--}}
                                                {{--@if($detail->user_type!="Admin")--}}
                                                {{--onClick="this.parentElement.childNodes[1].submit();"--}}
                                                {{--@endif--}}
                                                {{-->--}}
                                                {{--<h5 class="description-header"><i class="fa fa-2x fa-database"></i></h5>--}}
                                                {{--</a>--}}
                                                {{--</div>--}}
                                                {{--<!-- /.description-block -->--}}
                                                {{--</div>--}}

                                                <div class="col-sm-4">
                                                    <div class="description-block">
                                                        <a
                                                                @if($detail->user_type!="Admin" && $detail->deleted_at ==null)
                                                                href="/update-user-profile/{{$detail->user_id}}"
                                                                @endif
                                                                class="description-text">
                                                            <h5 class="description-header"><i
                                                                        class="fa fa-2x fa-pencil"></i></h5>
                                                        </a>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>

                                                {{--<div class="col-sm-4">--}}
                                                {{--<div class="description-block">--}}
                                                {{--<a--}}
                                                {{--@if($detail->user_type!="Admin")--}}
                                                {{--href="/login-to-account/{{$detail->user_id}}"--}}
                                                {{--@endif--}}
                                                {{--class="description-text">--}}
                                                {{--<h5 class="description-header"><i class="fa fa-2x fa-sign-in"></i></h5>--}}
                                                {{--</a>--}}
                                                {{--</div>--}}
                                                {{--<!-- /.description-block -->--}}
                                                {{--</div>--}}

                                                <div class="col-sm-4">
                                                    <div class="description-block">
                                                        <form role="form" method="post">
                                                            <input type="hidden" class="user_id"
                                                                   value="{{$detail->user_id}}">
                                                            <a class="description-text password-reset"
                                                               data-toggle="modal" data-target="#passwordReset">
                                                                <h5 class="description-header"><i
                                                                            class="fa fa-2x fa-lock"></i></h5>
                                                            </a>
                                                        </form>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>

                                                @if($detail->deleted_at)
                                                    <div class="col-sm-4">
                                                        <div class="description-block">
                                                            <form role="form" method="post"
                                                                  onsubmit="return confirm('Do you really want to restore account?');"
                                                                  action="/other-users/restore/{{$detail->user_id}}">
                                                                <input type="hidden" name="_method"
                                                                       value="put">
                                                                <input type="hidden" name="_token"
                                                                       value="{{csrf_token()}}">

                                                                <a class="description-text"
                                                                   onclick="$(this).closest('form').submit();">
                                                                    <h5 class="description-header">
                                                                        <i class="fa fa-2x fa-ban"
                                                                           style="color: red;"></i>
                                                                    </h5>
                                                                </a>
                                                            </form>
                                                        </div>
                                                        <!-- /.description-block -->
                                                    </div>
                                                @else
                                                    <div class="col-sm-4">
                                                        <div class="description-block">
                                                            <form role="form" method="post"
                                                                  onsubmit="return confirm('Do you really want to ban this users?');"
                                                                  action="/other-users/delete/{{$detail->user_id}}">
                                                                <input type="hidden" name="_method"
                                                                       value="delete">
                                                                <input type="hidden" name="_token"
                                                                       value="{{csrf_token()}}">

                                                                <a class="description-text"
                                                                   onclick="$(this).closest('form').submit();">
                                                                    <h5 class="description-header">
                                                                        <i class="fa fa-2x fa-ban"></i>
                                                                    </h5>
                                                                </a>
                                                            </form>
                                                        </div>
                                                        <!-- /.description-block -->
                                                    </div>
                                                @endif

                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>

    <div id="passwordReset" class="modal fade modal-success" role="dialog">
        <div class="modal-dialog modal-sm">

            <form class="form-horizontal" action="/password-reset" method="post" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title text-center">Password Reset</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="id" id="user_id" value="{{ old('id')}}">

                        <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" name="password" placeholder="password">
                            <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                        </div>
                        @if ($errors->has('password'))
                            <div class="input-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                    <strong>{{ $errors->first('password') }}</strong>
                                </label>
                            </div>
                        @endif<br>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" name="password_confirmation"
                                   placeholder="confirm-password">
                            <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline">Done</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>

@endsection

@section('script')
    <script>
        $("#manageUser").addClass("active");
    </script>
    <script type="text/javascript">
        $(document).ready()
        {
            window.open('{{$loginOtherUser}}', '_self');
        }
    </script>
    <script type="text/javascript">

        @if($errors->has('password'))$('#passwordReset').modal();
        @endif

              $(".password-reset").click(function () {

            $("#user_id").val($(this).parent().children(".user_id").val());
        });

        document.getElementById(localStorage.getItem('other-user-tab1').trim() + '-menu').click();
        document.getElementById(localStorage.getItem('other-user-tab2').trim() + '-menu').click();

    </script>
@endsection
