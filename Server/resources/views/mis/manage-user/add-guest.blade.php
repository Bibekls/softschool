@extends('layouts.mis')
@section('header')
    <h1>
        Guest User Registration
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">guest-registration</li>
    </ol>
@endsection
@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Guest Registration Form</h3>
        </div>
        <div class="box-body">
            <div class="col-lg-10 col-lg-offset-1">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/add-guest') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="first_name"
                                       value="{{ old('first_name') }}" placeholder="First-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('first_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                        <div class="col-md-4">

                            <div class="input-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="middle_name"
                                       value="{{ old('middle_name') }}" placeholder="Middle-Name">
                            </div>
                            @if ($errors->has('middle_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                        <div class="col-md-4">

                            <div class="input-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}"
                                       placeholder="Last-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('last_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </label>
                                </div><br>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-4 col-md-offset-1">
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="1" checked
                                           @if (old('gender')==="1")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-male"></i>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="0"
                                           @if (old('gender')==="0")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-female"></i>
                                </label>
                            </div>
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6 col-md-offset-1">

                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('permanent_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="permanent_address"
                                       value="{{ old('permanent_address') }}" placeholder="Permanent-Address">

                            </div>
                            @if ($errors->has('permanent_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('permanent_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('current_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="current_address"
                                       value="{{ old('current_address') }}" placeholder="Current-Address">
                            </div>
                            @if ($errors->has('current_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('current_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="number" class="form-control" name="mobile_number"
                                       value="{{ old('mobile_number') }}" placeholder="Mobile Number">
                            </div>
                            @if ($errors->has('mobile_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input type="number" class="form-control" name="phone_number"
                                       value="{{ old('phone_number') }}" placeholder="Phone Number">
                            </div>
                            @if ($errors->has('phone_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>


                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-12">
                            <div class="input-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                <textarea type="text" class="form-control" name="description" rows="5"
                                          placeholder="Write detail about the guest ">{{ old('description') }}</textarea>

                            </div>
                            @if ($errors->has('description'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <br>

                        <div class="pull-right">
                            <div class="input-group col-md-4"><br>
                                <button type="submit" class="btn btn-primary btn-flat">
                                    <i class="fa fa-plus"></i> Submit Form &nbsp
                                </button>
                            </div>
                        </div>

                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('#date_of_birth').datepicker({
            autoclose: true
        });

        $(function () {
            $(".select2").select2();
        });

    </script>
    <script>
        $("#manageUser").addClass("active");
    </script>
@endsection