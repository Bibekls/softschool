@extends('layouts.mis')
@section('header')
    <h1>
        Update Student Profile
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">manage-user</li>
        <li class="active">Update student Profile</li>
    </ol>
@endsection
@section('content')

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Profile Update Form</h3>
            <div class="pull-right">
                <a href="/other-users">Go Back</a>
            </div>
        </div>
        <div class="box-body">
            <div class="col-lg-10 col-lg-offset-1">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/update-student') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="id" value="{{$user->id}}">

                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <img class="img img-responsive img-thumbnail center-block"
                                 src="/images/{{$user->profile_picture->file_name}}" alt="User profile picture">
                            <button type="button" class="btn btn-info btn-sm btn-flat btn-block" data-toggle="modal"
                                    data-target="#myModal">Change profile pic
                            </button>
                        </div>
                        <div class="col-md-1"></div>
                        <!--Fname -->

                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="username" value="{{ $user->name }}"
                                       placeholder="Username">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('username'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </label>
                                </div>
                            @endif<br>

                            <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="email" class="form-control" name="email" value="{{ $user->email }}"
                                       placeholder="Email">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('email'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </label>
                                </div>
                            @endif<br>
                            <div class="input-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="first_name"
                                       value="{{ $student->first_name}}" placeholder="First-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('first_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </label>
                                </div>
                            @endif <br>
                            <div class="input-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="middle_name"
                                       value="{{ $student->middle_name }}" placeholder="Middle-Name">
                            </div>
                            @if ($errors->has('middle_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </label>
                                </div>
                            @endif <br>
                            <div class="input-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="last_name"
                                       value="{{ $student->last_name }}" placeholder="Last-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('last_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </label>
                                </div>
                            @endif <br>

                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('parent_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="parent_name"
                                       value="{{ $student->parent_name}}" placeholder="Parent-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('parent_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('parent_name') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input id="date_of_birth" type="text" class="form-control" name="date_of_birth"
                                       value="{{date_format(date_create($student->date_of_birth ), 'm/d/Y') }}">

                            </div>
                            @if ($errors->has('date_of_birth'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-4 col-md-offset-1">
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="1"
                                           @if ($student->gender=="1")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-male"></i>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="0"
                                           @if ($student->gender=="0")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-female"></i>
                                </label>
                            </div>
                        </div>

                        <!--Name of author -->
                        <div class="col-md-6 col-md-offset-1">

                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('permanent_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="permanent_address"
                                       value="{{ $student->permanent_address }}" placeholder="Permanent-Address">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('permanent_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('permanent_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('current_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="current_address"
                                       value="{{ $student->current_address }}" placeholder="Current-Address">
                            </div>
                            @if ($errors->has('current_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('current_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('symbol_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
                                <input type="number" class="form-control" name="symbol_number"
                                       value="{{ $student->symbol_number }}" placeholder="Symbol Number">
                            </div>
                            @if ($errors->has('symbol_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('symbol_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('registration_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                <input type="text" class="form-control" name="registration_number"
                                       value="{{ $student->registration_number }}" placeholder="Registration Number">
                            </div>
                            @if ($errors->has('registration_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('registration_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('batch') ? ' has-error' : '' }}" style="margin: 0">

                                <select name="batch" id="" class="form-control select2" style="width: 100%;"
                                        placeholder="Batch [year]">
                                    <option value="">Select-Batch</option>
                                    <?php for($i = 2070;$i <= 2090;$i++){ ?>
                                    <option value="{{$i}}" @if($student->batch==$i) selected @endif >{{$i}}</option>
                                    <?php } ?>
                                </select>

                            </div>
                            @if ($errors->has('batch'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('batch') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-folder-open"></i></span>
                                <select class="form-control" name="faculty_id">
                                    <?php $counter = 1; ?>
                                    @foreach($faculties as $faculty)
                                        <option
                                                @if($faculty->id==$student->faculty_id)
                                                selected
                                                @endif
                                        >
                                            {{$faculty->name}}
                                            <?php $counter++; ?>
                                        </option>
                                    @endforeach
                                </select>
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="number" class="form-control" name="mobile_number"
                                       value="{{ $student->mobile_number }}" placeholder="mobile_number">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('mobile_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input type="number" class="form-control" name="phone_number"
                                       value="{{ $student->phone_number }}" placeholder="Phone Number">
                            </div>
                            @if ($errors->has('phone_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-4 pull-right">
                            <div class="input-group"><br>
                                <button type="submit" class="btn btn-primary btn-flat">
                                    <i class="fa fa-plus"></i> Update Profile &nbsp
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="myModal" class="modal fade modal-success" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Profile Picture</h4>
                </div>
                <div class="modal-body">
                    <form action="/update-user-profile-pic" class="dropzone " id="addImages" style="color: black;">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <input type="hidden" name="_method" value="PUT">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>

        @endsection

        @section('script')
            <script type="text/javascript">
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });

                $('#date_of_birth').datepicker({
                    autoclose: true
                });

                $(function () {
                    $(".select2").select2();
                });

            </script>
            <script>
                $("#manageUser").addClass("active");
            </script>

            <script>
                Dropzone.options.addImages = {
                    maxFileSize: 2,
                    acceptedFiles: 'image/*',
                    success: function (file, response) {
                        if (file.status == 'success') {
                            handleDropzoneFileUpload.handleSuccess(response);
                        }
                        else {
                            handleDropzoneFileUpload.handleError(response);
                        }
                        console.log(file);
                        console.log(response);

                    }
                };

                var handleDropzoneFileUpload = {
                    handleError: function (response) {
                        window.location = "/update-user-profile/{{$user->id}}";
                    },
                    handleSuccess: function (response) {
                        window.location = "/update-user-profile/{{$user->id}}";
                    }
                }

            </script>
@endsection