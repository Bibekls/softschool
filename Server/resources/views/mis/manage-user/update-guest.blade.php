@extends('layouts.mis')
@section('header')
    <h1>
        Update Guest Profile
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">manage-guest</li>
        <li class="active">Update guest Profile</li>
    </ol>
@endsection
@section('content')

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Profile Update Form</h3>
            <div class="pull-right">
                <a href="/other-users">Go Back</a>
            </div>
        </div>
        <div class="box-body">
            <div class="col-lg-10 col-lg-offset-1">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/update-guest') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="id" value="{{$user->id}}">

                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <img class="img img-responsive img-thumbnail center-block"
                                 src="/images/{{$user->profile_picture->file_name}}" alt="User profile picture">
                            <button type="button" class="btn btn-info btn-sm btn-flat btn-block" data-toggle="modal"
                                    data-target="#myModal">Change profile pic
                            </button>
                        </div>
                        <div class="col-md-1"></div>
                        <!--Fname -->

                        <div class="col-md-6">

                            {{--<div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>--}}
                            {{--<input type="email" class="form-control" name="email" value="{{ $user->email }}"--}}
                            {{--placeholder="Email">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-asterisk"></i></span>--}}
                            {{--</div>--}}
                            {{--@if ($errors->has('email'))--}}
                            {{--<div class="input-group has-error">--}}
                            {{--<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>--}}
                            {{--<strong>{{ $errors->first('email') }}</strong>--}}
                            {{--</label>--}}
                            {{--</div>--}}
                            {{--@endif--}}
                            {{--<br>--}}
                            <div class="input-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="first_name"
                                       value="{{ $guest->first_name}}" placeholder="First-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('first_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </label>
                                </div>
                            @endif <br>
                            <div class="input-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="middle_name"
                                       value="{{ $guest->middle_name }}" placeholder="Middle-Name">
                            </div>
                            @if ($errors->has('middle_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </label>
                                </div>
                            @endif <br>
                            <div class="input-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="last_name" value="{{ $guest->last_name }}"
                                       placeholder="Last-Name">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('last_name'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </label>
                                </div>
                            @endif <br>

                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-4 col-md-offset-1">
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="1"
                                           @if ($guest->gender=="1")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-male"></i>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <input type="radio" name="gender" value="0"
                                           @if ($guest->gender=="0")
                                           checked
                                           @endif class="flat-red">
                                    <i class="fa fa-2x fa-female"></i>
                                </label>
                            </div>
                        </div>

                        <!--Name of author -->
                        <div class="col-md-6 col-md-offset-1">

                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('permanent_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="permanent_address"
                                       value="{{ $guest->permanent_address }}" placeholder="Permanent-Address">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('permanent_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('permanent_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('current_address') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="current_address"
                                       value="{{ $guest->current_address }}" placeholder="Current-Address">
                            </div>
                            @if ($errors->has('current_address'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('current_address') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="number" class="form-control" name="mobile_number"
                                       value="{{ $guest->mobile_number }}" placeholder="mobile_number">
                                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                            </div>
                            @if ($errors->has('mobile_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!--Name of author -->
                        <div class="col-md-6">
                            <div class="input-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input type="number" class="form-control" name="phone_number"
                                       value="{{ $guest->phone_number }}" placeholder="Phone Number">
                            </div>
                            @if ($errors->has('phone_number'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <!--Name of book -->
                        <div class="col-md-12">
                            <div class="input-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                <textarea type="text" class="form-control" name="description" rows="5"
                                          placeholder="Write detail about the guest ">{{ $guest->description }}</textarea>

                            </div>
                            @if ($errors->has('description'))
                                <div class="input-group has-error">
                                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-4 pull-right">
                                <div class="input-group"><br>
                                    <button type="submit" class="btn btn-primary btn-flat">
                                        <i class="fa fa-plus"></i> Update Profile &nbsp
                                    </button>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>


    <div id="myModal" class="modal fade modal-success" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Profile Picture</h4>
                </div>
                <div class="modal-body">
                    <form action="/update-user-profile-pic" class="dropzone " id="addImages" style="color: black;">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <input type="hidden" name="_method" value="PUT">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>

        @endsection

        @section('script')
            <script type="text/javascript">
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });

                $('#date_of_birth').datepicker({
                    autoclose: true
                });

                $(function () {
                    $(".select2").select2();
                });

            </script>
            <script>
                $("#manageUser").addClass("active");
            </script>

            <script>
                Dropzone.options.addImages = {
                    maxFileSize: 2,
                    acceptedFiles: 'image/*',
                    success: function (file, response) {
                        if (file.status == 'success') {
                            handleDropzoneFileUpload.handleSuccess(response);
                        }
                        else {
                            handleDropzoneFileUpload.handleError(response);
                        }
                        console.log(file);
                        console.log(response);

                    }
                };

                var handleDropzoneFileUpload = {
                    handleError: function (response) {
                        window.location = "/update-user-profile/{{$user->id}}";
                    },
                    handleSuccess: function (response) {
                        window.location = "/update-user-profile/{{$user->id}}";
                    }
                }

            </script>
@endsection