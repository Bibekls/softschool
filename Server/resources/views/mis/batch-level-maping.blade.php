@extends('layouts.mis')
@section('style')
    <style>
        .level-radio{
            padding-left: 25px;
        }
    </style>
@endsection
@section('header')
    <h1>
        Batch-Level Mapping
        <small>assign level to batch</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">batch-level-mapping</li>
    </ol>
@endsection
@section('content') 

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Batch level mapping</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
            </div>
            <div class="box-body">                  
                <ol class="list-group">
                    @foreach($blmData['faculties'] as $faculty)
                        <li class="list-group-item">
                            <h3>{{$faculty['faculty']}}</h3>
                            <ol class="list-group">
                                @foreach ($faculty['batches'] as $batch)
                                <li class="list-group-item">
                                    <div class="col-md-4">
                                        Batch:{{$batch['batch']}}
                                    </div>
                                    <div class="pull-rigt">
                                        <form class="form-inline" method="POST" action="{{ url('/batch-level-mapping') }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="faculty_id" value="{{$faculty['id']}}">
                                            <input type="hidden" name="batch" value="{{$batch['batch']}}">

                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="1" 
                                                  @if($batch['level']==1)checked @endif>
                                                  </label>
                                                </div>                                            
                                            </div>

                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="2" 
                                                  @if($batch['level']==2)checked @endif>
                                                  </label>
                                                </div>                                            
                                            </div>
                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="3" 
                                                  @if($batch['level']==3)checked @endif>
                                                  </label>
                                                </div>                                            
                                            </div>
                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="4" 
                                                  @if($batch['level']==4)checked @endif>
                                                  </label>
                                                </div>                                            
                                            </div>
                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="5" 
                                                  @if($batch['level']==5)checked @endif>
                                                  </label>
                                                </div>                                            
                                            </div>
                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="6" 
                                                  @if($batch['level']==6)checked @endif>
                                                  </label>
                                                </div>                                            
                                            </div>
                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="7" 
                                                  @if($batch['level']==7)checked @endif>
                                                  </label>
                                                </div>                                            
                                            </div>
                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="8" 
                                                  @if($batch['level']==8)checked @endif>
                                                  </label>
                                                </div>                                            
                                            </div>  
                                            <div class="form-group level-radio">
                                                <div class="radio">
                                                  <label><input type="radio" 
                                                  class="flat-red" name="level" value="0" 
                                                  @if($batch['level']==0)checked @endif>
                                                  None
                                                  </label>
                                                </div>                                            
                                            </div>

                                            <div class="form-group level-radio">                        
                                                <button type="submit" class="btn btn-flat btn-primary">
                                                    Update
                                                </button>
                                            </div>
                                        </form>                                        
                                    </div>
                                </li>
                                @endforeach
                            </ol>
                        </li>
                    @endforeach
                </ol>

            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
    <script type="text/javascript">
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

    </script>
    <script>
        $( "#batch-level-mapping" ).addClass( "active" );
    </script>
@endsection