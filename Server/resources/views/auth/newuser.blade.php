@extends('auth/layout/auth')

@section('title')
Login
@endsection

@section('content')    
        <div class="alert alert-success">
            	Your request is not verified by admin
            	<br>
            	It may take 1 day to verify your request.
            	<br> 
        </div>
@endsection
