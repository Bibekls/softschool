@extends('auth/layout/auth')

@section('title')
Login
@endsection

@section('content')    
        <div class="alert alert-success">
            Your student form is not verified by admin 
            <br>It may take 1 day to verify your account.

        </div>
@endsection
