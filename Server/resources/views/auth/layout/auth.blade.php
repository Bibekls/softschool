<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        @yield('title')
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">    
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="http://hetaudacitycollege.edu.np/bootstrap/css/box.css">

</head>
<body class="hold-transition login-page">
<div class="box1 login-box" style="background-color: #c7254e;">
    <div class="login-logo">
        <a style="color: white;" href="www.hetaudacitycollege.edu.np"><b>HCC</b> Network</a>
    </div><!-- /.login-logo -->
    <div class="login-box-body" style="padding: 30px;">
        @yield('content')
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<script src="https://code.jquery.com/jquery-3.1.1.min.js" 
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous">
  </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- iCheck -->
<script src="http://hetaudacitycollege.edu.np/plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
