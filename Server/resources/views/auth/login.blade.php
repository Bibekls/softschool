@extends('auth/layout/auth')

@section('title')
Login
@endsection

@section('content')
    @if (session('message'))
        <div class="alert alert-danger">
            {{ session('message') }}
            {{session()->forget('message')}}
        </div>
    @endif
<p class="login-box-msg">Sign in to start your session</p>
<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
    {!! csrf_field() !!}
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
        <input type="text" class="form-control" placeholder="User Name" name="name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="row">
        <div class="col-xs-7">
            <div class="checkbox icheck">
                <label>
                    <input type="checkbox"> Remember Me
                </label>
            </div>
        </div><!-- /.col -->
        <div class="col-xs-5">
            <button type="submit" class="btn btn-primary btn-block btn-flat">
                <i class="glyphicon glyphicon-log-in "></i> Sign In
            </button>
        </div><!-- /.col -->
    </div>
</form>
<br><br>

<a href="/password/reset">I forgot my password</a><br>
<a href="/signup" class="text-center">Register a new membership</a>
@endsection
