@extends('layouts.teacher')
@section('header')
    <h1>
        Feedback
        <small>Feedback</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">feedback</li>
    </ol>
@endsection
@section('content')
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Give your feedback to make our system more stable.</h3>
            </div>
            <div class="box-body">
                <div class="col-lg-10 col-lg-offset-1">                                       
                    <form class="form-horizontal" role="form" method="POST" 
                        action="{{ url('/feedback') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group{{ $errors->has('feedback') ? ' has-error' : '' }}">
                                    <span class="input-group-addon">
                                        <i class="fa fa-newspaper-o"></i>
                                    </span>
                                    <textarea class="form-control" rows="5" name="feedback" placeholder="Write your Feedback here.....">{{ old('feedback') }}</textarea>
                                </div>

                                @if ($errors->has('feedback'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('feedback') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="pull-right">
                                <div class="input-group"><br>
                                    <button type="submit" id="searchstudent" class="btn btn-primary btn-flat">
                                        <i class="fa fa-plus"></i> Send 
                                        <i id="addStudent" class="fa fa-circle-o-notch fa-spin hidden"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 


@endsection

@section('script')
    <script>
        $( "#feedback" ).addClass( "active" );
    </script>
@endsection