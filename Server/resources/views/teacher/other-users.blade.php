@extends('layouts.teacher')
@section('header')
    <h1>
        Profile
        <small>Profile information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">profile</li>
    </ol>
@endsection

@section('content')

    <div class="row">
    @foreach($details as $detail)
       <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box1 box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header 
            @if ($detail->user_type=='Teacher') 
              bg-yellow-active            
            @elseif ($detail->user_type=='Student') 
              bg-red-active
            @endif
            ">
              <h3 class="widget-user-username">{{$detail->username}}</h3>
              <h5 class="widget-user-desc">{{$detail->user_type}}</h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="images/thumbnails/{{$detail->profile_pic}}" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">                
                <div class="col-sm-4">
                  <div class="description-block">
                    <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/others-storage') }}">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="directory" value="">                               
                                <input type="hidden" name="fileName" value=""> 
                                <input type="hidden" name="user_id" value="{{$detail->user_id}}">
                    </form>                                    
                    <a href="#" class="description-text" onClick="this.parentElement.childNodes[1].submit();">
                    <h5 class="description-header"><i class="fa fa-2x fa-database"></i></h5>    
                        Storage
                    </a>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
    @endforeach
    </div>
@endsection

@section('script')
    <script>
        $( "#other-users" ).addClass( "active" );
    </script>
@endsection
