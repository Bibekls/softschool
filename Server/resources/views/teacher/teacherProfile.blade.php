@extends('layouts.teacher')
@section('header')
    <h1>
        Profile
        <small>Profile information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">profile</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class=" box1 box box-primary">
                <div class="box-body box-profile">
                    <a href="{{url($user->profile_picture->file_path)}}" data-lightbox="profile_pic">
                        <img id="profile_pic" class="profile-user-img img-responsive img-circle" src="{{url('images/thumbnails')}}/{{$user->profile_picture->file_name}}" alt="User profile picture">
                    </a>
                    <h3 class="profile-username text-center">{{$user->name}}</h3>
                    <p class="text-muted text-center">Teacher</p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>First Name</b> <a class="pull-right">{{$detail->first_name}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Middle Name</b> <a class="pull-right">{{$detail->middle_name}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Last Name</b> <a class="pull-right">{{$detail->last_name}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Email</b> <a class="pull-right">{{$user->email}}</a>
                        </li>
                    </ul>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <!-- About Me Box -->
            <div class="box1 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-user margin-r-5"></i>Parent Name</strong>
                    <p class="text-muted text-right">
                      {{$detail->parent_name}}
                    </p>

                    <hr>
                    <strong><i class="fa fa-calendar margin-r-5"></i>Date of birth</strong>
                    <p class="text-muted text-right">
                        {{$detail->date_of_birth}}
                    </p>

                    <hr>
                    <strong><i class="fa fa-book margin-r-5"></i>Gender</strong>
                    <p class="text-muted text-right">
                        @if($detail->gender=='1')
                            Male
                        @elseif($detail->gender=='0')
                            Female
                        @else
                            Error
                        @endif
                    </p>

                    <hr>
                    <strong><i class="fa fa-info margin-r-5"></i>Religion</strong>
                    <p class="text-muted text-right">
                        {{$detail->religion}}
                    </p>

                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i>Permanent-Address</strong>
                    <p class="text-muted text-right">
                        {{$detail->permanent_address}}
                    </p>

                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i>Current-Address</strong>
                    <p class="text-muted text-right">
                        {{$detail->current_address}}
                    </p>

                    <hr>
                    <strong><i class="fa fa-mobile margin-r-5"></i>Mobile-Number</strong>
                    <p class="text-muted text-right">
                        {{$detail->mobile_number}}
                    </p>
                    <hr>
                    <strong><i class="fa fa-phone margin-r-5"></i>Phone-Number</strong>
                    <p class="text-muted text-right">
                        {{$detail->phone_number}}
                    </p>

                  </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div><!-- /.col -->
        <div class="col-md-9">
            <div>
                <!--Timeline -->               
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Time Line</h3>
                    </div>
                    <div class="box-body">
                        @foreach($newsfeed as $post)
                        <?php if($counter%2==0){?>
                            <div class="row">
                        <?php   } ?>   
                            <div class="col-md-6">                                   
                                  <div class="box box7 box-widget">
                                        <div class='box-header with-border'>
                                          <div class='user-block'>
                                            <img class='img-circle' src="/images/thumbnails/{{$post['profile_pic']->profile_pic}}" alt='user image'>
                                            <span class='username'><a href="#">{{$post['post']->user->name}}</a></span>
                                            <span class='description'>{{$post['postTime']}}</span>
                                          </div><!-- /.user-block -->
                                          <div class='box-tools'>                   
                                            <?php if($post['post']->user->id==Auth::user()->id){ ?>
                                              <form class="form-horizontal" role="form" method="POST" 
                                              action="{{ url('/post') }}">                    
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="delete">
                                                <input type="hidden" name="post_id" value="{{$post['post']->id}}">
                                                <button type="submit" class='btn btn-box-tool'><i class='fa fa-2x fa-trash'></i>
                                                </button>
                                              <?php } ?>
                                            </form>   
                                          </div><!-- /.box-tools -->
                                        </div><!-- /.box-header -->
                                        <div class='box-body'>
                                          <p class="text-justify"><a>{{$post['post']->post}}</a></p>
                                         
                                          <span class='pull-right text-muted'>{{ $post['comments']->count()}} comments</span>
                                        </div><!-- /.box-body -->
                                        <div class='box-footer box-comments'>
                                            <div class="box box-warning collapsed-box">
                                            <div class="box-header with-border">
                                              <h3 class="box-title">Read Comments</h3>

                                              <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                              </div>
                                              <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">

                                        @foreach( $post['comments'] as $comment)
                                          <div class='box-comment'>
                                            <!-- User image -->
                                            <img class='img-circle img-sm' src="/images/thumbnails/{{$comment->profile_pic}}" alt='user image'>
                                            <div class='comment-text'>
                                              <span class="username">
                                            {{$comment->username}}                                            
                                                <?php if($comment->user_id==Auth::user()->id){ ?>
                                                <div class="pull-right">
                                                   <form class="form-horizontal" role="form" method="POST" 
                                                        action="{{ url('/comment') }}">
                                                        
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="_method" value="delete">
                                                        <input type="hidden" name="comment_id" value="{{$comment->id}}">
                                                        <button type="submit" class='btn btn-box-tool'><i class='fa fa-trash'></i>
                                                        </button>
                                                    </form>
                                                </div>
                                              <?php } ?>                                            
                                               <br>
                                                <span class='text-muted'>
                                                        {{Carbon::createFromTimeStamp(strtotime($comment->time))->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime($comment->time))))->diffForHumans() }}
                                                </span>
                                              </span><!-- /.username -->
                                              <br>
                                                  <p class="text-justify">{{$comment->comment}}</p>
                                            </div><!-- /.comment-text -->
                                          </div><!-- /.box-comment -->
                                          @endforeach    
                                          </div>
                                          </div>             
                                        </div><!-- /.box-footer -->
                                        <div class="box-footer">
                                          <form action="{{url('/comment')}}" method="POST">

                                            <input type="hidden" name="post_id" value="{{$post['post']->id}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <img class="img-responsive img-circle img-sm" src="/images/thumbnails/{{$profile_pic->file_name}}" alt="alt text">
                                            <!-- .img-push is used to add margin to elements next to floating images -->
                                            <div class="img-push">                                                
                                                <input type="text" class="form-control input-sm" placeholder="Press enter to post comment. 140 character only" name="comment">
                                            </div>
                                          </form>
                                        </div><!-- /.box-footer -->
                                  </div><!-- /.Post --> 
                                  <br>
                            </div> 
                        <?php $counter++; if( $counter%2==0) { ?>
                          </div>
                        <?php   } ?>                                                                                       
                         @endforeach   

                        <?php if( $counter%2!=0) { ?>
                              </div>
                        <?php   } ?>                         
                          
                        <div class="col-md-12">
                            <div class="container">    
                                <div class="pagination"> {{ $pagelink->links() }} </div>  
                            </div>
                        </div>
                     </div>                     
                </div><!-- /.row --> 

            </div>         
        </div>
    </div>
@endsection

@section('script')
    <script>
        $( "#profile" ).addClass( "active" );
    </script>
@endsection
