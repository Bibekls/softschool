@extends('layouts.teacher')
@section('header')
    <h1>
        Important URLs
        <small>List of url</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">URL</li>
    </ol>
@endsection
@section('content') 
    <div class="row">    
    @foreach($urls as $url)
    <div class="col-md-4">
              <!-- Box Comment -->
              <div class="box box1 box-widget">
                <div class='box-header with-border'>
                  <div class='user-block'>
                    <img class='img-circle' src="/images/thumbnails/{{$url->profile_pic}}" alt='user image'>
                    <span class='username'><a href="#">{{$url->user->name}}</a></span>
                    <span class='description'>
                        {{Carbon::createFromTimeStamp(strtotime($url->created_at))->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime($url->created_at))))->diffForHumans() }}
                    </span>
                  </div><!-- /.user-block -->
                  <div class='box-tools'>                   
                    <?php if($url->user->id==Auth::user()->id){ ?>
                      <div class="pull-right">
                         <form class="form-horizontal" role="form" method="POST" 
                              action="{{ url('/url') }}">
                              
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <input type="hidden" name="_method" value="delete">
                              <input type="hidden" name="url_id" value="{{$url->id}}">
                              <button type="submit" class='btn btn-box-tool'><i class='fa fa-2x fa-trash'></i>
                              </button>
                          </form>
                      </div>
                    <?php } ?>     
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class='box-body'>                  
                  <h4 class="text-center"><a href="{{$url->url}}" target="_blank">{{$url->title}}</a></h4>
                  <p class="text-justify">{{$url->description}}</p>
                  <p class="text-center"><a href="{{$url->url}}" target="_blank">{{$url->url}}</a></p>
                </div><!-- /.box-body -->                           
              </div><!-- /.box -->
            </div><!-- /.col -->
        @endforeach
    </div>  

    <div class="row">
        <div class="container">    
            <div class="pagination"> {{$urls->links() }} </div>  
        </div>
    </div>        


@endsection

@section('script')
    <script>
        $( "#url" ).addClass( "active" );
    </script>
@endsection