@extends('layouts.teacher')
@section('header')
    <h1>
        Profile
        <small>Profile information</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">profile</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="box1 box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">My Subjects List</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        @foreach($subjects as $subject)
                            <li><a href="#"
                                   onclick="getStudentList({{$subject->id}})">{{$subject->subject}}</a>
                            </li>
                        @endforeach
                    </ul>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

        <div class="col-md-8" style="display: none" id="student-result">
            <div>
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Student Attendance</h3>
                    </div>
                    <div class="box-body">
                        <form action="/take-attendance" method="post" id="attendanceForm">
                            <input type="hidden" name="subject_id" value="" id="selectedSubject">
                            <input type="hidden" name="date" value="" id="selectedDate">
                            <input type="hidden" id="updateForm" name="update" value="0" id="selectedDate">

                            <div class="form-group col-md-3">
                                <label>Date</label>
                                <input type="text" class="form-control datepicker" name="date"
                                       id="attendanceDatePicker" value="">
                            </div>

                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <div class="form-group col-md-12">
                                <ul class="nav nav-pills nav-stacked" id="studentList">

                                </ul>
                            </div>

                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-primary">Done</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#attendance").addClass("active");

        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('.datepicker').datepicker({
            autoclose: true
        });

        function getStudentList(id) {

            $('#updateForm').attr("value",0);
            $('#selectedSubject').val(id)
            $('#attendanceForm').attr("action","/take-attendance");

            $.post("get-student-list",
                {
                    _token: '{{csrf_token()}}',
                    id: id,
                    date: $("#attendanceDatePicker").val()
                },
                function (data, status) {
                    console.log(data)

                    studentList = ''
                    for (i in data.student_list) {
                        if (data.student_list[i].attendance) {
                            studentList = studentList + '<li> <label><input type="checkbox" class="flat-red" name="attendance[]" checked value="' + data.student_list[i].id + '"> ' +
                                '&nbsp;&nbsp;&nbsp;&nbsp;' + data.student_list[i].fname + ' ' +
                                data.student_list[i].lname + ' ['+ data.student_list[i].total_attendance +'/'+ data.totalClass +']' + '</label></li>'
                        } else {
                            studentList = studentList + '<li> <label><input type="checkbox" class="flat-red" name="attendance[]" value="' + data.student_list[i].id + '"> ' +
                                '&nbsp;&nbsp;&nbsp;&nbsp;' + data.student_list[i].fname + ' ' +
                                data.student_list[i].lname + ' ['+ data.student_list[i].total_attendance +'/'+ data.totalClass +']' +'</label></li>'
                        }
                    }

                    for (i in data.student_list) {
                        studentList = studentList + '<input type="hidden" name="studentList[]" value="' + data.student_list[i].id + '">'
                    }

                    $("#studentList").html(studentList);

                    $('#student-result').css('display', 'block');

                    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                        checkboxClass: 'icheckbox_flat-green',
                        radioClass: 'iradio_flat-green'
                    });
                });
        }

        $("#attendanceDatePicker").datepicker().datepicker("setDate", new Date());

        $("#attendanceDatePicker").on('changeDate', function ($event) {

            $('#updateForm').attr("value",1);

            getStudentList( $('#selectedSubject').val() )

            {{--$.post("get-student-list",--}}
                {{--{--}}
                    {{--_token: '{{csrf_token()}}',--}}
                    {{--id: $('#selectedSubject').val(),--}}
                    {{--date: $("#attendanceDatePicker").val()--}}
                {{--},--}}
                {{--function (data, status) {--}}
                    {{--console.log(data)--}}

                    {{--studentList = ''--}}
                    {{--for (i in data.student_list) {--}}
                        {{--if (data.student_list[i].attendance) {--}}
                            {{--studentList = studentList + '<li> <label><input type="checkbox" class="flat-red" name="attendance[]" checked value="' + data.student_list[i].id + '"> ' +--}}
                                {{--'&nbsp;&nbsp;&nbsp;&nbsp;' + data.student_list[i].fname + ' ' +--}}
                                {{--data.student_list[i].lname + ' ['+ data.student_list[i].total_attendance +'/'+ data.totalClass +']' + '</label></li>'--}}
                        {{--} else {--}}
                            {{--studentList = studentList + '<li> <label><input type="checkbox" class="flat-red" name="attendance[]" value="' + data.student_list[i].id + '"> ' +--}}
                                {{--'&nbsp;&nbsp;&nbsp;&nbsp;' + data.student_list[i].fname + ' ' +--}}
                                {{--data.student_list[i].lname + ' ['+ data.student_list[i].total_attendance +'/'+ data.totalClass +']' + '</label></li>'--}}
                        {{--}--}}
                    {{--}--}}

                    {{--for (i in data.student_list) {--}}
                        {{--studentList = studentList + '<input type="hidden" name="studentList[]" value="' + data.student_list[i].id + '">'--}}
                    {{--}--}}

                    {{--$("#studentList").html(studentList);--}}

                    {{--$('#student-result').css('display', 'block');--}}

                    {{--$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({--}}
                        {{--checkboxClass: 'icheckbox_flat-green',--}}
                        {{--radioClass: 'iradio_flat-green'--}}
                    {{--});--}}
                {{--});--}}

        })

    </script>
@endsection
