@extends('layouts.teacher')
@section('header')
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">dashboard</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3 col-xs-12">
            <!-- small box -->
            <div class="box1 small-box bg-aqua">
                <div class="inner">
                    <h3>Time</h3>
                    <p>
                        {{Carbon::createFromTimeStamp(strtotime(Auth::user()->last_login_time))->toDayDateTimeString()}} 
                        <br>
                        {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime(Auth::user()->last_login_time))))->diffForHumans() }}
                    </p>                    
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>                
            </div>
        </div><!-- ./col -->
        <div class="col-md-3 col-xs-12">
            <!-- small box -->
            <div class="box1 small-box bg-green">
                <div class="inner">                    
                    <h3>IP</h3>
                    <p>{{Auth::user()->last_login_ip}}</p>

                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i> 
                </div>
                <a href="#" class="small-box-footer">Last Login IP <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-md-3 col-xs-12">
            <!-- small box -->
            <div class="box1 small-box bg-red">
                <div class="inner">                    
                    <h3>{{$totalFeedback}}</h3>
                    <p>Feedbacks</p>

                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i> 
                </div>
                <a href="#" class="small-box-footer">Total Feedbacks <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->

        <div class="col-md-3 col-xs-12">
            <!-- small box -->
            <div class="box1 small-box bg-aqua">
                <div class="inner">
                    <h3>{{$totalImage}}</h3>
                    <p>Images</p>
                </div>
                <div class="icon">
                    <i class="fa fa-image"></i>
                </div>
                <a href="#" class="small-box-footer">Total Images <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->

       
        {{--<div class="col-md-3 col-xs-12">--}}
            {{--<!-- small box -->--}}
            {{--<div class="box1 small-box bg-yellow">--}}
                {{--<div class="inner">--}}
                    {{--<h3>{{$totalPost}}</h3>--}}
                    {{--<p>Total Posts</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="fa fa-newspaper-o"></i>--}}
                {{--</div>--}}
                {{--<a href="#" class="small-box-footer">Total Posts<i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        {{--</div><!-- ./col -->        --}}
    </div><!-- /.row -->


    {{--<div class="row">--}}
        {{--<div class="col-md-3 col-xs-12">--}}
            {{--<!-- small box -->--}}
            {{--<div class="box1 small-box bg-yellow">--}}
                {{--<div class="inner">--}}
                    {{--<h3>{{$totalComment}}</h3>--}}
                    {{--<p>Total Comments</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="fa fa-info"></i>--}}
                {{--</div>--}}
                {{--<a href="#" class="small-box-footer">Total Comments <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        {{--</div><!-- ./col --> --}}

        {{--<div class="col-md-3 col-xs-12">--}}
            {{--<!-- small box -->--}}
            {{--<div class="box1 small-box bg-green">--}}
                {{--<div class="inner">                    --}}
                    {{--<h3>{{$totalUrl}}</h3>--}}
                    {{--<p>Urls</p>--}}

                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="fa fa-link"></i> --}}
                {{--</div>--}}
                {{--<a href="#" class="small-box-footer">Total Added Urls <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        {{--</div><!-- ./col -->--}}
        {{--<div class="col-md-3 col-xs-12">--}}
            {{--<!-- small box -->--}}
            {{--<div class="box1 small-box bg-red">--}}
                {{--<div class="inner">--}}
                    {{--<h3>0</h3>--}}
                    {{--<p>Inbox</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="fa fa-download"></i>--}}
                {{--</div>--}}
                {{--<a href="#" class="small-box-footer">Total Received Message <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        {{--</div><!-- ./col -->              --}}
    {{--</div><!-- /.row -->--}}

    {{--<div class="row">--}}
        {{--<div class="col-md-3 col-xs-12">--}}
            {{--<!-- small box -->--}}
            {{--<div class="box1 small-box bg-red">--}}
                {{--<div class="inner">--}}
                    {{--<h3>0</h3>--}}
                    {{--<p>Sent</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="fa fa-upload"></i>--}}
                {{--</div>--}}
                {{--<a href="#" class="small-box-footer">Total Sent Message <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        {{--</div><!-- ./col -->  --}}
    {{--</div><!-- /.row -->--}}

@endsection

@section('script')
    <script>
        $( "#dashboard" ).addClass( "active" );
    </script>
@endsection
