@extends('layouts.newteacher')
@section('header')
    <h1>
        Registration
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">registration</li>
    </ol>
@endsection
@section('content')

<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Registration Form</h3>
        </div>
        <div class="box-body">
            <div class="col-lg-10 col-lg-offset-1">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/registration-form') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-md-5">
                               <img class="img img-responsive img-thumbnail center-block" src="/images/thumbs/{{$profilePic->file_name}}" alt="User profile picture">
                        </div>                  
                        
                        <!--Fname -->
                        <div class="row">
                            <div class="col-md-5 col-md-offset-1"><br><br><br><br>
                                <div class="input-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First-Name">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                </div><br>
                                @if ($errors->has('first_name'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </label>
                                    </div><br>
                                @endif

                                <div class="input-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" name="middle_name" value="{{ old('middle_name') }}" placeholder="Middle-Name">
                                </div><br>
                                @if ($errors->has('middle_name'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('middle_name') }}</strong>
                                        </label>
                                    </div><br>
                                @endif

                                <div class="input-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last-Name">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                </div><br>
                                @if ($errors->has('last_name'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </label>
                                    </div><br>
                                @endif

                            </div>
                        </div><br>

                        <div class="row">
                            <!--Name of book -->
                            <div class="col-md-6">
                                <div class="input-group{{ $errors->has('parent_name') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" name="parent_name" value="{{ old('parent_name') }}" placeholder="Parent-Name">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                </div>
                                @if ($errors->has('parent_name'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('parent_name') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <!--Name of author -->
                            <div class="col-md-6">
                                <div class="input-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="date" class="form-control" name="date_of_birth" value="{{ old('date_of_birth') }}">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                </div>
                                @if ($errors->has('date_of_birth'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('date_of_birth') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div><br>

                        <div class="row">
                            <!--Name of book -->
                            <div class="col-md-4 col-md-offset-1" >
                                <div class="icheck radio col-md-6">
                                    <input type="radio" name="gender" value="1"
                                           @if (old('gender')==1)
                                           checked
                                            @endif>
                                    <label class="col-md-6 control-label"><i class="fa fa-2x fa-male"></i></label>
                                </div>            
                                <div class="icheck radio col-md-6">
                                    <input type="radio" name="gender" value="0"
                                           @if (old('gender')==0)
                                           checked
                                            @endif>
                                    <label class="col-md-6 control-label"><i class="fa fa-2x fa-female"></i></label>
                                </div>
                            </div>
                            <!--Name of author -->
                            <div class="col-md-6 col-md-offset-1">
                                <div class="input-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                    <select class="form-control" name="religion">

                                        <option
                                                @if(old('religion')=="Hinduism")
                                                selected
                                                @endif
                                        >Hinduism</option>
                                        <option
                                                @if(old('religion')=="Buddhism")
                                                selected
                                                @endif
                                        >Buddhism</option>
                                        <option
                                                @if(old('religion')=="Islam")
                                                selected
                                                @endif
                                        >Islam</option>
                                        <option
                                                @if(old('religion')=="Christianity")
                                                selected
                                                @endif
                                        >Christianity</option>
                                        <option
                                                @if(old('religion')=="Other")
                                                selected
                                                @endif
                                        >Other</option>

                                    </select>
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                </div>
                                @if ($errors->has('religion'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('religion') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div><br>

                        <div class="row">
                            <!--Name of book -->
                            <div class="col-md-6">
                                <div class="input-group{{ $errors->has('permanent_address') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                    <input type="text" class="form-control" name="permanent_address" value="{{ old('permanent_address') }}" placeholder="Permanent-Address">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                </div>
                                @if ($errors->has('permanent_address'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('permanent_address') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <!--Name of author -->
                            <div class="col-md-6">
                                <div class="input-group{{ $errors->has('current_address') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                    <input type="text" class="form-control" name="current_address" value="{{ old('current_address') }}" placeholder="Current-Address">                                    
                                </div>
                                @if ($errors->has('current_address'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('current_address') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div><br>                    

                        <div class="row">
                            <!--Name of book -->
                            <div class="col-md-6">
                                <div class="input-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                    <input type="number" class="form-control" name="mobile_number" value="{{ old('mobile_number') }}" placeholder="mobile_number">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                </div>
                                @if ($errors->has('mobile_number'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('mobile_number') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                            <!--Name of author -->
                            <div class="col-md-6">
                                <div class="input-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input type="number" class="form-control" name="phone_number" value="{{ old('phone_number') }}" placeholder="Phone Number">
                                </div>
                                @if ($errors->has('phone_number'))
                                    <div class="input-group has-error">
                                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                        </label>
                                    </div>
                                @endif
                            </div>
                        </div><br>

                        <div class="row">
                            <div class="col-md-4 pull-right">
                                <div class="input-group"><br>
                                    <button type="submit" class="btn btn-primary btn-flat">
                                        <i class="fa fa-plus"></i> Submit Form &nbsp
                                    </button>
                                </div>
                            </div>
                        </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script>
        $( "#dashboard" ).addClass( "active" );
    </script>
@endsection