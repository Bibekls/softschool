@extends('layouts.student')
@section('header')
    <h1>
        News Feed
        <small>view posts from all users</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">newsfeed</li>
    </ol>
@endsection
@section('content') 

    @foreach($newsfeed as $post)
        <?php if($counter%3==0){?>
            <div class="row">
        <?php   } ?>

        <div class="col-md-4">
          <!-- Box Comment -->
          <div class="box box1 box-widget">
            <div class='box-header with-border'>
              <div class='user-block'>
                <img class='img-circle' src="/images/thumbnails/{{$post['profile_pic']->profile_pic}}" alt='user image'>
                <span class='username'><a href="#">{{$post['post']->user->name}}</a></span>
                <span class='description'>{{$post['postTime']}}</span>
              </div><!-- /.user-block -->
              <div class='box-tools'> 
                <?php if($post['post']->user->id==Auth::user()->id){ ?>
                  <form class="form-horizontal" role="form" method="POST" 
                  action="{{ url('/post') }}">                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="post_id" value="{{$post['post']->id}}">
                    <button type="submit" class='btn btn-box-tool'><i class='fa fa-2x fa-trash'></i>
                    </button>
                  <?php } ?>
                </form>                                                  
              </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class='box-body'>
              <p class="text-justify"><a>{{$post['post']->post}}</a></p>                 
              <span class='pull-right text-muted'>{{ $post['comments']->count()}} comments</span>
            </div><!-- /.box-body -->
            <div class='box-footer box-comments'>
              <div class="box box-danger collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Read Comments</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">

            @foreach( $post['comments'] as $comment)
              <div class='box-comment'>
                <!-- User image -->
                <img class='img-circle img-sm' src="/images/thumbnails/{{$comment->profile_pic}}" alt='user image'>
                <div class='comment-text'>
                  <span class="username">
                   {{$comment->username}}

                      <?php if($comment->user_id==Auth::user()->id){ ?>
                        <div class="pull-right">
                           <form class="form-horizontal" role="form" method="POST" 
                                action="{{ url('/comment') }}">
                                
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="comment_id" value="{{$comment->id}}">
                                <button type="submit" class='btn btn-box-tool'><i class='fa fa-trash'></i>
                                </button>
                            </form>
                        </div>
                      <?php } ?>
                    <br>
                    <span class='text-muted'>
                        {{Carbon::createFromTimeStamp(strtotime($comment->time))->toDayDateTimeString()}} {{Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp(strtotime($comment->time))))->diffForHumans() }}
                    </span>
                  </span><!-- /.username -->
                  <br>
                      <p class="text-justify">{{$comment->comment}}</p>
                </div><!-- /.comment-text -->
              </div><!-- /.box-comment -->
              @endforeach 
              </div>
              </div>
                              
            </div><!-- /.box-footer -->
            <div class="box-footer">
              <form action="{{url('/comment')}}" method="POST">
                  <input type="hidden" name="post_id" value="{{$post['post']->id}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <img class="img-responsive img-circle img-sm" src="images/thumbnails/{{$profile_pic->file_name}}" alt="alt text">
                <!-- .img-push is used to add margin to elements next to floating images -->
                <div class="img-push">
                  <input type="text" class="form-control input-sm" placeholder="Press enter to post comment. 140 character only" name="comment">
                </div>
              </form>
            </div><!-- /.box-footer -->
          </div><!-- /.box -->
        </div><!-- /.col -->

        <?php $counter++; if( $counter%3==0) { ?>
          </div>
        <?php   } ?>
     @endforeach

     <?php if( $counter%3!=0) { ?>
          </div>
      <?php   } ?>

    <div class="row">
        <div class="container">    
            <div class="pagination"> {{ $pagelink->links() }} </div>  
        </div>
    </div>


@endsection

@section('script')
    <script>
        $( "#newsfeed" ).addClass( "active" );
    </script>
@endsection