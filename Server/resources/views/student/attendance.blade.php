@extends('layouts.student')
@section('header')
    <h1>
        Student Attendance
        <small>View attendance</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">student-attendance</li>
    </ol>
@endsection
@section('style')
<style>  
  .danger{
    color: #ff4d4d;
  }
  .primary{
    color:#004d99;
  }
</style>

<script type="text/javascript">
    function dayString(id){
      if(id==1) return 'Sunday';
      else if(id==2) return 'Monday';
      else if(id==3) return 'Tuesday';
      else if(id==4) return 'Wednesday';
      else if(id==5) return 'Thursday';
      else if(id==5) return 'Friday';
      else if(id==5) return 'Saturday';
    }
</script>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">View all attendance</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                </div>
                <div class="box-body">  
                  
                  <form class="form-inline" method="POST" action="{{ url('/attendance') }}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">                     

                      <div class="form-group col-md-4 col-md-offset-6">
                        <label>Date range </label>

                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right reservation" name="date-range" value="{{$dateRange}}">
                        </div>
                        <!-- /.input group -->
                      </div>

                      <div class="form-group col-md-2">                          
                          <button class="btn btn-primary btn-flat btn-block">Done</button>
                      </div>
                  </form>

                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Date</th>                        
                        @foreach($batch['subject-list'] as $subject)
                            <th>{{$subject['subject_name']}}</th>
                        @endforeach
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($batch['date'] as $day)
                        <tr>
                          <td>
                            {{$day['current-date']}} 
                            [
                              <script type="text/javascript">
                                  d=new Date('{{$day['current-date']}}');                                
                                  document.write(dayString(d.getDay()));
                              </script>
                            ]
                          </td>
                          
                          @foreach($day['subjects'] as $subAttendance)
                          @foreach($subAttendance as $attendanceStatus)                                    
                            <td>
                              @if($attendanceStatus->attendance==1)
                                <i class="fa fa-check fa-2x primary"></i>
                              @elseif($attendanceStatus->attendance==0)
                                <i class="fa fa-times fa-2x danger"></i>
                              @endif
                            </td>                                
                          @endforeach
                          @endforeach
                        </tr> 
                      @endforeach                                                                        
                    </tbody>
                  </table>                            
                </div>
            </div>
    </div>
</div>

@endsection

@section('script')
    <script>
        $( "#attendance" ).addClass( "active" );
    </script>

<script>
  $(function () {

    $('.reservation').daterangepicker();
    //Date picker
    });     
  
</script>
@endsection