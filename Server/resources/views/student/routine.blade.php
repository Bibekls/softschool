@extends('layouts.student')
@section('header')
    <h1>
        Subject Routine
        <small>list Routine</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">subject-routine</li>
    </ol>
@endsection
@section('style')
<style>
  .progress-block {height: 60px !important;}
</style>
@endsection
@section('content')
<div class="box1 box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Class Routine</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
          <div class="col-md-12">

      <table class="table table-condensed">
        <thead>
          <tr>
            <th>Day</th>
            <th>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" style="width:11.11%">
                    06:00 - 06:30                                   
                  </div>
                  <div class="progress-bar  progress-bar-info" role="progressbar" style="width:11.11%">
                    06:30 - 07:00                                   
                  </div>
                  <div class="progress-bar" role="progressbar" style="width:11.11%">
                    07:00 - 07:30                                   
                  </div>
                  <div class="progress-bar  progress-bar-info" role="progressbar" style="width:11.11%">
                    07:30 - 08:00                                   
                  </div>
                  <div class="progress-bar" role="progressbar" style="width:11.11%">
                    08:00 - 08:30                                   
                  </div>
                  <div class="progress-bar  progress-bar-info" role="progressbar" style="width:11.11%">
                    08:30 - 09:00                                   
                  </div>
                  <div class="progress-bar" role="progressbar" style="width:11.11%">
                    09:00 - 09:30                                   
                  </div>
                  <div class="progress-bar  progress-bar-info" role="progressbar" style="width:11.11%">
                    09:30 - 10:00                                   
                  </div> 
                  <div class="progress-bar" role="progressbar" style="width:11.11%">
                    10:00 - 10:30                                
                  </div>                                                                      
                </div>
            </th>                              
          </tr>
        </thead>
        <tbody>
          @foreach($routines as $day)
          <tr>
          
            <td>@if($day['day']==1) Sunday 
                @elseif($day['day']==2) Monday 
                @elseif($day['day']==3) Tuesday 
                @elseif($day['day']==4) Wednesday
                @elseif($day['day']==5) Thursday
                @elseif($day['day']==6) Friday
                @elseif($day['day']==7) Saturday
                @endif
            </td>
            <td>
              <div class="progress progress-block ">
                <?php $counterTR=0; $previousPeriod=21600; $break=false;?>
                @foreach($day['period'] as $period)
                    <?php
                          //6:00 - 11:30
                          if($period->start_at-$previousPeriod>300)
                          $break=true;
                          else
                          $break=false;

                          $counterTR++;
                          $totalTime=16200;                                            
                          $barwidth=($period->end_at-$period->start_at)/$totalTime;
                          $breakWidht=($period->start_at-$previousPeriod)/$totalTime;
                          $previousPeriod=$period->end_at;
                      ?>
                    @if($break)
                        <div class="progress-bar" role="progressbar"
                          style="width:{{$breakWidht*100}}%"> --
                        </div>
                    @endif

                    <div class="progress-bar 
                                  @if($counterTR%4==0) progress-bar-success
                                  @elseif($counterTR%4==1) progress-bar-warning
                                  @elseif($counterTR%4==2) progress-bar-success
                                  @elseif($counterTR%4==3) progress-bar-warning
                                  @endif" 
                      role="progressbar"
                      style="width:{{$barwidth*100}}%">
                      {{$period->subject}}<br>{{$period->fname}}-{{$period->lname}}
                    </div>                                       
                 @endforeach                                  
              </div>                                                              
            </td>
          </tr>                           
          @endforeach           
        </tbody>
      </table>
</div>
    </div><!-- /.box-body -->
</div><!-- /.box -->


@endsection

@section('script')
    <script>
        $( "#routine" ).addClass( "active" );
    </script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    $(".timepicker").timepicker({
      showInputs: false
    });  
  });     
  
</script>
@endsection