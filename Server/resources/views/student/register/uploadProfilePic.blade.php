@extends('layouts.newstudent')
@section('header')
    <h1>
        Registration
        <small>Upload Profile Pic</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>home</a></li>
        <li class="active">registration</li>
    </ol>
@endsection
@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Upload Profile Picture</h3>
        </div>
        <div class="box-body">
            <div class="col-lg-10 col-lg-offset-1">                
                <div class="col-md-3 col-xs-3">
                    <img src="/img/male-face.jpg" class="img img-responsive img-rounded">
                </div>   
                <div class="col-md-6 col-xs-6">
                    <div class="box box-primary">
                        <i><h4 class="text-center">You must upload you profile-image in college uniform. <br>Maximum image size is 2 MB.</h4></i>
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" src="/img/male.jpg" alt="User profile picture">
                            <form action="/upload-profile-pic" class="dropzone " id="addImages">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="profilePic" value="{{Auth::user()->id}}">

                            </form>
                            <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>
                            <p class="text-muted text-center">Student</p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Email</b> <a class="pull-right">{{Auth::user()->email}}</a>
                                </li>
                            </ul>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-3 col-xs-3">
                    <img src="/img/female-face.jpg" class="img img-responsive img-rounded">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $( "#dashboard" ).addClass( "active" );
    </script>
@endsection