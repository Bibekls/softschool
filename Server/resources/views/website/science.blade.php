@extends('layouts.pages')

@section('title')
    Science
@endsection
@section('meta_description')

@endsection
@section('meta_keywords')

@endsection
@section('meta_author')

@endsection


@section('content')
        <!-- 
    ================================================== 
        Global Page Section Start
    ================================================== -->
    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>+2 Science</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">
                                    <i class="ion-ios-home"></i>
                                    Home
                                </a>
                            </li>
                            <li class="active">Science</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- 
    ================================================== 
        Company Description Section Start
    ================================================== -->
    <section class="company-description">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                    <img src="img/about/about-science.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <div class="block">
                        <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Introduction</h3>
                        <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                            The Higher Secondary Education Board (HSEB) is involved in running the 10 + 2 system in the country in both English and Nepali medium except for Science which is available in English only. Streams offered at +2 Science Management We follows the text books recommended by the HSEB. Besides, we emphasize on ECAs/CCAs and additional life skills. Divided into two years, Saipal provides HSEB Management, Science, and Humanities programmes which stress on the importance of broad educational background for accomplishing its objectives at the higher study level. At present ZIHSS offers the following courses at +2 levels.
                        </p>
                    </div>                
                </div>                
            </div>
        </div><br>
    </section>
    

    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">DO U WANT TO LEARN ABOUT BSc.CSIT ?</h2>
                        <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Bachelor of Science in Computer Science and Information Technology (B. Sc. CSIT), affiliated to T.U. is the course composition of 4 years IT and computer courses. 
                        <br>For fulfilling the need of the job market in the field of IT profession, many elective courses have been also introduced. The program comprises computer science and allied courses of 126 credit hours.
                        </p>
                        <a href="/bsccsit" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Learn More</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--
    ==================================================
    Footer Section Start
    ================================================== -->
@endsection