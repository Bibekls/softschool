<br><br><br><br>
<h1><center>Welcome to Hetauda City College</center></h1>

{{--@extends('layouts.pages')--}}

{{--@section('title')--}}
    {{--Home--}}
{{--@endsection--}}
{{--@section('meta_description')--}}

{{--@endsection--}}
{{--@section('meta_keywords')--}}

{{--@endsection--}}
{{--@section('meta_author')--}}

{{--@endsection--}}


{{--@section('content')    --}}
    {{----}}
        {{----}}
        {{--<!----}}
        {{--==================================================--}}
        {{--Slider Section Start--}}
        {{--================================================== -->--}}
        {{--<section id="hero-area" >--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12 text-center">--}}
                        {{--<div class="block wow fadeInUp" data-wow-delay=".3s">--}}
                            {{----}}
                            {{--<!-- Slider -->--}}
                            {{--<section class="cd-intro">--}}
                                {{--<h1 class="wow fadeInUp animated cd-headline slide" data-wow-delay=".4s" >--}}
                                {{--<span>WELCOME TO HETAUDA CITY COLLEGE</span><br>--}}
                                {{--<span class="hidden-xs cd-words-wrapper">--}}
                                    {{--<b class="is-visible text-center">First IT college of <br>Hetauda</b>--}}
                                    {{--<b class="text-center">Success is our <br>address</b>--}}
                                    {{--<b class="text-center">Enrich yourself on <br>global perspective</b>--}}
                                {{--</span>--}}
                                {{--</h1>--}}
                                {{--</section> <!-- cd-intro -->--}}
                                {{--<!-- /.slider -->--}}
                                {{--<h2 class="wow fadeInUp animated" data-wow-delay=".6s" >--}}
                                    {{--Celebrating 3 Year of success<br>                                    --}}
                                {{--</h2>--}}
                                {{--<a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green" data-wow-delay=".9s" href="#works" data-section="#works" >Start</a>--}}
                                {{----}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section><!--/#main-slider-->--}}
            {{--<!----}}
            {{--==================================================--}}
            {{--Slider Section Start--}}
            {{--================================================== -->--}}
            {{--<!----}}
            {{--==================================================--}}
            {{--Portfolio Section Start--}}
            {{--================================================== -->--}}
            {{--<section id="works" class="works">--}}
                {{--<div class="container">--}}
                    {{--<div class="section-heading">--}}
                        {{--<h1 class="title wow fadeInDown" data-wow-delay=".3s">Latest Works</h1>--}}
                        {{--<p class="wow fadeInDown" data-wow-delay=".5s">                            --}}
                            {{--Our recent three work is listed below.<br>                       --}}
                        {{--</p>--}}
                        {{--<a href="/allwork" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">View All Work</a>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                        {{--@foreach($works as $work)--}}
                        {{--<div class="col-sm-4 col-xs-12">--}}
                            {{--<figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">--}}
                                {{--<div class="img-wrapper">--}}
                                    {{--<img src="{{$work->banner}}" class="img-responsive" alt="this is a title" >--}}
                                    {{--<div class="overlay">--}}
                                        {{--<div class="buttons">--}}
                                            {{--<a rel="gallery" class="fancybox" href="{{$work->banner}}">Demo</a>--}}
                                            {{--<a target="_blank" href="/work/{{$work->id}}">Details</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<figcaption>--}}
                                {{--<h4>--}}
                                {{--<a href="/work/{{$work->id}}">--}}
                                    {{--{{$work->title}}--}}
                                {{--</a>--}}
                                {{--</h4>--}}
                                {{--<p>--}}
                                    {{--{{$work->summary}}--}}
                                {{--</p>--}}
                                {{--</figcaption>--}}
                            {{--</figure>--}}
                        {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section> <!-- #works -->--}}
            {{--<!----}}
            {{--==================================================--}}
            {{--Portfolio Section Start--}}
            {{--================================================== -->--}}
            {{--<section id="feature">--}}
                {{--<div class="container">--}}
                    {{--<div class="section-heading">--}}
                        {{--<h1 class="title wow fadeInDown" data-wow-delay=".3s">Facility</h1>--}}
                        {{--<p class="wow fadeInDown" data-wow-delay=".5s">--}}
                            {{--We offer best facility<br>--}}
                          {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-4 col-lg-4 col-xs-12">--}}
                            {{--<div class="media wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">--}}
                                {{--<div class="media-left">--}}
                                    {{--<div class="icon">--}}
                                        {{--<i class="ion-ios-book-outline"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="media-body">--}}
                                    {{--<h4 class="media-heading">Library</h4>--}}
                                    {{--<p>Library at HCC is resourceful. It is enriched with basic textbooks, reference materials, journals, magazines and newspapers.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-lg-4 col-xs-12">--}}
                            {{--<div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="600ms">--}}
                                {{--<div class="media-left">--}}
                                    {{--<div class="icon">--}}
                                        {{--<i class="ion-android-bus"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="media-body">--}}
                                    {{--<h4 class="media-heading">Bus Facility</h4>--}}
                                    {{--<p>The HCC college offers comfortable and reliable transportation service for non-residential students coming from different parts of Hetauda.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-lg-4 col-xs-12">--}}
                            {{--<div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="900ms">--}}
                                {{--<div class="media-left">--}}
                                    {{--<div class="icon">--}}
                                        {{--<i class="ion-ios-home-outline"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="media-body">--}}
                                    {{--<h4 class="media-heading">Hostel</h4>--}}
                                    {{--<p>The HCC college offers comfortable ,reliable and provides separate hostels for boys and girls.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-4 col-lg-4 col-xs-12">--}}
                            {{--<div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="1200ms">--}}
                                {{--<div class="media-left">--}}
                                    {{--<div class="icon">--}}
                                        {{--<i class="ion-ios-monitor-outline"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="media-body">--}}
                                    {{--<h4 class="media-heading">ICT Lab</h4>--}}
                                    {{--<p>The college has well-equipped computer lab with unlimited internet facilities. A Major portion of teaching is computer supported.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-lg-4 col-xs-12">--}}
                            {{--<div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="1500ms">--}}
                                {{--<div class="media-left">--}}
                                    {{--<div class="icon">--}}
                                        {{--<i class="ion-ios-videocam-outline"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="media-body">--}}
                                    {{--<h4 class="media-heading">Audio Visual Class</h4>--}}
                                    {{--<p>Audio visual class are available for presentation and seminar. The student present their presentation on Smart board in ICT lab. Teacher use Audio visual class to make the effective teaching learning process.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-4 col-lg-4 col-xs-12">--}}
                            {{--<div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="1500ms">--}}
                                {{--<div class="media-left">--}}
                                    {{--<div class="icon">--}}
                                        {{--<i class="ion-ios-videocam-outline"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="media-body">--}}
                                    {{--<h4 class="media-heading">Audio Visual Class</h4>--}}
                                    {{--<p>Audio visual class are available for presentation and seminar. The student present their presentation on Smart board in ICT lab. Teacher use Audio visual class to make the effective teaching learning process.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{----}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section> <!-- /#feature -->--}}
                            {{----}}
            {{--<!----}}
            {{--==================================================--}}
            {{--Call To Action Section Start--}}
            {{--================================================== -->--}}
            {{--<section id="call-to-action">--}}
                {{--<div class="container">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<div class="block">--}}
                                {{--<h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">DO U WANT TO LEARN ABOUT BSc.CSIT ?</h2>--}}
                                {{--<p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Bachelor of Science in Computer Science and Information Technology (B. Sc. CSIT), affiliated to T.U. is the course composition of 4 years IT and computer courses. --}}
                                {{--<br>For fulfilling the need of the job market in the field of IT profession, many elective courses have been also introduced. The program comprises computer science and allied courses of 126 credit hours.--}}
                                {{--</p>--}}
                                {{--<a href="/bsccsit" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Learn More</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{----}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section>--}}
            {{--<!----}}
            {{--==================================================--}}
            {{--Footer Section Start--}}
            {{--================================================== -->--}}
{{--@endsection--}}