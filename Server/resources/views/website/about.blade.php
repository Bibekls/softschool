@extends('layouts.pages')

@section('title')
    About
@endsection
@section('meta_description')

@endsection
@section('meta_keywords')

@endsection
@section('meta_author')

@endsection


@section('content')
        <!-- 
    ================================================== 
        Global Page Section Start
    ================================================== -->
    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>About Hetauda City College</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">
                                    <i class="ion-ios-home"></i>
                                    Home
                                </a>
                            </li>
                            <li class="active">About</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- 
    ================================================== 
        Company Description Section Start
    ================================================== -->
    <section class="company-description">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                    <img src="img/about/about-company.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <div class="block">
                        <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Introduction</h3>
                        <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                            Hetauda City College/ZIHSS, is home to an intensive study and research, several nationally ranked programs of graduate study and a vibrant research community that is committed to making a difference in practice.
                            <br>
                            Zenith Int’l Higher Secondary School was established in 2052 B.S. formerly known as Shining Star Boarding School was renamed as ZIHSS in 2067 BS. ZIHSS provides quality education from play group to class 12 (Science and Management). And ZIHSS is one and only successful higher secondary school established by private sector.
                            <br>
                            Hetauda City College was established in 2070 BS with sole motto of providing Quality Information Technology Education in Makawanpur District. We started B.Sc. CSIT (Computer Science & Information Technology ) in 2070 BS (Bachelor in Business Studies) and BBS in 2071 BS and under affiliation process to start BCA ( Bachelor in Computer Education) under Tribhuwan University. We are proud to say that we are “first IT college of Hetauda”.

                        </p>                        
                        
                    </div>                
                </div>
                <div class="col-md-12">
                    <div class="block">
                        <p  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                            Under the guidance of renowned administrators and faculty members in different departments, students have opportunities to pursue or advance careers in Computer Science and Information Technology (TU, BSc. CSIT), Business Studies (TU, BBS), Science and Management (+2) and Bachelor in Computer Application (TU, Proposed from year 2072)
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- 
    ================================================== 
        Company Feature Section Start
    ================================================== -->
    <section class="hidden-xs about-feature clearfix">
        <div class="container-fluid">
            <div class="row">
                <div class="block about-feature-1 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">
                    <h2>
                    Why Choose Us ?
                    </h2>
                    <p>
                       Innovative teacher education programs, outstanding faculty and strong research opportunities make Hetauda City College a dynamic place for learning to teach and to contribute to the fields of Science, Technology and Management
                    </p>
                </div>
                <div class="block about-feature-2 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                    <h2 class="item_title">
                    Academic Areas
                    </h2>
                    <p>
                        Under the guidance of renowned administrators and faculty members in different departments, students have opportunities to pursue or advance careers in Computer Science and Information Technology, Business Studies, Computer Application, Social work, Science and management
                    </p>
                </div>
                <div class="block about-feature-3 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".7s">
                    <h2 class="item_title">
                    Key Priorities
                    </h2>
                    <p>
                        The College of Science and technology has established a reputation for excellence and visionary thinking in its efforts to improve teaching and learning across our nation and world, particularly within the contexts of urban and global education. The faculty remains committed to addressing the educational and physical needs of all learners across the life span, and to working closely with educators, leaders and policymakers in the field.
                    </p>
                </div>
            </div>
        </div>
    </section>


    <!-- 
    ================================================== 
        Team Section Start
    ================================================== -->
    <section id="team">
        <div class="container">
            <div class="row">
                <h2 class="subtitle text-center">Meet The Team</h2>
                <div class="col-md-3">
                    <div class="team-member wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".3s">
                        <div class="team-img">
                            <img src="img/team/team-1.jpg" class="team-pic" alt="">
                        </div>
                        <h3 class="team_name">Anil Pandey</h3>
                        <p class="team_designation">Principal,Founder</p>
                        <p class="team_text">Works at top level of College and School</p>
                        <p class="social-icons">
                            <a href="#" class="facebook" target="_blank"><i class="ion-social-facebook-outline"></i></a>
                            <a href="#" target="_blank"><i class="ion-social-twitter-outline"></i></a>
                            <a href="#" target="_blank"><i class="ion-social-googleplus-outline"></i></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-member wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".5s">
                        <div class="team-img">
                            <img src="img/team/team-2.jpg" class="team-pic" alt="">
                        </div>
                        <h3 class="team_name">Madhu Kumar Sing</h3>
                        <p class="team_designation">Adviser of CSIT program</p>
                        <p class="team_text">Responsible for all the activities of CSIT.</p>
                        <p class="social-icons">
                            <a href="#" class="facebook" target="_blank"><i class="ion-social-facebook-outline"></i></a>
                            <a href="#" target="_blank"><i class="ion-social-twitter-outline"></i></a>
                            <a href="#" target="_blank"><i class="ion-social-googleplus-outline"></i></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-member wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".7s">
                        <div class="team-img">
                            <img src="img/team/team-3.jpg" class="team-pic" alt="">
                        </div>
                        <h3 class="team_name">Arun Rumba</h3>
                        <p class="team_designation">Co-ordinator of CSIT</p>
                        <p class="team_text">Co-ordinate all the activities of CSIT.</p>
                        <p class="social-icons">
                            <a href="#" class="facebook" target="_blank"><i class="ion-social-facebook-outline"></i></a>
                            <a href="#" target="_blank"><i class="ion-social-twitter-outline"></i></a>
                            <a href="#" target="_blank"><i class="ion-social-googleplus-outline"></i></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-member wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".9s">
                        <div class="team-img">
                            <img src="img/team/team-4.jpg" class="team-pic" alt="">
                        </div>
                        <h3 class="team_name">Lekh Nath Paudel</h3>
                        <p class="team_designation">Co-ordinator of Science Faculty</p>
                        <p class="team_text">Responsible for Science Faculty.</p>
                        <p class="social-icons">
                            <a href="#" class="facebook" target="_blank"><i class="ion-social-facebook-outline"></i></a>
                            <a href="#" target="_blank"><i class="ion-social-twitter-outline"></i></a>
                            <a href="#" target="_blank"><i class="ion-social-googleplus-outline"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>   
   <!--
    ==================================================
    Call To Action Section Start
    ================================================== -->
    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">DO U WANT TO LEARN ABOUT BSc.CSIT ?</h1>
                        <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Bachelor of Science in Computer Science and Information Technology (B. Sc. CSIT), affiliated to T.U. is the course composition of 4 years IT and computer courses. 
                        <br>For fulfilling the need of the job market in the field of IT profession, many elective courses have been also introduced. The program comprises computer science and allied courses of 126 credit hours.
                        </p>
                        <a href="/bsccsit" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Learn More</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--
    ==================================================
    Footer Section Start
    ================================================== -->
@endsection