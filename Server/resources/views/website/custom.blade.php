@extends('layouts.pages')

@section('title')
    {{$page->title}}
@endsection
@section('meta_description')

@endsection
@section('meta_keywords')

@endsection
@section('meta_author')

@endsection

@section('style')

@endsection

@section('content')
    
    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>{{$page->title}}</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">
                                    <i class="ion-ios-home"></i>
                                    Home
                                </a>
                            </li>
                            <li class="active">{{$page->name}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>   
    </section>

    <section>
        <br><br><br>
        <div class="container-fluid">
            <div class="col-md-2">
                <ul type="square">
                    @foreach($PageCategory->pages as $pagelist)        
                        <li>
                            <h4><a href="{{$pagelist->tag}}">{{$pagelist->name}}</a></h4>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-10">
                <div class="reset-css">
                    <?php echo $page->content; ?>
                </div>
            </div>
        </div>
    </section>

    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">DO U WANT TO LEARN ABOUT BSc.CSIT ?</h2>
                        <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Bachelor of Science in Computer Science and Information Technology (B. Sc. CSIT), affiliated to T.U. is the course composition of 4 years IT and computer courses. 
                        <br>For fulfilling the need of the job market in the field of IT profession, many elective courses have been also introduced. The program comprises computer science and allied courses of 126 credit hours.
                        </p>
                        <a href="/bsccsit" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Learn More</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
        
@endsection

 