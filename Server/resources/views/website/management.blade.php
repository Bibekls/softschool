@extends('layouts.pages')

@section('title')
    Management
@endsection
@section('meta_description')

@endsection
@section('meta_keywords')

@endsection
@section('meta_author')

@endsection


@section('content')
        <!-- 
    ================================================== 
        Global Page Section Start
    ================================================== -->
    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>+2 Management</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">
                                    <i class="ion-ios-home"></i>
                                    Home
                                </a>
                            </li>
                            <li class="active">+2 Management</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- 
    ================================================== 
        Company Description Section Start
    ================================================== -->
    <section class="company-description">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                    <img src="img/about/about-management.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <div class="block">
                        <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Introduction</h3>
                        <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                            Affiliated to HSEB, the objective of the college in running this program has been to communicate demanding and incorporated management education from an early age so that students find their later University undergraduate education a smoother sailing. The college follows scientific teaching technique, allowing students to come up to their subjects with a dedicated insight. As a result, they come up with a clear model of the subjects and develop their own idea about them. Consequently, it helps the students track their higher studies with confidence. Students can decide from an extensive array of subjects ranging from Hotel Management, Travel & Tourism and Computer Science among others to support and augment their future careers.
                        </p>                        
                        
                    </div>                
                </div>                
            </div>
        </div><br>
    </section>

    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">DO U WANT TO LEARN ABOUT BSc.CSIT ?</h2>
                        <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Bachelor of Science in Computer Science and Information Technology (B. Sc. CSIT), affiliated to T.U. is the course composition of 4 years IT and computer courses. 
                        <br>For fulfilling the need of the job market in the field of IT profession, many elective courses have been also introduced. The program comprises computer science and allied courses of 126 credit hours.
                        </p>
                        <a href="/bsccsit" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Learn More</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--
    ==================================================
    Footer Section Start
    ================================================== -->
@endsection