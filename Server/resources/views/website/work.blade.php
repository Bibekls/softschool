@extends('layouts.pages')

@section('title')
    Work
@endsection
@section('meta_description')

@endsection
@section('meta_keywords')

@endsection
@section('meta_author')

@endsection


@section('content')
    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>{{$work->title}}</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">
                                    <i class="ion-ios-home"></i>
                                    Home
                                </a>
                            </li>
                            <li class="active">{{$work->title}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>   
    </section>

    <section><br><br>
        <div class="container">
            <div class="col-md-4">
                <img src="{{$work->banner}}" class="img img-responsive">
            </div>

            <div class="col-md-8">
                <h1 class="text-center">{{$work->summary}}</h1>
                <h3 class="text-center">Work Category : {{$work->workCategory->name}}</h3>
                <h3 class="text-center">Created Date : {{$work->created_at}}</h3>
            </div>
        </div>
    </section>
    <hr>
    <section>
        <div class="container">
            <div class="col-md-12">
                <?php echo $work->content; ?>
            </div>
        </div>
    </section>

    <section id="works" class="works">
        <div class="container">
            <div class="section-heading">
                <h1 class="title wow fadeInDown" data-wow-delay=".3s">Related Works</h1>
                <p class="wow fadeInDown" data-wow-delay=".5s">                            
                    Related works is listed below.<br>
                </p>
                <a href="/allwork" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">View All Work</a>
            </div>
            <div class="row">
                @foreach($relatedWork as $work)
                <div class="col-sm-4 col-xs-12">
                    <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                        <div class="img-wrapper">
                            <img src="{{$work->banner}}" class="img-responsive" alt="this is a title" >
                            <div class="overlay">
                                <div class="buttons">
                                    <a rel="gallery" class="fancybox" href="{{$work->banner}}">Demo</a>
                                    <a target="_blank" href="/work/{{$work->id}}">Details</a>
                                </div>
                            </div>
                        </div>
                        <figcaption>
                        <h4>
                        <a href="/work/{{$work->id}}">
                            {{$work->title}}
                        </a>
                        </h4>
                        <p>
                            {{$work->summary}}
                        </p>
                        </figcaption>
                    </figure>
                </div>
                @endforeach
            </div>
        </div>
    </section> <!-- #works -->

    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">DO U WANT TO LEARN ABOUT BSc.CSIT ?</h2>
                        <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Bachelor of Science in Computer Science and Information Technology (B. Sc. CSIT), affiliated to T.U. is the course composition of 4 years IT and computer courses. 
                        <br>For fulfilling the need of the job market in the field of IT profession, many elective courses have been also introduced. The program comprises computer science and allied courses of 126 credit hours.
                        </p>
                        <a href="/bsccsit" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Learn More</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
        
@endsection

 