@extends('layouts.pages')

@section('title')
    Syllabus
@endsection
@section('meta_description')

@endsection
@section('meta_keywords')

@endsection
@section('meta_author')

@endsection


@section('content')
        <!-- 
    ================================================== 
        Global Page Section Start
    ================================================== -->
    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>BSc.CSIT SYLLABUS</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">
                                    <i class="ion-ios-home"></i>
                                    Home
                                </a>
                            </li>
                            <li class="active">BSc.CSIT SYLLABUS</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container"><br><br>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                    <div class="panel-heading">
                        First Semester
                    </div>
                    <div class="panel-body">
                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 101: Introduction to Information Technology </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 102: Fundamentals of Computer Programming</td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> STA 103: Probability and Statistics </td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> MTH 104: Calculus and Analytical Geometry </td>
                            </tr>

                            </tbody>

                        </table>
                        (Natural Science Elective I: Student can opt for any one of the following course)




                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"> </span> PHY 105: Physics I</td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> BIO 106: Biology I </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> GEO 107: Geology I</td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> STA 108: Statistics I</td>
                            </tr>

                            </tbody>

                        </table>

                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <a href="#">
                            <span class="badge">Macro Syllubus</span>
                        </a>
                    </div>
                </div>
            </div>







            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                    <div class="panel-heading">
                        Second Semester
                    </div>
                    <div class="panel-body">

                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CCSC 151: Digital Logic </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 152: Discrete Structures</td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 153: Microprocessor </td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 154: Data Structure and Algorithms </td>
                            </tr>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span>  MTH 155 Linear Algebra</td>
                            </tr>

                            </tbody>

                        </table>
                        (Natural Science Elective II: Student can opt for any one of the following course)




                        <table class="table  table-hover">
                            <tbody>

                            <tr class="active">
                                <td><span class="glyphicon glyphicon-list-alt"></span> PHY 105: Physics II </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> BIO 106: Biology II </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> GEO 107: Geology II </td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> STA 108: Statistics II </td>
                            </tr>

                            </tbody>

                        </table>

                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <a href="#">
                            <span class="badge">Macro Syllubus </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                    <div class="panel-heading">
                        Third Semester
                    </div>
                    <div class="panel-body">

                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 201: Computer Architecture </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 202: Object Oriented Programming Language</td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 203: Operating Systems </td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 204: Numerical Method </td>
                            </tr>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span>  MGT 205: Introduction to Management</td>
                            </tr>

                            </tbody>

                        </table>

                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <a href="#">
                            <span class="badge">Macro Syllubus </span>
                        </a>
                    </div>
                </div>
            </div>












            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                    <div class="panel-heading">
                        Fourth Semester
                    </div>
                    <div class="panel-body">

                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 251: Theory of Computation </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 252: System of Analysis and Design</td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 253: Database Management System </td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 254: Computer Graphics /td&gt;
                            </td></tr>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 255: Introduction to Cognitive Science </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> ENG 256: Technical Writing </td>
                            </tr>

                            </tbody>

                        </table>


                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <a href="#">
                            <span class="badge">Macro Syllubus </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                    <div class="panel-heading">
                        Fifth Semester
                    </div>
                    <div class="panel-body">

                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 301: Computer Network</td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 302: Simulation and Modeling</td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 303: Design and Analysis of Algorithms</td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 304: Artificial Intelligence </td>
                            </tr>

                            </tbody>

                        </table>


                        (Computer Science Elective I: Student can opt for any one of the following course)

                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 305: Microprocessor Based Design</td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 306: Applied Logic </td>
                            </tr>

                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 307: E-Governance </td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 308: Concepts of Wireless Networking</td>
                            </tr>
                            <tr class="active">
                                <td><span class="glyphicon glyphicon-list-alt"></span> MGT 309: International Business Management </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> MGT 310: International Marketing </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 311: Neutral Networks </td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 312: Computer Hardware Design </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 313: Introduction to Cryptography </td>
                            </tr>

                            </tbody>

                        </table>

                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <a href="#">
                            <span class="badge">Macro Syllubus </span>
                        </a>
                    </div>
                </div>
            </div>




















            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                    <div class="panel-heading">
                        Sixth Semester
                    </div>
                    <div class="panel-body">

                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 351: Software Engineering </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 352: Compiler Design and Construction</td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 354: Real Time System </td>
                            </tr>


                            </tbody>

                        </table>
                        (Computer Science Elective II: Student can opt for any one of the following course)




                        <table class="table  table-hover">
                            <tbody>

                            <tr class="active">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 355: Knowlegde Management</td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 356: Fundamentals of E-commerce </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 357: Society and Ethics in Information Technology</td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 358: Automation and Robotics </td>
                            </tr>
                            <tr class="active">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 359: Digital System Design</td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 360: Net Centric Computing </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 361: Web Centric Computing</td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 362: Embedded System Programming </td>
                            </tr>

                            <tr class="active">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 363: Image Processing </td>
                            </tr>

                            </tbody>

                        </table>

                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <a href="#">
                            <span class="badge">Macro Syllubus </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                    <div class="panel-heading">
                        Seventh Semester
                    </div>
                    <div class="panel-body">

                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 401: Advance Database and Information System </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 402: Internet Technology </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 403: Advance Java Programming </td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 404: Project Work </td>
                            </tr>

                            </tbody>

                        </table>
                        (Computer Science Elective III: Student can opt for any one of the following course)




                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 405: Information Retrieval</td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 406: Database Administration </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 407: Network and System Administration</td>
                            </tr>


                            </tbody>

                        </table>

                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <a href="#">
                            <span class="badge">Macro Syllubus </span>
                        </a>
                    </div>
                </div>
            </div>




















            <div class="col-md-6 col-sm-6">
                <div class="panel panel-primary wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                    <div class="panel-heading">
                        Eight Semester
                    </div>
                    <div class="panel-body">

                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 451: Data Warehousing and Data Mining </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 452: Internship</td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 455: Network Security</td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> Elective IV </td>
                            </tr>
                            <tr class="active ">
                                <td><span class="glyphicon glyphicon-list-alt"></span> Elective V </td>
                            </tr>


                            </tbody>

                        </table>
                        Elective Courses: (Any Two of the following)




                        <table class="table  table-hover">
                            <tbody>

                            <tr class="active">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 453: Advance Networking with IPv6 </td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 454: Distribution Networking </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 456: Multimedia Database</td>
                            </tr>
                            <tr class="danger">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 457: Distribution and Object Oriented Database </td>
                            </tr>
                            <tr class="active">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 458: Cloud Computing</td>
                            </tr>
                            <tr class="success">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 459: Geographical Information System </td>
                            </tr>
                            <tr class="warning">
                                <td><span class="glyphicon glyphicon-list-alt"></span> CSC 460: Decision Support Systems</td>
                            </tr>


                            </tbody>

                        </table>

                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <a href="#">
                            <span class="badge">Macro Syllubus </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--
    ==================================================
    Call To Action Section Start
    ================================================== -->
    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">DO U WANT TO LEARN ABOUT BSc.CSIT ?</h2>
                        <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Bachelor of Science in Computer Science and Information Technology (B. Sc. CSIT), affiliated to T.U. is the course composition of 4 years IT and computer courses. 
                        <br>For fulfilling the need of the job market in the field of IT profession, many elective courses have been also introduced. The program comprises computer science and allied courses of 126 credit hours.
                        </p>
                        <a href="/bsccsit" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Learn More</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--
    ==================================================
    Footer Section Start
    ================================================== -->
@endsection