@extends('layouts.pages')

@section('title')
    BSc Csit
@endsection
@section('meta_description')

@endsection
@section('meta_keywords')

@endsection
@section('meta_author')

@endsection


@section('content')
        <!-- 
    ================================================== 
        Global Page Section Start
    ================================================== -->
    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>BSc.CSIT</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">
                                    <i class="ion-ios-home"></i>
                                    Home
                                </a>
                            </li>
                            <li class="active">BSc.CSIT</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- 
    ================================================== 
        Company Description Section Start
    ================================================== -->
    <section class="company-description">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                    <img src="img/about/about-company.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <div class="block">
                        <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Introduction</h3>
                        <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                            Bachelor of Science in Computer Science and Information Technology (B. Sc. CSIT), affiliated to T.U. is the course composition of 4 years IT and computer courses. For fulfilling the need of the job market in the field of IT profession, many elective courses have been also introduced. The program comprises computer science and allied courses of 126 credit hours. The program involves, in addition to conventional lectures, a great deal of practical and project works. The program develops the underlying principles of both Computer Science and Information Technology and shows how these principles can be applied to real world problems. this program develops the skills that are essential for both for both computer professionals and IT managers.

                        </p>                        
                        
                    </div>                
                </div>                
            </div>
        </div>
    </section>


    <!-- 
    ================================================== 
        Company Feature Section Start
    ================================================== -->
    <section class="hidden-xs about-feature clearfix">
        <div class="container-fluid">
            <div class="row">
                <div class="block about-feature-1 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">
                    <h2>
                    Objectives
                    </h2>
                    <p>
                       The main aim of B.Sc. (CSIT) program is 
                       <ul>
                           <li>To offer intensive knowledge in the theory, design, programming and application of computers. 
                           </li>
                           <li>To provide necessary knowledge in the field of functional knowledge of hardware system and the and necessary knowledge of computer software system.
                           </li>
                       </ul>
                    </p>
                </div>
                <div class="block about-feature-2 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                    <h2 class="item_title">
                        Curriculum Structure
                    </h2>
                    <p>
                        <table class="table table-hover">
                            <tbody>
                            <tr class="active ">
                                <td>Computer Science Core Courses</td>
                                <td>75 Credit Hours</td>
                            </tr>
                            <tr class="success">
                                <td>Natural Science Elective Coursess</td>
                                <td>6 Credit Hours</td>
                            </tr>
                            <tr class="warning">
                                <td>Mathematics Courses</td>
                                <td>12 Credit Hours</td>
                            </tr>
                            <tr class="danger">
                                <td>English Courses</td>
                                <td>3 Credit Hours</td>
                            </tr>
                            <tr class="active">
                                <td>Social Science and Management</td>
                                <td>6 Credit Hours</td>
                            </tr>
                            <tr class="success">
                                <td>Computer Science Elective Courses</td>
                                <td>15 Credit Hours</td>
                            </tr>
                            <tr class="warning">
                                <td>Internship/Project</td>
                                <td>9 Credit Hours</td>
                            </tr>
                            <tr class="danger">
                                <td><b>Total</b></td>
                                <td><b>126 Credit Hours</b></td>
                            </tr>
                            </tbody>
                        </table>
                    </p>
                </div>
                <div class="block about-feature-3 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".7s">
                    <h2 class="item_title">
                    Grading System
                    </h2>
                    <p>
                        <table class="table  table-hover">
                            <tbody>
                            <tr class="active ">
                                <td>Pass Division</td>
                                <td>40%</td>

                            </tr>
                            <tr class="success">
                                <td>Second Division</td>
                                <td>55%</td>
                            </tr>
                            <tr class="warning">
                                <td>First Division</td>
                                <td>70%</td>
                            </tr>
                            <tr class="danger">
                                <td>First Division with Distinction</td>
                                <td>80% and above</td>
                            </tr>
                            </tbody>

                        </table>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">Course Composition</h1>
                        <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">
                        The Nature of Course of "CSC-404: Project Work" under Seventh Semester is "Project" which has the Full Marks of 100 and the Pass Marks of 40. 
                        <br>
                        The Nature of Course of "CSC-452: Internship" under Eighth Semester is "Project" which has the Full Marks of 200 and the Pass Marks of 80. 
                        <br>
                        The Nature of Course of "CSC-408: Software Project Management" under Seventh Semester is "Theory (3Hrs.) + Practical (3Hrs.)" which has the Full Marks of 100 [60 for Board Exam, 20 for Internal Evaluation and 20 for Project Evaluation] and the Pass Marks of 40 [24 for Board Exam, 8 for Internal Evaluation and 8 for Project Evaluation].
                        <br>
                        The Nature of Course of all other rest of the subjects under Seventh and Eighth Semester is "Theory (3Hrs.) + Lab (3Hrs.)" which has the Full Marks of 100 [60 for Board Exam, 20 for Internal Evaluation and 20 for Practical Evaluation) and the Pass Marks of 40 [24 for Board Exam, 8 for Internal Evaluation and 8 for Practical Evaluation]. 
                        </p>
                        <a href="/syllabus" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">CSIT SYLLABUS</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--
    ==================================================
    Footer Section Start
    ================================================== -->
@endsection