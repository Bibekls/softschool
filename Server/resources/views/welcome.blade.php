<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>
</head>
<body>
<h1>Welcome Page</h1>
</body>
</html>
<script src="//js.pusher.com/3.0/pusher.min.js"></script>
<script>
var pusher = new Pusher("{{env("PUSHER_KEY")}}",{'cluster':'ap1','encrypted':false})
var channel = pusher.subscribe('test-channel');
channel.bind('test-event', function(data) {
  alert(data.text);
});
</script>