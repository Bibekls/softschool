<?php

namespace HCCNetwork\Events;
use HCCNetwork\Notification;
use Log;

use Illuminate\Queue\SerializesModels;

class FCMNotification extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
