<?php

namespace HCCNetwork\Events;
use Log;
use HCCNetwork\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventName extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $data;

    public function __construct()
    {
        $this->data = array(
            'power'=> '10'
        );
     Log::info('boardcasted');   
    }

    public function broadcastOn()
    {
        Log::info('boardcasted');
        return ['test-channel'];
    }
}
