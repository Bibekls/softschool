<?php

namespace HCCNetwork\Events\library;
use Log;

use HCCNetwork\book;
use HCCNetwork\User;

use HCCNetwork\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BookIssued extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $book;
    public $user;

    public function __construct(User $user,book $book)
    {
        $this->user=$user; 
        $this->book=$book;        
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
