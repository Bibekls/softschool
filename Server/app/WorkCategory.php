<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class workCategory extends Model
{
    public $table='work_categories';
    protected $fillable = ['name'];
    
    public function works()
    {
        return $this->hasMany('HCCNetwork\Work','category_id','id');
    }
}
