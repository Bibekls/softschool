<?php

namespace HCCNetwork\Listeners;
use Log;
use Mail;
use Request;
use Auth;
use HCCNetwork\Events\student\StudentFormRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle($event)
    { 
        Mail::send('emails.login.login',['email'=>$event->user->email], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Student Form Rejected');
                $message->to($event->user->email);
             
            });          
           return 0;      
    }
}
