<?php

namespace HCCNetwork\Listeners\Teacher;
use Log;
use Mail;
use HCCNetwork\Events\teacher\TeacherFormVerified;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TeacherFormVerifiedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(TeacherFormVerified $event)
    {
       
            Mail::send('emails.teacher.TeacherFormVerifiedEmail',['username'=>$event->user->name], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Teacher Form Verified');
                $message->to($event->user->email);
             
            });          
           return 0;           
        
        
        
    }
}
