<?php

namespace HCCNetwork\Listeners\teacher;
use Log;
use Mail;
use HCCNetwork\Events\teacher\TeacherRegister;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(TeacherRegister $event)
    {
       
            Mail::send('emails.teacher.verifyemail',['token'=>$event->user->token], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Email Verification Link');
                $message->to($event->user->email);
             
            });          
           return 0;           
        
        
        
    }
}
