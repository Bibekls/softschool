<?php

namespace HCCNetwork\Listeners\teacher;
use Log;
use Mail;
use HCCNetwork\Events\teacher\TeacherVerified;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TeacherVerifiedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(TeacherVerified $event)
    {
       
            Mail::send('emails.teacher.TeacherVerifiedEmail',['username'=>$event->user->name], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Teacher Request Accepted');
                $message->to($event->user->email);
             
            });          
           return 0;           
        
        
        
    }
}
