<?php

namespace HCCNetwork\Listeners\teacher;
use Log;
use Mail;
use HCCNetwork\Events\teacher\TeacherFormRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TeacherFormRejectedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(TeacherFormRejected $event)
    {      
            Mail::send('emails.teacher.TeacherFormRejectedEmail',['email'=>$event->user->email], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Teacher Form Rejected');
                $message->to($event->user->email);
             
            });          
           return 0;           
        
        
        
    }
}
