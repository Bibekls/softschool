<?php

namespace HCCNetwork\Listeners\teacher;
use Log;
use Mail;
use HCCNetwork\Events\teacher\TeacherRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TeacherRejectedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(TeacherRejected $event)
    {           
            
            Mail::send('emails.teacher.TeacherRejectedEmail',['email'=>$event->user], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Teacher Request Rejected');
                $message->to($event->user);
             
            });          
           return 0;           
        
        
        
    }
}
