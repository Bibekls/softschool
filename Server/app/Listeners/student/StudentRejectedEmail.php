<?php

namespace HCCNetwork\Listeners\student;
use Log;
use Mail;
use HCCNetwork\Events\student\StudentRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StudentRejectedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(StudentRejected $event)
    {      
            Mail::send('emails.student.StudentRejectedEmail',['email'=>$event->user], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Student Request Rejected');
                $message->to($event->user);
             
            });          
           return 0;           
        
        
        
    }
}
