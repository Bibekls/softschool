<?php

namespace HCCNetwork\Listeners\student;
use Log;
use Mail;
use HCCNetwork\Events\student\StudentFormVerified;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StudentFormVerifiedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(StudentFormVerified $event)
    {
       
            Mail::send('emails.student.StudentFormVerifiedEmail',['username'=>$event->user->name], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Student Form Verified');
                $message->to($event->user->email);
             
            });          
           return 0;           
        
        
        
    }
}
