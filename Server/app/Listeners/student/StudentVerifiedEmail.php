<?php

namespace HCCNetwork\Listeners\student;
use Log;
use Mail;
use HCCNetwork\Events\student\StudentVerified;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StudentVerifiedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(StudentVerified $event)
    {
       
            Mail::send('emails.student.StudentVerifiedEmail',['username'=>$event->user->name], function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Student Request Accepted');
                $message->to($event->user->email);
             
            });          
           return 0;           
        
        
        
    }
}
