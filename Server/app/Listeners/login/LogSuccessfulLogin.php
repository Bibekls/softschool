<?php

namespace HCCNetwork\Listeners\login;
use Log;
use Mail;
use Request;
use Auth;
use DB;
use HCCNetwork\LoginHistory;
use HCCNetwork\User;
use Carbon\Carbon;
use HCCNetwork\Events\student\StudentFormRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle($event)
    {  
            $loginHistory=LoginHistory::select(DB::raw('ip, max(created_at) as time'))
                        ->where('user_id',$event->user->id)                        
                        ->first();          

            $event->user->last_login_ip=$loginHistory->ip;
            $event->user->last_login_time=$loginHistory->time;
            $event->user->save();
            
            LoginHistory::create(array(
                                    'user_id'=>$event->user->id,
                                    'ip'=>Request::getClientIp()
            )); 
            
            return 0;
            Mail::send('emails.login.login',[
                                            'ip'=>Request::getClientIp(),
                                        ], 
            function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Login From New Browser');
                $message->to($event->user->email);
             
            });          
           return 0;      
    }
}
