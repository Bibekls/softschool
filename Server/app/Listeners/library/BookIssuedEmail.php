<?php

namespace HCCNetwork\Listeners\library;
use Log;
use Mail;
use HCCNetwork\Events\library\BookIssued;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookIssuedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //Log::info("hello listener is constructed");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(BookIssued $event)
    {           
       
            Mail::send('emails.library.BookIssuedEmail',
                [
                    'id'=>$event->book->id,
                    'name'=>$event->book->name,
                    'author'=>$event->book->author,
                    'publisher'=>$event->book->publisher,
                    'updated_at'=>$event->book->updated_at,
                ], 

            function($message) use ($event)
            {
                $message->from('admin@hcc.edu.np', 'Hetauda City College');
                $message->subject('Book Issued');
                $message->to($event->user->email);
             
            });          
           return 0;  
        
    }
}
