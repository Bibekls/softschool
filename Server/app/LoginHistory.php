<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;

class LoginHistory extends Model
{
 	public $table='login_history';
    
    protected $fillable = ['user_id','ip'];

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo('HCCNetwork\User','user_id','id');
    }
   
}

