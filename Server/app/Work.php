<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class work extends Model
{
    public $table='works';
    protected $fillable = [
    'category_id','title','banner','summary','content'];
    protected $primaryKey = 'id';
    public function workCategory()
    {
        return $this->belongsTo('HCCNetwork\WorkCategory','category_id','id');
    }
}
