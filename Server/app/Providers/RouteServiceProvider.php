<?php

namespace HCCNetwork\Providers;

use HCCNetwork\Feedback;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */

    protected $namespace = 'HCCNetwork\Http\Controllers';
    protected $libraryNamespace = 'HCCNetwork\Http\Controllers\Library';
    protected $MISNamespace = 'HCCNetwork\Http\Controllers\MIS';
    protected $MIS_APINamespace = 'HCCNetwork\Http\Controllers\MIS_API';
    protected $studentNamespace = 'HCCNetwork\Http\Controllers\Student';
    protected $teacherNamespace = 'HCCNetwork\Http\Controllers\Teacher';
    protected $websiteNamespace = 'HCCNetwork\Http\Controllers\WebSite';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();

        view()->share('unreadFeedback',Feedback::where('seen',false)->count());
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapLibraryRoutes();

        $this->mapMISRoutes();

        $this->mapMIS_APIRouter();
        
        $this->mapStudentRoutes();

        $this->mapTeacherRoutes();

        $this->mapWebsiteRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */

    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }


    protected function mapMIS_APIRouter()
    {
        Route::prefix('mis_api')
            ->middleware('api')
            ->namespace($this->MIS_APINamespace)
            ->group(base_path('routes/mis_api.php'));
    }


    protected function mapLibraryRoutes()
    {
        Route::prefix('api/library')
            ->middleware('api')
            ->namespace($this->libraryNamespace)
            ->group(base_path('routes/library.php'));
    }

    protected function mapMISRoutes()
    {
        Route::middleware('web')
            ->namespace($this->MISNamespace)
            ->group(base_path('routes/mis.php'));
    }

    protected function mapStudentRoutes()
    {
        Route::middleware('web')
            ->namespace($this->studentNamespace)
            ->group(base_path('routes/student.php'));
    }

    protected function mapTeacherRoutes()
    {
        Route::middleware('web')
            ->namespace($this->teacherNamespace)
            ->group(base_path('routes/teacher.php'));
    }

    protected function mapWebsiteRoutes()
    {
        Route::middleware('web')
            ->namespace($this->websiteNamespace)
            ->group(base_path('routes/website.php'));
    }
}
