<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class routine extends Model
{
    public $table='routines';
    public $timestamps = false;
    protected $fillable = ['teacher_id','subject_id','day','start_at','end_at'];
    
}