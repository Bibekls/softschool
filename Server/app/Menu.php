<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    public $table='menus';
    protected $fillable = [
    'parent_id','url','name', 'icon','description','target','order','status'
    ];

    public function parent()
    {
        return $this->belongsTo('HCCNetwork\Menu','parent_id','id');
    }

    public function submenu()
    {
        return $this->hasMany('HCCNetwork\Menu','parent_id','id');
    }
}
