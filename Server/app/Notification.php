<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $table = 'notifications';

    protected $fillable = ['title','sender_id', 'description','schedule_date','time','target_user','multicast_id','message_id','status','send','target_input'];

    protected $primaryKey = 'id';

    public function notificationUser()
    {
        return $this->hasMany('HCCNetwork\NotificationUser');
    }
    public function getTargetUserAttribute($value)
    {
        return json_decode($value,true);
    }


}

