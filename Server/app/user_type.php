<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class user_type extends Model
{
    public $table='user_types';
    protected $fillable =  ['id','name'];

    protected $primaryKey = 'id';
    public $timestamps=false;

    public function faqs()
    {
        return $this->hasMany('HCCNetwork\Faqs','user_type_id','id');
    }

	public function userRequest()
    {
        return $this->hasMany('HCCNetwork\userrequest','user_type_id','id');
    }

}
