<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
	 public $table='faqs';  
	   
    protected $fillable = ['title', 'description','user_type_id'];

    protected $primaryKey = 'id';

    public function user_type()
    {
        return $this->belongsTo('HCCNetwork\user_type','user_type_id','id');
    }
   
}

