<?php namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;
use Validator;

class BookReturn extends Model {
    public $table='returns';
    protected $fillable =  ['student_id','book_id'];
    public static $rules= array(
        'student_id'=>'required',
        'book_id'=>'required|min:3',
    );

    public static function validateReturn($data)
    {
        return Validator::make($data,static::$rules);
    }
}
