<?php namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;
use Validator;

class BookIssue extends Model {

    public $table='issues';
    protected $fillable =  ['student_id','book_id','state']; 
}
