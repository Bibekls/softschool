<?php namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class FacultyTeacher extends Model {

    public $table='faculty_teacher';
    protected $fillable =  ['faculty_id','teacher_id'];

}

