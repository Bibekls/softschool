<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class page extends Model
{
    public $table='pages';
    protected $fillable = [
    'category_id','order','name','tag','title','content'];

    public function pageCategory()
    {
        return $this->belongsTo('HCCNetwork\PageCategory','category_id','id');
    }
}
