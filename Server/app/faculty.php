<?php namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;
use DB;
class faculty extends Model {

    public $table='faculties';
    protected $fillable =  ['id','name','start_at','end_at','faculty_wise_class'];
    protected $primaryKey = 'id';

    public function books()
    {
        return $this->hasMany('HCCNetwork\book');
    }
    public function batch()
    {
        return $this->hasMany('HCCNetwork\BatchLevelMaping');
    }
    public function subject()
    {
        return $this->hasMany('HCCNetwork\Subject')->groupBy('batch')->groupBy('faculty_id')->select(DB::raw("subjects.faculty_id,'1' as test,'student' as type,subjects.batch as name"));
    }
    public function subjectLevel()
    {
        return $this->hasMany('HCCNetwork\Subject')->groupBy('level')->groupBy('faculty_id')->select(DB::raw("subjects.faculty_id,'2' as test,'student' as type,subjects.level as name"));
    }
    public function getFacultyWiseClassAttribute($value)
    {
        return json_decode($value,true);
    }
}

