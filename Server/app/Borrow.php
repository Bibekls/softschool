<?php namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Borrow extends Model {
    public $table='borrow';
    protected $fillable =  ['id','user_id','sub_code','issue_date','return_date'];
}