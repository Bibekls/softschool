<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
    public $table='galleries';
    protected $fillable = ['title','image','publish'];
    
    public function images()
    {
        return $this->hasMany('HCCNetwork\image','gallery_id','id');
    }
}