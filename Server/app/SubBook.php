<?php
namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubBook extends Model {

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable =  ['id','book_id', 'price','source','edition','edition_year','available','block','remark'];
    public $incrementing = false;
}
