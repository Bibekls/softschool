<?php namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class EventCalendar extends Model {

    public $table='event_calendar';
    protected $fillable =  ['id','title','start','actions','color','description','start_at','end_at','date'];
    protected $primaryKey = 'id';

}

