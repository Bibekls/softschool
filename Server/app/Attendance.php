<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;
use HCCNetwork\attendence;
class attendance extends Model
{
    public $table='attendances';    
    protected $fillable = [
        'teacher_id','subject_id','student_id','date','attendance',
        'replaced_with','extra','start_time','end_time'
    ];
    // public function student()
    // {
    //     return $this->belongsTo('HCCNetwork\Attendence');
    // }
}