<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	 public $table='messages';  
	   
    protected $fillable = ['message', 'from','to','seen'];

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo('HCCNetwork\User','from','id');
    }
   
}

