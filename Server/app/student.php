<?php

namespace HCCNetwork;

use Validator;
use Input;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use HCCNetwork\attendance;
class student extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public $table='students';
    protected $fillable =  ['id','first_name','middle_name','last_name','parent_name','date_of_birth','permanent_address','current_address','gender','batch',
        'faculty_id','symbol_number','image','join_date','student_record_id','parent_id','registration_number','mobile_number','phone_number'];

    public static $rule= array(        
        'first_name'=>'required|min:3',
        'middle_name'=>'min:3',
        'last_name'=>'required|min:3',
        'parent_name'=>'required|min:3',
        'date_of_birth'=>'required',
        'permanent_address'=>'required|min:5',
        'current_address'=>'min:5',
        'gender'=>'required',
        'batch'=>'required',
        'symbol_number'=>'min:3|unique:students',
        'registration_number'=>'unique:students',
        'mobile_number'=>'required|min:10',
        'phone_number'=>'min:9',
    );
   
    protected $primaryKey = 'id';

    public static function validateStudent($data)
    {
        return Validator::make($data,static::$rule);
    }

	public function username()
    {
        return $this->hasOne('HCCNetwork\User','id');
    }
    public function faculty()
    {
        return $this->belongsTo('HCCNetwork\faculty','faculty_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'id','id');
    }
    public function getFacultyWiseClassAttribute($value)
    {
        return json_decode($value,true);
    }
    public function attendence(){
        return $this->hasMany('HCCNetwork\attendance');
    }
}
