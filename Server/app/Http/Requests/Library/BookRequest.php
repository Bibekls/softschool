<?php

namespace HCCNetwork\Http\Requests\Library;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "bookName"=>"required",
            "authorName"=>"required",
            "publisherName"=>"required",
            "ISBN"=>"",
            "faculty"=>"required",
            "level"=>"required",
            "noOfBook"=>"required",
            "price"=>"",
            "source"=>"",
            "edition"=>"",
            "editionYear"=>""
        ];
    }
}
