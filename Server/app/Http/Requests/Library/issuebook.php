<?php

namespace HCCNetwork\Http\Requests\library;

use HCCNetwork\Http\Requests\Request;

class issuebook extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id'=>'required',
            'book_id'=>'required|min:3', 
            'api_token'=>'required',           
        ];
    }
}
