<?php

namespace HCCNetwork\Http\Requests\library;

use HCCNetwork\Http\Requests\Request;

class EditBook extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [                
                'name'=>'required|min:4',
                'author'=>'required|min:4',
                'price'=>'required|min:2',
                'publisher'=>'required|min:4',
                'edition'=>'required',
                'faculty'=>'required',
                'level'=>'required',
                'description'=>'min:10',
        ];
    }
}
