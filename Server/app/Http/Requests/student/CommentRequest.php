<?php

namespace HCCNetwork\Http\Requests\student;

use HCCNetwork\Http\Requests\Request;

class CommentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [                
                'comment'=>'required|min:10|max:512',                
                'post_id'=>'exists:posts,id',  
        ];
    }
}
