<?php

namespace HCCNetwork\Http\Requests\student;
use HCCNetwork\Http\Requests\Request;

class FeedbackRequest extends Request
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {    
        return [                
                'feedback'=>'required|min:10|max:1024',
        ];
    }
}
