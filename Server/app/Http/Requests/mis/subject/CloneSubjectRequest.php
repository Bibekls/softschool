<?php

namespace HCCNetwork\Http\Requests\mis\subject;

use HCCNetwork\Http\Requests\Request;

class CloneSubjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'past_year' => 'required|exists:subjects,batch',
            'year' => 'required|numeric|min:2070|max:2090',
            'faculty'=>'required|numeric|exists:faculties,id'
        ];
    }
}
