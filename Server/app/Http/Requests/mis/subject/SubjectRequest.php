<?php

namespace HCCNetwork\Http\Requests\mis\subject;

use HCCNetwork\Http\Requests\Request;

class SubjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_name' => 'required|min:3|max:50',
            'level' => 'required|numeric|min:1|max:8',
            'faculty' => 'required|exists:faculties,name',
            'year' => 'numeric|required|min:2070|max:2090',
        ];
    }
}
