<?php

namespace HCCNetwork\Http\Requests\mis\user;

use HCCNetwork\Http\Requests\Request;

class TeacherRegistrationFormRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|max:255|unique:users,name|alpha_dash',
            'email' => 'email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'first_name' => 'required|min:3',
            'middle_name' => 'nullable|min:3',
            'last_name' => 'required|min:3',
            'parent_name' => 'nullable|min:3',
            'permanent_address' => 'nullable|min:5',
            'current_address' => 'nullable|min:5',
            'gender' => 'required',

            'joining_date' => 'nullable|date',
            'leaving_date' => 'nullable|date',

            'mobile_number' => 'nullable|min:10',
            'phone_number' => 'nullable|min:10',
        ];
    }
}
