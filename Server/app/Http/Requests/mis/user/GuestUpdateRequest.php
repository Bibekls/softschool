<?php

namespace HCCNetwork\Http\Requests\mis\user;

use HCCNetwork\Http\Requests\Request;

class GuestUpdateRequest  extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name'=>'required|min:3',
            'last_name'=>'required|min:3',
            'gender'=>'required',
        ];
    }
}
