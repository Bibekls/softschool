<?php

namespace HCCNetwork\Http\Requests\mis\user;

use HCCNetwork\Http\Requests\Request;

class PasswordResetRequest  extends Request
{    
    public function authorize()
    {
        return true;
    }
       
    public function rules()
    {
        return [
                'password' => 'required|confirmed|min:6'
                ];
    }
}
