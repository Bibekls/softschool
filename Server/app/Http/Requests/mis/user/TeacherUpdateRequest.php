<?php

namespace HCCNetwork\Http\Requests\mis\user;

use HCCNetwork\Http\Requests\Request;

class TeacherUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|max:255|alpha_dash|unique:users,name,' . $this->id,
            'email' => 'email|max:255|unique:userrequests|unique:users,email,' . $this->id,

            'first_name' => 'required|min:3',
            'middle_name' => 'nullable|min:3',
            'last_name' => 'required|min:3',
            'parent_name' => 'nullable|min:3',
            'permanent_address' => 'nullable|min:5',
            'current_address' => 'nullable|min:5',
            'gender' => 'required',


            'mobile_number' => 'nullable|min:10',
            'phone_number' => 'nullable|min:9',
        ];
    }
}
