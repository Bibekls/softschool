<?php

namespace HCCNetwork\Http\Requests\mis;

use HCCNetwork\Http\Requests\Request;

class RegistrationFormRequest extends Request
{    
    public function authorize()
    {
        return true;
    }
       
    public function rules()
    {        
        return [
                'username' => 'required|max:255|unique:users,name|unique:userrequests,name|alpha_dash',
                'email' => 'required|email|max:255|unique:users|unique:userrequests',            
                'password' => 'required|confirmed|min:6',
                'first_name'=>'required|min:3',
                'middle_name'=>'nullable|min:3',
                'last_name'=>'required|min:3',
                'parent_name'=>'required|min:3',
                'date_of_birth'=>'required',
                'permanent_address'=>'required|min:5',
                'current_address'=>'nullable|min:5',
                'gender'=>'required',
                'religion'=>'required',                
                'mobile_number'=>'required|min:10',
                'phone_number'=>'nullable|min:9',
        ];
    }
}
