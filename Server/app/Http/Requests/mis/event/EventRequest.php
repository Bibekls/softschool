<?php

namespace HCCNetwork\Http\Requests\mis\event;

use HCCNetwork\Http\Requests\Request;

class EventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:128',
            'description' => 'required|max:1024',
            'date' => 'required',
            'start_at' => '',
            'end_at' => '',
        ];
    }
}
