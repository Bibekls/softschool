<?php

namespace HCCNetwork\Http\Requests\mis\routine;

use HCCNetwork\Http\Requests\Request;

class RoutineRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [                
                'teacher'=>'required|exists:users,id',
                'subject'=>'required|exists:subjects,id',
                'day'=>'required',//|in:Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday',  
                'start_at'=>'required',
                'end_at'=>'required'
        ];
    }
}
