<?php

namespace HCCNetwork\Http\Requests\teacher;

use HCCNetwork\Http\Requests\Request;

class RegistrationFormRequest extends Request
{    
    public function authorize()
    {
        return true;
    }
       
    public function rules()
    {        
        return [                
                'first_name'=>'required|min:3',
                'middle_name'=>'min:3',
                'last_name'=>'required|min:3',
                'parent_name'=>'required|min:3',
                'date_of_birth'=>'required',
                'permanent_address'=>'required|min:5',
                'current_address'=>'min:5',
                'gender'=>'required',
                'religion'=>'required',                
                'mobile_number'=>'required|min:10',
                'phone_number'=>'min:9',            
        ];
    }
}
