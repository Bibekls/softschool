<?php

namespace HCCNetwork\Http\Requests\teacher;
use HCCNetwork\Http\Requests\Request;

class FeedbackRequest extends Request
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {    
        return [                
                'feedback'=>'required|max:1024',
        ];
    }
}
