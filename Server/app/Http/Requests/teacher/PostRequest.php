<?php

namespace HCCNetwork\Http\Requests\teacher;

use HCCNetwork\Http\Requests\Request;
use Session;
class PostRequest extends Request
{    
    public function authorize()
    {
        return true;
    }
       
    public function rules()
    {
        Session::put('active','addpost');
        return [                
                'post'=>'required|min:10|max:1024',                
        ];
    }
}
