<?php

namespace HCCNetwork\Http\Requests\teacher;
use Session;
use HCCNetwork\Http\Requests\Request;

class UrlRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Session::put('active','addUrl');
        return [                
                'title'=>'required|min:5|max:50|unique:urls,title',                
                'url'=>'required|min:10|max:140|URL|unique:urls,url',                
                'description'=>'required|min:10|max:1024',                

        ];
    }
}
