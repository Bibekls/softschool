<?php
namespace HCCNetwork\Http\Controllers\Teacher;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\Post;
use HCCNetwork\Url;
use Carbon;

use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UrlController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');      
    }  
    public function url()
    {
        
        $time=Carbon::createFromFormat('Y-m-d H:i:s',Auth::user()->created_at)->toDayDateTimeString();//->format('Y-m-d H:i:s'); //this format function return string so used it only before render the result 
        //$time->toDayDateTimeString();     
        
        
        $dt=Carbon::now()->subSeconds( Carbon::now()->diffInSeconds(Auth::user()->created_at) )->diffForHumans(); 
        //$dt=$time->diffInHours(\Carbon\Carbon::now());
        

        return View::make('teacher.url',['title'=>'URL'])
                    ->with('profile_pic',image::find(Auth::user()->profile_pic))
                    ->with('urls',Url::join('users','users.id','=','urls.user_id')
                                        ->join('images','images.id','=','users.profile_pic')
                                        ->select('images.file_name as profile_pic','urls.*')
                                        ->orderBy('created_at','desc')->paginate(5) );
                    
    }

    public function deleteUrl(Request $request)
    {        
        if(Url::find($request->input('url_id'))->user->id==Auth::user()->id){
            Url::find($request->input('url_id'))->delete();            
        } 
        return redirect()->back();       
    }
      
}
