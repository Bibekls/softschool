<?php
namespace HCCNetwork\Http\Controllers\Teacher;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\mis\routine\RoutineRequest;
use HCCNetwork\Routine;
use HCCNetwork\Teacher;
use HCCNetwork\Subject;
use HCCNetwork\BatchLevelMaping;
use HCCNetwork\User;
use HCCNetwork\faculty;

class RoutineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
    }  
    public function routine()
    {   

        $days=Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')                    
                    ->where('routines.teacher_id',Auth::user()->id)
                    ->select('day')
                    ->groupBy('day')
                    ->get()
                    ->toArray();

        $level=array();

        foreach ($days as $day){
            
            $day['name']=$day['day'];
            $day['period']=Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                ->join('teachers', 'routines.teacher_id', '=', 'teachers.id')
                ->join('faculties', 'subjects.faculty_id', '=', 'faculties.id')
                ->where('routines.teacher_id',Auth::user()->id)                
                ->where('routines.day',$day['day'])
                ->select('subjects.name as subject','routines.start_at as start_at','routines.end_at as end_at','subjects.level as level','faculties.name as faculty')                                
                ->orderBy('routines.start_at')
                ->get();
            array_push($level,$day);                                
        }
       return View::make('teacher.routine')
        ->with('title','Routine')
        ->with('routines',$level);
    }  
    
}