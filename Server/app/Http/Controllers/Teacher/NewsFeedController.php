<?php
namespace HCCNetwork\Http\Controllers\Teacher;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use Carbon;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\Post;

use HCCNetwork\Http\Requests;
use Illuminate\Support\Facades\Auth;


class NewsFeedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');      
    }  
    public function newsfeed()
    {   
        $data= array(); 
        $newsfeed=array();
        
        foreach ($pagelink=Post::orderBy('created_at','desc')->paginate(9) as $post)
        {
            $data['post']=$post;           

            $data['postTime']=Carbon::createFromTimeStamp(strtotime($post->created_at))->toDayDateTimeString()." ".Carbon::now()->                  subSeconds( Carbon::now()->diffInSeconds($post->created_at) )->diffForHumans();

            $data['profile_pic']=User::join('images','images.id','=','users.profile_pic')
                                        ->join('posts','posts.user_id','=','users.id')
                                        ->where('users.id',$post->user_id)
                                        ->select('images.file_name as profile_pic')
                                        ->first();
            $data['comments']=User::join('images','users.profile_pic','=','images.id')
                                        ->join('comments','users.id','=','comments.user_id')
                                        ->join('posts','comments.post_id','=','posts.id')
                                        ->where('comments.post_id',$post->id)
                                        ->select('images.file_name as profile_pic','comments.comment','comments.id','users.id as user_id','users.name as username','comments.created_at as time')
                                        ->get();
            array_push($newsfeed,$data);            
        }
       
        return View::make('teacher.newsfeed')
        ->with('title','newsfeed')
        ->with('profile_pic',image::find(Auth::user()->profile_pic))
        ->with('newsfeed',$newsfeed)
        ->with('pagelink',$pagelink)
        ->with('counter',0);
    }  
}
