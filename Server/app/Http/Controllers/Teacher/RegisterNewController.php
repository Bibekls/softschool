<?php
namespace HCCNetwork\Http\Controllers\Teacher;
use HCCNetwork\Http\Controllers\Controller;

use HCCNetwork\faq;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\faculty;
use HCCNetwork\book;
use HCCNetwork\Teacher; //always include related model
use HCCNetwork\BookIssue;
use HCCNetwork\BookReturn;

use Illuminate\Http\Request;
use HCCNetwork\Http\Requests;
use HCCNetwork\Http\Requests\teacher\RegistrationFormRequest;

use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class registerNewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('newteacher');
    }

    public function profilePicForm(){        
        if(Auth::user()->profile_pic==null){

             return View::make('teacher.register.uploadProfilePic',['title'=>'Add-Teacher'])           
                ->with('user',User::find(Auth::user()->id));
            
        }else{            
            return redirect('/registration-form');
        }

    }

    public function form()
    {
        if(Auth::user()->profile_pic==null){
            return redirect('/upload-profile-pic');
        }else{

            return View::make('teacher.register.registrationForm',['title'=>'Add-Teacher'])
                ->with('faculties',faculty::all())
                ->with('user',User::find(Auth::user()->id))
                ->with('profilePic',\HCCNetwork\image::find(Auth::user()->profile_pic) );
        }
    }
    public function uploadProfilePic(Request $request)
    {
        if(Auth::user()->profile_pic==null)
        {
            $file=$request->file('file');
            $filename=uniqid().$file->getClientOriginalName();
            if(!file_exists('images'))
            {
                mkdir('images',0777,true);
            }
            $file->move('images',$filename);
            /*
            **  Resize image to 720 * x
            */
            Image::make('images/'.$filename)->resize(720,null,function ($constraint){$constraint->aspectRatio();})->save('images/'.$filename,100);
            /*
            **  Thumbs creation
            */

            if(!file_exists('images/thumbs'))
            {
                mkdir('images/thumbs',0777,true);
            }

            $thumb=Image::make('images/'.$filename)->resize(256,null,function ($constraint){$constraint->aspectRatio();})->save('images/thumbs/'.$filename,50);
            /*
            **  Thumbnail Creation
            */

            if(!file_exists('images/thumbnails'))
            {
                mkdir('images/thumbnails',0777,true);
            }

            $thumb=Image::make('images/'.$filename)->resize(64,64)->save('images/thumbnails/'.$filename,50);
            /*
            **  End of image saving procedure
            */


            $user=User::find(Auth::user()->id);
            $image=$user->images()->create([
                'user_id'=>Auth::user()->id,
                'file_name'=>$filename,
                'file_size'=>$file->getClientSize(),
                'file_mime'=>$file->getClientMimeType(),
                'file_path'=>'images/'.$filename
            ]);

            User::find(Auth::user()->id)
                ->update(array(
                'profile_pic' =>$image->id,
                ));

            return $image;
        }
        else
        {
            return response("already uploaded",401);
        }

    }

    public function create(RegistrationFormRequest $request)
    {
        $faculty=faculty::where('name',$request->input('faculty_id'))->first();
       
            Teacher::create(array(
                'id' => Auth::user()->id,
                'first_name' => Input::get('first_name'),
                'middle_name' => Input::get('middle_name'),
                'last_name' => Input::get('last_name'),
                'parent_name' => Input::get('parent_name'),
                'date_of_birth' => Input::get('date_of_birth'),
                'gender' => Input::get('gender'),
                'religion' => Input::get('religion'),
                'permanent_address' => Input::get('permanent_address'),
                'current_address' => Input::get('current_address'),                
                'mobile_number' => Input::get('mobile_number'),
                'phone_number' => Input::get('phone_number'),
            ));

            User::where('id',Auth::user()->id)->update(array(
                    'active' => '1'
                ));

            return redirect('/');
        
    }  
}
