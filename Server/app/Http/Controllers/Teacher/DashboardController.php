<?php
namespace HCCNetwork\Http\Controllers\Teacher;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\BookIssue;
use HCCNetwork\Post;
use HCCNetwork\Comment;
use HCCNetwork\Url;
use HCCNetwork\Feedback;

use HCCNetwork\Http\Requests;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');      
    }  
    public function dashboard()
    {
        return View::make('teacher.dashboard')
        ->with('title','Dashboard')
        ->with('profile_pic',image::find(Auth::user()->profile_pic))        
        ->with('totalPost','')
        ->with('totalComment','')
        ->with('totalImage',image::where('user_id',Auth::user()->id)->count() )
        ->with('totalUrl','')
        ->with('totalFeedback',Feedback::where('user_id',Auth::user()->id)->count() )
        ;

    }  
}
