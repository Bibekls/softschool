<?php
namespace HCCNetwork\Http\Controllers\Teacher;

use HCCNetwork\BatchLevelMaping;
use HCCNetwork\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\mis\routine\RoutineRequest;
use HCCNetwork\Routine;
use HCCNetwork\student;
use HCCNetwork\Subject;
use HCCNetwork\User;
use HCCNetwork\faculty;
use HCCNetwork\Attendance;
use DB;

class StudentAttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
    }

    public function index()
    {
        $attendances = array();
        $batch = array();
        $attendance = array();

        foreach ($facultylink = Faculty::all() as $faculty) {
            $attendance['faculty'] = $faculty;
            $attendance['batch'] = array();
            // get all batch of following faculty
            $batchs = Attendance::rightJoin('students', 'attendances.student_id', '=', 'students.id')
                ->join('batch_level_maping', 'students.batch', '=', 'batch_level_maping.batch')
                ->leftJoin('subjects', 'attendances.subject_id', '=', 'subjects.id')
                ->where('students.faculty_id', $faculty->id)
                ->where('students.deleted_at', null)
                ->where('batch_level_maping.level', '<>', 0)
                ->select('students.batch as batch')
                ->groupBy('batch')
                ->get()
                ->toArray();
            $attendance['batch-list'] = $batchs;

            foreach ($batchs as $batch) {
                $batch['name'] = $batch['batch'];
                $batch['attendance-date'] = date("m/d/Y");

                // get array of students of following batch ans faculty
                $students = Attendance::join('students', 'attendances.student_id', '=', 'students.id')
                    ->where('students.faculty_id', $faculty->id)
                    ->where('students.batch', $batch['batch'])
                    ->where('students.deleted_at', null)
                    ->select('students.id as id', 'students.first_name as first_name', 'students.last_name as last_name',
                        'students.mobile_number as mobile_number', 'students.phone_number as phone_number')
                    ->orderBy('students.first_name')
                    ->groupBy('students.first_name')
                    ->get()
                    ->toArray();

                // get subjects of current batch and level or semester
                $subjects = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                    ->join('students', 'attendances.student_id', '=', 'students.id')
                    // fetch teh subject of current mapped logic start
                    ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
                    ->where('batch_level_maping.faculty_id', $faculty->id)
                    ->where('batch_level_maping.batch', $batch['batch'])
                    //end
                    ->where('subjects.faculty_id', $faculty->id)
                    ->where('students.deleted_at', null)
                    ->where('students.batch', $batch['batch'])
                    ->select('subjects.id as id', 'subjects.name as subject_name')
                    ->orderBy('subjects.name')
                    ->groupBy('subjects.name')
                    ->get()
                    ->toArray();

                $batch['subject-list'] = $subjects;
                $batch['students'] = array();

                foreach ($students as $student) {

                    $student['name'] = $student['first_name'] . ' ' . $student['last_name'];
                    $student['contact'] = $student['mobile_number'];
                    $student['parent_contact'] = $student['phone_number'];

                    $student ['subjects'] = array();
                    foreach ($subjects as $subject) {

                        $subjectAttendance = array();

                        array_push($subjectAttendance, [
                            $this->getTypeAttendance(
                                $subject['id'],
                                $faculty->id,
                                $batch['name'],
                                $batch['attendance-date'],
                                $student['id'],
                                $subject['subject_name'],
                                false,
                                false
                            ),
                            Attendance::where('subject_id', $subject['id'])
                                ->where('replaced_with', 0)
                                ->where('extra', false)
                                ->select('date')
                                ->groupBy('date')
                                ->get()
                                ->count()
                        ]);

                        array_push($subjectAttendance, [
                            $this->getTypeAttendance(
                                $subject['id'],
                                $faculty->id,
                                $batch['name'],
                                $batch['attendance-date'],
                                $student['id'],
                                $subject['subject_name'],
                                true,
                                false
                            ),
                            Attendance::where('subject_id', $subject['id'])
                                ->where('replaced_with', '<>', 0)
                                ->where('extra', false)
                                ->select('date')
                                ->groupBy('date')
                                ->get()
                                ->count()
                        ]);

                        array_push($subjectAttendance, [
                            $this->getTypeAttendance(
                                $subject['id'],
                                $faculty->id,
                                $batch['name'],
                                $batch['attendance-date'],
                                $student['id'],
                                $subject['subject_name'],
                                false,
                                true
                            ),
                            Attendance::where('subject_id', $subject['id'])
                                ->where('replaced_with', 0)
                                ->where('extra', true)
                                ->select('date')
                                ->groupBy('date')
                                ->get()
                                ->count()
                        ]);

                        // finally push all the attendance
                        array_push($student['subjects'],$subjectAttendance);

                    }
                    array_push($batch['students'], $student);
                }

                array_push($attendance['batch'], $batch);

            }
            array_push($attendances, $attendance);
        }


        return View::make('teacher.student-attendance.today-attendance')
            ->with('title', 'Routine')
            ->with('batches', student::groupBy('batch')->pluck('batch'))
            ->with('faculties', faculty::all())
            ->with('Routine', Routine::all())
            ->with('students', Student::all())
            ->with('subjects', Subject::all())
            ->with('attendances', $attendances);
    }

    private function getTypeAttendance($subjectId, $facultyId, $batchName, $attendanceDate, $studentId, $subjectName, $replace, $extra)
    {

        //count the total attendanc of all student on regular classes
        $regSubQuery = DB::table('attendances as att')->select(DB::raw('count(attendance) as total_attendance'), 'student_id as sid')
            ->where('subject_id', '=', $subjectId)
            ->where('attendance', '=', 1)
            ->where(function ($sql) use ($replace) {
                if ($replace)
                    $sql->where('replaced_with', '<>', 0);
                else
                    $sql->where('replaced_with', 0);
            })
            ->where('extra', $extra)
            ->groupBy('att.student_id');

        // get today attendance
        $regAttendance = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
            ->join('students', 'attendances.student_id', '=', 'students.id')
            // join attendance of each student
            ->leftJoin(DB::raw(' ( ' . $regSubQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
            ->mergeBindings($regSubQuery)
            ->where('subjects.faculty_id', $facultyId)
            ->where('students.batch', $batchName)
            ->where('students.id', $studentId)
            ->where('students.deleted_at', null)
            ->where('attendances.date',date_format(date_create($attendanceDate), "Y-m-d"))
            //regular classes
            ->where(function ($sql) use ($replace) {
                if ($replace)
                    $sql->where('attendances.replaced_with', '<>', 0);
                else
                    $sql->where('attendances.replaced_with', 0);

            })
            ->where('attendances.extra', $extra)
            //
            ->where('subjects.name', $subjectName)
            ->select('attendances.attendance as attendance', 'subjects.name as subject', 'counted.total_attendance as total_attendance')
            ->orderBy('subjects.name')
            ->groupBy('subjects.name')
            ->get();

        // This is added if attendance is null ie attendance is not taken today
        if (count($regAttendance) == 0) {
            $regAttendance = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                ->join('students', 'attendances.student_id', '=', 'students.id')
                ->leftJoin(DB::raw(' ( ' . $regSubQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                ->mergeBindings($regSubQuery)
                ->where('subjects.faculty_id', $facultyId)
                ->where('students.batch', $batchName)
                ->where('students.id', $studentId)
                ->where('students.deleted_at', null)
                // removing below clause extract attendance of any date
//                                ->where('attendances.date', date_format(date_create($batch['attendance-date']), "Y-m-d"))
                ->where('subjects.name', $subjectName)
                //Regular attendance login start
                ->where(function ($sql) use ($replace) {
                    if ($replace)
                        $sql->where('attendances.replaced_with', '<>', 0);
                    else
                        $sql->where('attendances.replaced_with', 0);

                })
                ->where('attendances.extra', $extra)
                //Regular attendance login end
                ->select('attendances.attendance as attendance', 'subjects.name as subject', 'counted.total_attendance as total_attendance')
                ->orderBy('subjects.name')
                ->groupBy('subjects.name')
                ->get();


            if (count($regAttendance) == 0) {
                //if attendance is still 0 ie there is no attendance of particular subject
                //get any record from database and make attendance 2 for not taken
                $regAttendance = Attendance::limit(1)->get();
                $regAttendance[0]->attendance = 2;
            } else {
                // mark attendance at 2 for not taken
                // index 0 use used because there will be only one record
                $regAttendance[0]->attendance = 2;
            }
        }
        //

        return $regAttendance[0];

    }

    public function dayWise(Request $request)
    {
        $batch = array();
        $batch['name'] = $request->input('batch');
        $batch['attendance-date'] = $request->input('date');

        $students = Attendance::join('students', 'attendances.student_id', '=', 'students.id')
            ->where('students.faculty_id', faculty::where('name', $request->input('faculty'))->pluck('id')->first())
            ->where('students.batch', $request->input('batch'))
            ->where('students.deleted_at', null)
            ->select('students.id as id', 'students.first_name as first_name', 'students.last_name as last_name', 'students.mobile_number as mobile_number', 'students.phone_number as phone_number')
            ->orderBy('students.first_name')
            ->groupBy('students.first_name')
            ->get()
            ->toArray();

        $subjects = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
            ->join('students', 'attendances.student_id', '=', 'students.id')
            // fetch teh subject of current mapped logic start
            ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
            ->where('batch_level_maping.faculty_id', faculty::where('name', $request->input('faculty'))->pluck('id')->first())
            ->where('batch_level_maping.batch', $request->input('batch'))
            //end
            ->where('subjects.faculty_id', faculty::where('name', $request->input('faculty'))->pluck('id')->first())
            ->where('students.batch', $request->input('batch'))
            ->where('students.deleted_at', null)
            ->select('subjects.id as id', 'subjects.name as subject_name')
            ->orderBy('subjects.name')
            ->groupBy('subjects.name')
            ->get()
            ->toArray();

        $batch['subject-list'] = $subjects;
        $batch['students'] = array();

        foreach ($students as $student) {

            $student['name'] = $student['first_name'] . ' ' . $student['last_name'];
            $student['contact'] = $student['mobile_number'];
            $student['parentContact'] = $student['phone_number'];

            $student ['subjects'] = array();
            foreach ($subjects as $subject) {

                $subjectAttendance = array();

                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        faculty::where('name', $request->input('faculty'))->pluck('id')->first(),
                        $batch['name'],
                        $batch['attendance-date'],
                        $student['id'],
                        $subject['subject_name'],
                        false,
                        false
                    ),
                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', 0)
                        ->where('extra', false)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                ]);

                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        faculty::where('name', $request->input('faculty'))->pluck('id')->first(),
                        $batch['name'],
                        $batch['attendance-date'],
                        $student['id'],
                        $subject['subject_name'],
                        true,
                        false
                    ),
                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', '<>', 0)
                        ->where('extra', false)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                ]);

                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        faculty::where('name', $request->input('faculty'))->pluck('id')->first(),
                        $batch['name'],
                        $batch['attendance-date'],
                        $student['id'],
                        $subject['subject_name'],
                        false,
                        true
                    ),
                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', 0)
                        ->where('extra', true)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                ]);

                // finally push all the attendance
                array_push($student['subjects'],$subjectAttendance);

            }
            array_push($batch['students'], $student);
        }

        return View::make('teacher.student-attendance.attendance-day-wise')
            ->with('batches', student::groupBy('batch')->pluck('batch'))
            ->with('faculties', faculty::all())
            ->with('students', Student::all())
            ->with('title', 'Attendance-Daywise')
            ->with('faculty', $request->input('faculty'))
            ->with('batch', $batch);
    }

    public function studentWise(Request $request)
    {
        if ($request->input('student') == null || $request->input('date-range') == null) return redirect()->back();

        $user = User::find($request->input('student'));
        $studentId = $request->input('student');
        $dateRange = (explode('-', $request->input('date-range')));
        $start_date = date_format(date_create($dateRange[0]), "Y-m-d");
        $end_date = date_format(date_create($dateRange[1]), "Y-m-d");

        $batch = array();
        $batch['name'] = $request->input('batch');
        $batch['attendance-date'] = $request->input('date');

        $dates = Attendance:: where('student_id', $studentId)
            ->whereBetween('date', [$start_date, $end_date])
            ->select('date')
            ->orderBy('date', 'desc')
            ->groupBy('date')
            ->get()
            ->toArray();

        $subjects = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
            ->where('attendances.student_id', $studentId)
            // fetch teh subject of current mapped logic start
            ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
//            ->where('batch_level_maping.faculty_id', faculty::where('name', $request->input('faculty'))->pluck('id')->first())
//            ->where('batch_level_maping.batch', $request->input('batch'))
            //end
            ->select('subjects.id as id', 'subjects.name as subject_name')
            ->orderBy('subjects.name')
            ->groupBy('subjects.name')
            ->get()
            ->toArray();

        $batch['subject-list'] = $subjects;
        $batch['date'] = array();

        foreach ($dates as $date) {
            $day['current-date'] = $date['date'];
            $day ['subjects'] = array();
            foreach ($subjects as $subject) {

                $subAttendance = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                    ->where('attendances.student_id', $studentId)
                    ->where('attendances.date', $day['current-date'])
                    ->where('subjects.name', $subject['subject_name'])
                    ->select('attendances.attendance as attendance')
                    ->orderBy('subjects.name')
                    ->groupBy('subjects.name')
                    ->get();

                if (count($subAttendance) == 0) {
                    $subAttendance = Attendance::limit(1)->get();
                    $subAttendance[0]->attendance = 2;
                }

                array_push($day['subjects'], $subAttendance);

            }
            array_push($batch['date'], $day);
        }

        return View::make('teacher.student-attendance.attendance-student-wise')
            ->with('title', 'Attendance-Daywise')
            ->with('batches', student::groupBy('batch')->pluck('batch'))
            ->with('faculties', faculty::all())
            ->with('students', Student::all())
            ->with('faculty', $request->input('faculty'))
            ->with('student', $user->student->first_name . ' ' . $user->student->last_name)
            ->with('student_id', $user->id)
            ->with('dateRange', $request->input('date-range'))
            ->with('batch', $batch);
    }


    public function show($id)
    {

    }

    public function destroy($id)
    {
        Routine::find($id)->delete();
        return redirect()->back();
    }

    public function store(RoutineRequest $request)
    {


        $teacher = User::where('name', $request->input('teacher'))->first();

        $subject = Subject::where('name', $request->input('subject'))->first();
        $start_at = $request->input('start_at');
        $end_at = $request->input('end_at');
        $start_at_sec = strtotime("1970-01-01 $start_at UTC");
        $end_at_sec = strtotime("1970-01-01 $end_at UTC");
        $day = null;
        $dayInput = $request->input('day');

        switch ($dayInput) {
            case "Sunday":
                $day = 1;
                break;
            case "Monday":
                $day = 2;
                break;
            case "Tuesday":
                $day = 3;
                break;
            case "Wednesday":
                $day = 4;
                break;
            case "Thursday":
                $day = 5;
                break;
            case "Friday":
                $day = 6;
                break;
            case "Saturday":
                $day = 7;
                break;
        }


        if ($start_at_sec < 21600 || $end_at_sec > 37800 || $start_at_sec > $end_at_sec) return "Error on time interval";

        foreach ($hisRoutine = Routine::where('teacher_id', $teacher->id)->where('day', $day)->get() as $timeInterval) {
            if ($start_at_sec > $timeInterval->start_at
                && $start_at_sec < $timeInterval->end_at
                ||
                $end_at_sec > $timeInterval->start_at
                && $end_at_sec < $timeInterval->end_at
            )
                return "Time Overlap";
        }

        Routine::create(array(
            'teacher_id' => $teacher->id,
            'subject_id' => $subject->id,
            'day' => $day,
            'start_at' => $start_at_sec,
            'end_at' => $end_at_sec,
        ));
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {

    }

    public function getStudent(Request $request)
    {
        return student::join('batch_level_maping', function ($join) {
            $join->on('batch_level_maping.faculty_id', '=', 'students.faculty_id')
                ->on('batch_level_maping.batch', '=', 'students.batch');
        })
            ->where('students.faculty_id', $request->input('faculty'))
            ->where('students.batch', $request->input('batch'))
            ->where('students.deleted_at', null)
            ->where('batch_level_maping.level', $request->input('level'))
            ->select('students.id as id', 'students.first_name as first_name', 'students.last_name as last_name')
            ->get();
    }

    public function slugify($text)
    {
        $text = preg_split('/[^[:alnum:]]+/', $text);
        $finalText = "";
        foreach ($text as $value) {
            if ($finalText == "") $finalText = $value;
            else $finalText = $finalText . "-" . $value;
        }
        return $finalText;
    }


    public function downloadStudentAttendance(Request $request)
    {
        $faculty = faculty::find($request->input('faculty_id'));

        $subjects = Subject::
        // it must be in attendance
        join('attendances', 'subjects.id', 'attendances.subject_id')
            ->where('subjects.batch', $request->input('batch_name'))
            ->where('subjects.faculty_id', $request->input('faculty_id'))
            ->where('subjects.level', $request->input('level'))
            ->select('subjects.id as id', 'subjects.name as name')
            ->groupBy('subjects.id')
            ->get();

        foreach ($subjects as $subject) {
            $subject->name = $this->slugify($subject->name);
            str_replace('*', '', $subject->name);
            str_replace(':', '', $subject->name);
            str_replace('/', '', $subject->name);
            str_replace('\\', '', $subject->name);
            str_replace('?', '', $subject->name);
            str_replace('[', '', $subject->name);
            str_replace(']', '', $subject->name);
            $subject->name = substr($subject->name, 0, 30);
        }


        if (count($subjects) > 0)
            Excel::create('attendance-' . $faculty->name . '-' . $request->input('batch_name') . '-' . $request->input('level'), function ($excel) use ($request, $subjects) {

                $heading = array();

                array_push($heading, 'Date');
                array_push($heading, 'Type');
                array_push($heading, 'Time');

                $studentTable = student::where('batch', $request->input('batch_name'))
                    ->where('faculty_id', $request->input('faculty_id'))
                    ->select(
                        'students.id as id',
                        DB::raw("concat(students.first_name,' ',students.middle_name,' ',students.last_name) as name")
                    )
                    ->orderBy('students.first_name')
                    ->get();

                foreach ($studentTable as $student) {
                    array_push($heading, $student->name);
                }

                foreach ($subjects as $subject) {


                    // sheet array
                    $sheetArray = array();

                    array_push($sheetArray, $heading);

                    $attendances = Attendance::join('subjects', 'attendances.subject_id', 'subjects.id')
                        ->join('students', 'attendances.student_id', 'students.id')
                        ->where('subjects.batch', $request->input('batch_name'))
                        ->where('subjects.faculty_id', $request->input('faculty_id'))
                        ->where('subjects.level', $request->input('level'))
                        ->where('subjects.id', $subject->id)
                        ->select(
                            'attendances.student_id as student_id',
                            'subjects.id as subject_id',
                            'attendances.date as date',
                            DB::raw("concat(students.first_name,' ',students.middle_name,' ',students.last_name) as name"),
                            DB::raw("CASE WHEN attendances.extra = 1 THEN 'Extra' WHEN attendances.replaced_with <> 0 THEN 'Replace' ELSE 'Regular' END as type"),
                            'attendances.start_time as start_time',
                            'attendances.end_time as end_time',
                            DB::raw("CASE WHEN attendances.attendance = 1 THEN 'P' WHEN attendances.attendance = 0 THEN 'A' ELSE '-' END as attendance")
                        )
//                        ->groupBy('students.id')
                        //->orderBy('attendances.date')
                        ->get();


                    $dateArray = Attendance::join('subjects', 'attendances.subject_id', 'subjects.id')
                        ->join('students', 'attendances.student_id', 'students.id')
                        ->where('subjects.batch', $request->input('batch_name'))
                        ->where('subjects.faculty_id', $request->input('faculty_id'))
                        ->where('subjects.level', $request->input('level'))
                        ->where('subjects.id', $subject->id)
                        ->select(
                            'attendances.date as date'
                        )
                        ->groupBy('attendances.date')
                        ->orderBy('attendances.date')
                        ->get();

                    foreach ($dateArray as $date) {

                        $regularClassAttendance = array();
                        $replacedClassAttendance = array();
                        $extraClassAttendance = array();


                        foreach ($attendances as $attendance) {
                            if ($attendance->date == $date->date) {
                                if ($attendance->type == "Extra") {
                                    array_push($extraClassAttendance, $attendance);
                                } else if ($attendance->type == "Replace") {
                                    array_push($replacedClassAttendance, $attendance);
                                } else {
                                    array_push($regularClassAttendance, $attendance);
                                }
                            }
                        }


                        //For regular class create single row
                        $singleRow = array();
                        //push date as first colom
                        array_push($singleRow, $date->date);
                        //this is regular class so pushed it as second column
                        array_push($singleRow, 'Regular');
                        // search the time range from regular class of this class
                        $dateFound = false;

                        foreach ($regularClassAttendance as $regular) {
                            if ($regular->date == $date->date) {
                                array_push($singleRow, $regular->start_time . '-' . $regular->end_time);
                                $dateFound = true;
                                break;
                            }
                        }

                        if ($dateFound) {
                            foreach ($studentTable as $singleStudent) {
                                $foundStudent = false;
                                foreach ($regularClassAttendance as $regular) {
                                    if ($regular->student_id == $singleStudent->id) {
                                        array_push($singleRow, $regular->attendance);
                                        $foundStudent = true;
                                        break;
                                    }
                                }
                                if (!$foundStudent) {
                                    array_push($singleRow, ' - ');
                                }
                            }

                            array_push($sheetArray, $singleRow);
                        }

                        // FOR REPLACE CLASS

                        $singleRow = array();
                        //push date as first colom
                        array_push($singleRow, $date->date);
                        //this is regular class so pushed it as second column
                        array_push($singleRow, 'Replaced');
                        // search the time range from regular class of this class
                        $dateFound = false;
                        foreach ($replacedClassAttendance as $replace) {
                            if ($replace->date == $date->date) {
                                array_push($singleRow, $replace->start_time . '-' . $replace->end_time);
                                $dateFound = true;
                                break;
                            }
                        }
                        if ($dateFound) {
                            foreach ($studentTable as $singleStudent) {
                                $foundStudent = false;
                                foreach ($replacedClassAttendance as $replace) {
                                    if ($replace->student_id == $singleStudent->id) {
                                        array_push($singleRow, $replace->attendance);
                                        $foundStudent = true;
                                        break;
                                    }
                                }
                                if (!$foundStudent) {
                                    array_push($singleRow, ' - ');
                                }
                            }

                            array_push($sheetArray, $singleRow);
                        }


                        // FOR Extra CLASS

                        $singleRow = array();
                        //push date as first colom
                        array_push($singleRow, $date->date);
                        //this is regular class so pushed it as second column
                        array_push($singleRow, 'Extra');
                        // search the time range from regular class of this class
                        $dateFound = false;
                        foreach ($extraClassAttendance as $extra) {
                            if ($extra->date == $date->date) {
                                array_push($singleRow, $extra->start_time . '-' . $extra->end_time);
                                $dateFound = true;
                                break;
                            }
                        }
                        if ($dateFound) {
                            foreach ($studentTable as $singleStudent) {
                                $foundStudent = false;
                                foreach ($extraClassAttendance as $extra) {
                                    if ($extra->student_id == $singleStudent->id) {
                                        array_push($singleRow, $extra->attendance);
                                        $foundStudent = true;
                                        break;
                                    }
                                }
                                if (!$foundStudent) {
                                    array_push($singleRow, ' - ');
                                }
                            }
                            array_push($sheetArray, $singleRow);
                        }
                    }

                    //final footer


                    $singleRow = array();
                    array_push($singleRow, 'Total Classes');
                    //total attendance
                    $totalClasses =
                        //regular attendance
                        attendance::where('subject_id', $subject->id)
                            ->where('replaced_with', 0)
                            ->where('extra', false)
                            ->groupBy('date')
                            ->get()
                            ->count()
                        +
                        //replaced attendance
                        attendance::where('subject_id', $subject->id)
                            ->where('replaced_with', '<>', 0)
                            ->where('extra', false)
                            ->groupBy('date')
                            ->get()
                            ->count()
                        +
                        attendance::where('subject_id', $subject->id)
                            ->where('replaced_with', 0)
                            ->where('extra', true)
                            ->groupBy('date')
                            ->get()
                            ->count();


                    array_push($singleRow, $totalClasses);
                    array_push($singleRow, '');
                    // calculate each student class attend sum
                    foreach ($studentTable as $singleStudent) {
                        $totalAttendanceOfStudent = attendance::where('student_id', $singleStudent->id)
                            ->where('subject_id', $subject->id)
                            ->where('attendance', true)
                            ->count();
                        $attendPer = intval(($totalAttendanceOfStudent / $totalClasses) * 100);
                        array_push($singleRow, $totalAttendanceOfStudent . ' ( ' . $attendPer . '% )');
                    }

                    array_push($sheetArray, $singleRow);

                    $excel->sheet($subject->name, function ($sheet) use ($sheetArray) {
                        $sheet->fromModel($sheetArray);
                    });
                }

            })
                ->export('xlsx');

// or
//                ->store('xlsx');
        return redirect()->back();
    }

}