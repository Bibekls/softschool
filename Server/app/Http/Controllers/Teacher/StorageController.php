<?php
namespace HCCNetwork\Http\Controllers\Teacher;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\Post;
use HCCNetwork\Url;
use Carbon;
use Storage;

use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Folders
{
	public $folderName;
}
class Files
{
	public $fileName;
	public $extension;	
	public $lastModifiedDate;
	public $size;

}

class StorageController extends Controller
{
    public $_STORAGE_LIMIT=10240;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');      
    }  
    public function baseUrl(){
    	return $baseUrl=Auth::user()->user_type_id."/".Auth::user()->name.'/';
    }

    public function folderSize($dir)
    {
        $size = 0;
        foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : $this->folderSize($each);
        }
        return (int)($size/1024);
    }

    public function getFolders($directory){
        /*
        **  Directory 
        */
        $folders=array();
        foreach(Storage::directories($directory) as $folder){ 
            $fileObject=new Folders;
            $info=pathinfo($folder);
            $fileObject->folderName=$info['basename'];                   
            
            array_push($folders,$fileObject);
        }
        return $folders;
    }

    public function getFiles($directory){        
        /*
        **  Files
        */
        $files=array();
        foreach(Storage::files($directory) as $file){
            $fileObject=new Files;
            $info=pathinfo($file);
            $fileObject->fileName=$info['basename'];
            $fileObject->extension=$info['extension'];
            $fileObject->size = Storage::size($file);
            $fileObject->lastModifiedDate = Storage::lastModified($file);
            
            array_push($files,$fileObject);
        }
        return $files;
    }

    public function drive()
    {    	
    	if(empty(Storage::directories( $this->baseUrl()))){
    		Storage::makeDirectory(	$this->baseUrl() );
    		Storage::makeDirectory(	$this->baseUrl()."public");
    		Storage::makeDirectory(	$this->baseUrl()."private");
            Session::put('fileManagerError','There was No file in your dirve. System has created default folder for you. Please upload/create file in private folder and move it to public folder if you want to share in the Network.');   
    	}

    	
        return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl()))
                        ->with('files',$this->getFiles($this->baseUrl()))
                        ->with('directory',null)
                        ->with('view',false);

    }   

    public function directory(Request $request)
    {
        if($request->input('directory')!=null){
            if($request->input('folderName')!=null){
                $directory=$request->input('directory').'/'.$request->input('folderName');
            } 
            else $directory=$request->input('directory');
        }          
        else $directory=$request->input('folderName');
        
        if($directory=='.')$directory=null;
        return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl().$directory))
                        ->with('files',$this->getFiles($this->baseUrl().$directory))
                        ->with('directory',$directory)
                        ->with('view',false);                                  
                              
    }
    public function oneLevelUp(Request $request)
    { 
        $directory=dirname($request->input('directory'));
        if($directory=='.')$directory=null;
        return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl().$directory))
                        ->with('files',$this->getFiles($this->baseUrl().$directory))
                        ->with('directory',$directory)
                        ->with('view',false);                                  
    }

    public function makeDirectory(Request $request)
    {
        $directory=$request->input('directory');
        $folderName=$request->input('folderName');

        if(empty(Storage::directories( $this->baseUrl().$directory.'/'.$folderName ))){
            Storage::makeDirectory( $this->baseUrl().$directory.'/'.$folderName );            
        }

        return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl().$directory))
                        ->with('files',$this->getFiles($this->baseUrl().$directory))
                        ->with('directory',$directory)
                        ->with('view',false);                                    
    }

    public function createFile(Request $request)
    {
        $directory=$request->input('directory');
        $fileName=$request->input('fileName');
       
        if(!Storage::exists($this->baseUrl().$directory.'/'.$fileName))
        Storage::put($this->baseUrl().$directory.'/'.$fileName,'Start Document in cloud...');    
        else Session::put('fileManagerError','untitled.html already exist in this directory please remane it and add file.');      
        

        return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl().$directory))
                        ->with('files',$this->getFiles($this->baseUrl().$directory))
                        ->with('directory',$directory)
                        ->with('view',false);                                    
    }

    public function deleteFilesAndFolder(Request $request)
    { 
        $directory=$this->baseUrl().$request->input('directory').'/'.$request->input('fileAndFolder');
        $fullDirectory='../storage/app/'.$directory;
             
        if(is_file($fullDirectory)) {
                Storage::delete($directory);
        }else{ 
                Storage::deleteDirectory($directory);
        }
       return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl().$request->input('directory')))
                        ->with('files',$this->getFiles($this->baseUrl().$request->input('directory')))
                        ->with('directory',$request->input('directory'))
                        ->with('view',false);     
    	                                  
    }
    public function copyFilesAndFolder(Request $request)
    { 
        $directory=$this->baseUrl().$request->input('directory').'/'.$request->input('fileAndFolder');
        $fullDirectory='../storage/app/'.$directory;

        $targetDirectory=$this->baseUrl().$request->input('targetDirectory');
        $fullTargetDirectory='../storage/app/'.$targetDirectory;        
           
        if(is_file($fullDirectory))
        {
            if(is_dir($fullTargetDirectory))
            {
                 if(!Storage::exists($targetDirectory.'/'.$request->input('fileAndFolder') )){
                 Storage::copy($directory,$targetDirectory.'/'.$request->input('fileAndFolder')); 
                }
            }               
        }else if(is_dir($fullDirectory))
        { 
            if(is_dir($fullTargetDirectory))
            {
                foreach (Storage::allFiles($directory) as $temp) 
                { 
                    $choppedtemp=substr($temp,strlen( $this->baseUrl().$request->input('directory') )); 
                    if(!Storage::exists($targetDirectory.'/'.$choppedtemp )){
                        Storage::copy($temp,$targetDirectory.'/'.$choppedtemp); 
                    } 
                } 
            }              
        }

       return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($targetDirectory))
                        ->with('files',$this->getFiles($targetDirectory))
                        ->with('directory',$request->input('targetDirectory'))
                        ->with('view',false);     
                                          
    }

    public function moveFilesAndFolder(Request $request)
    { 
        $directory=$this->baseUrl().$request->input('directory').'/'.$request->input('fileAndFolder');
        $fullDirectory='../storage/app/'.$directory;

        $targetDirectory=$this->baseUrl().$request->input('targetDirectory');
        $fullTargetDirectory='../storage/app/'.$targetDirectory;        
           
        if(is_file($fullDirectory))
        {
            if(is_dir($fullTargetDirectory))
            {                 
                if(!Storage::exists($targetDirectory.'/'.$request->input('fileAndFolder') )){
                 Storage::move($directory,$targetDirectory.'/'.$request->input('fileAndFolder')); 
                }
            }               
        }else if(is_dir($fullDirectory))
        { 
            if(is_dir($fullTargetDirectory))
            {               
                foreach (Storage::allFiles($directory) as $temp) 
                {   
                    $choppedtemp=substr($temp,strlen( $this->baseUrl().$request->input('directory') )); 
                    echo $choppedtemp; 
                    if(!Storage::exists($targetDirectory.'/'.$choppedtemp )){
                        Storage::move($temp,$targetDirectory.'/'.$choppedtemp); 
                    }                    
                }
                
            }              
        }

       return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($targetDirectory))
                        ->with('files',$this->getFiles($targetDirectory))
                        ->with('directory',$request->input('targetDirectory'))
                        ->with('view',false);     
                                          
    }
    public function rename(Request $request)
    {  

        $directory=$this->baseUrl().$request->input('directory').'/'.$request->input('fileAndFolder');
        $oldfile='../storage/app/'.$directory;            
        $info=pathinfo($request->input('fileAndFolder'));
       
        if(is_file($oldfile))
        { 
            $newfile='../storage/app/'.$this->baseUrl().$request->input('directory').'/'.$request->input('rename').'.'.$info['extension'];          
            if($request->input('fileAndFolder')!=$request->input('rename').'.'.$info['extension']){                           
                if (!rename($oldfile,$newfile)) {
                    if (copy ($oldfile,$newfile)) {
                        unlink($oldfile);                
                    }        
                }  
            }  
        }
        if(is_dir($oldfile))
        {
            /*  Create new folder
            */
            if($request->input('fileAndFolder')!=$request->input('rename')){
                $newfolder=$this->baseUrl().$request->input('directory').'/'.$request->input('rename'); 
                Storage::makeDirectory( $newfolder ); 

                /*  Move
                */
                foreach (Storage::allFiles($directory) as $temp) 
                {   
                    $choppedtemp=substr($temp,strlen( $this->baseUrl().$request->input('directory').'/'.$request->input('fileAndFolder') ));                      
                    Storage::move($temp,$newfolder.'/'.$choppedtemp);
                }                       
                
                /*  Delete
                */     
                Storage::deleteDirectory($directory);      

            }
            
        }

        return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders( $this->baseUrl().$request->input('directory')))
                        ->with('files',$this->getFiles( $this->baseUrl().$request->input('directory')))
                        ->with('directory',$request->input('directory'))
                        ->with('view',false);     
                                          
    }
    public function upload(Request $request)
    {
        if($this->folderSize('../storage/app/'.$this->baseUrl())< $this->_STORAGE_LIMIT)
        {
            $directory=$request->input('directory');
            $filename=null;
            if($file=$request->file('uploadFile')){
                $filename=$file->getClientOriginalName();
                $fileinformation=pathinfo($filename);
                $supportedExtension=array(
                       'txt','doc','dot','dotx','docx','xls','xlsx','xlm','ppt','pot','pps','pdf','jpg','jpeg','tif','gif','png','bmp','mp4','html','htm','css','js','zip','7z','sql'
                    );
                $supportedFileFormat="'txt', 'doc', 'dot', 'dotx', 'docx', 'xls', 'xlsx', 'xlm', 'ppt', 'pot', 'pps', 'pdf', 'jpg', 'jpeg', 'tif', 'gif', 'png', 'bmp', 'mp4', 'html', 'htm', 'css', 'js', 'zip', '7z', 'sql'";
                if(in_array($fileinformation['extension'], $supportedExtension, true)){
                    $file->move('../storage/app/'.$this->baseUrl().$directory,$filename);
                }
                else Session::put('fileManagerError','Unsupported format. Supported format'.$supportedFileFormat);
            }
            
            return View::make('teacher.storage',['title'=>'Storage'])
                            ->with('folders',$this->getFolders($this->baseUrl().$directory))
                            ->with('files',$this->getFiles($this->baseUrl().$directory))
                            ->with('directory',$directory)
                            ->with('view',false);   
        }else return 'Memory full';
    }   
    
    public function view(Request $request)
    {
        $directory=$this->baseUrl().$request->input('directory').'/'.$request->input('fileAndFolder');
        $fullDirectory='../storage/app/'.$directory;
             
        if(is_file($fullDirectory)) {
            $tempfile='temp/'.uniqid().$request->input('fileAndFolder');
            Storage::copy($directory,$tempfile);
            rename('../storage/app/'.$tempfile,$tempfile);            
        }else{ 
            return redirect()->back();
        }
        return View::make('teacher.storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl().$directory))
                        ->with('files',$this->getFiles($this->baseUrl().$directory))
                        ->with('directory',$tempfile)
                        ->with('view',true);   
    
    }
    
    public function download(Request $request)
    {
        $directory=$this->baseUrl().$request->input('directory').'/'.$request->input('fileAndFolder');
        $fullDirectory='../storage/app/'.$directory;
             
        if(is_file($fullDirectory)) {
            $tempfile='temp/'.uniqid().$request->input('fileAndFolder');
            Storage::copy($directory,$tempfile);
            rename('../storage/app/'.$tempfile,$tempfile);
            return redirect($tempfile);
        }else{ 
            return redirect()->back();
        }
    
    }
    public function save(Request $request){        
        Storage::put($this->baseUrl().$request->input('directory').'/'.$request->input('fileName'),$request->input('data'));
        return View::make('teacher.editor',['title'=>'Edit'])
                        ->with('data',$request->input('data') )
                        ->with('fileName',$request->input('fileName'))
                        ->with('directory',$request->input('directory'))
                        ->with('storageSession',true);
    }
    public function edit(Request $request){

        return View::make('teacher.editor',['title'=>'Edit'])
                        ->with('data',Storage::get($this->baseUrl().$request->input('directory').'/'.$request->input('fileName') ))
                        ->with('fileName',$request->input('fileName'))
                        ->with('directory',$request->input('directory'))
                        ->with('storageSession',true);
                          
    }
}
