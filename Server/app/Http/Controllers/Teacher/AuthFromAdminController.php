<?php
namespace HCCNetwork\Http\Controllers\Teacher;
use HCCNetwork\Http\Controllers\Controller;

use Auth;
use Session;
/*
**  Events
*/
use HCCNetwork\Events\student\StudentRegister;
/*
**  Model
*/
use HCCNetwork\User;
use HCCNetwork\userrequest;
use HCCNetwork\LoginHistory;


class AuthFromAdminController extends Controller
{
	public function Auth($api,$id,$remember){
        
        Auth::loginUsingId($id,false);
        $api_token=explode('api_token',$api);        
        $remember_token=explode('remember_token',$remember);
        
        if(Auth::user()->api_token==$api_token[1] && Auth::user()->remember_token==$remember_token[1]){
        
	 		User::where('id',Auth::user()->id) 
	 			->update(array(
	 				'api_token'=>str_random(60) 
	 				));      	

    	}else{    		
    		Auth::logout();
    	}
    	return redirect('/');
    }
}
