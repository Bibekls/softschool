<?php
namespace HCCNetwork\Http\Controllers\Teacher;
use HCCNetwork\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use View;

use Auth;
use Session;
/*
**  Events
*/
use Event;
use HCCNetwork\Events\teacher\TeacherRegister;
/*
**  Model
*/
use HCCNetwork\User;
use HCCNetwork\userrequest;
use HCCNetwork\LoginHistory;


/*
use HCCNetwork\faq;
use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\faculty;
use HCCNetwork\book;
use HCCNetwork\student; //always include related model
use HCCNetwork\BookIssue;
use HCCNetwork\BookReturn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Html;
use Intervention\Image\Facades\Image;
*/
class SignUpController extends Controller
{

    public function signup(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255|unique:users|unique:userrequests|alpha_dash',
                'email' => 'required|email|max:255|unique:users|unique:userrequests',            
                'password' => 'required|confirmed|min:6',
            ]);
            if ($validator->fails()) {
                return redirect('signup')
                            ->withErrors($validator)
                            ->withInput();
            }else{

                $user=userrequest::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'user_type_id'=>'3',
                'password' => bcrypt($request->input('password')),           
                'token' => str_random(60), 
                ]);

                Event::fire(new TeacherRegister($user));    
                Session::put('message','We have e-mailed your Email verification link!. Please check your email');
                return View::make('auth.signup');
            }
            
        }
        public function verifyemail(Request $request,$token){
            if ($userrequest=userrequest::where('token',$token)->first()) {

                $user=User::create([
                'name' => $userrequest->name,
                'email' => $userrequest->email,
                'verified'=>'1',
                'user_type_id'=>$userrequest->user_type_id,
                'password' => $userrequest->password,           
                'api_token' => str_random(60), 
                ]);

                $delete= userrequest::where('token',$token)->delete(); 
                LoginHistory::create(array(
                                    'user_id'=>$user->id,
                                    'ip'=>$request->getClientIp()
                ));

                Auth::attempt(['name' => $user->name,'password' => $user->password,'user_type_id'=>$user->user_type_id]);
                
                return redirect('/upload-profile-pic');

            }
            else{
                return redirect('login');
            }
        }
}