<?php
namespace HCCNetwork\Http\Controllers\Teacher;

use HCCNetwork\Attendance;
use HCCNetwork\BatchLevelMaping;
use HCCNetwork\Http\Controllers\Controller;

use HCCNetwork\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use Carbon;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\Post;
use HCCNetwork\routine;
use HCCNetwork\Http\Requests;
use Illuminate\Support\Facades\Auth;


class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
    }

    function getSubject()
    {
        $subjects = Subject::join('routines', 'subjects.id', '=', 'routines.subject_id')
            ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
            ->join('faculties', 'subjects.faculty_id', '=', 'faculties.id')
            ->where('routines.teacher_id', Auth::id())
            ->select('subjects.name as subject', 'subjects.id as id')
            ->groupBy('subject')
            ->orderBy('subject')
            ->get();

        return view('teacher.take-attendance')
            ->with('subjects', $subjects)
            ->with('title', 'Student Attendance');
    }

    function getStudentList(Request $request)
    {

        $subjectId = $request->input('id');

        $date = $request->input('date');
        $extra = $request->input('extra');
        $replace = $request->input('replace');

        $subject = Subject::find($request->input('id'));
        $batch = BatchLevelMaping::where('faculty_id', $subject->faculty_id)->where('level', $subject->level)->first()->batch;

        $teacherId = Auth::id();

        $subQuery = DB::table('attendances as att')->select(DB::raw('count(attendance) as total_attendance'), 'student_id as sid')
            ->where('subject_id', '=', $subjectId)
            ->where('teacher_id', '=', $teacherId)
            ->where('attendance', '=', 1)
            ->where(function ($query) use ($extra, $replace) {
                if ($replace) {
                    //check for replaced class
                    $query->where('replaced_with', '<>', 0);
                } else if ($extra) {
                    //check for extra class
                    $query->where('extra', '=', true);
                } else {
                    // check for regular class class
                    $query->where('extra', '=', false)->where('replaced_with', 0);
                }
            })
            ->groupBy('att.student_id');

        if (Attendance::where('subject_id', $subjectId)
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('extra', '=', false)->where('replaced_with', 0);
                    }
                })
                ->where('date', $date ?  date("Y-m-d",strtotime($date)) : date("Y-m-d"))
                ->first() == null
        ) {

            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->join('users', 'students.id', '=', 'users.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->leftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                ->mergeBindings($subQuery)
                ->select('students.id as id',
                    'students.mobile_number as contact',
                    'students.first_name as fname',
                    'students.last_name as lname',
                    'images.file_name as avatar',
                    'counted.*'
                )
                ->where('students.batch', $batch)
                ->where('students.deleted_at', null)
                ->where('subjects.id', $subjectId)
                ->where('routines.teacher_id', Auth::id())
                ->groupBy('fname')
                ->orderBy('fname')
                ->get();


        } else {


//            return DB::query()->select(DB::raw('count(attendance) as total_attendance'),'student_id')
//                ->from('attendances as att')
//                ->where('subject_id', '=', $subjectId)
//                ->where('teacher_id', '=', $teacherId)
//                ->where('attendance','=',1)
//                ->groupBy('att.student_id')->get();


            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->join('attendances as attend', 'students.id', '=', 'attend.student_id')
                ->LeftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                ->mergeBindings($subQuery)
                ->join('users', 'students.id', '=', 'users.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->select('students.id as id',
                    'students.mobile_number as contact',
                    'students.first_name as fname',
                    'students.last_name as lname',
                    'attend.attendance as attendance',
                    'images.file_name as avatar',
                    'counted.*'
                )
                ->where('students.batch', $batch)
                ->where('students.deleted_at', null)
                ->where('subjects.id', $subjectId)
                ->where('routines.teacher_id', Auth::id())
                ->where('attend.subject_id', $subjectId)
                ->where('attend.teacher_id', Auth::id())
                ->where('attend.date', $date ?  date("Y-m-d",strtotime($date)) : date("Y-m-d"))
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('attend.replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('attend.extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('attend.extra', '=', false)->where('attend.replaced_with', 0);
                    }
                })
                ->groupBy('students.first_name')
                ->orderBy('students.first_name')
                ->get();

        }

        return response()->json([
            'status' => 'success',
            'student_list' => $students,
            'totalClass' => Attendance::where('subject_id', $subjectId)
                ->where('teacher_id', Auth::id())
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('extra', '=', false)->where('replaced_with', 0);
                    }
                })
                ->select('date')
                ->groupBy('date')
                ->get()
                ->count(),
            'replaceableClass' => Subject::where('batch', $batch)
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->where('routines.day', '=', date('w') + 1)
                ->where('faculty_id', $subject->faculty_id)
                ->where('level', $subject->level)
                ->where('subjects.id', '<>', $subject->id)
                ->get()
        ]);
    }

    function takeAttendance(Request $request)
    {
        // this preprocess is required to make compatible with site request
        $attend = [];
        foreach ($request->input('studentList') as $student) {
            $match = false;
            if ($request->input('attendance'))
                foreach ($request->input('attendance') as $a) {
                    if ($a == $student) {
                        $match = true;
                        break;
                    }
                }
            if ($match)
                array_push($attend, ["id" => $student, "attendance" => true]);
            else
                array_push($attend, ["id" => $student, "attendance" => false]);
        }

        $subjectId = $request->input('subject_id');

        $extra = $request->input('extra');
        $replace = $request->input('replace');
        $update = $request->input('update');

        $date =  date("Y-m-d",strtotime($request->input('date')));

        $regularClassTime = routine::where('subject_id', $subjectId)
            ->where('day', $date ? date('w', strtotime($date)) + 1 : date('w') + 1)->first();
        $replaceClassTime = routine::where('subject_id', $request->input('replaceClass'))
            ->where('day', $date ? date('w', strtotime($date)) + 1 : date('w') + 1)->first();


        if ($replace) {
            //replace class start end time
            if ($regularClassTime) {
                $start_at = gmdate('H:i:s', $replaceClassTime->start_at);
                $end_at = gmdate('H:i:s', $replaceClassTime->end_at);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Replaced subject don\'t have class on selected date'
                ]);
            }

        } else if ($extra) {
            if ($request->input('start_time')) {
                $start_at = gmdate('H:i:s', strtotime($request->input('start_time')));
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'you must input class start time'
                ]);
            }

            if ($request->input('end_time')) {
                $end_at = gmdate('H:i:s', strtotime($request->input('end_time')));
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'you must input class end time'
                ]);
            }

        } else {
            //regular class start end time
            if ($regularClassTime) {
                $start_at = gmdate('H:i:s', $regularClassTime->start_at);
                $end_at = gmdate('H:i:s', $regularClassTime->end_at);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'You don\'t have class on selected date'
                ]);
            }

        }

        foreach ($attend as $attendance) {

            //check if the student on particular date and particular subject on particular type exixts or not
            if (Attendance::where('subject_id', $subjectId)
                    ->where('student_id', $attendance['id'])
                    ->where('date', $update ? date("Y-m-d",strtotime($date)) : date("Y-m-d"))
                    ->where(function ($query) use ($extra, $replace) {
                        if ($replace) {
                            //check for replaced class
                            $query->where('replaced_with', '<>', 0);
                        } else if ($extra) {
                            //check for extra class
                            $query->where('extra', '=', true);
                        } else {
                            // check for regular class class
                            $query->where('extra', '=', false)->where('replaced_with', 0);
                        }
                    })
                    ->first() == null
            ) {


                //this is new entries so we need to insert record
                Attendance::create(array(
                    'student_id' => $attendance['id'],
                    'teacher_id' => Auth()->id(),
                    'attendance' => $attendance['attendance'],
                    'date' => $update ? date($date) : date("Y-m-d"),
                    'subject_id' => $subjectId,
                    'replaced_with' => $request->input('replace') ? $request->input('replaceClass') : 0,
                    'extra' => $request->input('extra') ? $request->input('extra') : 0,
                    //if extra then time is fetch from request
                    //if replaced then time is fetch from replaced_by subject from routine
                    //if regular then time is fetch from regular subject from routine
                    'start_time' => $start_at,
                    'end_time' => $end_at

                ));

            } else {



                Attendance::where('subject_id', $subjectId)
                    ->where('student_id', $attendance['id'])
                    ->where('date', $update ? date($date) : date("Y-m-d"))
                    ->where(function ($query) use ($extra, $replace) {
                        if ($replace) {
                            //check for replaced class
                            $query->where('replaced_with', '<>', 0);
                        } else if ($extra) {
                            //check for extra class
                            $query->where('extra', '=', true);
                        } else {
                            // check for regular class class
                            $query->where('extra', '=', false)->where('replaced_with', 0);
                        }
                    })
                    ->update(array(
                        'student_id' => $attendance['id'],
                        'teacher_id' => Auth()->id(),
                        'attendance' => $attendance['attendance'],
                        'date' => $update ? date($date) : date("Y-m-d"),
                        'subject_id' => $subjectId,
                        'replaced_with' => $request->input('replace') ? $request->input('replaceClass') : 0,
                        'extra' => $request->input('extra') ? $request->input('extra') : 0,
                        'start_time' => $start_at,
                        'end_time' => $end_at
                    ));
            }
        }

        return redirect()->back();

    }

    function getStudentAttendance(Request $request)
    {
        $extra = $replace = false;
        if ($request->input('type') == 3) {
            $extra = true;
        } else if ($request->input('type') == 2) {
            $replace = true;
        }

        $attendance = Attendance::where('student_id', $request->input('id'))
            ->where('subject_id', $request->input('subject_id'))
            ->where(function ($query) use ($extra, $replace) {
                if ($replace) {
                    //check for replaced class
                    $query->where('replaced_with', '<>', 0);
                } else if ($extra) {
                    //check for extra class
                    $query->where('extra', '=', true);
                } else {
                    // check for regular class class
                    $query->where('extra', '=', false)->where('replaced_with', 0);
                }
            })
            ->select('date', 'attendance')
            ->get();

        $dateCollector = collect([]);
        foreach ($attendance as $att) {
            $date = $att->date;
            $newDate = date_create($date)->format('Y-m-d');
            $att->date = $newDate;
            $dateCollector->push($att->date);
        }

        return response()->json([
            "date" => $attendance,
            "start" => $dateCollector->min(),
            "end" => $dateCollector->max()
        ]);
    }


//    public function getSubject()
//    {
//        $subjects = subject::join('routines', 'subjects.id', '=', 'routines.subject_id')
//            ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
//            ->join('faculties', 'subjects.faculty_id', '=', 'faculties.id')
//            ->where('routines.teacher_id', Auth::id())
//            ->select('subjects.name as subject', 'subjects.id as id')
//            ->groupBy('subject')
//            ->orderBy('subject')
//            ->get();
//
//        return view('teacher.take-attendance')
//            ->with('subjects', $subjects)
//            ->with('title', 'Student Attendance');
//    }
//
//    function getStudentListOfDate(Request $request)
//    {
//        $subjectId = $request->input('id');
//        $date = date("Y-m-d", strtotime($request->input('date')));
//
//        $subject = Subject::find($request->input('id'));
//        $batch = BatchLevelMaping::where('faculty_id', $subject->faculty_id)->where('level', $subject->level)->first()->batch;
//
//        $teacherId = Auth::id();
//
//        $subQuery = DB::table('attendances as att')->select(DB::raw('count(attendance) as total_attendance'), 'student_id as sid')
//            ->where('subject_id', '=', $subjectId)
//            ->where('teacher_id', '=', $teacherId)
//            ->where('attendance', '=', 1)
//            ->groupBy('att.student_id');
//
//        if (Attendance::where('subject_id', $subjectId)->where('date', date($date))->first() == null) {
//
//            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
//                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
//                ->join('users', 'students.id', '=', 'users.id')
//                ->join('images', 'users.profile_pic', '=', 'images.id')
//                ->leftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
//                ->mergeBindings($subQuery)
//                ->select('students.id as id',
//                    'students.mobile_number as contact',
//                    'students.first_name as fname',
//                    'students.last_name as lname',
//                    'images.file_name as avatar',
//                    'counted.*'
//                )
//                ->where('students.batch', $batch)
//                ->where('students.deleted_at',null)
//                ->where('subjects.id', $subjectId)
//                ->where('routines.teacher_id', Auth::id())
//                ->groupBy('fname')
//                ->orderBy('fname')
//                ->get();
//
//
//        } else {
//
//
////            return DB::query()->select(DB::raw('count(attendance) as total_attendance'),'student_id')
////                ->from('attendances as att')
////                ->where('subject_id', '=', $subjectId)
////                ->where('teacher_id', '=', $teacherId)
////                ->where('attendance','=',1)
////                ->groupBy('att.student_id')->get();
//
//
//            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
//                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
//                ->join('attendances as attend', 'students.id', '=', 'attend.student_id')
//                ->LeftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
//                ->mergeBindings($subQuery)
//                ->join('users', 'students.id', '=', 'users.id')
//                ->join('images', 'users.profile_pic', '=', 'images.id')
//                ->select('students.id as id',
//                    'students.mobile_number as contact',
//                    'students.first_name as fname',
//                    'students.last_name as lname',
//                    'attend.attendance as attendance',
//                    'images.file_name as avatar',
//                    'counted.*'
//                )
//                ->where('students.batch', $batch)
//                ->where('students.deleted_at',null)
//                ->where('subjects.id', $subjectId)
//                ->where('routines.teacher_id', Auth::id())
//                ->where('attend.subject_id', $subjectId)
//                ->where('attend.teacher_id', Auth::id())
//                ->where('attend.date', date($date))
//                ->groupBy('students.first_name')
//                ->orderBy('students.first_name')
//                ->get();
//
//        }
//
//        return response()->json([
//            'student_list' => $students,
//            'totalClass' => Attendance::where('subject_id', $subjectId)
//                ->where('teacher_id', Auth::id())
//                ->select('date')
//                ->groupBy('date')
//                ->get()
//                ->count()
//        ]);
//    }
//
//
//    function getStudentList(Request $request)
//    {
//
//        $subjectId = $request->input('id');
//        $subject = Subject::find($request->input('id'));
//        $batch = BatchLevelMaping::where('faculty_id', $subject->faculty_id)->where('level', $subject->level)->first()->batch;
//
//        $teacherId = Auth::id();
//
//        $subQuery = DB::table('attendances as att')->select(DB::raw('count(attendance) as total_attendance'), 'student_id as sid')
//            ->where('subject_id', '=', $subjectId)
//            ->where('teacher_id', '=', $teacherId)
//            ->where('attendance', '=', 1)
//            ->groupBy('att.student_id');
//
//        if (Attendance::where('subject_id', $subjectId)->where('date', date("Y-m-d"))->first() == null) {
//
//            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
//                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
//                ->join('users', 'students.id', '=', 'users.id')
//                ->join('images', 'users.profile_pic', '=', 'images.id')
//                ->leftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
//                ->mergeBindings($subQuery)
//                ->select('students.id as id',
//                    'students.mobile_number as contact',
//                    'students.first_name as fname',
//                    'students.last_name as lname',
//                    'images.file_name as avatar',
//                    'counted.*'
//                )
//                ->where('students.batch', $batch)
//                ->where('students.deleted_at',null)
//                ->where('subjects.id', $subjectId)
//                ->where('routines.teacher_id', Auth::id())
//                ->groupBy('fname')
//                ->orderBy('fname')
//                ->get();
//
//
//        } else {
//
//
////            return DB::query()->select(DB::raw('count(attendance) as total_attendance'),'student_id')
////                ->from('attendances as att')
////                ->where('subject_id', '=', $subjectId)
////                ->where('teacher_id', '=', $teacherId)
////                ->where('attendance','=',1)
////                ->groupBy('att.student_id')->get();
//
//
//            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
//                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
//                ->join('attendances as attend', 'students.id', '=', 'attend.student_id')
//                ->LeftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
//                ->mergeBindings($subQuery)
//                ->join('users', 'students.id', '=', 'users.id')
//                ->join('images', 'users.profile_pic', '=', 'images.id')
//                ->select('students.id as id',
//                    'students.mobile_number as contact',
//                    'students.first_name as fname',
//                    'students.last_name as lname',
//                    'attend.attendance as attendance',
//                    'images.file_name as avatar',
//                    'counted.*'
//                )
//                ->where('students.batch', $batch)
//                ->where('students.deleted_at',null)
//                ->where('subjects.id', $subjectId)
//                ->where('routines.teacher_id', Auth::id())
//                ->where('attend.subject_id', $subjectId)
//                ->where('attend.teacher_id', Auth::id())
//                ->where('attend.date', date("Y-m-d"))
//                ->groupBy('students.first_name')
//                ->orderBy('students.first_name')
//                ->get();
//
//        }
//
//        return response()->json([
//            'student_list' => $students,
//            'totalClass' => Attendance::where('subject_id', $subjectId)
//                ->where('teacher_id', Auth::id())
//                ->select('date')
//                ->groupBy('date')
//                ->get()
//                ->count()
//        ]);
//    }
//
//    function takeAttendance(Request $request)
//    {
//        return $request->all();
//
//        $attend = [];
//        foreach ($request->input('studentList') as $student) {
//            $match = false;
//            foreach ($request->input('attendance') as $a) {
//                if ($a == $student) {
//                    $match = true;
//                    break;
//                }
//            }
//            if ($match)
//                array_push($attend, ["id" => $student, "attendance" => true]);
//            else
//                array_push($attend, ["id" => $student, "attendance" => false]);
//        }
//
//        $subjectId = $request->input('subject_id');
//
//        foreach ($attend as $attendance) {
//
//            if (Attendance::where('subject_id', $subjectId)
//                    ->where('student_id', $attendance['id'])
//                    ->where('date', date("Y-m-d"))
//                    ->first() == null
//            ) {
//                Attendance::create(array(
//                    'student_id' => $attendance['id'],
//                    'teacher_id' => Auth()->id(),
//                    'attendance' => $attendance['attendance'],
//                    'date' => date("Y-m-d"),
//                    'subject_id' => $subjectId,
//                ));
//
//
//            } else {
//                if (Attendance::where('subject_id', $subjectId)
//                    ->where('student_id', $attendance['id'])
//                    ->where('date', date("Y-m-d"))->get()
//                ) {
//
//                    Attendance::where('subject_id', $subjectId)
//                        ->where('student_id', $attendance['id'])
//                        ->where('date', date("Y-m-d"))
//                        ->update(array(
//                            'student_id' => $attendance['id'],
//                            'teacher_id' => Auth()->id(),
//                            'attendance' => $attendance['attendance'],
//                            'date' => date("Y-m-d"),
//                            'subject_id' => $subjectId,
//                        ));
//                } else {
//                    Attendance::create(array(
//                        'student_id' => $attendance['id'],
//                        'teacher_id' => Auth()->id(),
//                        'attendance' => $attendance['attendance'],
//                        'date' => date("Y-m-d"),
//                        'subject_id' => $subjectId,
//                    ));
//                }
//            }
//        }
//
//        $request->merge(['id' => $request->input('subject_id')]);
//        return redirect()->back();
//
//        return $this->getStudentList($request);
//        return response()->json([
//            'status' => 'success'
//        ]);
//        //return response(json_encode(date("Y-m-d") . ' Attendance transaction for subject ' . $subject . ' done successfully.'), 200);
//
//    }
//
//
//    function updateAttendance(Request $request)
//    {
//
//        $attend = [];
//        foreach ($request->input('studentList') as $student) {
//            $match = false;
//            foreach ($request->input('attendance') as $a) {
//                if ($a == $student) {
//                    $match = true;
//                    break;
//                }
//            }
//            if ($match)
//                array_push($attend, ["id" => $student, "attendance" => true]);
//            else
//                array_push($attend, ["id" => $student, "attendance" => false]);
//        }
//
//        $subjectId = $request->input('subject_id');
//        $date = date("Y-m-d", strtotime($request->input('date')));
//        foreach ($attend as $attendance) {
//
//            if (Attendance::where('subject_id', $subjectId)
//                    ->where('student_id', $attendance['id'])
//                    ->where('date', date($date))
//                    ->first() == null
//            ) {
//                Attendance::create(array(
//                    'student_id' => $attendance['id'],
//                    'teacher_id' => Auth()->id(),
//                    'attendance' => $attendance['attendance'],
//                    'date' => date($date),
//                    'subject_id' => $subjectId,
//                ));
//
//            } else {
//                if (Attendance::where('subject_id', $subjectId)
//                    ->where('student_id', $attendance['id'])
//                    ->where('date', date($date))->get()
//                ) {
//                    Attendance::where('subject_id', $subjectId)
//                        ->where('student_id', $attendance['id'])
//                        ->where('date', date($date))
//                        ->update(array(
//                            'student_id' => $attendance['id'],
//                            'teacher_id' => Auth()->id(),
//                            'attendance' => $attendance['attendance'],
//                            'date' => date($date),
//                            'subject_id' => $subjectId,
//                        ));
//                } else {
//                    Attendance::create(array(
//                        'student_id' => $attendance['id'],
//                        'teacher_id' => Auth()->id(),
//                        'attendance' => $attendance['attendance'],
//                        'date' => date($date),
//                        'subject_id' => $subjectId,
//                    ));
//                }
//            }
//        }
//
//        $request->merge(['id' => $request->input('subject_id')]);
//
//        return redirect()->back();
//
//        return $this->getStudentListOfDate($request);
//
//        return response()->json([
//            'status' => 'success'
//        ]);
//        //return response(json_encode(date("Y-m-d") . ' Attendance transaction for subject ' . $subject . ' done successfully.'), 200);
//    }
//
//
//    function getStudentAttendance(Request $request)
//    {
//        $attendance = Attendance::where('student_id', $request->input('id'))
//            ->where('subject_id', $request->input('subject_id'))
//            ->select('date', 'attendance')
//            ->get();
//
//        $dateCollector = collect([]);
//        foreach ($attendance as $att) {
//            $date = $att->date;
//            $newDate = date_create($date)->format('Y-m-d');
//            $att->date = $newDate;
//            $dateCollector->push($att->date);
//        }
//
//        return response()->json([
//            "date" => $attendance,
//            "start" => $dateCollector->min(),
//            "end" => $dateCollector->max()
//        ]);
//    }
}
