<?php

namespace HCCNetwork\Http\Controllers\Library;

use HCCNetwork\book;
use HCCNetwork\Borrow;
use HCCNetwork\faculty;
use HCCNetwork\SubBook;
use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use HCCNetwork\User;


class LoginController extends Controller
{
    public function login(Request $request)
    {
        $http = new Client();
        $response = $http->post(env('OAUTH_SERVER') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => env('APP_CLIENT'),
                'username' => $request->input("email"),
                'password' => $request->input("password"),
            ],
        ]);

        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'name';
        $id = null;
        if (Auth::once([$field => $request->input("email"), 'password' => $request->input("password")])) {
            $id = Auth::id();
            $userType = Auth::user()->user_type_id;

            if ($userType == 2) {
                return [
                    "id" => $id,
                    "access_token" => json_decode((string)$response->getBody(), true)['access_token'],
                    "user_type" => $userType
                ];
            } else {
                return response('You are not authorize', 401);
            }
        }
    }

    public function getDashboard()
    {
        return response()
            ->json([
                "totalMember" => User::count(),
                "totalBook" => SubBook::count(),
                "totalFaculties" => faculty::count(),
                "totalExpiredBook" => 0,
                "totalLostBook" => SubBook::onlyTrashed()->count(),
                "totalPrice" => SubBook::sum('price'),
                "recentTransaction" => Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
                    ->join('books', 'sub_books.book_id', '=', 'books.id')
                    ->join('users', 'borrow.user_id', '=', 'users.id')
                    ->leftJoin('students', 'users.id', '=', 'students.id')
                    ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
                    ->leftJoin('guests', 'users.id', '=', 'guests.id')
                    ->orderBy('borrow.updated_at', 'desc')
                    ->where('users.deleted_at',null)
                    ->where('sub_books.deleted_at',null)
                    ->select(
                        'sub_books.id as sub_code',
                        'books.id as book_id',
                        'books.name as book_name',
                        'users.id as member_id',
                        'students.first_name as s_first_name',
                        'students.middle_name as s_middle_name',
                        'students.last_name as s_last_name',
                        'teachers.first_name as t_first_name',
                        'teachers.middle_name as t_middle_name',
                        'teachers.last_name as t_last_name',
                        'guests.first_name as g_first_name',
                        'guests.middle_name as g_middle_name',
                        'guests.last_name as g_last_name',
                        'borrow.issue_date as issue_date',
                        'borrow.return_date as return_date'
                    )
                    ->limit(10)
                    ->get()
            ]);
    }
}
