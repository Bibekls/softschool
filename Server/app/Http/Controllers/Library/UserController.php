<?php

namespace HCCNetwork\Http\Controllers\Library;

use HCCNetwork\book;
use HCCNetwork\Borrow;
use HCCNetwork\faculty;
use HCCNetwork\Guest;
use HCCNetwork\Http\Requests\Library\BookRequest;
use HCCNetwork\student;
use HCCNetwork\SubBook;
use HCCNetwork\Teacher;
use HCCNetwork\User;
use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    public function searchUser(Request $request)
    {
        if ($request->input('userType') == 4) {
            $searchResult = student::join('users', 'students.id', '=', 'users.id')
                ->join('faculties', 'students.faculty_id', '=', 'faculties.id')
                ->join('batch_level_maping', function ($join) {
                    $join
                        ->on('batch_level_maping.batch', '=', 'students.batch')
                        ->on('batch_level_maping.faculty_id', '=', 'students.faculty_id');
                })
                ->where(function ($sql) use ($request) {
                    if ($request->input('faculty'))
                        $sql->where('students.faculty_id', $request->input('faculty'));
                    if ($request->input('level'))
                        $sql->Where('batch_level_maping.level', $request->input('level'));
                })
                ->where(function ($sql) use ($request) {
                    $sql->where('students.first_name', 'like', $request->input('name') . '%')
                        ->orWhere('students.last_name', 'like', $request->input('name') . '%');
                })
                ->where('users.deleted_at',null)
                ->select(
                    'students.id as id',
                    'students.first_name as first_name',
                    'students.middle_name as middle_name',
                    'students.last_name as last_name',
                    'users.email as email',
                    'students.phone_number as phone_number',
                    'faculties.name as faculty',
                    'students.batch as batch'
                )
                ->get();
        } else {
            $searchTeacher = Teacher::join('users', 'teachers.id', '=', 'users.id')
                ->join('faculty_teacher', 'teachers.id', '=', 'faculty_teacher.teacher_id')
                ->join('faculties', 'faculty_teacher.faculty_id', '=', 'faculties.id')
                ->where(function ($sql) use ($request) {
                    if ($request->input('faculty'))
                        $sql->where('faculty_teacher.faculty_id', $request->input('faculty'));
                })
                ->where(function ($sql) use ($request) {
                    $sql->where('teachers.first_name', 'like', $request->input('name') . '%')
                        ->orWhere('teachers.last_name', 'like', $request->input('name') . '%');
                })
                ->where('users.deleted_at',null)
                ->select(
                    'teachers.id as id',
                    'teachers.first_name as first_name',
                    'teachers.middle_name as middle_name',
                    'teachers.last_name as last_name',
                    'users.email as email',
                    'teachers.phone_number as phone_number',
                    'faculties.name as faculty'
                )
                ->get();

            $searchGuest = Guest::join('users', 'guests.id', '=', 'users.id')
                ->where(function ($sql) use ($request) {
                    $sql->where('guests.first_name', 'like', $request->input('name') . '%')
                        ->orWhere('guests.last_name', 'like', $request->input('name') . '%');
                })
                ->where('users.deleted_at',null)
                ->select(
                    'guests.id as id',
                    'guests.first_name as first_name',
                    'guests.middle_name as middle_name',
                    'guests.last_name as last_name',

                    'guests.phone_number as phone_number'
                )
                ->get();

            $searchResult = collect($searchTeacher);
            $searchResult = $searchResult->merge($searchGuest);

        }

        return $searchResult;
    }

    public function getFacultyList()
    {
        return faculty::all();
    }

    public function getBatchList()
    {
        return Student::groupBy('batch')->get();
    }

    function searchUserForIR(Request $request)
    {

        if ($request->input('userType') == 4) {
            $searchResult = student::join('users', 'students.id', '=', 'users.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->join('faculties', 'students.faculty_id', '=', 'faculties.id')
                ->join('batch_level_maping', function ($join) {
                    $join
                        ->on('batch_level_maping.batch', '=', 'students.batch')
                        ->on('batch_level_maping.faculty_id', '=', 'students.faculty_id');
                })
                ->where(function ($sql) use ($request) {
                    if ($request->input('faculty'))
                        $sql->where('batch_level_maping.faculty_id', $request->input('faculty'));
                    if ($request->input('level'))
                        $sql->where('batch_level_maping.level', $request->input('level'));
                })
                ->where(function ($sql) use ($request) {
                    $sql->where('students.first_name', 'like', $request->input('name') . '%')
                        ->orWhere('students.last_name', 'like', $request->input('name') . '%');
                })
                ->where('users.deleted_at',null)
                ->select(
                    'students.id as id',
                    'students.first_name as first_name',
                    'students.middle_name as middle_name',
                    'students.last_name as last_name',
                    'users.email as email',
                    'students.phone_number as phone_number',
                    'faculties.name as faculty',
                    'students.batch as batch',
                    'images.file_name as images'
                )
                ->get();

            foreach ($searchResult as $result) {
                $bookIssue = Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
                    ->join('books', 'sub_books.book_id', '=', 'books.id')
                    ->where('user_id', $result->id)
                    ->where('return_date', null)
                    ->select(
                        'sub_books.id as sub_code',
                        'books.name as name',
                        'borrow.issue_date as issue_date'
                    )
                    ->get();
                $result['book_issue'] = $bookIssue;
            }

        } else {
            $searchTeacher = Teacher::join('users', 'teachers.id', '=', 'users.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->join('faculty_teacher', 'teachers.id', '=', 'faculty_teacher.teacher_id')
                ->join('faculties', 'faculty_teacher.faculty_id', '=', 'faculties.id')
                ->where(function ($sql) use ($request) {
                    if ($request->input('faculty'))
                        $sql->where('faculty_teacher.faculty_id', $request->input('faculty'));
                })
                ->where(function ($sql) use ($request) {
                    $sql->where('teachers.first_name', 'like', $request->input('name') . '%')
                        ->orWhere('teachers.last_name', 'like', $request->input('name') . '%');
                })
                ->where('users.deleted_at',null)
                ->select(
                    'teachers.id as id',
                    'teachers.first_name as first_name',
                    'teachers.middle_name as middle_name',
                    'teachers.last_name as last_name',
                    'users.email as email',
                    'teachers.phone_number as phone_number',
                    'faculties.name as faculty',
                    'images.file_name as images'
                )
                ->get();

            $searchGuest = Guest::join('users', 'guests.id', '=', 'users.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->where(function ($sql) use ($request) {
                    $sql->where('guests.first_name', 'like', $request->input('name') . '%')
                        ->orWhere('guests.last_name', 'like', $request->input('name') . '%');
                })
                ->where('users.deleted_at',null)
                ->select(
                    'guests.id as id',
                    'guests.first_name as first_name',
                    'guests.middle_name as middle_name',
                    'guests.last_name as last_name',
                    'images.file_name as images',
                    'guests.phone_number as phone_number'
                )
                ->get();

            $searchResult = collect($searchTeacher);
            $searchResult = $searchResult->merge($searchGuest);

            foreach ($searchResult as $result) {
                $bookIssue = Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
                    ->join('books', 'sub_books.book_id', '=', 'books.id')
                    ->where('user_id', $result->id)
                    ->where('return_date', null)
                    ->select(
                        'sub_books.id as sub_code',
                        'books.name as name',
                        'borrow.issue_date as issue_date'
                    )
                    ->get();
                $result['book_issue'] = $bookIssue;
            }
        }
        return $searchResult;
    }

    public function getUserInfo(Request $request)
    {
        $user = User::with('profile_picture', 'student.faculty', 'teacher','guest')->find($request->input('userId'));

        $user["book_issue"] = Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
            ->join('books', 'sub_books.book_id', 'books.id')
            ->where('user_id', $user->id)
            ->select(
                'books.id as book_id',
                'books.name as book_name',
                'sub_books.id as sub_code',
                'borrow.issue_date as issue_date',
                'borrow.return_date as return_date'
            )
            ->get();

        return $user;
    }
}
