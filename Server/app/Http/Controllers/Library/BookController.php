<?php

namespace HCCNetwork\Http\Controllers\Library;

use HCCNetwork\book;
use HCCNetwork\Borrow;
use HCCNetwork\Http\Requests\Library\BookRequest;
use HCCNetwork\SubBook;
use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    public function addBook(BookRequest $request)
    {
        // ALTER TABLE books AUTO_INCREMENT=1001 sets initial value

        $lastBookId = book::orderBy('id', 'desc')
            ->first();
        if ($lastBookId) {
            $bookId = $lastBookId->id;
        } else {
            $bookId = '1000';
        }
        $book = book::create([
            "id" => $bookId + 1,
            "name" => $request->input('bookName'),
            "author" => $request->input('authorName'),
            "publisher" => $request->input('publisherName'),
            "ISBN" => $request->input('ISBN'),
            "faculty_id" => $request->input('faculty'),
            "level" => $request->input('level'),
            "description" => $request->input('description')
        ]);

        $subBook = array();

        $lastSubBookId = SubBook::where('book_id', $book->id)
            ->orderBy('id', 'desc')
            ->first();
        $lastSubBookBlock = SubBook::where('book_id', $book->id)
            ->orderBy('block', 'desc')
            ->first();

        if ($lastSubBookId) {
            $subBookId = $lastSubBookId->id;
        } else {
            $subBookId = '1000';
        }

        if ($lastSubBookBlock) {
            $subBookBlock = $lastSubBookBlock->block;
        } else {
            $subBookBlock = '0';
        }

        for ($i = 0; $i < $request->input('noOfBook'); $i++) {
            array_push($subBook, [
                "id" => $book->id . '-bk-' . ++$subBookId,
                "book_id" => $book->id,
                "price" => $request->input('price'),
                "source" => $request->input('source'),
                "edition" => $request->input('edition'),
                "edition_year" => $request->input('editionYear'),
                "available" => true,
                "block" => $subBookBlock + 1
            ]);
        }

        SubBook::insert($subBook);

        return response()->json([
            "book_id" => $book->id
        ]);
    }

    public function editBook(Request $request)
    {
        book::find($request->input('id'))->update([
            "name" => $request->input('bookName'),
            "author" => $request->input('authorName'),
            "publisher" => $request->input('publisherName'),
            "ISBN" => $request->input('ISBN'),
            "faculty_id" => $request->input('faculty'),
            "level" => $request->input('level'),
            "description" => $request->input('description')
        ]);

        return response()->json([
            "book_id" => $request->input('id')
        ]);
    }

    public function editBookBlock(Request $request)
    {
        $books = SubBook::where('block', $request->input('block'))
            ->where('book_id', $request->input('id'))
            ->get();

        foreach ($books as $book) {
            $book->update([
                "edition" => $request->input('edition'),
                "edition_year" => $request->input('editionYear'),
                "price" => $request->input('price'),
                "source" => $request->input('source')
            ]);
        }

        return response()->json([
            "book_id" => $request->input('id')
        ]);
    }

    function getBookDetail(Request $request)
    {
        $book = book::with('subBook', 'faculty')->find($request->input('id'));
        foreach ($book->subBook as $b) {
            $b["issued_to"] = Borrow::join('users', 'borrow.user_id', '=', 'users.id')
                ->leftJoin('students', 'users.id', '=', 'students.id')
                ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
                ->leftJoin('guests', 'users.id', '=', 'guests.id')
                ->where('sub_code', $b->id)
                ->where('return_date', null)
                ->where('users.deleted_at',null)
                ->select(
                    'users.id as user_id',
                    'students.first_name as s_first_name',
                    'students.middle_name as s_middle_name',
                    'students.last_name as s_last_name',
                    'teachers.first_name as t_first_name',
                    'teachers.middle_name as t_middle_name',
                    'teachers.last_name as t_last_name',
                    'guests.first_name as g_first_name',
                    'guests.middle_name as g_middle_name',
                    'guests.last_name as g_last_name'
                )
                ->first();
        }
        return $book;
    }

    function getSubBookDetail(Request $request)
    {
        $book = SubBook::where('book_id', $request->input('id'))
            ->join('books', 'sub_books.book_id', '=', 'books.id')
            ->where('sub_books.deleted_at', null)
            ->where('block', $request->input('block'))->first();
        return $book;
    }

    function addBookQuantity(Request $request)
    {
        $subBook = array();

        $lastSubBookId = SubBook::withTrashed()->where('book_id', $request->input('id'))
            ->orderBy('id', 'desc')
            ->first();
        $lastSubBookBlock = SubBook::withTrashed()->where('book_id', $request->input('id'))
            ->orderBy('block', 'desc')
            ->first();

        if ($lastSubBookId) {
            $ids = explode("-", $lastSubBookId->id);
            $subBookId = $ids[2];
        } else {
            $subBookId = 1000;
        }

        if ($lastSubBookBlock) {
            $subBookBlock = $lastSubBookBlock->block;
        } else {
            $subBookBlock = '0';
        }

        for ($i = 0; $i < $request->input('noOfBook'); $i++) {
            array_push($subBook, [
                "id" => $request->input('id') . '-bk-' . ++$subBookId,
                "book_id" => $request->input('id'),
                "price" => $request->input('price'),
                "source" => $request->input('source'),
                "edition" => $request->input('edition'),
                "edition_year" => $request->input('editionYear'),
                "available" => true,
                "block" => $subBookBlock + 1
            ]);
        }

        SubBook::insert($subBook);
        return $this->getBookDetail($request);
    }

    function deleteBookBlock(Request $request)
    {
        foreach ($bookToDelete = SubBook::where('book_id', $request->input('id'))
            ->where('block', $request->input('block'))
            ->get()
            as
            $book){
            if(! count(Borrow::where('sub_code',$book->id)->where('return_date',null)->get())){
                $book->delete();
            }
        }

        return $this->getBookDetail($request);
    }

    function searchBook(Request $request)
    {
        return $this->searchBookForIR($request);
    }

    function searchBookForIR(Request $request)
    {
        if (!$request->input('subCode')) {
            $searchResult = book::join('sub_books', 'books.id', '=', 'sub_books.book_id')
                ->join('faculties', 'books.faculty_id', '=', 'faculties.id')
                ->where(function ($sql) use ($request) {
                    if ($request->input('faculty'))
                        $sql->where('books.faculty_id', $request->input('faculty'));
                    if ($request->input('level'))
                        $sql->Where('books.level', $request->input('level'));
                })
                ->where('books.name', 'like', $request->input('name') . '%')
                ->where('sub_books.deleted_at', null)
                ->select('books.id as book_id',
                    'sub_books.id as sub_code',
                    'books.author as author',
                    'books.publisher as publisher',
                    'faculties.name as faculty',
                    'sub_books.price as price',
                    'sub_books.source as source',
                    'sub_books.edition as edition',
                    'sub_books.edition_year as edition_year',
                    'sub_books.available as available',
                    'books.description as description',
                    'books.name as name')
                ->get();

        } else {

            $searchResult = book::join('sub_books', 'books.id', '=', 'sub_books.book_id')
                ->join('faculties', 'books.faculty_id', '=', 'faculties.id')
                ->where('sub_books.id', $request->input('subCode'))
                ->where('sub_books.deleted_at', null)
                ->select('books.id as book_id',
                    'sub_books.id as sub_code',
                    'books.author as author',
                    'books.publisher as publisher',
                    'faculties.name as faculty',
                    'sub_books.price as price',
                    'sub_books.source as source',
                    'sub_books.edition as edition',
                    'sub_books.edition_year as edition_year',
                    'sub_books.available as available',
                    'books.description as description',
                    'books.name as name')
                ->get();
        }

        return $searchResult;
    }

    function returnBook(Request $request)
    {
        // check if this book is issued to this user or not
        $issuedToThisUser = Borrow::where('sub_code', $request->input('book_id'))
            ->where('user_id', $request->input('user_id'))
            ->where('return_date', null)
            ->first();

        if ($issuedToThisUser) {
            $issuedToThisUser->update([
                "return_date" => date('Y-m-d H:m:s')
            ]);

            $bookIssue = Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
                ->join('books', 'sub_books.book_id', '=', 'books.id')
                ->where('borrow.id', $issuedToThisUser->id)
                ->where('sub_books.deleted_at', null)
                ->select(
                    'sub_books.id as sub_code',
                    'books.name as name',
                    'borrow.issue_date as issue_date'
                )
                ->first();

            SubBook::find($issuedToThisUser->sub_code)->update(["available" => true]);

            return $bookIssue;
        }
    }

    function deleteLostBook(Request $request)
    {

        // check if this book is issued to this user or not
        $issuedToThisUser = Borrow::where('sub_code', $request->input('book_id'))
            ->where('user_id', $request->input('user_id'))
            ->where('return_date', null)
            ->first();

        if ($issuedToThisUser) {
            $issuedToThisUser->update([
                "return_date" => date('Y-m-d H:m:s')
            ]);

            $bookIssue = Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
                ->join('books', 'sub_books.book_id', '=', 'books.id')
                ->where('borrow.id', $issuedToThisUser->id)
                ->where('sub_books.deleted_at', null)
                ->select(
                    'sub_books.id as sub_code',
                    'books.name as name',
                    'borrow.issue_date as issue_date'
                )
                ->first();

            SubBook::find($request->input('book_id'))->update([
                "remark" => $request->input('reason')
            ]);

            $delete = SubBook::find($request->input('book_id'));
            $delete->delete();

            return $bookIssue;
        }
    }

    function issueBook(Request $request)
    {
        // check if this book is issued to any user or not
        $issuedToOther = Borrow::where('sub_code', $request->input('book_id'))
            ->where('return_date', null)
            ->first();
        if (!$issuedToOther) {

            $borrow = Borrow::create([
                "user_id" => $request->input('user_id'),
                "sub_code" => $request->input('book_id'),
                "issue_date" => date('Y-m-d H:m:s'),
                "return_date" => null
            ]);

            SubBook::find($borrow->sub_code)->update(["available" => false]);

            $bookIssue = Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
                ->join('books', 'sub_books.book_id', '=', 'books.id')
                ->where('borrow.id', $borrow->id)
                ->where('sub_books.deleted_at', null)
                ->select(
                    'sub_books.id as sub_code',
                    'books.name as name',
                    'borrow.issue_date as issue_date'
                )
                ->first();

            return $bookIssue;
        }
    }

    function irHistory()
    {
        $issueHistory = Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
            ->join('books', 'sub_books.book_id', '=', 'books.id')
            ->join('users', 'borrow.user_id', '=', 'users.id')
            ->leftJoin('students', 'users.id', '=', 'students.id')
            ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
            ->leftJoin('guests', 'users.id', '=', 'guests.id')
            ->where('borrow.return_date', null)
            ->where('sub_books.deleted_at',null)
            ->where('users.deleted_at',null)
            ->orderBy('borrow.issue_date', 'desc')
            ->select(
                'sub_books.id as sub_code',
                'books.id as book_id',
                'books.name as book_name',
                'users.id as member_id',
                'students.first_name as s_first_name',
                'students.middle_name as s_middle_name',
                'students.last_name as s_last_name',
                'teachers.first_name as t_first_name',
                'teachers.middle_name as t_middle_name',
                'teachers.last_name as t_last_name',
                'guests.first_name as t_first_name',
                'guests.middle_name as t_middle_name',
                'guests.last_name as t_last_name',
                'borrow.issue_date as issue_date',
                'borrow.return_date as return_date'
            )
            ->get();

        $returnHistory = Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
            ->join('books', 'sub_books.book_id', '=', 'books.id')
            ->join('users', 'borrow.user_id', '=', 'users.id')
            ->leftJoin('students', 'users.id', '=', 'students.id')
            ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
            ->leftJoin('guests', 'users.id', '=', 'guests.id')
            ->where('borrow.return_date', '<>', null)
            ->where('sub_books.deleted_at',null)
            ->where('users.deleted_at',null)
            ->orderBy('borrow.return_date', 'desc')
            ->select(
                'sub_books.id as sub_code',
                'books.id as book_id',
                'books.name as book_name',
                'users.id as member_id',
                'students.first_name as s_first_name',
                'students.middle_name as s_middle_name',
                'students.last_name as s_last_name',
                'teachers.first_name as t_first_name',
                'teachers.middle_name as t_middle_name',
                'teachers.last_name as t_last_name',
                'guests.first_name as t_first_name',
                'guests.middle_name as t_middle_name',
                'guests.last_name as t_last_name',
                'borrow.issue_date as issue_date',
                'borrow.return_date as return_date'
            )
            ->get();

        return response()->json([
            "returnHistory" => $returnHistory,
            "issueHistory" => $issueHistory
        ]);
    }
}