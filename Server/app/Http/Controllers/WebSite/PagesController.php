<?php

namespace HCCNetwork\Http\Controllers\WebSite;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\UserImage;
use HCCNetwork\Video;
use HCCNetwork\Page;
use HCCNetwork\PageCategory;
use HCCNetwork\Gallery;

use View;
use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;

use HCCNetwork\Work;

class PagesController extends Controller
{
    public function home()
    {        
        return View::make('website.home');
       // ->with('works',Work::select('id','title','banner','summary')->orderBy('created_at', 'desc')->limit(3)->get());
    } 

    public function work($id)
    {
        $category=Work::find($id);   
        return View::make('website.work')
        ->with('relatedWork',Work::where('category_id',$category->category_id)
                                ->select('id','title','banner','summary')
                                ->orderBy('created_at', 'desc')->get())
        ->with('work',Work::find($id))
        ;
    } 

    public function allwork()
    {        
        return View::make('website.allwork')
        ->with('works',Work::select('id','title','banner','summary')->orderBy('created_at', 'desc')->get());
    } 

    public function about()
    {
        return View::make('website.about');
    }
    public function bsccsit()
    {
        return View::make('website.bsccsit');
    }
    public function syllabus()
    {
        return View::make('website.syllabus');
    }
    public function science()
    {
        return View::make('website.science');
    }
    public function management()
    {
        return View::make('website.management');
    }
    public function contact()
    {
        return View::make('website.contact');            
    }
   
    public function gallery()
    {
        return view('website.gallery')
        ->with('galleries',Gallery::all());
    }

    public function customMenu($page)
    {
        $page=Page::where('tag',$page)->first();
        return view('website.custom')
        ->with('page',$page)
        ->with('PageCategory',$page->pageCategory)
        ;
    }

}
