<?php

namespace HCCNetwork\Http\Controllers\WebSite;
use HCCNetwork\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\User;
use DB;
use HCCNetwork\userrequest;
use HCCNetwork\Http\Requests;

class AdminController extends Controller
{  
		public function backup()
		{
			   $table_name = "students";
			   $backup_file  = "/tmp/students.sql";

		    	$users = DB::select('SELECT * INTO OUTFILE students.sql FROM students');  
		
		}
	
// Admin login from api token //////////////////////////////////////////////////////////
 		public function adminlogin(Request $request)
	    {
	    	if (Auth::attempt(['name' => $request->input('name'),'password' => $request->input('password'),'user_type_id'=>1])) {
    			User::where('id',Auth::user()->id) ->update(array(
															'api_token'=>str_random(60),
															));
    			// Refresh new auth with new token
	   	 		Auth::attempt(['name' => $request->input('name'),'password' => $request->input('password'),'user_type_id'=>1]);
	    		return response (json_encode(Auth::user()),200);
	    	}	
	    	else return response (json_encode(['message'=>'Unauthorized User']),401);    	
	    }
// Librarian login from api token ////////////////////////////////////////////////////
		public function librarianlogin(Request $request)
	    {
	   	 	if (Auth::attempt(['name' => $request->input('name'),'password' => $request->input('password'),'user_type_id'=>2])) {
	   	 		User::where('id',Auth::user()->id) ->update(array(
															'api_token'=>str_random(60),
															));
	   	 		// Refresh new auth with new token
	   	 		Auth::attempt(['name' => $request->input('name'),'password' => $request->input('password'),'user_type_id'=>2]);
	    		return response (json_encode(Auth::user()),200);

	    	}
// Admin authentication for librarian ///////////////////////////////////////////////	    		
	    	else if (Auth::attempt(['name' => $request->input('name'),'password' => $request->input('password'),'user_type_id'=>1])) {
    			User::where('id',Auth::user()->id) ->update(array(
															'api_token'=>str_random(60),
															));
    			// Refresh new auth with new token
	   	 		Auth::attempt(['name' => $request->input('name'),'password' => $request->input('password'),'user_type_id'=>1]);
	    		return response (json_encode(Auth::user()),200);
	    	}	
	    	else return response (json_encode(['message'=>'Unauthorized User']),401);    	
	    }
// Email verification after the user has login from official web site//////////////////
	    public function verifyemail($token){
	    	if ($userrequest=userrequest::where('token',$token)->first()) {

	    		$user=User::create([
	            'name' => $userrequest->name,
	            'email' => $userrequest->email,
	            'user_type_id'=>$userrequest->user_type_id,
	            'password' => $userrequest->password,           
	            'api_token' => str_random(60), 
	            ]);

	            $delete= userrequest::where('token',$token)->delete(); 

	            Auth::attempt(['name' => $user->name,'password' => $user->password,'user_type_id'=>$user->user_type_id]);
	    		return redirect('/student/register');

	    	}
	    	else{
	    		return redirect('login');
	    	}
	    }

// End of the Admincontroller class //////////////////////////////////////////////////////////////
}
