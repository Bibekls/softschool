<?php

namespace HCCNetwork\Http\Controllers;

use HCCNetwork\attendance;
use Illuminate\Http\Request;
use HCCNetwork\Subject;
use Auth;

class StudentApiController extends Controller
{
    function getSubjectList()
    {
        $subjects = Subject::join('attendances', 'subjects.id', '=', 'attendances.subject_id')
            ->join('students', 'attendances.student_id', '=', 'students.id')
            ->join('routines', 'subjects.id', '=', 'routines.subject_id')
            ->join('teachers', 'routines.teacher_id', '=', 'teachers.id')
            ->join('users', 'teachers.id', '=', 'users.id')
            ->join('images', 'users.profile_pic', '=', 'images.id')
            ->where('students.id', Auth::id())
            ->where('students.deleted_at', null)
            ->select('subjects.name as subject', 'subjects.id as id', 'images.file_name as teacher_avatar')
            ->groupBy('subject')
            ->orderBy('subject')
            ->get();

        foreach ($subjects as $subject) {
            $regularAttendance = attendance::where('subject_id', $subject->id)
                ->where('replaced_with', null)
                ->where('extra', false)
                ->groupBy('date')
                ->count();

            $regularAttendancePresent = attendance::where('subject_id', $subject->id)
                ->where('replaced_with', null)
                ->where('student_id', Auth::id())
                ->where('attendance', true)
                ->where('extra', false)
                ->groupBy('date')
                ->count();

            $replacedAttendance = attendance::where('subject_id', $subject->id)
                ->where('replaced_with', '<>', null)
                ->where('extra', false)
                ->groupBy('date')
                ->count();


            $replacedAttendancePresent = attendance::where('subject_id', $subject->id)
                ->where('replaced_with', '<>', null)
                ->where('student_id', Auth::id())
                ->where('attendance', true)
                ->where('extra', false)
                ->groupBy('date')
                ->count();

            $extraAttendance = attendance::where('subject_id', $subject->id)
                ->where('replaced_with', null)
                ->where('extra', true)
                ->groupBy('date')
                ->count();

            $extraAttendancePresent = attendance::where('subject_id', $subject->id)
                ->where('replaced_with', null)
                ->where('student_id', Auth::id())
                ->where('attendance', true)
                ->where('extra', true)
                ->groupBy('date')
                ->count();

            $totalAttendance = $regularAttendance + $replacedAttendance + $extraAttendance;

            $totalPresentAttendance = $regularAttendancePresent + $replacedAttendancePresent + $extraAttendancePresent;

            if ($totalAttendance > 0)
                $subject['attendancePer'] = ($totalPresentAttendance / $totalAttendance) * 100;
        }

        return response($subjects);
    }
}
