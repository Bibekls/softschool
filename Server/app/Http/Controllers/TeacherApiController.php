<?php

namespace HCCNetwork\Http\Controllers;

use HCCNetwork\BatchLevelMaping;
use HCCNetwork\CoursePlan;
use HCCNetwork\faculty;
use HCCNetwork\Notification;
use HCCNetwork\NotificationUser;
use HCCNetwork\routine;
use HCCNetwork\student;
use Illuminate\Http\Request;

use HCCNetwork\Subject;
use HCCNetwork\Attendance;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TeacherApiController extends Controller
{
    private $userFCMToken;

    public function __construct()
    {
        $this->userFCMToken = array();
    }

    function getSubjectList()
    {
        $subjects = Subject::join('routines', 'subjects.id', '=', 'routines.subject_id')
            ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
            ->join('faculties', 'subjects.faculty_id', '=', 'faculties.id')
            ->where('routines.teacher_id', Auth::id())
            ->select('subjects.name as subject', 'subjects.id as id')
            ->groupBy('subject')
            ->orderBy('subject')
            ->get();

        return response($subjects);
    }
    public function getSubjectOfSchool(){
        $subjects = Subject::join('routines', 'subjects.id', '=', 'routines.subject_id')
        ->join('studentRecord', 'subjects.level', '=', 'studentRecord.class')
        ->join('faculties', 'subjects.faculty_id', '=', 'faculties.id')
        // ->where('routines.teacher_id', 309)
        ->where('routines.teacher_id', Auth::id())
        ->select('subjects.name as subject','subjects.faculty_id as faculty_id' ,'subjects.id as id','studentRecord.class as class')
        ->groupBy('subject')
        ->orderBy('subject')
        ->get();

    return response($subjects);
    }    
    public function getAllSubjectOfSchool(Request $request){

        $tests=DB::table('year_mapping')->whereDate('form_ad','<=', date("Y-m-d"))->whereDate('to_ad','>=', date("Y-m-d"))->first();
        $subjects=Subject::where('batch',$tests->np_year)
                         ->where('faculty_id',$request['faculty_id'])
                         ->where('level',$request['class'])
                         ->get();
                     return response($subjects);
                }
    public function getSubject(){
            //  return response()->json(['test'=>Auth::id()]);
     $subjects = Subject::join('routines', 'subjects.id', '=', 'routines.subject_id')
            ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
            ->join('faculties', 'subjects.faculty_id', '=', 'faculties.id')
            ->where('routines.teacher_id', Auth::id())
            // ->where('routines.teacher_id',309)
            // ->where('subjects.faculty_id',Auth::faculty_id())
            ->select('subjects.name as subject','batch_level_maping.faculty_id as faculty_id','batch_level_maping.batch as batch','batch_level_maping.level as level','subjects.id as id')
            ->groupBy('subject')
            // ->orderBy('subject')
            ->get();
    return response($subjects);
    }

    function getStudentListOfSchool(Request $request){
        $subjectId = $request->input('id');
        $date = $request->input('date');
        $extra = $request->input('extra');
        $replace = $request->input('replace');
        $class=$request->input('class');
        $subject = Subject::find($request->input('id'));
        // return $subject;
        // $batch = BatchLevelMaping::where('faculty_id', $subject->faculty_id)->where('level', $subject->level)->first()->batch;
        $tests=DB::table('year_mapping')->whereDate('form_ad','<=', date("Y-m-d"))->whereDate('to_ad','>=', date("Y-m-d"))->first();

        // return $batch;
        // $teacherId = Auth::id();
        $teacherId = 309;
        $subQuery = DB::table('attendances as att')->select(DB::raw('count(attendance) as total_attendance'), 'student_id as sid')
            ->where('subject_id', '=', $subjectId)
            ->where('teacher_id', '=', $teacherId)
            ->where('attendance', '=', 1)
            ->where(function ($query) use ($extra, $replace) {
                if ($replace) {
                    //check for replaced class
                    $query->where('replaced_with', '<>', 0);
                } else if ($extra) {
                    //check for extra class
                    $query->where('extra', '=', true);
                } else {
                    // check for regular class class
                    $query->where('extra', '=', false)->where('replaced_with', 0);
                }
            })
            ->groupBy('att.student_id');

        if (Attendance::where('subject_id', $subjectId)
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('extra', '=', false)->where('replaced_with', 0);
                    }
                })
                ->where('date', $date ? date($date) : date("Y-m-d"))
                ->first() == null
        ) {

            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
                ->join('studentRecord', 'subjects.level', '=', 'studentRecord.class')
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->join('users', 'students.id', '=', 'users.id')
                // ->join('images', 'users.profile_pic', '=', 'images.id')
                ->leftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                ->mergeBindings($subQuery)
                ->select('students.id as id',
                    'students.mobile_number as contact',
                    'students.first_name as fname',
                    'students.last_name as lname',
                    // 'images.file_name as avatar',
                    'counted.*','studentRecord.faculty_id as faculty_id','studentRecord.class as class'
                )
                // ->where('students.batch', $batch)
                ->where('studentRecord.year',$tests->np_year)
                ->where('studentRecord.class',$class)

                ->where('students.deleted_at', null)
                ->where('subjects.id', $subjectId)
                ->where('routines.teacher_id', Auth::id())
                // ->where('routines.teacher_id', 309)
                ->groupBy('fname')
                ->orderBy('fname')
                ->get();
        //    return response($students)

        } else {

//            return DB::query()->select(DB::raw('count(attendance) as total_attendance'),'student_id')
//                ->from('attendances as att')
//                ->where('subject_id', '=', $subjectId)
//                ->where('teacher_id', '=', $teacherId)
//                ->where('attendance','=',1)
//                ->groupBy('att.student_id')->get();


            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
                 ->join('studentRecord', 'subjects.level', '=', 'studentRecord.class')
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->join('attendances as attend', 'students.id', '=', 'attend.student_id')
                ->LeftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                ->mergeBindings($subQuery)
                ->join('users', 'students.id', '=', 'users.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->select('students.id as id',
                    'students.mobile_number as contact',
                    'students.first_name as fname',
                    'students.last_name as lname',
                    'attend.attendance as attendance',
                    'images.file_name as avatar',
                    'counted.*','studentRecord.faculty_id as faculty_id','studentRecord.class as class'
                )
                // ->where('students.batch', $batch)
                ->where('studentRecord.year',$tests->np_year)
                ->where('studentRecord.class',$class)
                ->where('students.deleted_at', null)
                ->where('subjects.id', $subjectId)
                ->where('routines.teacher_id', Auth::id())
                // ->where('routines.teacher_id', 309)
                ->where('attend.subject_id', $subjectId)
                // ->where('attend.teacher_id', 309)
                ->where('attend.teacher_id', Auth::id())

                ->where('attend.date', $date ? date($date) : date("Y-m-d"))
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('attend.replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('attend.extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('attend.extra', '=', false)->where('attend.replaced_with', 0);
                    }
                })
                ->groupBy('students.first_name')
                ->orderBy('students.first_name')
                ->get();

        }

        return response()->json([
            'status' => 'success',
            'student_list' => $students,
            'totalClass' => Attendance::where('subject_id', $subjectId)
                ->where('teacher_id', Auth::id())
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('extra', '=', false)->where('replaced_with', 0);
                    }
                })
                ->select('date')
                ->groupBy('date')
                ->get()
                ->count(),
            'replaceableClass' => Subject::
                //    where('batch', $batch)
                where('batch',$tests->np_year)
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->where('routines.day', '=', date('w') + 1)
                ->where('faculty_id', $subject->faculty_id)
                ->where('level', $subject->level)
                ->where('subjects.id', '<>', $subject->id)
                ->select('subjects.id as id','subjects.name as name')
                ->get()
        ]);
    }


    function getStudentList(Request $request){
        $subjectId = $request->input('id');
        $date = $request->input('date');
        $extra = $request->input('extra');
        $replace = $request->input('replace');
    
        $subject = Subject::find($request->input('id'));
        // return $subject;
        $batch = BatchLevelMaping::where('faculty_id', $subject->faculty_id)->where('level', $subject->level)->first()->batch;
        // return $batch;
        // $teacherId = Auth::id();
        $teacherId = 245;
        $subQuery = DB::table('attendances as att')->select(DB::raw('count(attendance) as total_attendance'), 'student_id as sid')
            ->where('subject_id', '=', $subjectId)
            ->where('teacher_id', '=', $teacherId)
            ->where('attendance', '=', 1)
            ->where(function ($query) use ($extra, $replace) {
                if ($replace) {
                    //check for replaced class
                    $query->where('replaced_with', '<>', 0);
                } else if ($extra) {
                    //check for extra class
                    $query->where('extra', '=', true);
                } else {
                    // check for regular class class
                    $query->where('extra', '=', false)->where('replaced_with', 0);
                }
            })
            ->groupBy('att.student_id');

        if (Attendance::where('subject_id', $subjectId)
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('extra', '=', false)->where('replaced_with', 0);
                    }
                })
                ->where('date', $date ? date($date) : date("Y-m-d"))
                ->first() == null
        ) {

            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->join('users', 'students.id', '=', 'users.id')
                // ->join('images', 'users.profile_pic', '=', 'images.id')
                ->leftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                ->mergeBindings($subQuery)
                ->select('students.id as id',
                    'students.mobile_number as contact',
                    'students.first_name as fname',
                    'students.last_name as lname',
                    // 'images.file_name as avatar',
                    'counted.*'
                )
                ->where('students.batch', $batch)
                ->where('students.deleted_at', null)
                ->where('subjects.id', $subjectId)
                ->where('routines.teacher_id', Auth::id())
                // ->where('routines.teacher_id', 309)
                ->groupBy('fname')
                ->orderBy('fname')
                ->get();


        } else {

//            return DB::query()->select(DB::raw('count(attendance) as total_attendance'),'student_id')
//                ->from('attendances as att')
//                ->where('subject_id', '=', $subjectId)
//                ->where('teacher_id', '=', $teacherId)
//                ->where('attendance','=',1)
//                ->groupBy('att.student_id')->get();


            $students = Subject::join('students', 'subjects.faculty_id', '=', 'students.faculty_id')
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->join('attendances as attend', 'students.id', '=', 'attend.student_id')
                ->LeftJoin(DB::raw(' ( ' . $subQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                ->mergeBindings($subQuery)
                ->join('users', 'students.id', '=', 'users.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->select('students.id as id',
                    'students.mobile_number as contact',
                    'students.first_name as fname',
                    'students.last_name as lname',
                    'attend.attendance as attendance',
                    'images.file_name as avatar',
                    'counted.*'
                )
                ->where('students.batch', $batch)
                ->where('students.deleted_at', null)
                ->where('subjects.id', $subjectId)
                ->where('routines.teacher_id', Auth::id())
                // ->where('routines.teacher_id', 309)
                ->where('attend.subject_id', $subjectId)
                // ->where('attend.teacher_id', 309)
                ->where('attend.teacher_id', Auth::id())

                ->where('attend.date', $date ? date($date) : date("Y-m-d"))
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('attend.replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('attend.extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('attend.extra', '=', false)->where('attend.replaced_with', 0);
                    }
                })
                ->groupBy('students.first_name')
                ->orderBy('students.first_name')
                ->get();

        }

        return response()->json([
            'status' => 'success',
            'student_list' => $students,
            'totalClass' => Attendance::where('subject_id', $subjectId)
                ->where('teacher_id', Auth::id())
                ->where(function ($query) use ($extra, $replace) {
                    if ($replace) {
                        //check for replaced class
                        $query->where('replaced_with', '<>', 0);
                    } else if ($extra) {
                        //check for extra class
                        $query->where('extra', '=', true);
                    } else {
                        // check for regular class class
                        $query->where('extra', '=', false)->where('replaced_with', 0);
                    }
                })
                ->select('date')
                ->groupBy('date')
                ->get()
                ->count(),
            'replaceableClass' => Subject::where('batch', $batch)
                ->join('routines', 'subjects.id', '=', 'routines.subject_id')
                ->where('routines.day', '=', date('w') + 1)
                ->where('faculty_id', $subject->faculty_id)
                ->where('level', $subject->level)
                ->where('subjects.id', '<>', $subject->id)
                ->select('subjects.id as id','subjects.name as name')
                ->get()
        ]);
    }

    function takeAttendance(Request $request){

        $req = $request->input('data');
        $subjectId = $request->input('subject_id');
        $extra = $request->input('extra');
        $replace = $request->input('replace');
        $update = $request->input('update');
        $date = $request->input('date');
        // return response()->json([$request->all()]);
        $regularClassTime = routine::where('subject_id', $subjectId)
            ->where('day', $date? date('w',strtotime($date)) + 1 : date('w') + 1)->first();
        $replaceClassTime = routine::where('subject_id', $request->input('replaceClass'))
            ->where('day', $date? date('w',strtotime($date)) + 1 : date('w') + 1)->first();

        if ($replace) {
            //replace class start end time
            if ($replaceClassTime) {
                $start_at = gmdate('H:i:s', $replaceClassTime->start_at);
                $end_at = gmdate('H:i:s', $replaceClassTime->end_at);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Replaced subject don\'t have class on selected date'
                ]);
            }

        } else if ($extra) {
            if ($request->input('start_time')) {
                $start_at = gmdate('H:i:s', strtotime($request->input('start_time')));
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'you must input class start time'
                ]);
            }

            if ($request->input('end_time')) {
                $end_at = gmdate('H:i:s', strtotime($request->input('end_time')));
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'you must input class end time'
                ]);
            }

        } else {
            //regular class start end time
            if ($regularClassTime) {
                $start_at = gmdate('H:i:s', $regularClassTime->start_at);
                $end_at = gmdate('H:i:s', $regularClassTime->end_at);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'You don\'t have class on selected date'
                ]);
            }

        }
    // return response()->json(['Test'=>$req['list']]);
        foreach ($req['list'] as $attendance) {
            //check if the student on particular date and particular subject on particular type exixts or not
            if (Attendance::where('subject_id', $subjectId)
                    ->where('student_id', $attendance['id'])
                    ->where('date', $update ? date($date) : date("Y-m-d"))
                    ->where(function ($query) use ($extra, $replace) {
                        if ($replace) {
                            //check for replaced class
                            $query->where('replaced_with', '<>', 0);
                        } else if ($extra) {
                            //check for extra class
                            $query->where('extra', '=', true);
                        } else {
                            // check for regular class class
                            $query->where('extra', '=', false)->where('replaced_with', 0);
                        }
                    })
                    ->first() == null
            ) {

                //this is new entries so we need to insert record
                Attendance::create(array(
                    'student_id' => $attendance['id'],
                    'teacher_id' => Auth()->id(),
                    // 'teacher_id' => 309,
                    'attendance' => $attendance['attendance'],
                    'date' => $update ? date($date) : date("Y-m-d"),
                    'subject_id' => $subjectId,
                    'replaced_with' => $request->input('replace') ? $request->input('replaceClass') : 0,
                    'extra' => $request->input('extra')?$request->input('extra'):0,
                    //if extra then time is fetch from request
                    //if replaced then time is fetch from replaced_by subject from routine
                    //if regular then time is fetch from regular subject from routine
                    'start_time' => $start_at,
                    'end_time' => $end_at
                ));

            } else {
                Attendance::where('subject_id', $subjectId)
                    ->where('student_id', $attendance['id'])
                    ->where('date', $update ? date($date) : date("Y-m-d"))
                    ->where(function ($query) use ($extra, $replace) {
                        if ($replace) {
                            //check for replaced class
                            $query->where('replaced_with', '<>', 0);
                        } else if ($extra) {
                            //check for extra class
                            $query->where('extra', '=', true);
                        } else {
                            // check for regular class class
                            $query->where('extra', '=', false)->where('replaced_with', 0);
                        }
                    })
                    ->update(array(
                        'student_id' => $attendance['id'],
                        'teacher_id' => Auth()->id(),
                        // 'teacher_id' => 309,
                        'attendance' => $attendance['attendance'],
                        'date' => $update ? date($date) : date("Y-m-d"),
                        'subject_id' => $subjectId,
                        'replaced_with' => $request->input('replace') ? $request->input('replaceClass') : 0,
                        'extra' => $request->input('extra')?$request->input('extra'):0,
                        'start_time' => $start_at,
                        'end_time' => $end_at
                    ));
            }
        }

        $request->merge(['id' => $request->input('subject_id')]);
        if($request->input('test') == 0){
           return $this->getStudentList($request);
        }else{
           return $this->getStudentListOfSchool($request);
        }
        return response()->json([
            'status' => 'success'
        ]);
        //return response(json_encode(date("Y-m-d") . ' Attendance transaction for subject ' . $subject . ' done successfully.'), 200);
    }
    public function getSubjectForReplace(Request $request){
        $subjects=Subject::
                 where('faculty_id',$request['faculty'])
               ->where('level',$request['level'])
            //    ->where('batch',2070)
               ->where('batch',$request['batch'])
               ->get();
        return response()->json(['subjects'=>$subjects]);
    }
    function getStudentAttendance(Request $request){
        $extra = $replace = false;
        if ($request->input('type') == 3) {
            $extra = true;
        } else if ($request->input('type') == 2) {
            $replace = true;
        }

        $attendance = Attendance::where('student_id', $request->input('id'))
            ->where('subject_id', $request->input('subject_id'))
            ->where(function ($query) use ($extra, $replace) {
                if ($replace) {
                    //check for replaced class
                    $query->where('replaced_with', '<>', 0);
                } else if ($extra) {
                    //check for extra class
                    $query->where('extra', '=', true);
                } else {
                    // check for regular class class
                    $query->where('extra', '=', false)->where('replaced_with', 0);
                }
            })
            ->select('date', 'attendance')
            ->get();

        $dateCollector = collect([]);
        foreach ($attendance as $att) {
            $date = $att->date;
            $newDate = date_create($date)->format('Y-m-d');
            $att->date = $newDate;
            $dateCollector->push($att->date);
        }

        return response()->json([
            "date" => $attendance,
            "start" => $dateCollector->min(),
            "end" => $dateCollector->max()
        ]);
    }

        
    function pushNotification(Request $request){
        $batch = BatchLevelMaping::join('faculties', 'batch_level_maping.faculty_id', 'faculties.id')
            ->where('faculties.name', $request->input('facultyControl'))
            ->where('batch_level_maping.level', $request->input('levelControl'))
            ->select('faculties.name as faculty', 'batch_level_maping.level as level', 'batch_level_maping.batch as batch')
            ->first();

        if ($request->input("scheduleControl") == "on") {

            $notification = Notification::create([
                "sender_id" => Auth::id(),
                "title" => $request->input('title'),
                "description" => $request->input('description'),
                "schedule_date" => date_format(date_create($request->input('dateControl')), 'Y-m-d'),
                "time" => date("H:i:s", strtotime($request->input('timeControl'))),
                "target_user" => serialize($batch),
                "target_input" => null,
                "send" => false,
            ]);

        } else {

            $notification = Notification::create([
                "sender_id" => Auth::id(),
                "title" => $request->input('title'),
                "description" => $request->input('description'),
                "target_user" => serialize($batch),
                "schedule_date" => date("Y-m-d"),
                "time" => date("H:i:s"),
                "target_input" => null,
            ]);

            $this->sendNotification($notification);

        }

        return response()->json([
            "message" => "Success"
        ]);
    }

    function sendNotification(Notification $notification){
        $targetArray = unserialize($notification->target_user);

        $this->sendToAllStudentOfFacultyOfBatch($notification->id, $targetArray->faculty, $targetArray->batch);

        if (count($this->userFCMToken) > 0) {
            $this->userFCMToken = array_unique($this->userFCMToken);
            $response = $this->sentToFCM($notification);
            $notification->update([
                //"multicast_id"=>$response->multicast_id,
                //"message_id"=>$response->result->message_id,
                "status" => json_encode($response),
                "send" => true,
            ]);
        } else {
            Log::info("No device token found");
            $notification->update([
                //"multicast_id"=>$response->multicast_id,
                //"message_id"=>$response->result->message_id,
                "status" => 'No Device token found',
                "send" => true,
            ]);
        }
    }

    function sentToFCM(Notification $notification)
    {

        $notifi = array
        (
            'icon' => 'myicon',/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );

        $msg = array
        (

            'title' => $notification->title,
            'body' => $notification->description,
            "description" => $notification->description,
            "id" => $notification->id,
            "date" => $notification->schedule_date,
            "time" => $notification->time,

            'icon' => 'myicon',/*Default Icon*/
            'sound' => 'default'/*Default sound*/
        );
        $fields = array
        (
            'registration_ids' => $this->userFCMToken,
            //'notification' => $notifi,
            'data' => $msg
        );


        $headers = array
        (
            'Authorization: key=' . env('FCM_API_ACCESS_KEY'),
            'Content-Type: application/json'
        );

        //Send Reponse To FireBase Server

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        //Echo Result Of FireBase Server

        return $result;
    }

    public function sendToAllStudentOfFacultyOfBatch($notificationId, $facultyName, $batch)
    {
        $notificationUserArray = array();
        $facultyId = faculty::where('name', $facultyName)->first()->id;
        foreach (Student::where('faculty_id', $facultyId)->where('batch', $batch)->get() as $user) {
            if ($user->user) {
                $notificationUser = [
                    "notification_id" => $notificationId,
                    "user_id" => $user->id,
                    "fcm_token" => $user->user->fcm_token,
                ];

                if ($user->user->fcm_token)
                    array_push($this->userFCMToken, $user->user->fcm_token);

                array_push($notificationUserArray, $notificationUser);
            }
        }
        NotificationUser::insert($notificationUserArray);
    }

    public function getNotificationHistory(Request $request)
    {
        $notifications = Notification::where('sender_id', Auth::id())
//            ->limit(5)
            ->orderBy('id', 'desc')
            ->get();

        foreach ($notifications as $notification) {
            $target = unserialize($notification->target_user);
            $notification["faculty"] = $target->faculty;
            $notification["level"] = $target->level;
        }

        return $notifications;
    }


    function addCoursePlan(Request $request)
    {
        $coursePlan = CoursePlan::create([
            "title" => $request->input("title"),
            "description" => $request->input('description'),
            "planned_date" => $request->input("date"),
            "subject_id" => $request->input("subjectId"),
            "teacher_id" => Auth::id(),
        ]);
        return $coursePlan;
    }

    function deleteCoursePlan(Request $request)
    {
        $coursePlan = CoursePlan::find($request->input('id'));
        $coursePlan->delete();
        return $coursePlan;
    }

    function startCoursePlan(Request $request)
    {
        $coursePlan = CoursePlan::find($request->input('id'));
        $coursePlan->update([
            "started_date" => date('Y-m-d')
        ]);
        return $coursePlan;
    }

    function endCoursePlan(Request $request)
    {
        $coursePlan = CoursePlan::find($request->input('id'));
        $coursePlan->update([
            "end_date" => date('Y-m-d')
        ]);
        return $coursePlan;
    }

    function editCoursePlan(Request $request)
    {
        $coursePlan = CoursePlan::find($request->input('id'));
        $coursePlan->update([
            "started_date" => $request->input('started_date'),
            "end_date" => $request->input('end_date')
        ]);
        return $coursePlan;
    }

    function getMyClasses(Request $request)
    {
        return Attendance::where('teacher_id', Auth::id())
            ->join('subjects', 'attendances.subject_id', 'subjects.id')
            ->leftJoin('subjects as join_subject', 'attendances.replaced_with', 'join_subject.id')
            ->select(
                'subjects.name as subject_name',
                'join_subject.name as replaced_with_subject_name',
                'attendances.date as date',
                'attendances.start_time as start_time',
                'attendances.end_time as end_time',
                DB::raw("CASE WHEN attendances.extra = 1 THEN '3' WHEN attendances.replaced_with <> 0 THEN '2' ELSE '1' END as type")
            )
            ->groupBy('date')
            ->get();
    }


    public function routine(){   
        // return response()->json([Auth::user()->first_name]);
        $days=Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')                    
                    ->where('routines.teacher_id',Auth::id())
                    // ->where('routines.teacher_id',309)
                    ->select('day')
                    ->groupBy('day')
                    ->get()
                    ->toArray();

        $level=array();

        foreach ($days as $day){
            
            $day['name']=$day['day'];
            $day['period']=Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                ->join('teachers', 'routines.teacher_id', '=', 'teachers.id')
                ->join('faculties', 'subjects.faculty_id', '=', 'faculties.id')
                ->where('routines.teacher_id',Auth::id())           
                // ->where('routines.teacher_id',309)    
                ->where('routines.day',$day['day'])
                ->select('subjects.name as subject','routines.start_at as start_at','faculties.name as faculty_name','routines.end_at as end_at','subjects.level as level','faculties.name as faculty')                                
                ->orderBy('routines.start_at')
                ->get();
            array_push($level,$day);                                
        }
        return response()->json(['levels'=>$level]);
    } 
}
