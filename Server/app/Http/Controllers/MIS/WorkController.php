<?php
namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Work;
use HCCNetwork\WorkCategory;

class WorkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');      
    }  
    public function index()
    {      
        /*$data= array(); 
        $menus=array();
        
        foreach ($mainmenu=Menu::where('parent_id','=',null)->orderBy('order')->get() as $menu)
        {
            $data['menu']=$menu; 
            $data['submenu']=Menu::where('parent_id',$menu->id)->orderBy('order')->get();
            array_push($menus,$data);            
        } */      
       return View::make('mis.work.work')
        ->with('title','Work')
        ->with('works',WorkCategory::all());
    }
/*
    public function destroy($id){
        Menu::find($id)->delete();  
        foreach($submenu=Menu::where('parent_id',$id)->get() as $menu){
            $menu->delete();
        }

        return redirect()->back();
    }
*/
    public function store(Request $request){
        $category=WorkCategory::where('name',$request->input('category'))->first();
        $category_id=$category->id;        
        Work::create(array(
            'title' => $request->input('title'),            
            'banner' => $request->input('banner'),
            'summary' => $request->input('summary'),
            'category_id' =>$category_id,            
            'content' => $request->input('content'),

        ));
        return redirect()->back();        
    }

    public function show($id){
        return View::make('mis.work.editwork')
        ->with('title','Work')
        ->with('works',WorkCategory::all())
        ->with('editwork',Work::find($id));
    }
    public function destroy($id){
        Work::find($id)->delete();
        return redirect('work');
    }

    public function update(Request $request, $id){ 
        $category=WorkCategory::where('name',$request->input('category'))->first();
        $category_id=$category->id;        
        Work::where('id',$id)->update(array(
            'title' => $request->input('title'),            
            'banner' => $request->input('banner'),
            'summary' => $request->input('summary'),
            'category_id' =>$category_id,            
            'content' => $request->input('content'),

        ));
        return redirect('work');       
    }
}