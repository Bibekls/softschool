<?php
namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Work;
use HCCNetwork\WorkCategory;

class WorkCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');      
    }  
   

    public function destroy(Request $request){
        $deleteitem=WorkCategory::where('name',$request->input('category'))->first();       
        if($request->input('category')!="Default")WorkCategory::where('name',$deleteitem->name)->delete();
        foreach($works=Work::where('category_id',$deleteitem->id)->get() as $work){
            $work->delete();
        }
        return redirect()->back();
    }

    public function store(Request $request){       
        WorkCategory::create(array(
            'name' => $request->input('name')
        ));
        return redirect()->back();
    }

    public function update(Request $request, $id){
      
        WorkCategory::where('id',$id)->update(array(            
            'name' => $request->input('name')
        ));
        return redirect()->back();
    }
}