<?php
namespace HCCNetwork\Http\Controllers\MIS;

use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\Http\Requests\mis\subject\CloneSubjectRequest;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\mis\subject\SubjectRequest;
use HCCNetwork\Subject;
use HCCNetwork\faculty;
use HCCNetwork\Routine;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {

        $routine = array();
        $level = array();
        $routines = array();

        foreach ($facultylink = Faculty::all() as $faculty) {
            $routine['faculty'] = $faculty;
            $routine['batch'] = array();

            foreach ($batches = Subject::where('faculty_id', $faculty->id)->select('batch')->groupBy('batch')->get()->toArray() as $batch) {
                $batch['name'] = $batch['batch'];
                $batch['levels'] = array();

                foreach ($levels = Subject::where('faculty_id', $faculty->id)->where('batch', $batch['batch'])->select('level')->groupBy('level')->get()->toArray() as $level) {
                    $subject = Subject::where('level', $level)
                        ->where('batch', $batch['batch'])
                        ->where('faculty_id', $faculty->id)
                        ->get();
                    $level['name'] = $level['level'];
                    $level['subjects'] = $subject;
                    array_push($batch['levels'], $level);
                }
                array_push($routine['batch'], $batch);
            }
            array_push($routines, $routine);
        }

        return View::make('mis.subject.subject')
            ->with('title', 'Subject')
            ->with('subjects', Subject::all())
            ->with('faculties', Faculty::all())
            ->with('batches', Subject::groupBy('batch')->pluck('batch'))
            ->with('routines', $routines);
    }

    public function show($id)
    {

    }

    public function destroy($id)
    {
        foreach ($routines = Routine::where('subject_id', $id)->get() as $routine) {
            Routine::find($routine->id)->delete();
        }
        Subject::find($id)->delete();
        return redirect()->back();
    }

    public function store(SubjectRequest $request)
    {
        $faculty = Faculty::where('name', $request->input('faculty'))->first();
        Subject::create(array(
            'name' => $request->input('subject_name'),
            'level' => $request->input('level'),
            'faculty_id' => $faculty->id,
            'batch' => $request->input('year'),
        ));
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {

    }

    public function cloneSubject(CloneSubjectRequest $request)
    {
        $subject = Subject::where('batch', $request->input('past_year'))->where('faculty_id', $request->input('faculty'))->get();
        $subjectArr = array();

        foreach ($subject as $sub) {
            array_push($subjectArr, [
                'level' => $sub->level,
                'name' => $sub->name,
                'faculty_id' => $request->input('faculty'),
                'batch' => $request->input('year')
            ]);
        }

        Subject::insert($subjectArr);
        return redirect()->back();
    }

    public function updateSubject(Request $request)
    {
        if(Subject::find($request->input('subject-id'))){
            Subject::find($request->input('subject-id'))->update([
              "name"=>$request->input('subject-name')
            ]);
        }
        return redirect()->back();
    }

}