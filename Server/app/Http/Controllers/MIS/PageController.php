<?php
namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Page;
use HCCNetwork\PageCategory;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');      
    }  
    public function index()
    {      
        /*$data= array(); 
        $menus=array();
        
        foreach ($mainmenu=Menu::where('parent_id','=',null)->orderBy('order')->get() as $menu)
        {
            $data['menu']=$menu; 
            $data['submenu']=Menu::where('parent_id',$menu->id)->orderBy('order')->get();
            array_push($menus,$data);            
        } */      
       return View::make('mis.page.page')
        ->with('title','page')
        ->with('pages',PageCategory::all());
    }
/*
    public function destroy($id){
        Menu::find($id)->delete();  
        foreach($submenu=Menu::where('parent_id',$id)->get() as $menu){
            $menu->delete();
        }

        return redirect()->back();
    }
*/
    public function store(Request $request){
        $category=PageCategory::where('name',$request->input('category'))->first();
        $category_id=$category->id;        
        Page::create(array(
            'name' => $request->input('name'),
            'title' => $request->input('title'),
            'tag' => $request->input('tag'),
            'order' => $request->input('order'),
            'category_id' =>$category_id,            
            'content' => $request->input('content'),

        ));
        return redirect()->back();        
    }

    public function show($id){
        return View::make('mis.page.editpage')
        ->with('title','Edit Page')
        ->with('pages',PageCategory::all())
        ->with('editpage',Page::find($id));
    }

    public function destroy($id){
        Page::find($id)->delete();
        return redirect('page');
    }

    public function update(Request $request, $id){ 
        $category=PageCategory::where('name',$request->input('category'))->first();
        $category_id=$category->id;       
        
        Page::where('id',$id)->update(array(
            'name' => $request->input('name'),
            'title' => $request->input('title'),
            'tag' => $request->input('tag'),
            'order' => $request->input('order'),
            'category_id' =>$category_id,            
            'content' => $request->input('content'),

        ));
        return redirect('page');        
    }
}