<?php
namespace HCCNetwork\Http\Controllers\MIS;

use HCCNetwork\EventCalendar;
use HCCNetwork\Events\FCMNotification;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\Http\Requests\mis\notification\NotificationRequest;
use HCCNetwork\Notification;
use HCCNetwork\NotificationUser;
use Illuminate\Support\Facades\Log;
use Mockery\Matcher\Not;
use View;
use Auth;
use Request;
use Event;
use HCCNetwork\Subject;
use HCCNetwork\faculty;
use HCCNetwork\Routine;
use HCCNetwork\User;
use HCCNetwork\student;
use HCCNetwork\Teacher;

class NotificationController extends Controller
{
    private $userFCMToken;

    public function __construct()
    {
        $this->userFCMToken = array();
    }

    public function index()
    {

        return View::make('mis.notification.notification')
            ->with('title', 'Notification')
            ->with('faculties', Faculty::all())
            ->with('notifications', Notification::join('users', 'notifications.sender_id', '=', 'users.id')
                ->leftJoin('students', 'users.id', '=', 'students.id')
                ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
                ->select('users.user_type_id', 'notifications.*', 'students.first_name as first_name',
                    'students.last_name as last_name', 'teachers.first_name as first_name',
                    'teachers.last_name as last_name')
                ->get());
    }

    public function show($id)
    {

    }

    public function destroy($id)
    {
        foreach (Notification::find($id)->notificationUser as $singleUser) {
            $singleUser->delete();
        }
        Notification::find($id)->delete();
        return redirect()->back();
    }

    public function store(NotificationRequest $request)
    {
        $targetArray = array();

        foreach ($request->input('target') as $target) {
            $element = explode("#", $target);

            if (count($element) >= 1) {
                if (!array_key_exists($element[0] . '-data', $targetArray)) {
                    $targetArray[$element[0] . '-data'] = array();
                }
                if (count($element) == 1)
                    $targetArray[$element[0]] = true;
            }

            if (count($element) >= 2) {

                if (!array_key_exists($element[1] . '-data', $targetArray[$element[0] . '-data'])) {
                    $targetArray[$element[0] . '-data'][$element[1] . '-data'] = array();
                }

                if (count($element) == 2)
                    $targetArray[$element[0] . '-data'][$element[1]] = true;
            }

            if (count($element) >= 3) {

                if (!array_key_exists($element[2] . '-data', $targetArray[$element[0] . '-data'][$element[1] . '-data'])) {
                    $targetArray[$element[0] . '-data'][$element[1] . '-data'][$element[2] . '-data'] = array();
                }

                if (count($element) == 3)
                    $targetArray[$element[0] . '-data'][$element[1] . '-data'][$element[2]] = true;


            }


        }

        if ($request->input("schedule") == "on") {

            $notification = Notification::create([
                "sender_id" => Auth::id(),
                "title" => $request->input('title'),
                "description" => $request->input('description'),
                "schedule_date" => date_format(date_create($request->input('date')), 'Y-m-d'),
                "time" => date("H:i:s", strtotime($request->input('time'))),
                "target_user" => serialize($targetArray),
                "target_input" => serialize($request->input('target')),
                "send" => false,
            ]);

        } else {

            $notification = Notification::create([
                "sender_id" => Auth::id(),
                "title" => $request->input('title'),
                "description" => $request->input('description'),
                "target_user" => serialize($targetArray),
                "schedule_date" => date("Y-m-d"),
                "time" => date("H:i:s"),
                "target_input" => serialize($request->input('target'))
            ]);

            $this->sendNotification($notification);

        }

        return redirect()->back();
    }

    function sendPendingNotification()
    {
        foreach (Notification::where('send', false)->whereDate('schedule_date', '<=', date('Y-m-d'))->where('time', '<=', date('H:i:s'))->get() as $notification) {
            $this->sendNotification($notification);
        }
    }

    function sendNotification(Notification $notification)
    {
        $targetArray = unserialize($notification->target_user);

        if ($notification->sender_id == "1") {

            if (array_key_exists('all', $targetArray)) {
                $this->sendToAllMember($notification->id);
            } else {
                if (array_key_exists('student-data', $targetArray)) {
                    if (array_key_exists('student', $targetArray)) {
                        $this->sendToAllStudent($notification->id);
                    } else {
                        foreach (Faculty::all() as $faculty) {
                            if (array_key_exists($faculty->name . '-data', $targetArray['student-data'])) {

                                if (array_key_exists($faculty->name, $targetArray['student-data'])) {
                                    $this->sendToAllStudentOfFaculty($notification->id, $faculty->name);
                                } else {
                                    foreach ($faculty->batch as $batch) {
                                        if (array_key_exists($batch->batch . '-data', $targetArray['student-data'][$faculty->name . '-data'])) {
                                            $this->sendToAllStudentOfFacultyOfBatch($notification->id, $faculty->name, $batch->batch);
                                        }
                                    }

                                }

                            }
                        }

                    }
                }


                if (array_key_exists('teacher-data', $targetArray)) {
                    if (array_key_exists('teacher', $targetArray)) {
                        $this->sendToAllTeacher($notification->id);
                    } else {
                        foreach (Faculty::all() as $faculty) {
                            if (array_key_exists($faculty->name, $targetArray['teacher-data'])) {
                                $this->sendToAllTeacherOfFaculty($notification->id, $faculty->name);
                            }
                        }
                    }
                }
            }
        } else {
            $this->sendToAllStudentOfFacultyOfBatch($notification->id, $targetArray->faculty, $targetArray->batch);
        }

        if (count($this->userFCMToken) > 0) {
            $this->userFCMToken = array_unique($this->userFCMToken);
            $response = $this->sentToFCM($notification);
            $notification->update([
                //"multicast_id"=>$response->multicast_id,
                //"message_id"=>$response->result->message_id,
                "status" => json_encode($response),
                "send" => true,
            ]);
        } else {
            Log::info("No device token found");
            $notification->update([
                //"multicast_id"=>$response->multicast_id,
                //"message_id"=>$response->result->message_id,
                "status" => 'No Device token found',
                "send" => true,
            ]);
        }
    }

    function sentToFCM(Notification $noti)
    {

        $notification = Notification::leftJoin('users', 'notifications.sender_id', '=', 'users.id')
            ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
            ->where('notifications.id', $noti->id)
            ->orderBy('notifications.schedule_date', 'desc')
            ->select('notifications.id as id',
                'notifications.title as title',
                'notifications.description as description',
                'notifications.schedule_date as date',
                'notifications.time as time',
                'notifications.send as send',
                'teachers.first_name as first_name',
                'teachers.last_name as last_name',
                'users.user_type_id as user_type'
            )
            ->first();

        $notifi = array
        (
            'icon' => 'myicon',/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );

        $msg = array
        (

            'title' => $notification->title,
            'body' => $notification->description,
            "description" => $notification->description,
            "id" => $notification->id,
            "date" => $notification->schedule_date,
            "time" => $notification->time,
            "send" => $notification->send,
            "first_name" => $notification->first_name,
            "last_name" => $notification->last_name,
            "user_type" => $notification->user_type,

            'icon' => 'myicon',/*Default Icon*/
            'sound' => 'default'/*Default sound*/
        );
        $fields = array
        (
            'registration_ids' => $this->userFCMToken,
            //'notification' => $notifi,
            'data' => $msg
        );


        Log::info(env('FCM_API_ACCESS_KEY'));
        $headers = array
        (
            'Authorization: key=' . env('FCM_API_ACCESS_KEY'),
            'Content-Type: application/json'
        );

        //Send Reponse To FireBase Server

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        //Echo Result Of FireBase Server

        return $result;
    }

    public function sendToAllMember($notificationId)
    {
        $notificationUserArray = array();
        foreach (User::all() as $user) {
            $notificationUser = array(
                "notification_id" => $notificationId,
                "user_id" => $user->id,
                "fcm_token" => $user->fcm_token,

            );

            if ($user->fcm_token)
                array_push($this->userFCMToken, $user->fcm_token);

            array_push($notificationUserArray, $notificationUser);
        }

        NotificationUser::insert($notificationUserArray);
    }

    public function sendToAllStudent($notificationId)
    {
        $notificationUserArray = array();
        foreach (User::where('user_type_id', 4)->get() as $user) {
            if (!$user->user) continue;

            $notificationUser = [
                "notification_id" => $notificationId,
                "user_id" => $user->id,
                "fcm_token" => $user->fcm_token,

            ];

            if ($user->fcm_token)
                array_push($this->userFCMToken, $user->fcm_token);

            array_push($notificationUserArray, $notificationUser);
        }
        NotificationUser::insert($notificationUserArray);
    }

    public function sendToAllTeacher($notificationId)
    {
        $notificationUserArray = array();
        foreach (User::where('user_type_id', 3)->get() as $user) {
            $notificationUser = [
                "notification_id" => $notificationId,
                "user_id" => $user->id,
                "fcm_token" => $user->fcm_token,

            ];

            if ($user->fcm_token)
                array_push($this->userFCMToken, $user->fcm_token);

            array_push($notificationUserArray, $notificationUser);
        }
        NotificationUser::insert($notificationUserArray);
    }

    public function sendToAllStudentOfFaculty($notificationId, $facultyName)
    {
        $notificationUserArray = array();
        $facultyId = faculty::where('name', $facultyName)->first()->id;
        foreach (Student::where('faculty_id', $facultyId)->get() as $user) {
            if (!$user->user) continue;
            $notificationUser = [
                "notification_id" => $notificationId,
                "user_id" => $user->id,
                "fcm_token" => $user->user->fcm_token,

            ];

            if ($user->user->fcm_token)
                array_push($this->userFCMToken, $user->user->fcm_token);

            array_push($notificationUserArray, $notificationUser);
        }
        NotificationUser::insert($notificationUserArray);

    }

    public function sendToAllTeacherOfFaculty($notificationId, $facultyName)
    {

        $notificationUserArray = array();
        $facultyId = faculty::where('name', $facultyName)->first()->id;
        foreach (Teacher::join('faculty_teacher', 'teachers.id', '=', 'faculty_teacher.teacher_id')
                     ->join('faculties', 'faculty_teacher.faculty_id', '=', 'faculties.id')
                     ->join('users', 'teachers.id', '=', 'users.id')
                     ->where('faculties.id', $facultyId)
                     ->select('teachers.id as id', 'users.fcm_token as fcm_token')
                     ->get()
                 as $user) {


            $notificationUser = [
                "notification_id" => $notificationId,
                "user_id" => $user->id,
                "fcm_token" => $user->fcm_token,

            ];

            if ($user->fcm_token)
                array_push($this->userFCMToken, $user->fcm_token);

            array_push($notificationUserArray, $notificationUser);
        }
        NotificationUser::insert($notificationUserArray);

    }

    public function sendToAllStudentOfFacultyOfBatch($notificationId, $facultyName, $batch)
    {
        $notificationUserArray = array();
        $facultyId = faculty::where('name', $facultyName)->first()->id;
        foreach (Student::where('faculty_id', $facultyId)->where('batch', $batch)->get() as $user) {
            if (!$user->user) continue;
            $notificationUser = [
                "notification_id" => $notificationId,
                "user_id" => $user->id,
                "fcm_token" => $user->user->fcm_token,

            ];

            if ($user->user->fcm_token)
                array_push($this->userFCMToken, $user->user->fcm_token);

            array_push($notificationUserArray, $notificationUser);
        }
        NotificationUser::insert($notificationUserArray);
    }

    public function update(Request $request, $id)
    {


    }

}