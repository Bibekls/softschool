<?php
namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Menu;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');      
    }  
    public function index()
    {      
        $data= array(); 
        $menus=array();
        
        foreach ($mainmenu=Menu::where('parent_id','=',null)->orderBy('order')->get() as $menu)
        {
            $data['menu']=$menu; 
            $data['submenu']=Menu::where('parent_id',$menu->id)->orderBy('order')->get();
            array_push($menus,$data);            
        }       
       return View::make('mis.menu.menu')
        ->with('title','menu')
        ->with('menus',$menus);
    }

    public function destroy($id){
        Menu::find($id)->delete();  
        foreach($submenu=Menu::where('parent_id',$id)->get() as $menu){
            $menu->delete();
        }

        return redirect('menu');
    }

    public function store(Request $request){      
        if($request->input('parent')!="Select Parent"){
            $parent_menu=Menu::where('name',$request->input('parent'))->first();
            $parent_id=$parent_menu->id;
        }
        else{
            $parent_id=null;
        }
        
        
        Menu::create(array(
            'name' => $request->input('name'),
            'url' => $request->input('url'),
            'order' => $request->input('order'),
            'parent_id' =>$parent_id,
            'target' => $request->input('target'),
            'description' => $request->input('description'),

        ));
        return redirect()->back();        
    }

    public function show($id){
        $data= array(); 
        $menus=array();
        
        foreach ($mainmenu=Menu::where('parent_id','=',null)->orderBy('order')->get() as $menu)
        {
            $data['menu']=$menu; 
            $data['submenu']=Menu::where('parent_id',$menu->id)->orderBy('order')->get();
            array_push($menus,$data);            
        }       
       return View::make('mis.menu.editmenu')
        ->with('title','menu')
        ->with('editmenu',Menu::find($id))
        ->with('menus',$menus);        
    }

    public function update(Request $request, $id){      
        
        Menu::where('id',$id)->update(array(
            'name' => $request->input('name'),
            'url' => $request->input('url'),
            'order' => $request->input('order'),            
            'target' => $request->input('target'),
            'description' => $request->input('description'),

        ));
        return redirect('menu');        
    }
	   
}
