<?php
namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\EventCalendar;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Auth;
use Request;
use HCCNetwork\Http\Requests\mis\event\EventRequest;
use HCCNetwork\Subject;
use HCCNetwork\faculty;
use HCCNetwork\Routine;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function index()
    {
        return View::make('mis.event.event')
            ->with('title','Event Calendar')
            ->with('events',EventCalendar::orderBy('date','dec')->get())
            ->with('futureEvents',EventCalendar::orderBy('date','asce')->where('date','>=',date('y-m-d'))->get())
            ->with('pastEvents',EventCalendar::orderBy('date','desc')->where('date','<',date('y-m-d'))->get());
    }

    public function show($id)
    {

    }

    public function destroy($id){
        EventCalendar::find($id)->delete();
        return redirect()->back();
    }

    public function store(EventRequest $request){
        EventCalendar::create(array(
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'date' => date_format(date_create($request->input('date')),'Y-m-d'),
            'start_at' => $request->input('has_time_interval')==''?null:$request->input('start_at'),
            'end_at' => $request->input('has_time_interval')==''?null:$request->input('end_at'),

        ));
        return redirect()->back();
    }

    public function update(Request $request, $id){


    }

}