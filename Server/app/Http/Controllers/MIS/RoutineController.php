<?php
namespace HCCNetwork\Http\Controllers\MIS;

use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\Http\Middleware\student\student;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\mis\routine\RoutineRequest;
use HCCNetwork\Routine;
use HCCNetwork\Teacher;
use HCCNetwork\Subject;
use HCCNetwork\User;
use HCCNetwork\faculty;

class RoutineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $routine = array();
        $level = array();
        $day = array();
        $period = array();
        $routines = array();

        foreach ($facultylink = Faculty::all() as $faculty) {
            $routine['faculty'] = $faculty;
            $routine['levels'] = array();
            $levels = Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                ->where('subjects.faculty_id', $faculty->id)
                ->select('level')
                ->groupBy('level')
                ->get()
                ->toArray();
            $routine['level-list'] = $levels;

            foreach ($levels as $level) {
                $level['name'] = $level['level'];

                $days = Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                    ->where('subjects.faculty_id', $faculty->id)
                    ->where('subjects.level', $level['level'])
                    ->select('day')
                    ->groupBy('day')
                    ->get()
                    ->toArray();
                $level['days'] = array();
                foreach ($days as $day) {

                    $day['name'] = $day['day'];
                    $day['period'] = Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                        ->join('teachers', 'routines.teacher_id', '=', 'teachers.id')
                        ->where('subjects.faculty_id', $faculty->id)
                        ->where('subjects.level', $level['level'])
                        ->where('routines.day', $day['day'])
                        ->select('routines.id as id', 'subjects.name as subject', 'teachers.first_name as fname', 'teachers.last_name as lname', 'routines.start_at as start_at', 'routines.end_at as end_at')
                        ->orderBy('routines.start_at')
                        ->get();
                    array_push($level['days'], $day);
                }

                array_push($routine['levels'], $level);

            }
            array_push($routines, $routine);
        }

        return View::make('mis.routine.routine')
            ->with('title', 'Routine')
            ->with('batches', subject::groupBy('batch')->pluck('batch'))
            ->with('faculties', faculty::all())
            ->with('Routine', Routine::all())
            ->with('teachers', Teacher::all())
            ->with('subjects', Subject::all())
            ->with('routines', $routines);
    }

    public function show($id){

    }

    public function destroy($id)
    {
        Routine::find($id)->delete();
        return redirect()->back();
    }

    public function store(RoutineRequest $request)
    {

        $teacher = User::find($request->input('teacher'));

        $subject = Subject::find($request->input('subject'));
        $start_at = $request->input('start_at');
        $end_at = $request->input('end_at');
        $start_at_sec = strtotime("1970-01-01 $start_at UTC");
        $end_at_sec = strtotime("1970-01-01 $end_at UTC");
        $day = null;

        foreach ($request->input('day') as $dayInput) {

            switch ($dayInput) {
                case "Sunday":
                    $day = 1;
                    break;
                case "Monday":
                    $day = 2;
                    break;
                case "Tuesday":
                    $day = 3;
                    break;
                case "Wednesday":
                    $day = 4;
                    break;
                case "Thursday":
                    $day = 5;
                    break;
                case "Friday":
                    $day = 6;
                    break;
                case "Saturday":
                    $day = 7;
                    break;
            }


            if ($start_at_sec < 21600 || $end_at_sec > 37800 || $start_at_sec > $end_at_sec)
                return redirect()->back()
                    ->withErrors([
                        "time-interval"=>"Error on time interval"
                    ]);

            foreach ($hisRoutine = Routine::where('teacher_id', $teacher->id)->where('day', $day)->get() as $timeInterval) {
                if ($start_at_sec > $timeInterval->start_at
                    && $start_at_sec < $timeInterval->end_at
                    ||
                    $end_at_sec > $timeInterval->start_at
                    && $end_at_sec < $timeInterval->end_at
                )
                    return redirect()->back()
                        ->withErrors([
                            "time-interval"=>"Time interval overlaped"
                        ]);
            }

            Routine::create(array(
                'teacher_id' => $teacher->id,
                'subject_id' => $subject->id,
                'day' => $day,
                'start_at' => $start_at_sec,
                'end_at' => $end_at_sec,
            ));
        }


        return redirect()->back();
    }

    public function update(Request $request, $id)
    {


    }

    public function getTeacher(Request $request)
    {
        return Teacher::join('faculty_teacher', 'teachers.id', '=', 'faculty_teacher.teacher_id')
            ->join('users', 'teachers.id', '=', 'users.id')
            ->where('faculty_teacher.faculty_id',$request->input('faculty'))
            ->get();
    }

    public function getSubject(Request $request)
    {
        return Subject::where('batch', $request->input('batch'))
            ->where('faculty_id', $request->input('faculty'))
            ->where('level', $request->input('level'))
            ->get();
    }

}