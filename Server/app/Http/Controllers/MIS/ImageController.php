<?php
namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Gallery;
use HCCNetwork\user;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');      
    }

    public function store(Request $request){
        
        $file=$request->file('file');
        $filename=uniqid().$file->getClientOriginalName();
        if(!file_exists('images'))
        {
            mkdir('images',0777,true);
        }
        $file->move('images',$filename);
        /*
        **  Resize image to 720 * x
        */
        Image::make('images/'.$filename)->resize(720,null,function ($constraint){$constraint->aspectRatio();})->save('images/'.$filename,100);
        /*
        **  Thumbs creation
        */

        if(!file_exists('images/thumbs'))
        {
            mkdir('images/thumbs',0777,true);
        }

        $thumb=Image::make('images/'.$filename)->resize(256,null,function ($constraint){$constraint->aspectRatio();})->save('images/thumbs/'.$filename,50);
        /*
        **  Thumbnail Creation
        */

        if(!file_exists('images/thumbnails'))
        {
            mkdir('images/thumbnails',0777,true);
        }

        $thumb=Image::make('images/'.$filename)->resize(64,64)->save('images/thumbnails/'.$filename,50);
        /*
        **  End of image saving procedure
        */


        $user=User::find(Auth::user()->id);
        $image=$user->images()->create([
            'user_id'=>Auth::user()->id,
            'gallery_id'=>$request->input('gallery'),
            'file_name'=>$filename,
            'file_size'=>$file->getClientSize(),
            'file_mime'=>$file->getClientMimeType(),
            'file_path'=>'images/'.$filename
        ]);
        return redirect()->back();        
    }
}