<?php
namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;

use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use HCCNetwork\BatchLevelMaping;
use HCCNetwork\student;
use HCCNetwork\faculty;


class BLMController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');      
    }  

    public function getmap(){
        $blm=array();
        $facultyarray=array();
        $batcharray=array();

        $faculties=faculty::all();
        $blm['faculties']=array();
        foreach($faculties as $faculty){
            
            $facultyarray['faculty']=$faculty->name;
            $facultyarray['id']=$faculty->id;
            $facultyarray['batches']=array();            

            $batch=student::where('faculty_id',$faculty->id)
                            ->select('batch')
                            ->groupBy('batch')
                            ->orderBy('batch','desc')
                            ->get();
                      
            foreach($batch as $value)
            {               

                if(BatchLevelMaping::where('faculty_id',$faculty->id)
                                ->where('batch',$value->batch)
                                ->first() ==null){
                    BatchLevelMaping::create(array(
                                            'faculty_id' => $faculty->id,
                                            'batch'=>$value->batch
                                        ));
                }
                $batcharray['batch']=$value->batch;
                $batcharray['level']=BatchLevelMaping::where('batch',$value->batch)
                                                ->where('faculty_id',$faculty->id)
                                                ->pluck('level')
                                                ->first();
                array_push($facultyarray['batches'], $batcharray);
            }
            array_push($blm['faculties'], $facultyarray);
            
        }       

        return View::make('mis.batch-level-maping')
            ->with('title','Batch Level Mapping')
            ->with('blmData',$blm);        
    }

    public function setmap(Request $request)
    {
        BatchLevelMaping::where('faculty_id',$request->input('faculty_id'))
                        ->where('batch',$request->input('batch'))
                        ->update(array(
                                    'level'=>$request->input('level')        
                                ));

        return redirect()->back();
    }
      
}
