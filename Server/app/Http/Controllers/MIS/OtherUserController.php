<?php
namespace HCCNetwork\Http\Controllers\MIS;


use HCCNetwork\FacultyTeacher;
use HCCNetwork\Guest;
use HCCNetwork\Http\Controllers\Controller;

use HCCNetwork\Http\Requests\mis\user\GuestRegistrationFormRequest;
use HCCNetwork\Http\Requests\mis\user\GuestUpdateRequest;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\Post;
use HCCNetwork\url;
use HCCNetwork\Teacher;
use HCCNetwork\student;
use HCCNetwork\faculty;
use HCCNetwork\Feedback;
use Carbon;

use Intervention\Image\Facades\Image as Image;

use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatable;
use HCCNetwork\Http\Requests\mis\user\TeacherRegistrationFormRequest;
use HCCNetwork\Http\Requests\mis\user\StudentRegistrationFormRequest;

use HCCNetwork\Http\Requests\mis\user\TeacherUpdateRequest;
use HCCNetwork\Http\Requests\mis\user\StudentUpdateRequest;
use HCCNetwork\Http\Requests\mis\user\PasswordResetRequest;


use Illuminate\Support\Facades\Auth;


class OtherUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function users()
    {

        $userList = array();
        $userLists = array();
        $batch = array();

        foreach ($facultylink = Faculty::all() as $faculty) {
            $userList['faculty'] = $faculty;
            $userList['batch'] = array();
            $batchs = student::withTrashed()->where('faculty_id', $faculty->id)
                ->select('batch')
                ->groupBy('batch')
                ->get()
                ->toArray();
            $userList['batch-list'] = $batchs;

            foreach ($batchs as $batch) {
                $batch['name'] = $batch['batch'];

                $batch['students'] = User::withTrashed()->join('students', 'users.id', '=', 'students.id')
                    ->join('images', 'users.profile_pic', '=', 'images.id')
                    ->where('faculty_id', $faculty->id)
                    ->where('batch', $batch['batch'])
                    ->where('verified', '2')
                    ->select('users.deleted_at as deleted_at', 'users.id as user_id', 'students.first_name as first_name', 'students.middle_name as middle_name', 'students.last_name as last_name', 'images.file_name as file_name', 'students.gender as gender')
                    ->orderBy('first_name')
                    ->get();

                array_push($userList['batch'], $batch);

            }

            array_push($userLists, $userList);
        }


        return View::make('mis.manage-user.other-users', ['title' => 'Other-Users'])
            ->with('loginOtherUser', null)
            ->with('teachers', User::withTrashed()->join('teachers', 'users.id', '=', 'teachers.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->where('verified', '2')
                ->select('users.deleted_at as deleted_at', 'users.id as user_id', 'teachers.first_name as first_name', 'teachers.middle_name as middle_name', 'teachers.last_name as last_name', 'images.file_name as file_name', 'teachers.gender as gender')
                ->orderBy('first_name')
                ->get()
            )
            ->with('guests', User::withTrashed()->join('guests', 'users.id', '=', 'guests.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->where('verified', '2')
                ->select('users.deleted_at as deleted_at', 'users.id as user_id', 'guests.first_name as first_name', 'guests.middle_name as middle_name', 'guests.last_name as last_name', 'images.file_name as file_name', 'guests.gender as gender')
                ->orderBy('first_name')
                ->get()
            )
            ->with('userLists', $userLists);
    }

    public function addTeacherForm()
    {
        return View::make('mis.manage-user.add-teacher', ['title' => 'Add Teacher'])
            ->with('faculties', faculty::all());
    }

    public function addTeacher(TeacherRegistrationFormRequest $request)
    {
        $user = User::create([
            'name' => $request->input('username'),
            'email' => ($request->input('email') == "") ? null : $request->input('email'),
            'user_type_id' => '3',
            'password' => bcrypt($request->input('password')),
            'active' => '1',
            'verified' => '2'
        ]);

        Teacher::create(array(
            'id' => User::where('name', $request->input('username'))->pluck('id')->first(),
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'parent_name' => $request->input('parent_name'),
            'joining_date' => date_format(date_create($request->input('joining_date')), 'Y-m-d'),
            'leaving_date' => date_format(date_create($request->input('leaving_date')), 'Y-m-d'),
            'gender' => $request->input('gender'),
            'permanent_address' => $request->input('permanent_address'),
            'current_address' => $request->input('current_address'),
            'mobile_number' => $request->input('mobile_number'),
            'phone_number' => $request->input('phone_number'),
            $request->input('teacher_post') ? serialize($request->input('teacher_post')) : null
        ));

        if ($request->input('department'))
            foreach ($request->input('department') as $department) {
                FacultyTeacher::create([
                    "teacher_id" => $user->id,
                    "faculty_id" => $department
                ]);
            }


        /*
        **  Resize image to 720 * x
        */
        $filename = uniqid() . "default.jpg";
        Image::make('img/male.jpg')->resize(256, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('images/' . $filename, 100);
        /*
        **  Thumbs creation
        */

        if (!file_exists('images/thumbs')) {
            mkdir('images/thumbs', 0777, true);
        }

        $thumb = Image::make('images/' . $filename)->resize(128, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('images/thumbs/' . $filename, 50);
        /*
        **  Thumbnail Creation
        */

        if (!file_exists('images/thumbnails')) {
            mkdir('images/thumbnails', 0777, true);
        }

        $thumb = Image::make('images/' . $filename)->resize(64, 64)->save('images/thumbnails/' . $filename, 50);
        /*
        **  End of image saving procedure
        */


        $image = $user->images()->create([
            'user_id' => Auth::user()->id,
            'file_name' => $filename,
            'file_size' => '',
            'file_mime' => '',
            'file_path' => 'images/' . $filename
        ]);

        User::find($user->id)
            ->update(array(
                'profile_pic' => $image->id,
            ));


        return redirect()->back();
    }

    public function addStudentForm()
    {
        return View::make('mis.manage-user.add-student', ['title' => 'Add Student'])
            ->with('faculties', Faculty::all());
    }

    public function addStudent(StudentRegistrationFormRequest $request)
    {
        $faculty = faculty::where('name', $request->input('faculty_id'))->first();
        $user = User::create([
            'name' => $request->input('username'),
            'email' => ($request->input('email') == "") ? null : $request->input('email'),
            'user_type_id' => '4',
            'password' => bcrypt($request->input('username')),
            'active' => '1',
            'verified' => '2'
        ]);

        Student::create(array(
            'id' => User::where('name', $request->input('username'))->pluck('id')->first(),
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'parent_name' => $request->input('parent_name'),
            'date_of_birth' => date_format(date_create($request->input('date_of_birth')), 'Y-m-d'),
            'gender' => $request->input('gender'),
            'permanent_address' => $request->input('permanent_address'),
            'current_address' => $request->input('current_address'),
            'symbol_number' => $request->input('symbol_number'),
            'registration_number' => $request->input('registration_number'),
            'batch' => $request->input('batch'),
            'faculty_id' => $faculty->id,
            'mobile_number' => $request->input('mobile_number'),
            'phone_number' => $request->input('phone_number'),
        ));

        /*
        **  Resize image to 720 * x
        */
        $filename = uniqid() . "default.jpg";
        Image::make('img/male.jpg')->resize(256, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('images/' . $filename, 100);
        /*
        **  Thumbs creation
        */

        if (!file_exists('images/thumbs')) {
            mkdir('images/thumbs', 0777, true);
        }

        $thumb = Image::make('images/' . $filename)->resize(128, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('images/thumbs/' . $filename, 50);
        /*
        **  Thumbnail Creation
        */

        if (!file_exists('images/thumbnails')) {
            mkdir('images/thumbnails', 0777, true);
        }

        $thumb = Image::make('images/' . $filename)->resize(64, 64)->save('images/thumbnails/' . $filename, 50);
        /*
        **  End of image saving procedure
        */


        $image = $user->images()->create([
            'user_id' => Auth::user()->id,
            'file_name' => $filename,
            'file_size' => '',
            'file_mime' => '',
            'file_path' => 'images/' . $filename
        ]);

        User::find($user->id)
            ->update(array(
                'profile_pic' => $image->id,
            ));


        return redirect()->back();
    }


    public function addGuestForm()
    {
        return View::make('mis.manage-user.add-guest', ['title' => 'Add Guest'])
            ->with('faculties', Faculty::all());
    }

    public function addGuest(GuestRegistrationFormRequest $request)
    {
        $user = User::create([
            'name' => $request->input('first_name'),
            'user_type_id' => '5',
            'active' => '1',
            'verified' => '2',
            'password' => bcrypt('password'),
        ]);

        Guest::create(array(
            'id' => $user->id,
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'gender' => $request->input('gender'),
            'permanent_address' => $request->input('permanent_address'),
            'current_address' => $request->input('current_address'),
            'mobile_number' => $request->input('mobile_number'),
            'phone_number' => $request->input('phone_number'),
            'description' => $request->input('description'),
        ));

        /*
        **  Resize image to 720 * x
        */
        $filename = uniqid() . "default.jpg";
        Image::make('img/male.jpg')->resize(256, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('images/' . $filename, 100);
        /*
        **  Thumbs creation
        */

        if (!file_exists('images/thumbs')) {
            mkdir('images/thumbs', 0777, true);
        }

        $thumb = Image::make('images/' . $filename)->resize(128, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('images/thumbs/' . $filename, 50);
        /*
        **  Thumbnail Creation
        */

        if (!file_exists('images/thumbnails')) {
            mkdir('images/thumbnails', 0777, true);
        }

        $thumb = Image::make('images/' . $filename)->resize(64, 64)->save('images/thumbnails/' . $filename, 50);
        /*
        **  End of image saving procedure
        */


        $image = $user->images()->create([
            'user_id' => Auth::user()->id,
            'file_name' => $filename,
            'file_size' => '',
            'file_mime' => '',
            'file_path' => 'images/' . $filename
        ]);

        User::find($user->id)
            ->update(array(
                'profile_pic' => $image->id,
            ));


        return redirect()->back();
    }

    public function loginOtherUser($id)
    {
        $type = User::find($id);
        if ($type->user_type_id == '3') {
            $loginOtherUser = 'http://teacher.hcc.edu.np/auth-from-admin/api_token' . $type->api_token . "/" . $type->id . '/remember_token' . $type->remember_token;

        } else if ($type->user_type_id == '4') {
            $loginOtherUser = 'http://student.hcc.edu.np/auth-from-admin/api_token' . $type->api_token . "/" . $type->id . '/remember_token' . $type->remember_token;
        }


        $userList = array();
        $userLists = array();
        $batch = array();

        foreach ($facultylink = Faculty::all() as $faculty) {
            $userList['faculty'] = $faculty;
            $userList['batch'] = array();
            $batchs = Student::where('faculty_id', $faculty->id)
                ->select('batch')
                ->groupBy('batch')
                ->get()
                ->toArray();
            $userList['batch-list'] = $batchs;

            foreach ($batchs as $batch) {
                $batch['name'] = $batch['batch'];

                $batch['students'] = User::join('students', 'users.id', '=', 'students.id')
                    ->join('images', 'users.profile_pic', '=', 'images.id')
                    ->where('faculty_id', $faculty->id)
                    ->where('batch', $batch['batch'])
                    ->where('verified', '2')
                    ->select('users.id as user_id', 'students.first_name as first_name', 'students.middle_name as middle_name', 'students.last_name as last_name', 'images.file_name as file_name', 'students.gender as gender')
                    ->orderBy('first_name')
                    ->get();

                array_push($userList['batch'], $batch);

            }
            array_push($userLists, $userList);
        }

        return View::make('mis.manage-user.other-users', ['title' => 'Other-Users'])
            ->with('teachers', User::join('teachers', 'users.id', '=', 'teachers.id')
                ->join('images', 'users.profile_pic', '=', 'images.id')
                ->where('verified', '2')
                ->select('users.id as user_id', 'teachers.first_name as first_name', 'teachers.middle_name as middle_name', 'teachers.last_name as last_name', 'images.file_name as file_name', 'teachers.gender as gender')
                ->orderBy('first_name')
                ->get()
            )
            ->with('userLists', $userLists)
            ->with('loginOtherUser', $loginOtherUser);

    }

    public function updateUserProfile($id)
    {
        $type = User::find($id);
        if ($type->user_type_id == '3') {

            return View::make('mis.manage-user.update-teacher', ['title' => 'Teacher update'])
                ->with('user', User::find($id))
                ->with('teacher', Teacher::find($id))
                ->with('faculties', faculty::all());
        } else if ($type->user_type_id == '4') {

            return View::make('mis.manage-user.update-student', ['title' => 'Student update'])
                ->with('user', User::find($id))
                ->with('student', student::find($id))
                ->with('faculties', Faculty::all());

        } else if ($type->user_type_id == '5') {

            return View::make('mis.manage-user.update-guest', ['title' => 'Guest update'])
                ->with('user', User::find($id))
                ->with('guest', Guest::find($id))
                ->with('faculties', Faculty::all());

        }
    }

    public function updateUserProfilePic(Request $request)
    {

        $file = $request->file('file');
        $filename = uniqid() . $file->getClientOriginalName();
        if (!file_exists('images')) {
            mkdir('images', 0777, true);
        }
        $file->move('images', $filename);
        /*
        **  Resize image to 720 * x
        */
        Image::make('images/' . $filename)->resize(720, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('images/' . $filename, 100);
        /*
        **  Thumbs creation
        */

        if (!file_exists('images/thumbs')) {
            mkdir('images/thumbs', 0777, true);
        }

        $thumb = Image::make('images/' . $filename)->resize(256, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('images/thumbs/' . $filename, 50);
        /*
        **  Thumbnail Creation
        */

        if (!file_exists('images/thumbnails')) {
            mkdir('images/thumbnails', 0777, true);
        }

        $thumb = Image::make('images/' . $filename)->resize(64, 64)->save('images/thumbnails/' . $filename, 50);
        /*
        **  End of image saving procedure
        */


        $user = User::find($request->input('id'));

        $trashImage = \HCCNetwork\image::where('id', $user->profile_pic)->first();
        $imagename = $trashImage->file_name;
        $thumb = 'images/thumbs/' . $imagename;
        $thumbnail = 'images/thumbnails/' . $imagename;
        $imagename = 'images/' . $imagename;

        // delete images prfile pic and thumbnail
        if (file_exists($thumbnail))
            unlink($thumbnail);
        if (file_exists($thumb))
            unlink($thumb);
        if (file_exists($imagename))
            unlink($imagename);


        \HCCNetwork\image::where('id', $user->profile_pic)->update([
            'file_name' => $filename,
            'file_size' => $file->getClientSize(),
            'file_mime' => $file->getClientMimeType(),
            'file_path' => 'images/' . $filename
        ]);

        return 'ad';
    }

    public function updateTeacher(TeacherUpdateRequest $request)
    {

        $user = User::where('id', $request->input('id'))->update([
            'name' => $request->input('username'),
            'email' => ($request->input('email') == "") ? null : $request->input('email'),
        ]);

        Teacher::where('id', $request->input('id'))->update(array(
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'parent_name' => $request->input('parent_name'),
            'joining_date' => date_format(date_create($request->input('joining_date')), 'Y-m-d'),
            'leaving_date' => date_format(date_create($request->input('leaving_date')), 'Y-m-d'),
            'gender' => $request->input('gender'),
            'permanent_address' => $request->input('permanent_address'),
            'current_address' => $request->input('current_address'),
            'mobile_number' => $request->input('mobile_number'),
            'phone_number' => $request->input('phone_number') ? $request->input('phone_number') : null,
            'level' => $request->input('teacher_post') ? serialize($request->input('teacher_post')) : null,
        ));

        FacultyTeacher::where('teacher_id', $request->input('id'))->delete();
        if ($request->input('department'))
            foreach ($request->input('department') as $department) {
                FacultyTeacher::create([
                    "teacher_id" => $request->input('id'),
                    "faculty_id" => $department
                ]);
            }

        return redirect()->back();

    }

    public function updateStudent(StudentUpdateRequest $request)
    {

        $faculty = faculty::where('name', $request->input('faculty_id'))->first();
        $user = User::where('id', $request->input('id'))->update([
            'name' => $request->input('username'),
            'email' => ($request->input('email') == "") ? null : $request->input('email'),
        ]);
        Student::where('id', $request->input('id'))->update(array(
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'parent_name' => $request->input('parent_name'),
            'date_of_birth' => date_format(date_create($request->input('date_of_birth')), 'Y-m-d'),
            'gender' => $request->input('gender'),
            'religion' => $request->input('religion'),

            'symbol_number' => $request->input('symbol_number'),
            'registration_number' => $request->input('registration_number'),
            'batch' => $request->input('batch'),
            'faculty_id' => $faculty->id,

            'permanent_address' => $request->input('permanent_address'),
            'current_address' => $request->input('current_address'),
            'mobile_number' => $request->input('mobile_number'),
            'phone_number' => $request->input('phone_number'),
        ));
        return redirect()->back();

    }

    public function updateGuest(GuestUpdateRequest $request)
    {
        Guest::where('id', $request->input('id'))->update(array(
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'gender' => $request->input('gender'),
            'permanent_address' => $request->input('permanent_address'),
            'current_address' => $request->input('current_address'),
            'mobile_number' => $request->input('mobile_number'),
            'phone_number' => $request->input('phone_number'),
            'description' => $request->input('description'),
        ));
        return redirect()->back();
    }


    public function passwordReset(PasswordResetRequest $request)
    {
        User::find($request->input('id'))->update(['password' => bcrypt($request->input('password'))]);
        return redirect()->back();
    }

    public function restoreUser($id)
    {
        $user = User::onlyTrashed()->find($id);

        if ($user) {
            if ($user->user_type_id == 3) {
                $teacher = Teacher::onlyTrashed()->find($user->id);
                if ($teacher)
                    $teacher->restore();
            }
            if ($user->user_type_id == 4) {
                $student = student::onlyTrashed()->find($user->id);
                if ($student)
                    $student->restore();
            }
            $user->restore();
        }

        return redirect()->back();
    }

    public function deleteUser(Request $request, $id)
    {
        $user = User::find($id);

        if ($user) {
            if ($user->user_type_id == 3) {
                $teacher = Teacher::find($user->id);
                if ($teacher)
                    $teacher->delete();
            }
            if ($user->user_type_id == 4) {
                $student = student::find($user->id);
                if ($student)
                    $student->delete();
            }
            $user->delete();
        }

        return redirect()->back();
    }

    public function downloadTeacherLists()
    {
//        return Teacher::join('users', 'teachers.id', 'users.id')
//            ->select('users.id as ID ,users . email as EMAIL')
//            ->addSelect(DB::raw("concat('teachers.first_name,' ',teachers.middle_name,' ',teachers.last_name') as NAME"))
//            ->addSelect(
//                'teachers . permanent_address as PERMANENT - ADDRESS',
//                'teachers . current_address as CURRENT - ADDRESS',
//                'teachers . gender as GENDER',
//                'teachers . joining_date as JOINING - DATE',
//                'teachers . leaving_date as LEAVING - DATE',
//                'teachers . mobile_number as MOBILE - NUMBER',
//                'teachers . phone_number as PHONE - NUMBER'
//            )
//            ->toSql();


        Excel::create('teacher_lists', function ($excel) {
            $excel->sheet('teacher_lists', function ($sheet) {
                $sheet->fromModel(
                    Teacher::join('users', 'teachers.id', 'users.id')
                        ->addSelect(
//                            'users.id as ID',
                            DB::raw("concat(teachers.first_name,' ',teachers.middle_name,' ',teachers.last_name) as NAME"),
                            'users.email as EMAIL',
                            'teachers.permanent_address as PERMANENT - ADDRESS',
                            'teachers.current_address as CURRENT - ADDRESS',
                            DB::raw("CASE WHEN teachers.gender = 0 THEN 'FEMALE' WHEN teachers.gender = 1 THEN 'MALE' ELSE 'OTHER' END as GENDER"),
                            'teachers.joining_date as JOINING - DATE',
                            'teachers.leaving_date as LEAVING - DATE',
                            'teachers.mobile_number as MOBILE - NUMBER',
                            'teachers.phone_number as PHONE - NUMBER'
                        )
                        ->get()
                );
            });
        })
            ->export('xlsx');
//
// or
//            ->store('xlsx');
        return redirect()->back();
    }

    public function downloadStudentLists(Request $request)
    {
        $faculty = faculty::find($request->input('faculty_id'));
        Excel::create('student_lists-' . $faculty->name . '-' . $request->input('batch_name'), function ($excel) use ($request) {
            $excel->sheet('student_lists', function ($sheet) use ($request) {
                $sheet->fromModel(
                    student::join('users', 'students.id', 'users.id')
                        ->addSelect(
//                            'users.id as ID',
                            DB::raw("concat(students.first_name,' ',students.middle_name,' ',students.last_name) as NAME"),
                            'users.email as EMAIL',
                            'students.permanent_address as PERMANENT - ADDRESS',
                            'students.current_address as CURRENT - ADDRESS',
                            DB::raw("CASE WHEN students.gender = 0 THEN 'FEMALE' WHEN students.gender = 1 THEN 'MALE' ELSE 'OTHER' END as GENDER"),
                            'students.date_of_birth as DOB',
                            'students.mobile_number as MOBILE - NUMBER',
                            'students.phone_number as PHONE - NUMBER'
                        )
                        ->where('students.batch', $request->input('batch_name'))
                        ->where('students.faculty_id', $request->input('faculty_id'))
                        ->orderBy('students.first_name')
                        ->get()
                );
            });
        })->export('xlsx');

// or
//            ->store('xlsx');
        return redirect()->back();
    }
}