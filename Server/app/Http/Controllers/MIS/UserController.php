<?php

namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\Http\Controllers\Controller;

use HCCNetwork\faculty;
use View;
/*
**      Events
*/
use Event;
use HCCNetwork\Events\student\StudentVerified;
use HCCNetwork\Events\student\StudentRejected;
use HCCNetwork\Events\student\StudentFormRejected;
use HCCNetwork\Events\student\StudentFormVerified;
/**/
use HCCNetwork\Events\teacher\TeacherVerified;
use HCCNetwork\Events\teacher\TeacherRejected;
use HCCNetwork\Events\teacher\TeacherFormRejected;
use HCCNetwork\Events\teacher\TeacherFormVerified;

/**/
use HCCNetwork\Http\Requests;
use HCCNetwork\student; 
use HCCNetwork\Teacher; 
use HCCNetwork\book;
use HCCNetwork\User;
use HCCNetwork\userrequest;
use HCCNetwork\image;
use HCCNetwork\BookIssue;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');   
    }

    public function userRequest(){
        return View::make('mis.pendinguser.userrequest')
                    ->with('title','User Request')
                    ->with('userrequests',userrequest::all())
                    ->with('emailVerifiedUsers',User::where('verified','=','1')->get())
                    ;
    }

    public function pendingStudentForm(){
        return View::make('mis.pendinguser.pendingstudent')
                    ->with('title','Pending Student Form')
                    ->with('pendingStudentForms',User::join('students','users.id','=','students.id')
                                                                    ->join('images','users.profile_pic','=','images.id')
                                                                    ->join('faculties','students.faculty_id','=','faculties.id')
                                                                    ->where('verified',1)
                                                                    ->where('active',1)
                                                                    ->select('students.*','users.email','images.file_path as image','faculties.name as faculty')
                                                                    ->get());
    }

    public function pendingTeacherForm(){
        return View::make('mis.pendinguser.pendingteacher')
                    ->with('title','Pending Teacher Form')
                    ->with('pendingTeacherForms',User::join('teachers','users.id','=','teachers.id')
                                                ->join('images','users.profile_pic','=','images.id')    
                                                ->where('verified',1)
                                                ->where('active',1)
                                                ->select('teachers.*','users.email','images.file_path as image')
                                                ->get()
                            );
    }

    public function acceptPendingStudentForm(Request $request){

        $user=User::find($request->input('id'));
        User::find($request->input('id'))->update(['verified'=>'2']);
        Event::fire(new StudentFormVerified($user));
        
        return redirect()->back();
    }
    
     public function deletePendingStudentForm(Request $request){      

        $image=User::join('images','users.profile_pic','=','images.id')->select('images.file_name')->find($request->input('id'));
        $imagename=$image['file_name'];
        $thumbnail='images/thumbs/'.$imagename;
        $thumbnail='images/thumbnails/'.$imagename;
        $imagename='images/'.$imagename;

        // delete images prfile pic and thumbnail
        unlink($thumbnail);
        unlink($imagename);
        image::where('user_id',$request->input('id'))->delete(); 
        //delete student form
        student::find($request->input('id'))->delete();        
        // delete  student form 
        User::find($request->input('id'))->update(['active'=>'0','profile_pic'=>null]);
        $user=User::find($request->input('id'));
        Event::fire(new StudentFormRejected($user));        
        return redirect()->back();
    }

    public function acceptPendingTeacherForm(Request $request){
        $user=User::find($request->input('id'));
        User::find($request->input('id'))->update(['verified'=>'2']);
        Event::fire(new TeacherFormVerified($user));
        
        return redirect()->back();
    }

    public function deleteUnverifiedEmail(Request $request){
        userrequest::find($request->input('id'))->delete();
        return redirect()->back();
    }
    
     public function deletePendingTeacherForm(Request $request){      

        $image=User::join('images','users.profile_pic','=','images.id')->select('images.file_name')->find($request->input('id'));
        $imagename=$image['file_name'];
        $thumbnail='images/thumbs/'.$imagename;
        $thumbnail='images/thumbnails/'.$imagename;
        $imagename='images/'.$imagename;

        // delete images prfile pic and thumbnail
        unlink($thumbnail);
        unlink($imagename);
        image::where('user_id',$request->input('id'))->delete(); 
        //delete student form
        Teacher::find($request->input('id'))->delete();        
        // delete  student form 
        User::find($request->input('id'))->update(['active'=>'0','profile_pic'=>null]);
        $user=User::find($request->input('id'));
        Event::fire(new TeacherFormRejected($user));        
        return redirect()->back();
    }


}
