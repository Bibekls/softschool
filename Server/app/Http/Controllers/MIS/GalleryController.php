<?php
namespace HCCNetwork\Http\Controllers\MIS;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Gallery;
use HCCNetwork\image;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');      
    }  
    public function index()
    {     
       return View::make('mis.gallery.gallery')
        ->with('title','Gallery')
        ->with('galleries',Gallery::all());
    }

    public function show($id)
    {
        return View::make('mis.gallery.image')
        ->with('title','Image')
        ->with('images',image::where('gallery_id',$id)->get())
        ->with('gallery',Gallery::find($id));
    }

    public function destroy(Request $request){ 
        $deleteitem=Gallery::where('title',$request->input('category'))->first();       
        if($request->input('category')!="Default")Gallery::where('title',$deleteitem->title)->delete();
        foreach($images=image::where('gallery_id',$deleteitem->id)->get() as $image){
            $image->delete();
        }
        return redirect()->back();
    }

    public function store(Request $request){       
        Gallery::create(array(
            'title' => $request->input('name')
        ));
        return redirect()->back();
    }

    public function update(Request $request, $id){
      
        Gallery::where('id',$id)->update(array(            
            'name' => $request->input('name')
        ));
        return redirect()->back();
    }
    
}