<?php
namespace HCCNetwork\Http\Controllers\MIS;

use HCCNetwork\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\Post;
use HCCNetwork\Comment;
use HCCNetwork\book;
use HCCNetwork\Url;
use HCCNetwork\Feedback;

use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function dashboard()
    {
        return View::make('mis.dashboard')
            ->with('title', 'Dashboard')
            ->with('books', book::count())
            ->with('bookAvailable', '')
            ->with('publications', '')
            ->with('price', '')
            ->with('profile_pic', image::find(Auth::user()->profile_pic))
            ->with('totalPost', '')
            ->with('totalComment', '')
            ->with('totalImage', image::all()->count())
            ->with('totalUrl', '')
            ->with('totalFeedback', Feedback::all()->count());
    }

    public function changePassword(Request $request)
    {
        if ($request->input('newPassword') === $request->input('confirmPassword'))
            if (Hash::check($request->input('oldPassword'), Auth::user()->password)) {
                Auth::user()->update([
                    'password' => bcrypt($request['newPassword'])
                ]);
            return redirect('/logout');
        }

        return redirect()->back();
    }
}
