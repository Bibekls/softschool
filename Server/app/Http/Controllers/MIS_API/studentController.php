<?php

namespace HCCNetwork\Http\Controllers\MIS_API;

use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\User;
use HCCNetwork\student;
use HCCNetwork\GrandParent;
use HCCNetwork\studentRecord;
use DB;
class studentController extends Controller
{
    public function postStudent(Request $request){
        if($request['parentCheck'] == 1){
            $parentusers=User::create([
              'name'=>$request['parentFirstName'],
              'email'=>$request['parentEmail'],
              'user_type_id' => '6',
              'active' => '1',
              'verified' => '2'
            ]);
            $id=$parentusers->id;
          $grandParents=GrandParent::create([
              'id'=>$parentusers->id,
              'firstName'=>$request['parentFirstName'],
              'middleName'=>$request['parentMiddleName'],
              'lastName'=>$request['parentLastName'],
              'mobileNumber'=>$request['parentMobile'],
              'email'=>$request['parentEmail'],
            ]); 
             $user = User::create([
                'name' => $request['userName'],
                'email' => $request['email'],
                'user_type_id' => '4',
                'password' => bcrypt($request['userName']),
                'active' => '1',
                'verified' => '2'
            ]);
          
            $students=Student::create([
                'id' => $user->id,
                'parent_id'=>$grandParents->id,
                'first_name'=> $request['firstName'],
                'middle_name'=> $request['middleName'],
                'last_name'=> $request['lastName'],
                'parent_name'=> $request['parentName'],
                'join_date'=> $request['joinDate'],
                'permanent_address'=> $request['parmanentAddress'],
                'current_address'=> $request['currentAddress'],
                'gender'=> $request['gender'],
                'image'=> $request['image'],
                'batch'=> $request['batch'],
                'faculty_id'=>$request['department'],
                'symbol_number'=> $request['symbolNumber'],
                'registration_number'=> $request['registrationNumber'],
                'mobile_number'=> $request['mobileNumber'],
                'phone_number'=> $request['phoneNumber'],
            ]);
            if($request['batch'] == null){
                $student=DB::table('studentRecord')->insert([
                    'year'=>$request['year'],
                    'class'=>$request['class'],
                    'roll_no'=>$request['symbolNumber'],
                    'faculty_id'=>$request['department'],
                    'student_id'=> $user->id,
                ]);
            }
        return response()->json(['student'=>$students]);
          }else{
            $user = User::create([
                'name' => $request['userName'],
                'email' => $request['email'],
                'user_type_id' => '4',
                'password' => bcrypt($request['userName']),
                'active' => '1',
                'verified' => '2'
            ]);
            $students=Student::create([
                'id' => $user->id,
                'parent_id'=>$request['parent_id'],
                'first_name'=> $request['firstName'],
                'middle_name'=> $request['middleName'],
                'last_name'=> $request['lastName'],
                'parent_name'=> $request['parentName'],
                'join_date'=> $request['joinDate'],
                'permanent_address'=> $request['parmanentAddress'],
                'current_address'=> $request['currentAddress'],
                'gender'=> $request['gender'],
                'image'=> $request['image'],
                'batch'=> $request['batch'],
                'faculty_id'=>$request['department'],
                'symbol_number'=> $request['symbolNumber'],
                'registration_number'=> $request['registrationNumber'],
                'mobile_number'=> $request['mobileNumber'],
                'phone_number'=> $request['phoneNumber'],
            ]);
            if($request['batch'] ==null){
                $students=DB::table('studentRecord')->insert([
                    'year'=>$request['year'],
                    'roll_no'=>$request['symbolNumber'],
                    'faculty_id'=>$request['department'],           
                    'class'=>$request['class'],
                    'student_id'=>$user->id,
                ]);
            }
    return response()->json(['student'=>$students]);
        }
    }
    public function getParent(){
        $grandParent=GrandParent::get();
        return response()->json(['parent'=>$grandParent]);
    }
    public function postEmail( $id){
       $users=User::where('email', 'like', '%' . $id . '%')->get();
       return response()->json(['users'=>$users]);
    }
 }
