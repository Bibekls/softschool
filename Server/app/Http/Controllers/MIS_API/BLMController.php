<?php

namespace HCCNetwork\Http\Controllers\MIS_API;

use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\student;
use HCCNetwork\studentRecord; 

use HCCNetwork\faculty; 

use DB;
use HCCNetwork\BatchLevelMaping;
class BLMController extends Controller
 {

  public function getmap(){
    $blm=array();
    $facultyarray=array();
    $batcharray=array();

    $faculties=faculty::all();
    $blm['faculties']=array();
    foreach($faculties as $faculty){
        
        $facultyarray['faculty']=$faculty->name;
        $facultyarray['id']=$faculty->id;
        $facultyarray['batches']=array();            

        $batch=student::where('faculty_id',$faculty->id)
                       ->whereNotNull('batch')
                        ->select('batch')
                        ->groupBy('batch')
                        ->orderBy('batch','desc')
                        ->get();
                  
        foreach($batch as $value)
        {               

            if(BatchLevelMaping::where('faculty_id',$faculty->id)
                            ->where('batch',$value->batch)
                            ->first() ==null){
                BatchLevelMaping::create(array(
                                        'faculty_id' => $faculty->id,
                                        'batch'=>$value->batch
                                    ));
            }
            $batcharray['batch']=$value->batch;
            $batcharray['level']=BatchLevelMaping::where('batch',$value->batch)
                                            ->where('faculty_id',$faculty->id)
                                            ->pluck('level')
                                            ->first();
            array_push($facultyarray['batches'], $batcharray);
        }
        array_push($blm['faculties'], $facultyarray);
        
    }       

    // return View::make('mis.batch-level-maping')
    //     ->with('title','Batch Level Mapping')
    //     ->with('blmData',$blm);   
    return response()->json([$blm]);     
}


   public function getYear(){
     $year=DB::table('studentRecord')->groupBy('year')->get();
     return response()->json(['year'=>$year]);
   }
    public function getStudent(Request $request){
    $students=student::join('faculties','faculties.id','=','students.faculty_id')
    ->join('studentRecord','faculties.id','=','studentRecord.faculty_id')
    ->where('studentRecord.year',$request['batch'])
    ->where('studentRecord.faculty_id',$request['faculty'])
    ->where('studentRecord.class',$request['level'])
    ->groupBy('studentRecord.roll_no')
    ->select('students.*','students.id as student_id')
    ->get();
      return response()->json(['students'=>$students]);
    }
   public function promotedStudent(Request $request){
    $students = $request->input('students');   
     for($i = 0 ;$i < count($students);$i++){
       $studentdata=DB::table('studentRecord')->insert([
          'year'=>$request['promateYear'],
          'class'=>$request['promateTo'],
          'roll_no'=>$students[$i]['roll_no'],
          'student_id'=> $students[$i]['id'],
          'faculty_id'=>  $students[$i]['faculty_id']
         ]);
     }
     return response()->json(['students'=>$studentdata]);
   }


   public function getBLM(){
     $blm=BatchLevelMaping::join('faculties','faculties.id','=','batch_level_maping.faculty_id')->orderBy('batch_level_maping.faculty_id')->get();
     return response()->json(['blm'=>$blm]);
   }


   public function batchLevel(Request $request){
   $update=BatchLevelMaping::where('faculty_id',$request['faculty_id'])
                                -> where('batch',$request['batch'])
                                ->update([
                                     'level'=>$request['level'],
                                  ]);
     return response()->json([$update]);
   }    
}
