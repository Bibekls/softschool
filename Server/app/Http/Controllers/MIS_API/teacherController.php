<?php

namespace HCCNetwork\Http\Controllers\MIS_API;

use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\User;
use HCCNetwork\Teacher;
use HCCNetwork\GrandParent;
use HCCNetwork\faculty;
use HCCNetwork\FacultyTeacher;
class teacherController extends Controller
{
public function postTeacher(Request $request){
    $users=User::create([
        'name'=>$request['username'],
        'email'=>$request['email'],
        'user_type_id' => '3',
        'password' => bcrypt($request->input('password')),
        'active' => '1',
        'verified' => '2'
       ]);
    $teacher=Teacher::create([
        'id'=> User::where('name', $request->input('username'))->pluck('id')->first(),
        'first_name'=>$request['firstName'],
        'middle_name'=>$request['middleName'],
        'last_name'=>$request['lastName'],
        'parent_name'=>$request['parentName'],
        'permanent_address'=>$request['parmanentAddress'],
        'current_address'=>$request['currentAddress'],
        'gender'=>$request['gender'],
        'post'=>json_encode($request['posts']),
        'department'=>json_encode($request['departments']),
        'image'=>$request['image'],
        'leaving_date'=>$request['leaveDate'],
        'joining_date'=>$request['joinDate'],
        'mobile_number'=>$request['mobileNumber'],
        'phone_number'=>$request['phoneNumber']  
   ]);
   $faculties=Faculty::get();
   foreach($teacher->department as $dpt){
     foreach($faculties as $faculty){
       if($faculty->name == $dpt){
        FacultyTeacher::create([
                   "teacher_id" => $users->id,
                   "faculty_id" => $faculty->id
               ]);
          }
       }
   }
  //  if ($request->input('department'))
  //  foreach ($request->input('department') as $department) {
  //      FacultyTeacher::create([
  //          "teacher_id" => $user->id,
  //          "faculty_id" => $department
  //      ]);
  //  }
   return response()->json(['response'=>$teacher]);
  }
  
  public function getTeacher(){
    $teachers=Teacher::get();
    return response()->json(['teachers'=>$teachers]);
  }
  public function getFaculties(){
   $faculties=Faculty::get();
   return response()->json(['faculties'=>$faculties]);
  }
}
