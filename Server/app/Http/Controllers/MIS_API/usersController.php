<?php

namespace HCCNetwork\Http\Controllers\MIS_API;

use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\Post;
use HCCNetwork\url;
use HCCNetwork\Teacher;
use HCCNetwork\student;
use HCCNetwork\faculty;
use HCCNetwork\Feedback;
use HCCNetwork\GrandParent;
use Carbon;
use HCCNetwork\Guest;
// use DB;

class usersController extends Controller
{
    
    public function users(){
        $userList = array();
        $userLists = array();
        $batch = array();

        foreach ($facultylink = Faculty::all() as $faculty) {
            if($faculty->name != 'School'){
            $userList['faculty'] = $faculty;
            $userList['batch'] = array();
            $batchs = student::withTrashed()->where('faculty_id', $faculty->id)
                ->select('batch')
                ->groupBy('batch')
                ->get();
            $userList['batch_list'] = $batchs;
            foreach ($batchs as $batch) {
                $batch['name'] = $batch['batch'];
                $batch['students'] = User::withTrashed()->join('students', 'users.id', '=', 'students.id')
                    ->where('faculty_id', $faculty->id)
                    ->where('batch', $batch['batch'])
                    ->where('verified', '2')
                    ->select('users.deleted_at as deleted_at','students.image', 'users.id as user_id', 'students.first_name as first_name', 'students.middle_name as middle_name', 'students.last_name as last_name','students.gender as gender')
                    ->orderBy('first_name')
                    ->get();

                array_push($userList['batch'], $batch);
            }
            array_push($userLists, $userList);
          }
        }

        //  return View::make('mis.manage-user.other-users', ['title' => 'Other-Users'])
        //     ->with('loginOtherUser', null)
        //     ->with('teachers', User::withTrashed()->join('teachers', 'users.id', '=', 'teachers.id')
        //         // ->join('images', 'users.profile_pic', '=', 'images.id')
        //         ->where('verified', '2')
        //         ->select('users.deleted_at as deleted_at', 'users.id as user_id', 'teachers.first_name as first_name', 'teachers.middle_name as middle_name', 'teachers.last_name as last_name', 'teachers.gender as gender')
        //         ->orderBy('first_name')
        //         ->get()
        //     )
        //     ->with('guests', User::withTrashed()->join('guests', 'users.id', '=', 'guests.id')
        //         // ->join('images', 'users.profile_pic', '=', 'images.id')
        //         ->where('verified', '2')
        //         ->select('users.deleted_at as deleted_at', 'users.id as user_id', 'guests.first_name as first_name', 'guests.middle_name as middle_name', 'guests.last_name as last_name','guests.gender as gender')
        //         ->orderBy('first_name')
        //         ->get()
        //     )
        //     ->with('userLists', $userLists);
        return response()->json(['response'=>$userLists]);
    }
    public function getTeacher(){
        $teachers=User::withTrashed()->join('teachers', 'users.id', '=', 'teachers.id')
                ->where('verified', '2')
                ->select('users.deleted_at as deleted_at', 'users.id as user_id', 'teachers.first_name as first_name','teachers.image','teachers.middle_name as middle_name', 'teachers.last_name as last_name', 'teachers.gender as gender')
                ->orderBy('first_name')
                ->get();
       return response()->json(['teachers'=>$teachers]);
    }
    public function getGuest(){
    $guests=User::withTrashed()->join('guests', 'users.id', '=', 'guests.id')
                ->where('verified', '2')
                ->select('users.deleted_at as deleted_at','guests.image' ,'users.id as user_id', 'guests.first_name as first_name', 'guests.middle_name as middle_name', 'guests.last_name as last_name','guests.gender as gender')
                ->orderBy('first_name')
                ->get();
        return response()->json(['guests'=>$guests]);

    } 
    public function getSchoolStudent(){
        $schoolList = array();
        $schoolLists = array();
        $facultylink = Faculty::where('name','School')->get();
        // $year=DB::table('studentRecord')->max('year');
        $classes=DB::table('studentRecord')->groupBy('class')->select('class')->get()->toArray();
        $schoolList['students'] = array();
        foreach($classes as $class){
            $schoolList['class']=$class->class;
            $student=DB::table('studentRecord')->join('students','studentRecord.student_id','=','students.id')
                       ->where('year',DB::table('studentRecord')->max('year'))
                       ->where('class',$class->class)
                       ->groupBy('class')
                       ->select('class','studentRecord.year','students.id as user_id','studentRecord.id','students.image','students.first_name','students.middle_name','students.last_name')
                       ->get();
             if(count($student) > 0){
                array_push($schoolList['students'], $student);
             }
        }
        array_push($schoolLists,$schoolList);
        return response()->json(['students'=>$schoolLists]);
    }
    public function getStudentForUpdate(Request $request){
         $student=student::join('grand_parents','students.parent_id','=','grand_parents.id')
                        ->join('users','students.id','=','users.id')
                        ->join('faculties','faculties.id','=','students.faculty_id')
                        ->where('students.id',$request['id'])
                        ->select('users.name as userName','users.email as email','faculties.name as facultyname','students.id as student_id','students.*','grand_parents.*','grand_parents.id as grand_id','grand_parents.email as parent_email')
                        ->get();
         $email=User::join('students','students.id','=','users.id')->select('email')->where('students.id',$request['id'])->get();               
         return response()->json(['response'=>$student,'email'=>$email]);
    }
    public function updateStudent(Request $request){

            $parentusers=User::where('id',$request['parent_id'])->update([
              'name'=>$request['parentFirstName'],
              'email'=>$request['parentEmail'],
              'user_type_id' => '6',
              'active' => '1',
              'verified' => '2'
              ]);
            $grandParents=GrandParent::where('id',$request['parent_id'])->update([
              'firstName'=>$request['parentFirstName'],
              'middleName'=>$request['parentMiddleName'],
              'lastName'=>$request['parentLastName'],
              'mobileNumber'=>$request['parentMobile'],
              'email'=>$request['parentEmail'],
              ]); 
            $user = User::where('id',$request['studentId'])->update([
                'name' => $request['userName'],
                'email' => $request['email'],
                'user_type_id' => '4',
                'password' => bcrypt($request['userName']),
                'active' => '1',
                'verified' => '2'
              ]);
            $students=Student::where('id',$request['studentId'])->update([
                'first_name'=> $request['firstName'],
                'middle_name'=> $request['middleName'],
                'last_name'=> $request['lastName'],
                'parent_name'=> $request['parentName'],
                'join_date'=> $request['joinDate'],
                'permanent_address'=> $request['parmanentAddress'],
                'current_address'=> $request['currentAddress'],
                'gender'=> $request['gender'],
                'image'=> $request['image'],
                'batch'=> $request['batch'],
                'faculty_id'=>$request['department'],
                'symbol_number'=> $request['symbolNumber'],
                'registration_number'=> $request['registrationNumber'],
                'mobile_number'=> $request['mobileNumber'],
                'phone_number'=> $request['phoneNumber'],
              ]);
       return response()->json([$students]); 
    }
   public function getSchoolStudentForUpdate($id){
    $student=Student::join('studentRecord','studentRecord.student_id','=','students.id')
                    ->join('grand_parents','students.parent_id','=','grand_parents.id')
                    ->join('users','students.id','=','users.id')
                    ->where('students.id',$id)
                    ->where('year',DB::table('studentRecord')->max('year'))
                    ->groupBy('studentRecord.roll_no')
                    ->select('studentRecord.id as studentRecord_id','users.*','studentRecord.*','students.*','grand_parents.*','grand_parents.id as grand_parent_id','grand_parents.email as parent_email')
                    ->get();
    $email=User::join('students','students.id','=','users.id')->select('users.name','users.email')->where('students.id',$id)->get();               
    return response()->json(['students'=>$student,'email'=>$email]);
   }   


   public function updateSchoolStudent(Request $request){
     
                $parentusers=User::where('id',$request['parent_id'])->update([
                    'name'=>$request['parentFirstName'],
                    'email'=>$request['parentEmail'],
                    'user_type_id' => '6',
                    'active' => '1',
                    'verified' => '2'
                    ]);
                $grandParents=GrandParent::where('id',$request['parent_id'])->update([
                    'firstName'=>$request['parentFirstName'],
                    'middleName'=>$request['parentMiddleName'],
                    'lastName'=>$request['parentLastName'],
                    'mobileNumber'=>$request['parentMobile'],
                    'email'=>$request['parentEmail'],
                    ]); 
                $user = User::where('id',$request['student_id'])->update([
                    'name' => $request['userName'],
                    'email' => $request['email'],
                    'user_type_id' => '4',
                    'password' => bcrypt($request['userName']),
                    'active' => '1',
                    'verified' => '2'
                    ]);
                $students=Student::where('id',$request['student_id'])->update([
                    'first_name'=> $request['firstName'],
                    'middle_name'=> $request['middleName'],
                    'last_name'=> $request['lastName'],
                    'parent_name'=> $request['parentName'],
                    'join_date'=> $request['joinDate'],
                    'permanent_address'=> $request['parmanentAddress'],
                    'current_address'=> $request['currentAddress'],
                    'gender'=> $request['gender'],
                    'image'=> $request['image'],
                    'batch'=> $request['batch'],
                    'faculty_id'=>$request['department'],
                    'symbol_number'=> $request['symbolNumber'],
                    'registration_number'=> $request['registrationNumber'],
                    'mobile_number'=> $request['mobileNumber'],
                    'phone_number'=> $request['phoneNumber'],
                    ]);

                $students=DB::table('studentRecord')->where('id',$request['studentRecord'])->update([
                            'year'=>$request['year'],
                            'class'=>$request['class'],
                        ]);
        return response()->json([$students]); 
   }
   public function getTeacherUpdate($id){
       $teachers=Teacher::join('users','users.id','=','teachers.id')
                         ->where('users.id',$id)
                         ->select('users.id as user_id','users.*','teachers.id as teacher_id','teachers.*')
                         ->get();
        return response()->json(['teachers'=>$teachers]);
   }
   public function updateTeacher(Request $request){

    $users=User::where('id',$request['user_id'])->update([
        'name'=>$request['username'],
        'email'=>$request['email'],
        'user_type_id' => '3',
        'password' => bcrypt($request->input('password')),
        'active' => '1',
        'verified' => '2'
       ]);
       
    $teacher=Teacher::where('id',$request['teacher_id'])->update([
        'first_name'=>$request['firstName'],
        'middle_name'=>$request['middleName'],
        'last_name'=>$request['lastName'],
        'parent_name'=>$request['parentName'],
        'permanent_address'=>$request['parmanentAddress'],
        'current_address'=>$request['currentAddress'],
        'gender'=>$request['gender'],
        'post'=>json_encode($request['posts']),
        'department'=>json_encode($request['departments']),
        'image'=>$request['image'],
        'leaving_date'=>$request['leaveDate'],
        'joining_date'=>$request['joinDate'],
        'mobile_number'=>$request['mobileNumber'],
        'phone_number'=>$request['phoneNumber']  
   ]);
   return response()->json(['response'=>$teacher]);
   }
  public function getGuestUpdate($id){
    $guests=User::withTrashed()->join('guests', 'users.id', '=', 'guests.id')
    ->where('verified', '2')
    ->where('guests.id',$id)
    ->select('users.id as user_id','guests.*','users.*')
    ->orderBy('first_name')
    ->get();
return response()->json(['guests'=>$guests]);
  } 
 public function updateGuest(Request $request){
    $users=User::where('id',$request['guest_id'])->update([
        'name'=>$request['userName'],
        'email'=>$request['email'],
        'user_type_id' => '5',
        'active' => '1',
        'verified' => '2',
       ]);
       $guest=Guest::where('id',$request['guest_id'])->update([
         'first_name'=>$request['firstName'],
         'middle_name'=>$request['middleName'],
         'last_name'=>$request['lastName'],
         'permanent_address'=>$request['parmanentAddress'],
         'current_address'=>$request['currentAddress'],
         'gender'=>$request['gender'],
         'mobile_number'=>$request['mobileNumber'],
         'phone_number'=>$request['phoneNumber'],
         'image'=>$request['image'],
         'description'=>$request['description']
       ]);
    return response()->json(['guests'=>$guest]);   
  } 
}
