<?php

namespace HCCNetwork\Http\Controllers\MIS_API;

use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\Subject;
use HCCNetwork\faculty;
use HCCNetwork\Routine;
Use DB;
class subjectController extends Controller {
    public function cloneSubject(Request $request){
        $subjectArr = array();
        if($request['level']){
             $subject = Subject::where('batch', $request['past_year'])->where('faculty_id', $request['faculty'])->where('level', $request['level'])->get();
            foreach ($subject as $sub) {
                array_push($subjectArr, [
                    'level' => $sub->level,
                    'name' => $sub->name,
                    'faculty_id' => $request['faculty'],
                    'batch' => $request['year']
                ]);
            }
          }else{
            $subject = Subject::where('batch', $request['past_year'])->where('faculty_id', $request['faculty'])->get();         
            foreach ($subject as $sub) {
                array_push($subjectArr, [
                    'level' => $sub->level,
                    'name' => $sub->name,
                    'faculty_id' => $request['faculty'],
                    'batch' => $request['year']
                ]);
            }      
        }
        $response=Subject::insert($subjectArr);
        return response()->json(['response'=>$response]);
    }
public function storeSubject(Request $request){
        $subjects=Subject::create([
            'name' => $request['subjectName'],
            'level' => $request['level'],
            'faculty_id' => $request['faculty'],
            'batch' => $request['year'],
        ]);
      return response()->json(['response'=>$subjects]);
   }

   public function getLevel($id){
     $level=faculty::where('id',$id)->get();
     return response()->json(['level'=>$level]);
   }
   public function getSubject(){
    $routine = array();
    $level = array();
    $routines = array();

    foreach ($facultylink = Faculty::all() as $faculty) {
        $routine['faculty'] = $faculty;
        $routine['batch'] = array();

        foreach ($batches = Subject::where('faculty_id', $faculty->id)->select('batch')->groupBy('batch')->get()->toArray() as $batch) {
            $batch['name'] = $batch['batch'];
            $batch['levels'] = array();

            foreach ($levels = Subject::where('faculty_id', $faculty->id)->where('batch', $batch['batch'])->select('level')->groupBy('level')->get()->toArray() as $level) {
                $subject = Subject::where('level', $level)
                    ->where('batch', $batch['batch'])
                    ->where('faculty_id', $faculty->id)
                    ->get();
                $level['name'] = $level['level'];
                $level['subjects'] = $subject;
                array_push($batch['levels'], $level);
            }
            array_push($routine['batch'], $batch);
        }
        array_push($routines, $routine);
      }
    return response()->json(['subjects'=> $routines]);
   }
   public function editSubject(Request $request){
    $subjects=Subject::where('id',$request['id'])->update([
        'name' => $request['subjectName'],  
    ]);
    return response()->json(['response'=>$subjects]);
   }
   public function deleteSubject($id){
    $subjects=Subject::where('id',$id)->delete();
    return response()->json([$subjects]);
   }
   public function deleteInBlock(Request $request){
       $delete=$request->all();
    for($i = 0 ;$i < count($delete);$i++){
        $subjects=Subject::where('id',$delete[$i]['id'])->delete();
     }
    return response()->json([$subjects]);
   }
   public function getSubjectOfParticularTeacher(){
      $subject=Routine::join('teachers','teachers.id','=','routines.teacher_id')
                      ->join('subjects','subjects.id','=','routines.subject_id')
                      ->select('subjects.name','teachers.id as teacher_id','subjects.id as subject_id')
                      ->get();
      return response()->json(['subjects'=>$subject]);                 
   }
}
