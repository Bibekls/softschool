<?php

namespace HCCNetwork\Http\Controllers\MIS_API;

use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\faculty;
class configurationController extends Controller
{
    public function postFaculty(Request $request){
        if($request['is_edit']){
            $faculty=Faculty::where('id',$request['id'])->update([
                'name'=>$request['facultyName'],
                'start_at'=>$request['start_at'],
                'end_at'=>$request['end_at']
            ]);
            return response()->json(['faculties'=>$faculty]);
        }else{
            $faculty=Faculty::create([
                'name'=>$request['facultyName'],
                'start_at'=>$request['start_at'],
                'end_at'=>$request['end_at'],
                'faculty_wise_class'=>json_encode($request['facultyWiseClasses'])
            ]);
            return response()->json(['faculties'=>$faculty]);
        }

    }
    public function getFaculty(){
        $faculties=Faculty::get();
        return response()->json(['faculties'=>$faculties]);
    }
    public function deleteFaculty($id){
   $deletefaculty=Faculty::find($id)->delete();
   return response()->json(['deletefaculty'=>$deletefaculty]);
    }
}
