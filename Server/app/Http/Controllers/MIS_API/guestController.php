<?php

namespace HCCNetwork\Http\Controllers\MIS_API;
use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\Guest;
use HCCNetwork\User;
class guestController extends Controller
{
  public function postGuest(Request $request){
    $data=$request->all();
    $users=User::create([
     'name'=>$request['userName'],
     'email'=>$request['email'],
     'user_type_id' => '5',
     'active' => '1',
     'verified' => '2',
    ]);
    $id=$users->id;
    $guest=Guest::create([
      'id'=>$id,
      'first_name'=>$request['firstName'],
      'middle_name'=>$request['middleName'],
      'last_name'=>$request['lastName'],
      'permanent_address'=>$request['parmanentAddress'],
      'current_address'=>$request['currentAddress'],
      'gender'=>$request['gender'],
      'mobile_number'=>$request['mobileNumber'],
      'phone_number'=>$request['phoneNumber'],
      'image'=>$request['image'],
      'description'=>$request['description']
    ]);
    return response()->json(['response'=>$guest]);
  }
}
