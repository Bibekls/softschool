<?php

namespace HCCNetwork\Http\Controllers\MIS_API;

use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\EventCalendar;
class eventController extends Controller
{
    public function postEvent(Request $request){
        if($request['is_Edit']){
         $event=EventCalendar::where('id',$request['id'])->update([
                'title' => $request['title'],
                'description' => $request['discription'],
                'date' => $request['joinDate'] ,
                'start' => $request['joinDate'] ,
                'start_at' =>$request['startAt'] ,
                'end_at' =>$request['endAt'] ,
         ]);
         return response()->json(['event'=>$event]);
        }else{
            $event=EventCalendar::create([
                'title' => $request['title'],
                'description' => $request['discription'],
                'date' => $request['joinDate'] ,
                'start' => $request['joinDate'] ,
                'start_at' =>$request['startAt'] ,
                'end_at' =>$request['endAt'] ,
            ]);
            return response()->json(['event'=>$event]);
        }       
    }
   public function getEvent(){
      $event=EventCalendar::orderBy('date','dec')->get();
      return response()->json(['events'=>$event]);
  } 
   public function futureEvent(){
    $futureEvents=EventCalendar::orderBy('date','asce')->where('date','>=',date('y-m-d'))->get();
    return response()->json(['futureEvents'=>$futureEvents]);
   } 
   public function pastEvent(){
    $pastEvents=EventCalendar::orderBy('date','desc')->where('date','<',date('y-m-d'))->get();
    return response()->json(['pastEvents'=>$pastEvents]);
   }
   public function deleteEvent($id){
     $deleted= EventCalendar::find($id)->delete();
    return response()->json(['deleted'=> $deleted]);
   }
}


