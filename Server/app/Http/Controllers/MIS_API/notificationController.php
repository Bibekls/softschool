<?php
namespace HCCNetwork\Http\Controllers\MIS_API;
use HCCNetwork\EventCalendar;
use Illuminate\Http\Request;
use HCCNetwork\Events\FCMNotification;
use HCCNetwork\Http\Requests\mis\notification\NotificationRequest;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\Teacher;
use HCCNetwork\Subject;
use HCCNetwork\Routine;
use HCCNetwork\faculty;
use HCCNetwork\studentRecord;
use HCCNetwork\student;
use HCCNetwork\NotificationUser;
use Illuminate\Support\Facades\Log;
use DB;
use Mockery\Matcher\Not;
use View;
use Auth;
use Event;
use HCCNetwork\Notification;
class notificationController extends Controller
{
    private $userFCMToken;
    private $teacherArray;

    public function __construct()
    {
        $this->userFCMToken = array();
        $this->teacherArray = array();

    }
    public function getNotification(){
        // $notification=View::make('mis.notification.notification')
        // ->with('title', 'Notification')
        // ->with('faculties', Faculty::all())
        // ->with('notifications', Notification::join('users', 'notifications.sender_id', '=', 'users.id')
        //     ->leftJoin('students', 'users.id', '=', 'students.id')
        //     ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
        //     ->select('users.user_type_id', 'notifications.*', 'students.first_name as first_name',
        //         'students.last_name as last_name', 'teachers.first_name as first_name',
        //         'teachers.last_name as last_name'
        //     ->get());)
        $notification=Notification::join('users', 'notifications.sender_id', '=', 'users.id')
                                  ->leftJoin('students', 'users.id', '=', 'students.id')
                                  ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
                                  ->select('users.user_type_id', 'notifications.*', 'students.first_name as first_name',
                                   'students.last_name as last_name', 'teachers.first_name as first_name',
                                   'teachers.last_name as last_name')
                                  ->get();
       return response()->json([$notification]);      
    }
   public function getBatchAndFaculty(Request $request){
     $batches=faculty::with('subject')->get();
     $batchlevel=faculty::with('subjectLevel')->get();
     return response()->json(['batches'=>$batches,'batcheslevel'=>$batchlevel]);
   }
   public function getFacultyNotification(){
    $faculties=Faculty::select(DB::raw("faculties.id as faculty_id,faculties.name,'3' as test,'teacher' as type"))->get();
    return response()->json(['faculties'=>$faculties]);
   }
   public function postNotification(Request $request){
    $notification=Notification::create([
      "sender_id" => 1,
      "title" => $request->input('title'),
      "description" => $request->input('description'),
      "target_user" => json_encode($request->input('target_user')),
      "schedule_date" => date("Y-m-d"),
      "time" => date("H:i:s")
     ]); 
     $targetArrays=$notification->target_user;
     $notificationId=$notification->id;
            //get Recent Year

     $tests=DB::table('year_mapping')->whereDate('form_ad','<=', date("Y-m-d"))->whereDate('to_ad','>=', date("Y-m-d"))->first();
         for($i = 0 ;$i < count($targetArrays);$i++){           
               //Push notification to Student

             if($targetArrays[$i]['test'] == '1'){
                $notificationUserArray = array();
                  foreach (student::join('users','users.id','=','students.id')->where('students.faculty_id', $targetArrays[$i]['faculty_id'])->where('students.batch', $targetArrays[$i]['name'])->get() as $user) {
                   if (!$user->id) continue;
                    $notificationUser = [
                       "notification_id" => $notificationId,
                      "user_id" => $user->id,
                      "fcm_token" => $user->fcm_token,
                  ];
                  if ($user->fcm_token)
                      array_push($this->userFCMToken, $user->fcm_token);
      
                  array_push($notificationUserArray, $notificationUser);
              }
              $response=NotificationUser::insert($notificationUserArray);
            }
             if($targetArrays[$i]['test'] == '2'){
                 //Push Notification to Student of School

                $notificationUserArray = array();         
                   foreach(DB::table('studentRecord')
                            ->leftJoin('students','studentRecord.student_id','=','students.id')
                            ->leftJoin('users','users.id','=','students.id')
                            ->where('studentRecord.faculty_id', 1)
                            ->where('studentRecord.class',10)
                            ->where('studentRecord.year',$tests->np_year)
                            ->get() as $user) {
                         if (!$user->student_id) continue;
                            $notificationUser = [
                            "notification_id" => $notificationId,
                            "user_id" => $user->id,
                            "fcm_token" => $user->fcm_token,
                         ];
                        if ($user->fcm_token)
                            array_push($this->userFCMToken, $user->fcm_token);
            
                        array_push($notificationUserArray, $notificationUser);
                       }
                 $response=NotificationUser::insert($notificationUserArray);             
         }
            if($targetArrays[$i]['test'] == '3'){
                //Push Notifition to Teacher Batch Wise

                $notificationUserArray = array();
                  $teachers=Teacher::join('users','users.id','=','teachers.id')->get();
                  foreach($teachers as $teacher){
                         if($teacher->department){
                           foreach($teacher->department as $department){
                               if($department == $targetArrays[$i]['name']){
                                 array_push($this->teacherArray, $teacher);
                                }
                             }
                         }
                      }
                    //   return response()->json([$teachers]);
                 foreach($this->teacherArray as $user) {
                   if (!$user->id) continue;
                    $notificationUser = [
                     "notification_id" => $notificationId,
                     "user_id" => $user->id,
                     "fcm_token" => $user->fcm_token,
                ];
                if ($user->fcm_token)
                    array_push($this->userFCMToken, $user->fcm_token);
    
                 array_push($notificationUserArray, $notificationUser);
               }
            $response=NotificationUser::insert($notificationUserArray);             
             }             
          }
        if (count($this->userFCMToken) >= 0) {
            $this->userFCMToken = array_unique($this->userFCMToken);
            $response = $this->sentToFCM($notification);
            $notification->update([
                //"multicast_id"=>$response->multicast_id,
                //"message_id"=>$response->result->message_id,
                "status" => json_encode($response),
                "send" => true,
             ]);
           }else {
            Log::info("No device token found");
            $notification->update([
                //"multicast_id"=>$response->multicast_id,
                //"message_id"=>$response->result->message_id,
                "status" => 'No Device token found',
                "send" => true,
            ]);
        }
        // return response()->json([
        //     $this->sentToFCM($notification)
        // ]) ;  

      }
        function sentToFCM(Notification $noti){
            $notification = Notification::leftJoin('users', 'notifications.sender_id', '=', 'users.id')
            ->leftJoin('teachers', 'users.id', '=', 'teachers.id')
            ->where('notifications.id', $noti->id)
            // ->where('notifications.id', 49)
            ->orderBy('notifications.schedule_date', 'desc')
            ->select('notifications.id as id',
                        'notifications.title as title',
                        'notifications.description as description',
                        'notifications.schedule_date as date',
                        'notifications.time as time',
                        'notifications.send as send',
                        'teachers.first_name as first_name',
                        'teachers.last_name as last_name',
                        'users.user_type_id as user_type'
            )
            ->first();

            $notifi = array
            (
            'icon' => 'myicon',/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
            );
            $msg = array
            (
                'title' => $notification->title,
                'body' => $notification->description,
                "description" => $notification->description,
                "id" => $notification->id,
                // "date" => $notification->schedule_date,
                "time" => $notification->time,
                "send" => $notification->send,
                "first_name" => $notification->first_name,
                "last_name" => $notification->last_name,
                "user_type" => $notification->user_type,

                'icon' => 'myicon',/*Default Icon*/
                'sound' => 'default'/*Default sound*/
            );

            $fields = array
            (
            // 'registration_ids' => array("fqBJH5RNgI0:APA91bECynv6XHmET_Fqv-PqFEZnVNiB6EuuAAM84LkwXRrM50-rxXijDSk-AN0al1-oz1HdRF-URyCTFoo7ExzEThHLJsKKpZzCR7Jp1mrGzOcq1BVBycNEF-fOGtZETN2KN94izg0J"), 
            'registration_ids' => $this->userFCMToken,
            //'notification' => $notifi,
            'data' => $msg
            );

            Log::info(env('FCM_API_ACCESS_KEY'));
            $headers = array
            (
            'Authorization: key='."AAAAywg39tI:APA91bEtDez306l0KQ3-Cir9n5bn_hDe-Zm5bS73HVYVcwplQkFnvyKIUuGEqSVTXdQmXEruTRWy4bsta-QV6yrEunT1tIcr-zJzN1KJaoNsy573S6IyQnj94HYvKRUNKS2iPT9XylZY",
            'Content-Type: application/json'
            );

            //Send Reponse To FireBase Server

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);

            //Echo Result Of FireBase Server
            return response()->json([$result]);
        }
    
}
