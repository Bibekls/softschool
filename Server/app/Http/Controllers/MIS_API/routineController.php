<?php

namespace HCCNetwork\Http\Controllers\MIS_API;

use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\Teacher;
use HCCNetwork\Subject;
use HCCNetwork\Routine;
use HCCNetwork\faculty;
class routineController extends Controller
{
    public function getTeachers($id){
         $teachers=Teacher::join('faculty_teacher', 'teachers.id', '=', 'faculty_teacher.teacher_id')
            ->join('users', 'teachers.id', '=', 'users.id')
            ->where('faculty_teacher.faculty_id',$id)
            ->get();
            return response()->json(['teachers'=>$teachers]);
    }
    public function getBatch(){
        $batches=Subject::groupBy('batch')->get();
        return response()->json(['batches'=>$batches]);
    }
    public function getSubject(Request $request)
    {
        $subjects=Subject::where('batch', $request->input('batch'))
            ->where('faculty_id', $request->input('faculty'))
            ->where('level', $request->input('level'))
            ->get();
        return response()->json(['subjects'=>$subjects]);    
    }





    public function addRoutine(Request $request){
      $teacherId=$request['teacher']['id'];
      $subjectId=$request['subject']['id'];
      $start_at = $request['start_at'];
      $end_at = $request['end_at'];
      $start_at_sec = strtotime("1970-01-01 $start_at UTC");
      $end_at_sec = strtotime("1970-01-01 $end_at UTC");
      $day = null;
      foreach($request['days']  as $dayInput){
        switch ($dayInput) {
            case "Sunday":
                $day = 1;
                break;
            case "Monday":
                $day = 2;
                break;
            case "Tuesday":
                $day = 3;
                break;
            case "Wednesday":
                $day = 4;
                break;
            case "Thursday":
                $day = 5;
                break;
            case "Friday":
                $day = 6;
                break;
            case "Saturday":
                $day = 7;
                break;
        }

        if ($start_at_sec < 21600 || $end_at_sec > 37800 || $start_at_sec > $end_at_sec)
        return response()->json(['time_interval'=>'Error on time interval!','test'=>1]);
       
        
        foreach ($hisRoutine = Routine::where('teacher_id', $teacherId)->where('day', $day)->get() as $timeInterval) {
            if ($start_at_sec > $timeInterval->start_at
                && $start_at_sec < $timeInterval->end_at
                ||
                $end_at_sec > $timeInterval->start_at
                && $end_at_sec < $timeInterval->end_at
              )
              return response()->json(['time_interval'=>'Time interval overlaped!','test'=>2]);
      } 

      $routines=Routine::create(array(
        'teacher_id' => $teacherId,
        'subject_id' => $subjectId,
        'day' => $day,
        'start_at' => $start_at_sec,
        'end_at' => $end_at_sec,
    ));
    $routines->subject_id;
    $routines->teacher_id;
    $lists=Routine::join('subjects','subjects.id','=','routines.subject_id')
                    ->join('teachers','teachers.id','=','routines.teacher_id')
                    ->where('routines.teacher_id',$routines->teacher_id)
                    ->where('routines.subject_id',$routines->subject_id)
                    ->get();
    return response()->json(['time_interval'=>'Sucessfully Added Routine!','routine'=>$lists,'test'=>3]);
    }


 }
 public function getRoutine(){
    $routine = array();
    $level = array();
    $day = array();
    $period = array();
    $routines = array();
    foreach ($facultylink = Faculty::all() as $faculty) {
        $routine['faculty'] = $faculty;
        $routine['levels'] = array();
        $levels = Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
            ->where('subjects.faculty_id', $faculty->id)
            ->select('level')
            ->groupBy('level')
            ->get()
            ->toArray();
        $routine['level_list'] = $levels;

        foreach ($levels as $level) {
            $level['name'] = $level['level'];

            $days = Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                ->where('subjects.faculty_id', $faculty->id)
                ->where('subjects.level', $level['level'])
                ->select('day')
                ->groupBy('day')
                ->get()
                ->toArray();
            $level['days'] = array();
            foreach ($days as $day) {
                $day['name'] = $day['day'];
                $day['period'] = Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                    ->join('teachers', 'routines.teacher_id', '=', 'teachers.id')
                    ->where('subjects.faculty_id', $faculty->id)
                    ->where('subjects.level', $level['level'])
                    ->where('routines.day', $day['day'])
                    ->select('routines.id as id','subjects.style','subjects.name as subject', 'teachers.first_name as fname', 'teachers.last_name as lname', 'routines.start_at as start_at', 'routines.end_at as end_at')
                    ->orderBy('routines.start_at')
                    ->get();
                array_push($level['days'], $day);
               }
            array_push($routine['levels'], $level);
        }
        array_push($routines, $routine);
    }
    return response()->json(['routines'=>$routines]);
  }
  public function deleteRoutine(Request $request){
          $deleteroutine=Routine::where('id',$request['id'])->delete();         
          return response()->json(['deleteroutine'=>$deleteroutine]);
      }
  public function deleteBlockRoutine(Request $request){
      $ids=$request->all();
        foreach($ids as $id){
           $deleteroutine=Routine::where('id',$id['id'])->delete();         
            }
        return response()->json(['deleteroutine'=>$deleteroutine]);
      }
  }

