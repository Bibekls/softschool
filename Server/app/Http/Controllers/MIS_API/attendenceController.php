<?php
namespace HCCNetwork\Http\Controllers\MIS_API;
use Illuminate\Http\Request;
use HCCNetwork\Http\Controllers\Controller;
use HCCNetwork\student;
use HCCNetwork\BatchLevelMaping;
use Maatwebsite\Excel\Facades\Excel;
use View;
use Illuminate\Support\Facades\Auth;
use HCCNetwork\Http\Requests\mis\routine\RoutineRequest;
use HCCNetwork\Routine;
use HCCNetwork\Subject;
use HCCNetwork\User;
use HCCNetwork\faculty;
use HCCNetwork\Attendance;
use DB;
class attendenceController extends Controller
{
    public function getStudent(Request $request){
       $students=student::join('faculties','faculties.id','=','students.faculty_id')
                ->where('batch',$request['batch'])
                ->where('name',$request['faculty'])->select('faculties.*','students.*','students.id as student_id')
                ->get();
        return response()->json(['students'=>$students]);
    }
    public function getAttendence(){
        //  $attendances = array();
        //  $attend=student::with('attendence')->get();
        //  return response()->json([$attend]);

                $attendances = array();
                $batch = array();
                $attendance = array();
        
                foreach ($facultylink = Faculty::all() as $faculty) {
                    $attendance['faculty'] = $faculty;
                    $attendance['batch'] = array();
                    // get all batch of following faculty
                    $batchs = Attendance::Join('students', 'attendances.student_id', '=', 'students.id')
                        ->join('batch_level_maping', 'students.batch', '=', 'batch_level_maping.batch')
                        ->Join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                        ->where('students.faculty_id', $faculty->id)
                        ->where('students.deleted_at', null)
                        ->where('batch_level_maping.level', '<>', 0)
                        ->select('students.batch as batch')
                        ->groupBy('batch')
                        ->get()
                        ->toArray();
                    $attendance['batch-list']=$batchs;
                    foreach ($batchs as $batch){
                        $batch['name'] = $batch['batch'];
                        $batch['attendance-date'] = date("m/d/Y");
                        // get array of students of following batch ans faculty
                            $students = Attendance::join('students', 'attendances.student_id', '=', 'students.id')
                             ->where('students.faculty_id', $faculty->id)
                             ->where('students.batch', $batch['batch'])
                             ->where('students.deleted_at', null)
                             ->select('students.id as id', 'students.first_name as first_name', 'students.last_name as last_name',
                                'students.mobile_number as mobile_number', 'students.phone_number as phone_number')
                             ->orderBy('students.first_name')
                             ->groupBy('students.first_name')
                             ->get()
                             ->toArray();
                            $subjects = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                             ->join('students', 'attendances.student_id', '=', 'students.id')
                            // fetch teh subject of current mapped logic start
                             ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
                             ->where('batch_level_maping.faculty_id', $faculty->id)
                             ->where('batch_level_maping.batch', $batch['batch'])
                             //end
                             ->where('subjects.faculty_id', $faculty->id)
                             ->where('students.deleted_at', null)
                             ->where('students.batch', $batch['batch'])
                             ->select('subjects.id as id', 'subjects.name as subject_name')
                             ->orderBy('subjects.name')
                             ->groupBy('subjects.name')
                             ->get()
                             ->toArray();
                            $batch['subject_list'] = $subjects;
                            $batch['students'] = array();  
                            foreach($students as $student){
                                $student['name'] = $student['first_name'] . ' ' . $student['last_name'];
                                $student['contact'] = $student['mobile_number'];
                                $student['parent_contact'] = $student['phone_number'];
                                $student ['subjects'] = array();
                                foreach($subjects as $subject){
                                    $subjectAttendance = array();
                                    array_push($subjectAttendance, [
                                        $this->getTypeAttendance(
                                            $subject['id'],
                                            $faculty->id,
                                            $batch['name'],
                                            $batch['attendance-date'],
                                            $student['id'],
                                            $subject['subject_name'],
                                            false,
                                            false
                                        ),
                                        Attendance::where('subject_id', $subject['id'])
                                            ->where('replaced_with', 0)
                                            ->where('extra', false)
                                            ->select('date')
                                            ->groupBy('date')
                                            ->get()
                                            ->count()
                                       ]);
                                    array_push($subjectAttendance, [
                                        $this->getTypeAttendance(
                                            $subject['id'],
                                            $faculty->id,
                                            $batch['name'],
                                            $batch['attendance-date'],
                                            $student['id'],
                                            $subject['subject_name'],
                                            true,
                                            false
                                        ),
                                        Attendance::where('subject_id', $subject['id'])
                                            ->where('replaced_with', '<>', 0)
                                            ->where('extra', false)
                                            ->select('date')
                                            ->groupBy('date')
                                            ->get()
                                            ->count()
                                    ]);
            
                                    array_push($subjectAttendance, [
                                        $this->getTypeAttendance(
                                            $subject['id'],
                                            $faculty->id,
                                            $batch['name'],
                                            $batch['attendance-date'],
                                            $student['id'],
                                            $subject['subject_name'],
                                            false,
                                            true
                                        ),
                                        Attendance::where('subject_id', $subject['id'])
                                            ->where('replaced_with', 0)
                                            ->where('extra', true)
                                            ->select('date')
                                            ->groupBy('date')
                                            ->get()
                                            ->count()
                                       ]);
                                    // finally push all the attendance
                                    array_push($student['subjects'],$subjectAttendance);
                                 }
                                array_push($batch['students'], $student);
                              }
                            array_push($attendance['batch'], $batch);
                           }
                        array_push($attendances, $attendance);
                     }
                   return response()->json(['attendence'=>$attendances]);
                }

             private function getTypeAttendance($subjectId, $facultyId, $batchName, $attendanceDate, $studentId, $subjectName, $replace, $extra){
                //count the total attendanc of all student on regular classes
                    $regSubQuery = DB::table('attendances as att')->select(DB::raw('count(attendance) as total_attendance'), 'student_id as sid')
                              ->where('subject_id', '=', $subjectId)
                              ->where('attendance', '=', 1)
                              ->where(function ($sql) use ($replace) {
                                if ($replace)
                                     $sql->where('replaced_with', '<>', 0);
                                else
                                     $sql->where('replaced_with', 0);
                              })
                                ->where('extra', $extra)
                                ->groupBy('att.student_id');

                    // get today attendance
                    $regAttendance = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                    ->join('students', 'attendances.student_id', '=', 'students.id')
                    // join attendance of each student
                    ->leftJoin(DB::raw(' ( ' . $regSubQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                    ->mergeBindings($regSubQuery)
                    ->where('subjects.faculty_id', $facultyId)
                    ->where('students.batch', $batchName)
                    ->where('students.id', $studentId)
                    ->where('students.deleted_at', null)
                    ->where('attendances.date',date_format(date_create($attendanceDate), "Y-m-d"))
                    //regular classes
                    ->where(function ($sql) use ($replace) {
                        if ($replace)
                            $sql->where('attendances.replaced_with', '<>', 0);
                        else
                            $sql->where('attendances.replaced_with', 0);
                       })
                    ->where('attendances.extra', $extra)
                    //
                    ->where('subjects.name', $subjectName)
                    ->select('attendances.attendance as attendance', 'subjects.name as subject', 'counted.total_attendance as total_attendance')
                    ->orderBy('subjects.name')
                    ->groupBy('subjects.name')
                    ->get();

                    // This is added if attendance is null ie attendance is not taken today

                    if (count($regAttendance) == 0) {
                    $regAttendance = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                        ->join('students', 'attendances.student_id', '=', 'students.id')
                        ->leftJoin(DB::raw(' ( ' . $regSubQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                        ->mergeBindings($regSubQuery)
                        ->where('subjects.faculty_id', $facultyId)
                        ->where('students.batch', $batchName)
                        ->where('students.id', $studentId)
                        ->where('students.deleted_at', null)
                        // removing below clause extract attendance of any date
                       // ->where('attendances.date', date_format(date_create($batch['attendance-date']), "Y-m-d"))
                        ->where('subjects.name', $subjectName)
                        //Regular attendance login start
                        ->where(function ($sql) use ($replace) {
                            if ($replace)
                                $sql->where('attendances.replaced_with', '<>', 0);
                            else
                                $sql->where('attendances.replaced_with', 0);
                            })
                        ->where('attendances.extra', $extra)
                        //Regular attendance login end
                        ->select('attendances.attendance as attendance', 'subjects.name as subject', 'counted.total_attendance as total_attendance')
                        ->orderBy('subjects.name')
                        ->groupBy('subjects.name')
                        ->get();


                    if (count($regAttendance) == 0) {
                        //if attendance is still 0 ie there is no attendance of particular subject
                        //get any record from database and make attendance 2 for not taken
                        $regAttendance = Attendance::limit(1)->get();
                        $regAttendance[0]->attendance = 2;
                    } else {
                        // mark attendance at 2 for not taken
                        // index 0 use used because there will be only one record
                        $regAttendance[0]->attendance = 2;
                    }
               }
             return $regAttendance[0];
        }

    public function studentWise(Request $request){
        $studentId = $request->input('student_id');
        $start_date =$request->input('start');
        $end_date = $request->input('end');
        
        $batch = array();
        $batch['name'] = $request->input('batch');
        $dates = Attendance:: where('student_id', $studentId)
        ->whereBetween('date', [$start_date, $end_date])
        ->select('date')
        ->orderBy('date', 'desc')
        ->groupBy('date')
        ->get();

        $subjects = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
        ->where('attendances.student_id', $studentId)
        // fetch teh subject of current mapped logic start
        ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
//            ->where('batch_level_maping.faculty_id', faculty::where('name', $request->input('faculty'))->pluck('id')->first())
//            ->where('batch_level_maping.batch', $request->input('batch'))
        //end
        ->select('subjects.id as id', 'subjects.name as subject_name')
        ->orderBy('subjects.name')
        ->groupBy('subjects.name')
        ->get()
        ->toArray();
        
        $batch['subject_list'] = $subjects;
        $batch['date'] = array();
        foreach ($dates as $date) {
            $day['current_date'] = $date['date'];
            $day ['subjects'] = array();
            foreach ($subjects as $subject){
                $subAttendance = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                    ->where('attendances.student_id', $studentId)
                    ->where('attendances.date', $day['current_date'])
                    ->where('subjects.name', $subject['subject_name'])
                    ->select('attendances.attendance as attendance')
                    ->orderBy('subjects.name')
                    ->groupBy('subjects.name')
                    ->get();

                if (count($subAttendance) == 0) {
                    $subAttendance = Attendance::limit(1)->get();
                    $subAttendance[0]->attendance = 2;
                }

                array_push($day['subjects'], $subAttendance);
            }
            array_push($batch['date'], $day);
        }   
        return response()->json([$batch]);
    }
    public function dayWise(Request $request){
        $batch = array();
        $batch['name'] = $request['batch'];
        $batch['attendance_date'] = $request['date'];

        $students = Attendance::join('students', 'attendances.student_id', '=', 'students.id')
            ->where('students.faculty_id', faculty::where('name', $request->input('faculty'))->pluck('id')->first())
            ->where('students.batch', $request->input('batch'))
            ->where('students.deleted_at', null)
            ->select('students.id as id', 'students.first_name as first_name', 'students.last_name as last_name', 'students.mobile_number as mobile_number', 'students.phone_number as phone_number')
            ->orderBy('students.first_name')
            ->groupBy('students.first_name')
            ->get()
            ->toArray();

        $subjects = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
            ->join('students', 'attendances.student_id', '=', 'students.id')
            // fetch teh subject of current mapped logic start
            ->join('batch_level_maping', 'subjects.level', '=', 'batch_level_maping.level')
            ->where('batch_level_maping.faculty_id', faculty::where('name', $request->input('faculty'))->pluck('id')->first())
            ->where('batch_level_maping.batch', $request->input('batch'))
            //end
            ->where('subjects.faculty_id', faculty::where('name', $request->input('faculty'))->pluck('id')->first())
            ->where('students.batch', $request->input('batch'))
            ->where('students.deleted_at', null)
            ->select('subjects.id as id', 'subjects.name as subject_name')
            ->orderBy('subjects.name')
            ->groupBy('subjects.name')
            ->get()
            ->toArray();

        $batch['subject_list'] = $subjects;
        $batch['students'] = array();

        foreach ($students as $student) {

            $student['name'] = $student['first_name'] . ' ' . $student['last_name'];
            $student['contact'] = $student['mobile_number'];
            $student['parentContact'] = $student['phone_number'];

            $student ['subjects'] = array();
            foreach ($subjects as $subject) {

                $subjectAttendance = array();

                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        faculty::where('name', $request->input('faculty'))->pluck('id')->first(),
                        $batch['name'],
                        $batch['attendance_date'],
                        $student['id'],
                        $subject['subject_name'],
                        false,
                        false
                    ),
                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', 0)
                        ->where('extra', false)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                   ]); 
                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        faculty::where('name', $request->input('faculty'))->pluck('id')->first(),
                        $batch['name'],
                        $batch['attendance_date'],
                        $student['id'],
                        $subject['subject_name'],
                        true,
                        false
                    ),
                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', '<>', 0)
                        ->where('extra', false)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                ]);

                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        faculty::where('name', $request->input('faculty'))->pluck('id')->first(),
                        $batch['name'],
                        $batch['attendance_date'],
                        $student['id'],
                        $subject['subject_name'],
                        false,
                        true
                    ),

                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', 0)
                        ->where('extra', true)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                ]);

                // finally push all the attendance
                array_push($student['subjects'],$subjectAttendance);
            }
            array_push($batch['students'], $student);
        }
        return response()->json([$batch]);    
    }
    public function getAttendenceOfSchool(Request $request){
        $attendances = array();
        $batch = array();
        $attendance = array();
        $tests=DB::table('year_mapping')->whereDate('form_ad','<=', date("Y-m-d"))->whereDate('to_ad','>=', date("Y-m-d"))->first();
        $faculties=faculty::where('id',$request['faculty_id'])->get();
        $facultiesWise=$faculties[0]->faculty_wise_class;
        // return response($faculties[0]->id);
        for($i = 0 ;$i < count($facultiesWise);$i++){
            $batch['subject_list']=array();
            // $batch['name'] = $batch['batch'];
            $batch['attendance-date'] = date("m/d/Y");
            $attendance['classes']=$facultiesWise[$i]['faculties'];
            $attendance['faculty']=$faculties;
            $students = Attendance::join('students', 'attendances.student_id', '=', 'students.id')
                    ->join('studentRecord','studentRecord.student_id','=','students.id') 
                    ->where('students.faculty_id',$faculties[0]->id)
                    ->where('studentRecord.class', $facultiesWise[$i]['faculties'])
                    ->where('studentRecord.year',$tests->np_year)
                    // ->where('students.deleted_at', null)
                    ->select('students.id as id', 'students.first_name as first_name', 'students.last_name as last_name',
                        'students.mobile_number as mobile_number', 'students.phone_number as phone_number','studentRecord.class')
                    ->orderBy('students.first_name')
                    ->groupBy('students.first_name')
                    ->get()
                    ->toArray();

                    $subjects = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                    ->join('students', 'attendances.student_id', '=', 'students.id')
                    ->join('studentRecord','studentRecord.student_id','=','students.id') 
                //    ->where('students.faculty_id',$faculties[0]->id)
                    ->where('studentRecord.class', $facultiesWise[$i]['faculties'])
                    ->where('studentRecord.year',$tests->np_year)
                //         //end
                    ->where('subjects.faculty_id', $faculties[0]->id)
                    ->where('students.deleted_at', null)
                    // ->where('students.batch', $batch['batch'])
                    ->select('subjects.id as id', 'subjects.name as subject_name')
                    ->orderBy('subjects.name')
                    ->groupBy('subjects.name')
                    ->get()
                    ->toArray();
                    $batch['subject_list'] = $subjects;
                    // return response($batch);   
                    $batch['students'] = array();  
                    // foreach($subjects as $student){
                    foreach($students as $student){
                        $student['name'] = $student['first_name'] . ' ' . $student['last_name'];
                        $student['contact'] = $student['mobile_number'];
                        $student['parent_contact'] = $student['phone_number'];
                        $student ['subjects'] = array();
                        foreach($subjects as $subject){
                            $subjectAttendance = array();
                            array_push($subjectAttendance, [
                                $this->getTypeAttendanceSchool(
                                    $subject['id'],
                                    $faculties[0]->id,
                                    $tests->np_year,
                                    $batch['attendance-date'],
                                    $student['id'],
                                    $subject['subject_name'],
                                    false,
                                    false
                                
                                ),
                                Attendance::where('subject_id', $subject['id'])
                                    ->where('replaced_with', 0)
                                    ->where('extra', false)
                                    ->select('date')
                                    ->groupBy('date')
                                    ->get()
                                    ->count()
                                ]);
                            array_push($subjectAttendance, [
                                $this->getTypeAttendanceSchool(
                                    $subject['id'],
                                    $faculties[0]->id,
                                    $tests->np_year,
                                    $batch['attendance-date'],
                                    $student['id'],
                                    $subject['subject_name'],
                                    true,
                                    false
                                ),
                                Attendance::where('subject_id', $subject['id'])
                                    ->where('replaced_with', '<>', 0)
                                    ->where('extra', false)
                                    ->select('date')
                                    ->groupBy('date')
                                    ->get()
                                    ->count()
                            ]);
    
                            array_push($subjectAttendance, [
                                $this->getTypeAttendanceSchool(
                                    $subject['id'],
                                    $faculties[0]->id,
                                    $tests->np_year,
                                    $batch['attendance-date'],
                                    $student['id'],
                                    $subject['subject_name'],
                                    false,
                                    true
                                ),
                                Attendance::where('subject_id', $subject['id'])
                                    ->where('replaced_with', 0)
                                    ->where('extra', true)
                                    ->select('date')
                                    ->groupBy('date')
                                    ->get()
                                    ->count()
                                ]);

                            //     return response()->json(['subject'=>$subject['id'],
                            //     'faculty_id'=>$faculties[0]->id,
                            //     'year'=>$tests->np_year,
                            //    $batch['attendance-date'],
                            //    $student['id'],
                            //    $subject['subject_name']]);
                            // finally push all the attendance
                            array_push($student['subjects'],$subjectAttendance);
                           }
                         array_push($batch['students'], $student);
                        }
                    $attendance['batch']=$batch;
                // }
                array_push($attendances, $attendance);
            }
        return response()->json([$attendances]);     
    }








    private function getTypeAttendanceSchool($subjectId, $facultyId, $batchName, $attendanceDate, $studentId, $subjectName, $replace, $extra){
        //count the total attendanc of all student on regular classes
            $regSubQuery = DB::table('attendances as att')->select(DB::raw('count(attendance) as total_attendance'), 'student_id as sid')
                      ->where('subject_id', '=', $subjectId)
                      ->where('attendance', '=', 1)
                      ->where(function ($sql) use ($replace) {
                        if ($replace)
                             $sql->where('replaced_with', '<>', 0);
                        else
                             $sql->where('replaced_with', 0);
                      })
                        ->where('extra', $extra)
                        ->groupBy('att.student_id');

            // get today attendance
            $regAttendance = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
            ->join('students', 'attendances.student_id', '=', 'students.id')
            ->join('studentRecord', 'subjects.level', '=', 'studentRecord.class')
            // join attendance of each student
            ->leftJoin(DB::raw(' ( ' . $regSubQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
            ->mergeBindings($regSubQuery)
            ->where('subjects.faculty_id', $facultyId)
            ->where('studentRecord.year', $batchName)
            ->where('students.id', $studentId)
            ->where('students.deleted_at', null)
            ->where('attendances.date',date_format(date_create($attendanceDate), "Y-m-d"))
            //regular classes
            ->where(function ($sql) use ($replace) {
                if ($replace)
                    $sql->where('attendances.replaced_with', '<>', 0);
                else
                    $sql->where('attendances.replaced_with', 0);
               })
            ->where('attendances.extra', $extra)
            //
            ->where('subjects.name', $subjectName)
            ->select('attendances.attendance as attendance', 'subjects.name as subject', 'counted.total_attendance as total_attendance')
            ->orderBy('subjects.name')
            ->groupBy('subjects.name')
            ->get();

            // This is added if attendance is null ie attendance is not taken today

            if (count($regAttendance) == 0) {
            $regAttendance = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                ->join('students', 'attendances.student_id', '=', 'students.id')
                ->join('studentRecord', 'subjects.level', '=', 'studentRecord.class')

                ->leftJoin(DB::raw(' ( ' . $regSubQuery->toSql() . ' ) AS counted '), 'students.id', '=', 'counted.sid')
                ->mergeBindings($regSubQuery)
                ->where('subjects.faculty_id', $facultyId)
                ->where('studentRecord.year', $batchName)
                ->where('students.id', $studentId)
                ->where('students.deleted_at', null)
                // removing below clause extract attendance of any date
               // ->where('attendances.date', date_format(date_create($batch['attendance-date']), "Y-m-d"))
                ->where('subjects.name', $subjectName)
                //Regular attendance login start
                ->where(function ($sql) use ($replace) {
                    if ($replace)
                        $sql->where('attendances.replaced_with', '<>', 0);
                    else
                        $sql->where('attendances.replaced_with', 0);
                    })
                ->where('attendances.extra', $extra)
                //Regular attendance login end
                ->select('attendances.attendance as attendance', 'subjects.name as subject', 'counted.total_attendance as total_attendance')
                ->orderBy('subjects.name')
                ->groupBy('subjects.name')
                ->get();


            if (count($regAttendance) == 0) {
                //if attendance is still 0 ie there is no attendance of particular subject
                //get any record from database and make attendance 2 for not taken
                $regAttendance = Attendance::limit(1)->get();
                $regAttendance[0]->attendance = 2;
            } else {
                // mark attendance at 2 for not taken
                // index 0 use used because there will be only one record
                $regAttendance[0]->attendance = 2;
            }
       }
     return $regAttendance[0];
}










 public function getDatWiseAttendenceOfSchool(Request $request){
    $batch = array();
    $tests=DB::table('year_mapping')->whereDate('form_ad','<=', date("Y-m-d"))->whereDate('to_ad','>=', date("Y-m-d"))->first();
    $batch['name'] =$tests->np_year;
    $batch['attendance_date'] = $request['date'];

    $students = Attendance::join('students', 'attendances.student_id', '=', 'students.id')
         ->join('studentRecord','studentRecord.student_id','=','students.id') 
        ->where('students.faculty_id',$request->input('faculty_id'))
        ->where('studentRecord.year', $tests->np_year)
        ->where('studentRecord.class', $request['class'])
        ->where('students.deleted_at', null)
        ->select('students.id as id', 'students.first_name as first_name', 'students.last_name as last_name', 'students.mobile_number as mobile_number', 'students.phone_number as phone_number')
        ->orderBy('students.first_name')
        ->groupBy('students.first_name')
        ->get()
        ->toArray();

    $subjects = Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
        ->join('students', 'attendances.student_id', '=', 'students.id')
        // fetch teh subject of current mapped logic start
        ->join('studentRecord','studentRecord.student_id','=','students.id') 
        ->where('studentRecord.faculty_id',$request->input('faculty_id'))
        ->where('studentRecord.year',$tests->np_year)
        ->where('studentRecord.class', $request['class'])
        //end
        // ->where('subjects.faculty_id',$request->input('faculty_id'))
        // ->where('students.batch', $request->input('batch'))
        ->where('students.deleted_at', null)
        ->select('subjects.id as id', 'subjects.name as subject_name')
        ->orderBy('subjects.name')
        ->groupBy('subjects.name')
        ->get()
        ->toArray();
        $batch['subject_list'] = $subjects;
        $batch['students'] = array();
     
        foreach ($students as $student) {
            $student['name'] = $student['first_name'] . ' ' . $student['last_name'];
            $student['contact'] = $student['mobile_number'];
            $student['parentContact'] = $student['phone_number'];

            $student ['subjects'] = array();
            foreach ($subjects as $subject) {

                $subjectAttendance = array();

                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        $request->input('faculty_id'),
                        $batch['name'],
                        $batch['attendance_date'],
                        $student['id'],
                        $subject['subject_name'],
                        false,
                        false
                    ),
                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', 0)
                        ->where('extra', false)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                   ]);

                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        $request->input('faculty_id'),
                        $batch['name'],
                        $batch['attendance_date'],
                        $student['id'],
                        $subject['subject_name'],
                        true,
                        false
                    ),
                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', '<>', 0)
                        ->where('extra', false)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                ]);

                array_push($subjectAttendance, [
                    $this->getTypeAttendance(
                        $subject['id'],
                        $request->input('faculty_id'),
                        $batch['name'],
                        $batch['attendance_date'],
                        $student['id'],
                        $subject['subject_name'],
                        false,
                        true
                    ),
                    Attendance::where('subject_id', $subject['id'])
                        ->where('replaced_with', 0)
                        ->where('extra', true)
                        ->select('date')
                        ->groupBy('date')
                        ->get()
                        ->count()
                ]);
                // finally push all the attendance
                array_push($student['subjects'],$subjectAttendance);
            }
            array_push($batch['students'], $student);
        }
        return response()->json([$batch]);    
    }
}