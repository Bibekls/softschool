<?php
namespace HCCNetwork\Http\Controllers\Student;
use HCCNetwork\Http\Controllers\Controller;

use HCCNetwork\faq;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\faculty;
use HCCNetwork\book;
use HCCNetwork\student; //always include related model
use HCCNetwork\BookIssue;
use HCCNetwork\BookReturn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Html;
use Intervention\Image\Facades\Image;

class registerNewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('newstudent');
    }

    public function profilePicForm(){        
        if(Auth::user()->profile_pic==null){

             return View::make('student.register.uploadProfilePic',['title'=>'Add-Student'])           
                ->with('user',User::find(Auth::user()->id));
            
        }else{
            return redirect('/registration-form');
        }

    }

    public function form()
    {
        if(Auth::user()->profile_pic==null){
            return redirect('/upload-profile-pic');
        }else{

            return View::make('student.register.registrationForm',['title'=>'Add-Student'])
                ->with('faculties',faculty::all())
                ->with('user',User::find(Auth::user()->id))
                ->with('profilePic',\HCCNetwork\image::find(Auth::user()->profile_pic) );
        }
    }
    public function uploadProfilePic(Request $request)
    {
        if(Auth::user()->profile_pic==null)
        {
            $file=$request->file('file');
            $filename=uniqid().$file->getClientOriginalName();
            if(!file_exists('images'))
            {
                mkdir('images',0777,true);
            }
            $file->move('images',$filename);
            /*
            **  Resize image to 720 * x
            */
            Image::make('images/'.$filename)->resize(720,null,function ($constraint){$constraint->aspectRatio();})->save('images/'.$filename,100);
            /*
            **  Thumbs creation
            */

            if(!file_exists('images/thumbs'))
            {
                mkdir('images/thumbs',0777,true);
            }

            $thumb=Image::make('images/'.$filename)->resize(256,null,function ($constraint){$constraint->aspectRatio();})->save('images/thumbs/'.$filename,50);
            /*
            **  Thumbnail Creation
            */

            if(!file_exists('images/thumbnails'))
            {
                mkdir('images/thumbnails',0777,true);
            }

            $thumb=Image::make('images/'.$filename)->resize(64,64)->save('images/thumbnails/'.$filename,50);
            /*
            **  End of image saving procedure
            */


            $user=User::find(Auth::user()->id);
            $image=$user->images()->create([
                'user_id'=>Auth::user()->id,
                'file_name'=>$filename,
                'file_size'=>$file->getClientSize(),
                'file_mime'=>$file->getClientMimeType(),
                'file_path'=>'images/'.$filename
            ]);

            User::find(Auth::user()->id)
                ->update(array(
                'profile_pic' =>$image->id,
                ));

            return $image;
        }
        else
        {
            return response("already uploaded",401);
        }

    }

    public function create(Request $request)
    {
        $faculty=faculty::where('name',$request->input('faculty_id'))->first();

        if($request->input('symbol_number')==0)
            $request->merge(array( 'symbol_number' => null ));
        if($request->input('phone_number')==0)
            $request->merge(array( 'phone_number' => null ));
           

        $validation=student::validateStudent($request->all());
        if($validation->fails()){

            return redirect('/registration-form')
                ->withErrors($validation)->withInput();
        }

        else
        {
            student::create(array(
                'id' => Auth::user()->id,
                'first_name' => Input::get('first_name'),
                'middle_name' => Input::get('middle_name'),
                'last_name' => Input::get('last_name'),
                'parent_name' => Input::get('parent_name'),
                'date_of_birth' => Input::get('date_of_birth'),
                'gender' => Input::get('gender'),
                //'religion' => Input::get('religion'),
                'permanent_address' => Input::get('permanent_address'),
                'current_address' => Input::get('current_address'),
                'symbol_number' => Input::get('symbol_number'),
                'registration_number' => Input::get('registration_number'),
                'batch' => Input::get('batch'),
                'faculty_id'=>$faculty->id,
                'mobile_number' => Input::get('mobile_number'),
                'phone_number' => Input::get('phone_number'),
            ));

            if(Auth::user()->profile_pic!=null)
            {
                User::where('id',Auth::user()->id)->update(array(
                    'active' => '1'
                ));
            }

            return redirect('/');

        }

    }    


    public function edit($student_id)
    {
        return View::make('student.editstudent',['title'=>'Edit'])
            ->with('detail',student::find($student_id))
            ->with('faculties',faculty::all());
    }

    public function update(Request $request)
    {
        //nullable of number
        if($request->input('symbol_number')==0)
            $request->merge(array( 'symbol_number' => null ));
        if($request->input('phone_number')==0)
            $request->merge(array( 'phone_number' => null ));


        $validation=student::validateStudent($request->all());
        if($validation->fails()){

             return redirect()->route('studentRegister-form')
                ->withErrors($validation)->withInput();            
        }

        else
        {
            $faculty=faculty::where('name',$request->input('faculty_id'))->first();

            student::where('id',Auth::user()->id)->update(array(
                'first_name' => Input::get('first_name'),
                'middle_name' => Input::get('middle_name'),
                'last_name' => Input::get('last_name'),
                'parent_name' => Input::get('parent_name'),
                'date_of_birth' => Input::get('date_of_birth'),
                'gender' => Input::get('gender'),
                'religion' => Input::get('religion'),
                'permanent_address' => Input::get('permanent_address'),
                'current_address' => Input::get('current_address'),
                'symbol_number' => Input::get('symbol_number'),
                'registration_number' => Input::get('registration_number'),
                'batch' => Input::get('batch'),
                'faculty_id'=>$faculty->id,
                'mobile_number' => Input::get('mobile_number'),
                'phone_number' => Input::get('phone_number'),
            ));

            Session::put('message',Input::get('first_name').' Updated successfully');
            return redirect('/register');

        }
    }


}
