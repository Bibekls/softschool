<?php
namespace HCCNetwork\Http\Controllers\Student;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\faculty;
use HCCNetwork\book;
use HCCNetwork\student; //always include related model
use HCCNetwork\BookIssue;
use HCCNetwork\BookReturn;
use HCCNetwork\image;
use HCCNetwork\faq;
use HCCNetwork\url;
use HCCNetwork\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Html;

class studentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student');      
    }

    public function index()
    {
        return redirect('student/profile');

        /*return View::make('student.newsfeed',['title'=>'newsfeed'])
            ->with('student',student::find(Auth::user()->id))
            ->with('profile_pic',image::find(Auth::user()->profile_pic));*/
    }

    public function dashboard()
    {
        return View::make('student.dashboard',['title'=>'dashboard'])
            ->with('student',student::find(Auth::user()->id))
            ->with('profile_pic',image::find(Auth::user()->profile_pic));
    }

     public function profile()
    {
        Session::put('active','timeline');
        return View::make('student.profile',['title'=>'profile'])
            ->with('detail',student::find(Auth::user()->id))
            ->with('books',book::wherein('id',BookIssue::where('student_id',Auth::user()->id)->where('state',1)->lists('book_id'))->get())
            ->with('student',student::find(Auth::user()->id))
            ->with('profile_pic',image::find(Auth::user()->profile_pic));
    }

    public function faq()
    {
        return View::make('student.faq',['title'=>'FAQ'])
            ->with('faqs',faq::where('user_type',Auth::user()->type)->get())
            ->with('profile_pic',image::find(Auth::user()->profile_pic));
    }

    public function Url()
    {
        return View::make('student.url',['title'=>'FAQ'])
            ->with('urls',url::all() )
            ->with('profile_pic',image::find(Auth::user()->profile_pic));
    }
    public function UrlCreate()
    {
         $validation=url::validateUrl(Input::all());
        if($validation->fails()){               
                Session::put('active','addUrl');

            return redirect()->route('studentFullProfile')                
                ->withErrors($validation)->withInput();
        }

        else
        {           
            url::create(array(
                'user_id' => Auth::user()->id,
                'title' => Input::get('title'),
                'url' => Input::get('url'),
                'description' => Input::get('description')                
            ));

            Session::put('message',Input::get('fname').' added successfully');
            return redirect()->route('studentProfile')
                ->with('active','addUrl');

        }
       
    }














    public function form()
    {
        return View::make('student.addstudent',['title'=>'Add-Student'])
            ->with('faculties',faculty::all())
            ->with('counter','1');
    }

    public function searchForm()
    {
        return View::make('librarian.student.searchstudent',['title'=>'Search-Student'])
                ->with('student',null)
                ->with('students',null)
                ->with('counter',0);;
    }

    public function searchReturn()
    {
        if(Input::get('student_id')!=null)
        {
            return View::make('librarian.student.searchstudent',['title'=>'Search-Student'])
                ->with('student',student::find(Input::get('student_id')))
                ->with('students',null)
                ->with('counter',0);
        }
        else if(Input::get('f_name')!=null)
        {
            return View::make('librarian.student.searchstudent',['title'=>'Search-Student'])
                ->with('students',student::where('f_name',Input::get('f_name'))
                    ->orWhere('f_name', 'like', '%' . Input::get('f_name') . '%')
                    ->get())
                ->with('student',null)
                ->with('counter',0);
        }
        else
            return View::make('librarian.student.searchstudent',['title'=>'Search-Student'])
                ->with('student',null)
                ->with('students',null)
                ->with('counter',0);
    }

    public function create()
    {

        $validation=student::validateStudent(Input::all());
        if($validation->fails()){

            return redirect()->route('add-student-form')
                ->withErrors($validation)->withInput();
        }

        else
        {
            $faculty=faculty::where('faculty',Input::get('faculty'))->first();
            student::create(array(
                'fname' => Input::get('fname'),
                'lname' => Input::get('lname'),
                'mother' => Input::get('mother'),
                'dob' => Input::get('dob'),
                'gender' => Input::get('gender'),
                'religion' => Input::get('religion'),
                'p_address' => Input::get('p_address'),
                'c_address' => Input::get('c_address'),
                'symbol_no' => Input::get('symbol_no'),
                'reg_no' => Input::get('reg_no'),
                'batch' => Input::get('batch'),
                'faculty'=>$faculty->fid,
                'mobile' => Input::get('mobile'),
                'pno' => Input::get('pno'),
                'email' => Input::get('email')
            ));

            Session::put('message',Input::get('fname').' added successfully');
            return redirect()->route('list-students');

        }

    }

    public function listStudent()
    {

        return View::make('librarian.student.liststudent',['title'=>'List-Student'])
            ->with('students',student::all());

    }

    public function delete()
    {
        Session::put('message',Input::get('student_id').' deleted successfully');
        student::where('student_id', '=', Input::get('student_id'))->delete();
        return redirect()->action('StudentController@listStudent');

    }
    public function edit($student_id)
    {
        return View::make('student.editstudent',['title'=>'Edit'])
            ->with('detail',student::find($student_id))
            ->with('faculties',faculty::all());
    }

    public function update()
    {
        $validation=student::validateStudentUpdate(Input::all());
        if($validation->fails()){

            return redirect()->route('studentEdit',Input::get('student_id'))
                ->withErrors($validation)->withInput();
        }

        else
        {
            $faculty=faculty::where('faculty',Input::get('faculty'))->first();
            student::where('student_id',Input::get('student_id'))->update(array(
                'fname' => Input::get('fname'),
                'lname' => Input::get('lname'),
                'mother' => Input::get('mother'),
                'dob' => Input::get('dob'),
                'gender' => Input::get('gender'),
                'religion' => Input::get('religion'),
                'p_address' => Input::get('p_address'),
                'c_address' => Input::get('c_address'),
                'symbol_no' => Input::get('symbol_no'),
                'reg_no' => Input::get('reg_no'),
                'batch' => Input::get('batch'),
                'faculty'=>$faculty->fid,
                'mobile' => Input::get('mobile'),
                'pno' => Input::get('pno'),
                'email' => Input::get('email')
            ));

            Session::put('message',Input::get('fname').' Updated successfully');
            return redirect()->route('list-students');

        }
    }
    public function info($student_id)
    {

        return View::make('librarian.student.studentinformation',['title'=>'Student Information'])
            ->with('detail',student::find($student_id))
            ->with('books',book::wherein('book_id',BookIssue::where('student_id',$student_id)->where('state',1)->lists('book_id'))->get());

    }
}
