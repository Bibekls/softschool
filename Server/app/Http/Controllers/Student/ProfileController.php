<?php
namespace HCCNetwork\Http\Controllers\Student;

use HCCNetwork\Borrow;
use HCCNetwork\Http\Controllers\Controller;

use View;
use Session;
use Carbon;

use HCCNetwork\User;
use HCCNetwork\book;
use HCCNetwork\student;
use HCCNetwork\BookIssue;
use HCCNetwork\Post;
use HCCNetwork\image;
use HCCNetwork\Comment;
use HCCNetwork\Url;

use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\student\PostRequest;
use HCCNetwork\Http\Requests\student\CommentRequest;
use HCCNetwork\Http\Requests\student\UrlRequest;

use Illuminate\Support\Facades\Auth;


class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student');
    }

    public function profile(Request $request)
    {
        $data = array();
        $newsfeed = array();
        foreach ($pagelink = Post::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(10) as $post) {
            $data['post'] = $post;

            $data['postTime'] = Carbon::createFromTimeStamp(strtotime($post->created_at))->toDayDateTimeString() . " " . Carbon::now()->subSeconds(Carbon::now()->diffInSeconds($post->created_at))->diffForHumans();

            $data['profile_pic'] = User::join('images', 'images.id', '=', 'users.profile_pic')
                ->join('posts', 'posts.user_id', '=', 'users.id')
                ->where('users.id', $post->user_id)
                ->select('images.file_name as profile_pic')
                ->first();
            $data['comments'] = User::join('images', 'users.profile_pic', '=', 'images.id')
                ->join('comments', 'users.id', '=', 'comments.user_id')
                ->join('posts', 'comments.post_id', '=', 'posts.id')
                ->where('comments.post_id', $post->id)
                ->select('images.file_name as profile_pic', 'comments.id', 'comments.comment', 'comments.id', 'users.id as user_id', 'users.name as username', 'comments.created_at as time')
                ->get();
            array_push($newsfeed, $data);
        }


        if (!Session::has('active')) Session::put('active', 'addpost');

        return View::make('student.profile', ['title' => 'profile'])
            ->with('detail', student::find(Auth::user()->id))
            ->with('books',Borrow::join('sub_books','borrow.sub_code','=','sub_books.id')
                ->join('books','sub_books.book_id','=','books.id')
                ->where('borrow.user_id',Auth::id())
                ->where('borrow.return_date',null)
                ->get())
                //book::wherein('id', BookIssue::where('student_id', Auth::user()->id)->where('state', 1)->lists('book_id'))->get())
            ->with('student', student::find(Auth::user()->id))
            ->with('profile_pic', image::find(Auth::user()->profile_pic))
            ->with('newsfeed', $newsfeed)
            ->with('pagelink', $pagelink)
            ->with('counter', 0);
    }

    public function post(PostRequest $request)
    {
        Post::create(array(
            'post' => $request->input('post'),
            'user_id' => Auth::user()->id,
        ));

        return redirect('newsfeed');
    }

    public function comment(CommentRequest $request)
    {
        Comment::create(array(
            'comment' => $request->input('comment'),
            'user_id' => Auth::user()->id,
            'post_id' => $request->input('post_id'),
        ));

        return redirect()->back();
    }

    public function deletePost(Request $request)
    {
        if (Post::find($request->input('post_id'))->user->id == Auth::user()->id) {
            Post::find($request->input('post_id'))->delete();
        }
        return redirect()->back();
    }

    public function deleteComment(Request $request)
    {
        if (Comment::find($request->input('comment_id'))->user->id == Auth::user()->id) {
            Comment::find($request->input('comment_id'))->delete();
        }
        return redirect()->back();
    }

    public function url(UrlRequest $request)
    {
        url::create(array(
            'user_id' => Auth::user()->id,
            'title' => $request->input('title'),
            'url' => $request->input('url'),
            'description' => $request->input('description'),
        ));

        return redirect('/url');
    }
}
