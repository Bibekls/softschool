<?php
namespace HCCNetwork\Http\Controllers\Student;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\Post;
use HCCNetwork\url;
use HCCNetwork\Feedback;
use Carbon;

use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\student\FeedbackRequest;
use Illuminate\Support\Facades\Auth;


class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student');      
    }  
    public function addFeedback(FeedbackRequest $request)
    {      
        Feedback::create(array(
                'feedback' => $request->input('feedback'),
                'user_id' => Auth::user()->id,
        ));
        return redirect()->back();
    }

    public function deleteFeedback(Request $request)
    {        
        if(Feedback::find($request->input('feedback_id'))->user->id==Auth::user()->id){
            Feedback::find($request->input('feedback_id'))->delete();            
        } 
        return redirect()->back();
    }

     public function getFeedback()
    {
        return View::make('student.feedback',['title'=>'Feedback'])
                    ->with('profile_pic',image::find(Auth::user()->profile_pic))
                    ->with('feedbacks',Feedback::join('users','feedbacks.user_id','=','users.id')
                                                ->join('images','images.id','=','users.profile_pic')
                                                ->select('feedbacks.*','images.file_name as profile_pic')
                                                ->orderBy('created_at','desc')->paginate(5));
                    
    }
      
}
