<?php
namespace HCCNetwork\Http\Controllers\Student;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\mis\routine\RoutineRequest;
use HCCNetwork\Routine;
use HCCNetwork\Teacher;
use HCCNetwork\Subject;
use HCCNetwork\BatchLevelMaping;
use HCCNetwork\User;
use HCCNetwork\faculty;

class RoutineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student');
    }  
    public function routine()
    {   
        $level_value=BatchLevelMaping::where('faculty_id',Auth::user()->student->faculty_id)
                                                                ->where('batch',Auth::user()->student->batch)
                                                                ->pluck('level')
                                                                ->first();

        $days=Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                    ->where('subjects.faculty_id',Auth::user()->student->faculty_id)
                    ->where('subjects.level',$level_value)
                    ->select('day')
                    ->groupBy('day')
                    ->get()
                    ->toArray();

        $level=array();

        foreach ($days as $day){
            
            $day['name']=$day['day'];
            $day['period']=Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
                ->join('teachers', 'routines.teacher_id', '=', 'teachers.id')
                ->where('subjects.faculty_id',Auth::user()->student->faculty_id)
                ->where('subjects.level',$level_value)
                ->where('routines.day',$day['day'])
                ->select('routines.id as id','subjects.name as subject','teachers.first_name as fname','teachers.last_name as lname','routines.start_at as start_at','routines.end_at as end_at')                                
                ->orderBy('routines.start_at')
                ->get();
            array_push($level,$day);                                
        }
       return View::make('student.routine')
        ->with('title','Routine')
        ->with('routines',$level);
    }  
    
}