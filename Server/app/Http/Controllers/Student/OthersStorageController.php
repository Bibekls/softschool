<?php
namespace HCCNetwork\Http\Controllers\Student;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\Post;
use HCCNetwork\Url;
use Carbon;
use Storage;

use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Folders
{
	public $folderName;
}
class Files
{
	public $fileName;
	public $extension;	
	public $lastModifiedDate;
	public $size;

}

class OthersStorageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student');      
    }  
    public function baseUrl($user_id){
        $user=User::find($user_id);
    	return $baseUrl=$user->user_type_id."/".$user->name.'/public/';
    }

    public function getFolders($directory){
        /*
        **  Directory 
        */
        $folders=array();
        foreach(Storage::directories($directory) as $folder){ 
            $fileObject=new Folders;
            $info=pathinfo($folder);
            $fileObject->folderName=$info['basename'];                   
            
            array_push($folders,$fileObject);
        }
        return $folders;
    }

    public function getFiles($directory){        
        /*
        **  Files
        */
        $files=array();
        foreach(Storage::files($directory) as $file){
            $fileObject=new Files;
            $info=pathinfo($file);
            $fileObject->fileName=$info['basename'];
            $fileObject->extension=$info['extension'];
            $fileObject->size = Storage::size($file);
            $fileObject->lastModifiedDate = Storage::lastModified($file);
            
            array_push($files,$fileObject);
        }
        return $files;
    }      

    public function directory(Request $request)
    {
        if($request->input('directory')!=null){
            if($request->input('folderName')!=null){
                $directory=$request->input('directory').'/'.$request->input('folderName');
            } 
            else $directory=$request->input('directory');
        }          
        else $directory=$request->input('folderName');
        
        if($directory=='.')$directory=null;
        return View::make('student.others-storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl($request->input('user_id')).$directory))
                        ->with('files',$this->getFiles($this->baseUrl($request->input('user_id')).$directory))
                        ->with('directory',$directory)
                        ->with('user_id',$request->input('user_id'))
                        ->with('view',false);                                  
                              
    }
    public function oneLevelUp(Request $request)
    { 
        $directory=dirname($request->input('directory'));
        if($directory=='.')$directory=null;
        return View::make('student.others-storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl($request->input('user_id')).$directory))
                        ->with('files',$this->getFiles($this->baseUrl($request->input('user_id')).$directory))
                        ->with('directory',$directory)
                        ->with('user_id',$request->input('user_id'))
                        ->with('view',false);                                  
    }
    
    public function view(Request $request)
    {
        $directory=$this->baseUrl($request->input('user_id')).$request->input('directory').'/'.$request->input('fileAndFolder');
        $fullDirectory='../storage/app/'.$directory;
             
        if(is_file($fullDirectory)) {
            $tempfile='temp/'.uniqid().$request->input('fileAndFolder');
            Storage::copy($directory,$tempfile);
            rename('../storage/app/'.$tempfile,$tempfile);            
        }else{ 
            return redirect()->back();
        }
        return View::make('student.others-storage',['title'=>'Storage'])
                        ->with('folders',$this->getFolders($this->baseUrl($request->input('user_id')).$directory))
                        ->with('files',$this->getFiles($this->baseUrl($request->input('user_id')).$directory))
                        ->with('directory',$tempfile)
                        ->with('user_id',$request->input('user_id'))
                        ->with('view',true);   
    
    }
    
    public function download(Request $request)
    {
        $directory=$this->baseUrl($request->input('user_id')).$request->input('directory').'/'.$request->input('fileAndFolder');
        $fullDirectory='../storage/app/'.$directory;
             
        if(is_file($fullDirectory)) {
            $tempfile='temp/'.uniqid().$request->input('fileAndFolder');
            Storage::copy($directory,$tempfile);
            rename('../storage/app/'.$tempfile,$tempfile);
            return redirect($tempfile);
        }else{ 
            return redirect()->back();
        }
    
    }
  
}
