<?php
namespace HCCNetwork\Http\Controllers\Student;
use HCCNetwork\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\mis\routine\RoutineRequest;
use HCCNetwork\Routine;
use HCCNetwork\student;
use HCCNetwork\Subject;
use HCCNetwork\User;
use HCCNetwork\faculty;
use HCCNetwork\Attendance;

class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student');
    } 

    public function attendance(Request $request)
    {

        $studentId=Auth::user()->id;
        
        /*$dateRange=(explode('-', $request->input('date-range')));
        $start_date=date_format(date_create($dateRange[0]),"Y-m-d");
        $end_date=date_format(date_create($dateRange[1]),"Y-m-d");        
        */
        $start_date=date_format(date_create('1/1/2013'),"Y-m-d");
        $end_date=date_format(date_create('1/1/2020'),"Y-m-d");    

        $batch= array();
        $batch['name']=$request->input('batch');
        $batch['attendance-date']=$request->input('date');
        
        $dates=Attendance:: where('student_id',$studentId)
                            ->whereBetween('date',[$start_date,$end_date])
                            ->select('date')                                        
                            ->orderBy('date','desc')
                            ->groupBy('date')
                            ->get()
                            ->toArray();

        $subjects=Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                            ->where('attendances.student_id',$studentId)
                            ->select('subjects.id as id','subjects.name as subject_name')
                            ->orderBy('subjects.name')
                            ->groupBy('subjects.name')
                            ->get()
                            ->toArray();

        $batch['subject-list']=$subjects;
        $batch['date']=array();

            foreach ($dates as $date){                   
                $day['current-date']=$date['date'];
                $day ['subjects']=array();
                    foreach($subjects as $subject){

                        $subAttendance=Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                                        ->where('attendances.student_id',$studentId)
                                        ->where('attendances.date',$day['current-date'])
                                        ->where('subjects.name',$subject['subject_name'])
                                        ->select('attendances.attendance as attendance') 
                                        ->orderBy('subjects.name')
                                        ->groupBy('subjects.name')
                                        ->get(); 

                                    if(count($subAttendance)==0){
                                        $subAttendance = Attendance::limit(1)->get();
                                        $subAttendance[0]->attendance=2;
                                    }          

                        array_push($day['subjects'],$subAttendance);

                    }
                array_push($batch['date'],$day);                                
            }

        return View::make('student.attendance')
        ->with('title','Attendance-Daywise')
        ->with('dateRange','1/1/2013-1/1/2020')
        ->with('batch',$batch);        
    }

    public function customAttendance(Request $request)
    {

        $studentId=Auth::user()->id;
        
        $dateRange=(explode('-', $request->input('date-range')));
        $start_date=date_format(date_create($dateRange[0]),"Y-m-d");
        $end_date=date_format(date_create($dateRange[1]),"Y-m-d");        
    

        $batch= array();
        $batch['name']=$request->input('batch');
        $batch['attendance-date']=$request->input('date');
        
        $dates=Attendance:: where('student_id',$studentId)
                            ->whereBetween('date',[$start_date,$end_date])
                            ->select('date')                                        
                            ->orderBy('date','desc')
                            ->groupBy('date')
                            ->get()
                            ->toArray();

        $subjects=Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                            ->where('attendances.student_id',$studentId)
                            ->select('subjects.id as id','subjects.name as subject_name')
                            ->orderBy('subjects.name')
                            ->groupBy('subjects.name')
                            ->get()
                            ->toArray();

        $batch['subject-list']=$subjects;
        $batch['date']=array();

            foreach ($dates as $date){                   
                $day['current-date']=$date['date'];
                $day ['subjects']=array();
                    foreach($subjects as $subject){

                        $subAttendance=Attendance::join('subjects', 'attendances.subject_id', '=', 'subjects.id')
                                        ->where('attendances.student_id',$studentId)
                                        ->where('attendances.date',$day['current-date'])
                                        ->where('subjects.name',$subject['subject_name'])
                                        ->select('attendances.attendance as attendance') 
                                        ->orderBy('subjects.name')
                                        ->groupBy('subjects.name')
                                        ->get();           

                        array_push($day['subjects'],$subAttendance);

                    }
                array_push($batch['date'],$day);                                
            }

        return View::make('student.attendance')
        ->with('title','Attendance-Daywise')
        ->with('dateRange',$request->input('date-range'))
        ->with('batch',$batch);        
    }
    
    
}