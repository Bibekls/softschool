<?php
namespace HCCNetwork\Http\Controllers\Student;
use HCCNetwork\Http\Controllers\Controller;

use View;  //most important
use Input;  //you should add input to app.php in alishes array
use Session;
use HCCNetwork\User;
use HCCNetwork\image;
use HCCNetwork\Post;
use HCCNetwork\url;
use HCCNetwork\Feedback;
use Carbon;

use HCCNetwork\Http\Requests;
use Illuminate\Http\Request;
use HCCNetwork\Http\Requests\student\FeedbackRequest;
use Illuminate\Support\Facades\Auth;


class OtherUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student');      
    }  
    public function users()
    {   
        return View::make('student.other-users',['title'=>'Other-Users'])
        				->with('details',User::join('images','users.profile_pic','=','images.id')
        									->join('user_types','users.user_type_id','=','user_types.id')
        									->orderBy('users.name')
        									->select('users.id as user_id','users.name as username','images.file_name as profile_pic','user_types.name as user_type')
        									->get()
    										);
    }
      
}
