<?php
namespace HCCNetwork\Http\Controllers\Attendance;
use HCCNetwork\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\student;



class GetBatchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getBatch()
    {
    	$batchs=student::select('batch')  
                        ->groupBy('batch')
                        ->orderBy('batch')                             
                        ->get();
                                

       return response (json_encode($batchs),200); 
    }    
}
