<?php
namespace HCCNetwork\Http\Controllers\Attendance;
use HCCNetwork\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\student;
use HCCNetwork\Routine;
use HCCNetwork\Subject;
use HCCNetwork\BatchLevelMaping;



class GetSubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getSubject($id,$batch)
    {
    	$subjects=Subject::join('routines','subjects.id','=','routines.subject_id')
                        ->join('batch_level_maping','subjects.level','=','batch_level_maping.level')
                        ->join('faculties','subjects.faculty_id','=','faculties.id')
                        ->where('routines.teacher_id',$id)  
                        ->where('batch_level_maping.batch',$batch)
                        ->select('subjects.name as subject','faculties.name as faculty')
                        ->groupBy('subject')
                        ->orderBy('subject')                             
                        ->get();
                                

       return response (json_encode($subjects),200); 
    }    
}
