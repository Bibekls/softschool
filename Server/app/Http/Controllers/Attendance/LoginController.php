<?php
namespace HCCNetwork\Http\Controllers\Attendance;
use HCCNetwork\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\User;

class LoginController extends Controller
{
	public function login(Request $request)
    {
     	 	if (Auth::attempt(['name' => $request->input('username'),'password' => $request->input('password')])) {
     	 		User::where('id',Auth::user()->id) ->update(array(
  														'api_token'=>str_random(60),
  														));
     	 		// Refresh new auth with new token
     	 		Auth::attempt(['name' => $request->input('username'),'password' => $request->input('password'),'user_type_id'=>3]);
      		return response (json_encode(Auth::user()),200);
      	}      	
      	else return response (json_encode(['message'=>'Unauthorized User']),401);    	
    }
}
