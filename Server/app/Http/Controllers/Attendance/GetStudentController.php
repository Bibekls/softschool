<?php
namespace HCCNetwork\Http\Controllers\Attendance;
use HCCNetwork\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\student;
use HCCNetwork\Routine;
use HCCNetwork\Attendance;
use HCCNetwork\Subject;



class GetStudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getStudent($batch,$subject)
    {
        $subjectId=Subject::where('name',$subject)->pluck('id')->first();
        
        if(Attendance::where('subject_id',$subjectId)->where('date',date("Y-m-d"))->first()==null){
                $students=Subject::join('students','subjects.faculty_id','=','students.faculty_id')
                            ->join('routines','subjects.subject_id','=','routines.subject_id')
                            ->where('students.batch',$batch) 
                            ->where('subjects.name',$subject) 
                            ->select('students.id as id','students.mobile_number as contact','students.first_name as fname','students.last_name as lname')
                            ->groupBy('fname')
                            ->orderBy('fname')                             
                            ->get();
            return response (json_encode($students),200);

        }

        else {
            $students=Subject::join('students','subjects.faculty_id','=','students.faculty_id')
                            ->join('routines','subjects.subject_id','=','routines.subject_id')
                            ->where('students.batch',$batch) 
                            ->where('subjects.name',$subject) 
                            ->select('students.id as id','students.mobile_number as contact','students.first_name as fname','students.last_name as lname')
                            ->groupBy('fname')
                            ->orderBy('fname')                             
                            ->get();
            return response (json_encode($students),400);
        }	
       
    }    
}
