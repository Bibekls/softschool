<?php
namespace HCCNetwork\Http\Controllers\Attendance;
use HCCNetwork\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use HCCNetwork\student;
use HCCNetwork\Subject;
use HCCNetwork\Attendance;



class MakeAttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function makeAttendance($id,$subject,Request $request)
    {        
        foreach($request->data as $attendance)
        {            
            $subjectId=Subject::where('name',$subject)->pluck('id')->first();
            if(Attendance::where('subject_id',$subjectId)
                            ->where('student_id',$attendance['id'])
                            ->where('date',date("Y-m-d"))                            
                            ->first()==null
                            )
            {
                Attendance::create(array(
                    'student_id'=>$attendance['id'],
                    'teacher_id'=>$id,
                    'attendance'=>$attendance['attendance'],
                    'date'=>date("Y-m-d"),
                    'subject_id'=>Subject::where('name',$subject)->pluck('id')->first(),
                ));

            }else{
                Attendance::where('subject_id',$subjectId)
                            ->where('student_id',$attendance['id'])
                            ->where('date',date("Y-m-d"))
                            ->update(array(
                            'student_id'=>$attendance['id'],
                            'teacher_id'=>$id,
                            'attendance'=>$attendance['attendance'],
                            'date'=>date("Y-m-d"),
                            'subject_id'=>Subject::where('name',$subject)->pluck('id')->first(),
                        ));
            }
        }        
        return response (json_encode(date("Y-m-d").' Attendance transaction for subject '.$subject.' done successfully.'),200); 
    }    
}
