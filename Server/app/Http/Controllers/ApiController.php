<?php

namespace HCCNetwork\Http\Controllers;
use HCCNetwork\book;
use HCCNetwork\Borrow;
use HCCNetwork\CoursePlan;
use HCCNetwork\EventCalendar;
use HCCNetwork\Feedback;
use HCCNetwork\SubBook;
use HCCNetwork\Teacher;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Auth;
use HCCNetwork\Student;
use HCCNetwork\Routine;
use HCCNetwork\Http\Requests\PasswordResetRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    public function login(Request $request)
    {
        $http = new Client();
        $response = $http->post('http://admin.hcc.edu.np' . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                // 'client_secret' => env('APP_CLIENT'),
                'client_secret' =>'N1qmwOvYiPlpJVwQX9sKSk4pi1CUYSsN2IzaZyYv',
                'username' => $request->input("email"),
                'password' => $request->input("password"),
            ],
        ]);
        $userType = null;
        $batch = null;
        $faculty = null;
        $level = null;
        $id = null;
        $profile_pic = null;
        $email = null;

        $firstName = null;
        $middleName = null;
        $lastName = null;
        $otherInfo = null;

        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'name';

        if (Auth::once([$field => $request->input("email"), 'password' => $request->input("password")])) {
            $userType = Auth::user()->user_type_id;
            $id = Auth::user()->id;
            $email = Auth::user()->email;
            // $profile_pic = Auth::user()->profile_picture->file_name;
            // return response()->json(['abject'=>Auth::user()]);
            if ($userType == 4) {
                $batch = Auth::user()->student->batch;
                $faculty = Auth::user()->student->faculty->name;
                $level = Student::join('batch_level_maping', function ($join) {
                    $join->on('students.faculty_id', '=', 'batch_level_maping.faculty_id');
                    $join->on('students.batch', '=', 'batch_level_maping.batch');
                })->where('students.id', Auth::id())->first()->level;
                $firstName = Auth::user()->student->first_name;
                $middleName = Auth::user()->student->middle_name;;
                $lastName = Auth::user()->student->last_name;
                $otherInfo = Student::with('faculty', 'user')->find(Auth::id());
                
                
//                $otherInfo = Student::join('faculties', 'students.faculty_id', '=', 'faculties.id')
//                    ->select(
//                        'faculties.name as faculty',
//                        'students.date_of_birth as dob',
//                        'students.gender as gender',
//                        'students.permanent_address as permanent_address',
//                        'students.temporary_address as temporary_address',
//                        'students.symbol_number as symbol_number',
//                        'students.registration_number as registration_number',
//                        'students.batch as batch',
//                        'students.mobile_number as mobile_number',
//                        'students.phone_number as phone_number',
//                        'student.parent_name as parent_name'
//                    )
//                    ->get();

            }

            if ($userType == 3) {
                $firstName = Auth::user()->teacher->first_name;
                $middleName = Auth::user()->teacher->middle_name;
                $lastName = Auth::user()->teacher->last_name;
                $otherInfo = Teacher::with('faculties', 'user')->find(Auth::id());
                $otherInfo->level = json_encode(unserialize($otherInfo->level));
                           

                // return response()->json([$otherInfo]);
//                $otherInfo = Teacher::join('faculty_teacher','teachers.id','=','faculty_teacher.teacher_id')
//                    ->join('faculties', 'faculty_teacher.faculty_id', '=', 'faculties.id')
//                    ->select(
//                        'faculties.name as faculty',
//                        'students.date_of_birth as dob',
//                        'students.gender as gender',
//                        'students.permanent_address as permanent_address',
//                        'students.temporary_address as temporary_address',
//                        'students.symbol_number as symbol_number',
//                        'students.registration_number as registration_number',
//                        'students.batch as batch',
//                        'students.mobile_number as mobile_number',
//                        'students.phone_number as phone_number',
//                        'student.parent_name as parent_name'
//                    )
//                    ->get();
            }

            Auth::user()->update([
                'fcm_token' => $request->input('fcm_token')
            ]);
        }

        return [
            "id" => $id,
            "access_token" => json_decode((string)$response->getBody(), true)['access_token'],
            "user_type" => $userType,
            "batch" => $batch,
            "faculty" => $faculty,
            "level" => $level,
            "profile_pic" => $profile_pic,
            "email" => $email,
            "first_name" => $firstName,
            "middle_name" => $middleName,
            "last_name" => $lastName,
            "other_info" => $otherInfo,
        ];

    }

    function getCalendarEvent()
    {
        return EventCalendar::all();
    }

    function resetPassword(PasswordResetRequest $request)
    {
        if (Hash::check($request->input('old-password'), Auth::user()->password)) {
            Auth::user()->update([
                'password' => bcrypt($request['password'])
            ]);
        }
        return response()->json([
            'status' => 'success'
        ]);
    }

    function getRoutine()
    {
        return Routine::join('subjects', 'routines.subject_id', '=', 'subjects.id')
            ->join('teachers', 'routines.teacher_id', '=', 'teachers.id')
            ->join('faculties', 'subjects.faculty_id', '=', 'faculties.id')
            ->select('routines.id as id',
                'teachers.id as teacher_id',
                'teachers.first_name as first_name',
                'teachers.last_name as last_name',
                'routines.start_at as start_at',
                'routines.end_at as end_at',
                'subjects.level as level',
                'subjects.id as subject_id',
                'subjects.name as subject_name',
                'subjects.batch as batch',
                'routines.day as day',
                'faculties.name as faculty'
            )
            ->get();
    }

    function sendFeedback(Request $request)
    {
        Feedback::create([
            "user_id" => Auth::id(),
            "feedback" => $request->input('feedback')
        ]);
        return response()->json([
            "status" => "success"
        ]);
    }

    function searchAvailableBook(Request $request)
    {

// SELECT books.name as name , COUNT(sub_books.id) as available
// FROM sub_books
// JOIN books ON books.id = sub_books.book_id
// WHERE NOT EXISTS(
//        SELECT *
//        FROM borrow
//        WHERE
//    		borrow.sub_code = sub_books.id
//            AND
//            borrow.return_date IS NULL
//    )
//    GROUP BY books.name

//        return DB::select("
//            SELECT books.name as name , COUNT(sub_books.id) as available
//            FROM sub_books
//            JOIN books ON books.id = sub_books.book_id
//            WHERE NOT EXISTS(
//                SELECT *
//                FROM borrow
//                WHERE
//                borrow.sub_code = sub_books.id
//                AND
//                borrow.return_date IS NULL
//            )
//            AND books.name LIKE '%".$request->input('key')."%'
//            AND sub_books.deleted_at is NULL
//            AND books.deleted_at is NULL
//            GROUP BY books.name
//        ");

        return SubBook::select(DB::raw('books.name as name ,faculties.name as faculty , books.publisher as publisher , books.author as author, COUNT(sub_books.id) as available'))
            ->join('books', 'books.id', '=', 'sub_books.book_id')
            ->join('faculties', 'books.faculty_id', '=', 'faculties.id')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw('*'))
                    ->from('borrow','sub_books')
                    ->where('borrow.return_date', null)
                    // this statement is super important because on raw sql it doesnot work and replace with variable on right parameter
                    ->whereRaw ('sub_books.id = borrow.sub_code');
            })
            ->groupBy('books.name')
            ->where('books.name', 'like', '%' . $request->input('key') . '%')
            ->get();

    }

    function getLibraryInfo(Request $request)
    {
        return Borrow::join('sub_books', 'borrow.sub_code', '=', 'sub_books.id')
            ->join('books', 'sub_books.book_id', '=', 'books.id')
            ->where('borrow.user_id', Auth::id())
            ->select('sub_books.id as sub_code', 'books.name as book_name', 'borrow.issue_date as issue_date', 'borrow.return_date as return_date')
            ->get();
    }

    function getCoursePlan()
    {
        return CoursePlan::all();
    }
}
