<?php

namespace HCCNetwork\Http\Middleware\teacher;
use View;
use Closure;
use Session;
use Illuminate\Support\Facades\Auth;

class newteacher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {      
        if( Auth::user()->user_type_id==3 && Auth::user()->active==0 && Auth::user()->verified==1 ) {
            return $next($request);
        }
        else
        {            
            if( Auth::user()->user_type_id==3 && Auth::user()->active==1 && Auth::user()->verified==1 ) {
                    if ($request->ajax()) {
                        return response('Unauthorized.', 401);
                    } else {
                        Auth::logout();

                        Session::put('message','Your Teacher-Registration-Form is under verification process. It may take 1 day to verify.');

                        return redirect('/login');
                    }
            }
            else{
                    if ($request->ajax()) {
                        return response('Unauthorized.', 401);
                    } else {
                        Auth::logout();

                        Session::put('message','Your request is under verification process. It may take 1 day to verify your request.');

                        return redirect('/login');
                    }
            }
           
        }

    }

}
