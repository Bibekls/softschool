<?php

namespace HCCNetwork\Http\Middleware\teacher;

use Closure;
use Illuminate\Support\Facades\Auth;

class teacher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if( Auth::user()->user_type_id==3 && Auth::user()->active==1 && Auth::user()->verified==2) {             
            return $next($request);
        }
        else
        {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {

                return redirect('/upload-profile-pic');
            }
        }

    }

}



