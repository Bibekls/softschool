<?php

namespace HCCNetwork\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use HCCNetwork\Menu;
class webdata
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        if(!$request->session()->has('dataLoaded')){
            $request->session()->put('data',time());
            $request->session()->put('dataLoaded',true);
        }    

        $data= array(); 
        $menus=array();
        
        foreach ($mainmenu=Menu::where('parent_id','=',null)->orderBy('order')->get() as $menu)
        {
            $data['menu']=$menu; 
            $data['submenu']=Menu::where('parent_id',$menu->id)->orderBy('order')->get();
            $data['no_of_submenu']=$data['submenu']->count();
            array_push($menus,$data);            
        }
        $request->session()->put('customMenu',$menus);

        return $next($request);
    }

}
