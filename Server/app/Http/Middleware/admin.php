<?php

namespace HCCNetwork\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if( Auth::user()->user_type_id==1) {
            return $next($request);
        }
        else
        {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                    Auth::logout();

                    Session::put('message','This is not Admin account');

                    return redirect('/login');
            }
        }

    }

}
