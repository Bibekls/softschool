<?php

namespace HCCNetwork\Http\Middleware\student;

use Closure;
use Illuminate\Support\Facades\Auth;

class student
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if( Auth::user()->user_type_id==4 && Auth::user()->active==1 && Auth::user()->verified==2) {             
            return $next($request);
        }
        else
        {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {

                return redirect('/upload-profile-pic');
            }
        }

    }

}



