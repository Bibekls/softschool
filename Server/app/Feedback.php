<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
 	public $table='feedbacks';
    
    protected $fillable = ['user_id','feedback','seen'];

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo('HCCNetwork\User','user_id','id');
    }
   
}

