<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
 	public $table='urls';
    
    protected $fillable = ['user_id','title','url', 'description'];

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo('HCCNetwork\User','user_id','id');
    }
   
}

