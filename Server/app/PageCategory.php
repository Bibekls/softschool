<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class pageCategory extends Model
{
    public $table='page_categories';
    protected $fillable = ['name'];
    
    public function pages()
    {
        return $this->hasMany('HCCNetwork\Page','category_id','id');
    }
}
