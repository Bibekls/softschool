<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;

class BatchLevelMaping extends Model
{
 	public $table='batch_level_maping';
    public $timestamps = false;
    protected $fillable = ['faculty_id','batch','level'];    
    public function getFacultyWiseClassAttribute($value)
    {
        return json_decode($value,true);
    }
}

