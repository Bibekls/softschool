<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class userrequest extends Model
{
   
    protected $fillable = [
        'name', 'email','user_type_id','password','token',
    ];


    protected $hidden = [
        'password'
    ];

    public function userType()
    {
        return $this->belongsTo('HCCNetwork\user_type','user_type_id','id');
    }

    
}
