<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model
{
    public $table = 'notification_user';

    protected $fillable = ['notification_id', 'user_id','fcm_token','status'];

    protected $primaryKey = 'id';


}

