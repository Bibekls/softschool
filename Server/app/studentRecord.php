<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class studentRecord extends Model
{
    protected $fillable = ['id','year','class','roll_no','student_id','faculty_id'];
}
