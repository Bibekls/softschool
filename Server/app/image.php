<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class image extends Model
{
    public $table='images';
    protected $fillable = [
    'user_id','gallery_id','file_name','file_size', 'file_mime','file_path'
    ];

    protected $primaryKey = 'id';
    public function user()
    {
        return $this->belongsTo('HCCNetwork\User','user_id','id');
    }
        public function gallery()
    {
        return $this->belongsTo('HCCNetwork\Gallery','gallery_id','id');
    }
}
