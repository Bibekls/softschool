<?php
namespace HCCNetwork;
use Validator;
use Input;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class book extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public $table='books';
    protected $fillable =  ['id','name', 'author','publisher','ISBN','faculty_id','level','description'];

    protected $primaryKey = 'id';

    public $incrementing = false;

    public function faculty()
    {
        return $this->belongsTo(faculty::class,'faculty_id');
    }

    public function subBook(){
        return $this->hasMany(SubBook::class);
    }

}
