<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;
use HCCNetwork\attendance;

class subject extends Model
{
    public $table='subjects';
    public $timestamps = false;
    protected $fillable = ['name','faculty_id','level','batch'];
    
    public function faculty()
    {
        return $this->belongsTo('HCCNetwork\faculty','faculty_id','id');
    }
    public function subject_attendence(){
        return $this->hasMany('HCCNetwork\attendance');
    }
}