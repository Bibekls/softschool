<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $table='posts';  
	   
    protected $fillable = ['user_id', 'post'];

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo('HCCNetwork\User','user_id','id');
    }    
    public function comments()
    {
        return $this->hasMany('HCCNetwork\Comment','post_id','id');
    }
     
}

