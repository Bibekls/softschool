<?php namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class CoursePlan extends Model {
    public $table='course_plan';
    protected $fillable =  ['id','subject_id','teacher_id','title','description','planned_date','started_date','end_date'];
}