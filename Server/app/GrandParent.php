<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

class GrandParent extends Model
{
    protected $fillable = [
        'id', 'firstName', 'middleName', 'lastName',
        'mobileNumber', 'email'
        ];
}
