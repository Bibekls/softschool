<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guest extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $table = 'guests';

    protected $fillable = [
        'id', 'first_name', 'middle_name', 'last_name',
        'permanent_address', 'current_address', 'gender',
        'mobile_number','phone_number', 'image','description'
    ];


    protected $primaryKey = 'id';

    public function username()
    {
        return $this->hasOne('HCCNetwork\User', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'id');
    }

    public function faculties()
    {
        return $this->belongsToMany(faculty::class, 'faculty_teacher');
    }

    public function faculty()
    {
        return $this->hasMany(FacultyTeacher::class, 'teacher_id', 'id');
    }
}
