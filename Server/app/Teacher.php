<?php

namespace HCCNetwork;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public $table='teachers';
    protected $fillable =  ['id','first_name', 'post','department','image','leaving_date','middle_name','last_name','parent_name','date_of_birth','permanent_address','current_address','gender','joining_date','mobile_number','phone_number'];

    
    protected $primaryKey = 'id';

	public function username()
    {
        return $this->hasOne('HCCNetwork\User','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'id','id');
    }

    public function faculties()
    {
        return $this->belongsToMany(faculty::class, 'faculty_teacher');
    }

    public function faculty(){
	    return $this->hasMany(FacultyTeacher::class,'teacher_id','id');
    }
    public function getDepartmentAttribute($value)
    {
        return json_decode($value,true);
    }
    public function getPostAttribute($value)
    {
        return json_decode($value,true);
    }
}
