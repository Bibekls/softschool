<?php

namespace HCCNetwork;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    use HasApiTokens, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','user_type_id','active','is_admin','verified','profile_pic', 'password','api_token','fcm_token','last_login_ip','last_login_time'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function student()
    {
        return $this->belongsTo('HCCNetwork\student','id','id');
    }
    public function teacher()
    {
        return $this->belongsTo('HCCNetwork\Teacher','id','id');
    }

    public function guest()
    {
        return $this->belongsTo('HCCNetwork\Guest','id','id');
    }

    public function images()
    {
        return $this->hasMany('HCCNetwork\image','user_id','id');
    }
    public function profile_picture()
    {
        return $this->hasOne('HCCNetwork\image','id','profile_pic');
    }
    public function urls()
    {
        return $this->hasMany('HCCNetwork\Url','user_id','id');
    }
    public function sent()
    {
        return $this->hasMany('HCCNetwork\Message','from','id');
    }
    public function inBox()
    {
        return $this->hasMany('HCCNetwork\Message','to','id');
    }

    public function notifications()
    {
        return $this->hasMany('HCCNetwork\Notification','user_id','id');
    }
    public function posts()
    {
        return $this->hasMany('HCCNetwork\Post','user_id','id');
    }
    public function feedbacks()
    {
        return $this->hasMany('HCCNetwork\Feedback','user_id','id');
    }

    public function loginHistory()
    {
        return $this->hasMany('HCCNetwork\loginHistory','user_id','id');
    }
    public function getIsAdminAttribute()
    {
        return true;
    }

    public function userType()
    {
        return $this->belongsTo('HCCNetwork\user_type','user_type_id','id');
    }

    public function findForPassport($identifier) {
        return $this->orWhere('email', $identifier)->orWhere('name', $identifier)->first();
    }


}

