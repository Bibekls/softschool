<?php

namespace HCCNetwork;
use Illuminate\Database\Eloquent\Model;
use Carbon;

class Comment extends Model
{
	public $table='comments';  
	   
    protected $fillable = ['user_id', 'comment','post_id'];

    protected $primaryKey = 'id';

    public function post()
    {
        return $this->belongsTo('HCCNetwork\Post','post_id','id');
    }
    public function user()
    {
        return $this->belongsTo('HCCNetwork\User','user_id','id');
    }
    public function standardDateTime()
    {
    	return Carbon::createFromTimeStamp(strtotime($this->created_at))->toDayDateTimeString()." ".Carbon::now()->subSeconds( Carbon::now()->diffInSeconds($this->created_at) )->diffForHumans();
    }
   
}

