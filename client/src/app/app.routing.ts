import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { UserListsComponent } from './user-manager/user-lists/user-lists.component';
import { UserRegistrationComponent } from './user-manager/user-registration/user-registration.component';
import { SubjectManagementComponent } from './subject-management/subject-management.component';
import { RoutineManagementComponent } from './routine-management/routine-management.component';
import { EventManagementComponent } from './event-management/event-management.component';
import { NotificationManagementComponent } from './notification-management/notification-management.component';
import { StudentAttendenceComponent } from './student-attendence/student-attendence.component';
import { DashboardUserComponent } from './configuration/dashboard-user/dashboard-user.component';
import { BatchLevelMappingComponent } from './configuration/batch-level-mapping/batch-level-mapping.component';
import { FacultyComponent } from './configuration/faculty/faculty.component';

const routes: Routes =[
  { path: 'user-list',           component: UserListsComponent },
  { path: 'user-registration',   component: UserRegistrationComponent },
  { path: 'subject',             component: SubjectManagementComponent },
  { path: 'routine',             component: RoutineManagementComponent },
  { path: 'event',               component: EventManagementComponent },
  { path: 'notification',        component: NotificationManagementComponent },
  { path: 'attendence',          component: StudentAttendenceComponent },
  { path: 'dashboard-user',      component: DashboardUserComponent },
  { path: 'batch',               component: BatchLevelMappingComponent },
  { path: 'faculty',             component: FacultyComponent },
  { path: '',                    redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
