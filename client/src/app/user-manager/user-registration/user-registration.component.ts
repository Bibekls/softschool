import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
declare var $;

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss']
})
export class UserRegistrationComponent implements OnInit {
  public showStudent=false;
  public showGuest=false;
  public showTeacher=true;
  public active1=true;
  public active2=false;
  public active3=false;
  constructor() { }
  public showNotification(){
    // jQuery("#exampleModal").modal("show");
  }
  teacherRegistration(){
    // jQuery("#exampleModal").modal("hide");
    this.showGuest=false;
    this.showTeacher=true;
    this.showStudent=false;
    this.active1=true;
    this.active2=false;
    this.active3=false;
   
  }
  guestRegistration(){
    // jQuery("#exampleModal").modal("hide");
    this.showGuest=true;
    this.showTeacher=false;
    this.showStudent=false;
    this.active1=false;
    this.active2=false;
    this.active3=true;
  }
  studentRegistration(){
    // jQuery("#exampleModal").modal("hide");
    this.showGuest=false;
    this.showTeacher=false;
    this.showStudent=true;
    this.active1=false;
    this.active2=true;
    this.active3=false;
  }
  ngOnInit() {
    this.showNotification();
  }
}
