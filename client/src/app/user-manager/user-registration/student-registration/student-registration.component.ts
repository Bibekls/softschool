import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { UserRegistrationService } from '../user-registration.service';
import swal from 'sweetalert2'
import { Observable } from 'rxjs/Observable';
import "rxjs/Rx";

@Component({
  selector: 'app-student-registration',
  templateUrl: './student-registration.component.html',
  styleUrls: ['./student-registration.component.scss']
})
export class StudentRegistrationComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  cropperReady = false;
  public facultyLists=[]
  public studentForm:FormGroup;
  public exitingParent=false;
  public newParent=false;
  public parentLists=[];
  public check=false;
  public classes=[];
  public batchLists=[]
  public parentEmail:FormControl;
  public email:FormControl;
  public emailTest=false
  public emailparentTest=false;
  constructor(private service:UserRegistrationService) { }
  yearLists = [
    {id:1,batch:2070},
    {id:2,batch:2071},
    {id:3,batch:2072},
    {id:4,batch:2073},
    {id:5,batch:2074},
    {id:6,batch:2075},
    {id:7,batch:2076},
    {id:8,batch:2077},
    {id:9,batch:2078},
    {id:10,batch:2079},
    {id:11,batch:2081},
    {id:12,batch:2082},
    {id:13,batch:2083},
    {id:14,batch:2084},
    {id:15,batch:2085},
    {id:16,batch:2086},
    {id:17,batch:2087},
    {id:18,batch:2088},
    {id:19,batch:2089},
    {id:20,batch:2090},
  ];

  ngOnInit() {
    this.studentForm=new FormGroup({
      userName:new FormControl('',Validators.required),
      email:new FormControl('',Validators.required),
      firstName:new FormControl('',Validators.required),
      middleName:new FormControl(),
      lastName:new FormControl('',Validators.required),
      joinDate:new FormControl('',Validators.required),
      gender:new FormControl('',Validators.required),
      parmanentAddress:new FormControl('',Validators.required),
      currentAddress:new FormControl('',Validators.required),
      symbolNumber:new FormControl('',Validators.required),
      registrationNumber:new FormControl('',Validators.required),
      batch:new FormControl('',Validators.required),
      department:new FormControl('',Validators.required),
      mobileNumber:new FormControl('',Validators.required),
      phoneNumber:new FormControl('',Validators.required),
      parentCheck:new FormControl(''),
      parentFirstName:new FormControl(''),
      parentMiddleName:new FormControl(''),
      parentLastName:new FormControl(''),
      parentMobile:new FormControl(''),
      parentEmail:new FormControl(''),
      parent_id:new FormControl(''),
      class:new FormControl(),
      year:new FormControl()
      
    })
    this.service.getFaculty().subscribe(
      (response)=>{
         this.facultyLists=response.faculties;
         console.log(this.facultyLists)
      }
    )
    this.service.getParent().subscribe(
      (response)=>{
        console.log(response)
        this.parentLists=response.parent;
      }
    )
    this.service.getBatchYear().subscribe(
      (response)=>{
        console.log(response)
        this.batchLists=response.batches;
      }
    )


    this.email = new FormControl();
    this.email.valueChanges
     .debounceTime(400)
     .distinctUntilChanged()
     .subscribe(term => {
       if(term){
          console.log(term)
          this.getEmail(term).subscribe();
       }
});
this.parentEmail = new FormControl();
this.parentEmail.valueChanges
.debounceTime(400)
.distinctUntilChanged()
.subscribe(term => {
  if(term){
    console.log(term)
    this.getParentEmail(term).subscribe();
   }
  });

    this.studentForm.controls.department.valueChanges.subscribe(
      (dept)=>{
         console.log(dept)
         if(dept){
            if(dept.name.toLowerCase()=== "school"){
               this.check=true
               this.classes=dept.faculty_wise_class;
           }else{
            this.check=false

           }
         }
      }
    )

    this.studentForm.controls.parentCheck.valueChanges.subscribe(
      (value)=>{
            console.log(value)
            if(value == 1){
               this.exitingParent=false;
               this.newParent=true;
            }else{
              this.newParent=false;
              this.exitingParent=true; 
            }
        }
      )
  }

  getParentEmail(term){
    return new Observable(observer => {
      if(term){
        this.service.postEmail(term).subscribe(
          (response)=>{
            console.log(response)
            for(let x in response.users){
                if(term.toLowerCase() === response.users[x].email){
                   this.emailparentTest=true;
                }else{
                   this.emailparentTest=false;
                }
             }
           }
         )
       }
     })
  }
  getEmail(term){
    return new Observable(observer => {
      if(term){
        this.service.postEmail(term).subscribe(
          (response)=>{
            console.log(response)
            for(let x in response.users){
                if(term.toLowerCase() === response.users[x].email){
                   this.emailTest=true;
                }else{
                   this.emailTest=false;
                }
             }
           }
         )
       }
     })
   }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    console.log(event)
  }

  imageCropped(image: string) {
    this.croppedImage = image;
    console.log(image)
  }

  imageLoaded() {
    this.cropperReady = true;
  }

  imageLoadFailed() {
    console.log('Load failed');
  }
  submitStudentForm(){
    // if(this.studentForm.valid){
      if(this.studentForm.controls.userName.value){
         if(this.email.value){
           if(this.studentForm.controls.firstName.value){
             if(this.studentForm.controls.lastName.value){
                if(this.studentForm.controls.joinDate.value){
                  if(this.studentForm.controls.gender.value){
                    if(this.studentForm.controls.parmanentAddress.value){
                      if(this.studentForm.controls.department.value){
                        console.log(this.studentForm.controls.department.value)
                        if(this.studentForm.controls.department.value.name.toLowerCase() === 'school'){
                          if(this.studentForm.controls.year.value){
                            if(this.studentForm.controls.class.value){
                              console.log('I am IN')
                              console.log(this.email.value)
                              console.log(this.parentEmail.value)
                              console.log(this.studentForm.value)
                               let data=this.studentForm.value;
                               console.log(this.studentForm.controls.department.value)
                               let department=this.studentForm.controls.department.value.id;
                               data['email']=this.email.value;
                               data['parentEmail']=this.parentEmail.value;
                               data['batch']=''
                               if(this.studentForm.controls.batch.value){
                                  let batch=this.studentForm.controls.batch.value.batch;
                                  data['batch']=batch;
                                }
                               let parent_id=this.studentForm.controls.parent_id.value.id;
                               console.log(department)
                               data['image']=this.croppedImage;
                               data['class']='';
                               if(this.studentForm.controls.class.value){
                                 data['class']=this.studentForm.controls.class.value.faculties;
                               }
                               data['year']=''
                               if(this.studentForm.controls.year.value){
                               data['year']=this.studentForm.controls.year.value.batch
                               }
                               data['department']=department;
                               data['parent_id']=parent_id;
                               console.log('i ma data',data)
                               this.service.postStudent(data).subscribe(
                                (response)=>{
                                  swal({
                                   position: 'top-end',
                                   type: 'success',
                                   title: 'Sucessfully Registed Student!',
                                   showConfirmButton: false,
                                   timer: 4000
                                  })  
                                  // this.studentForm.reset();
                                 },(error)=>{
                                   swal({
                                     position: 'top-end',
                                     type: 'error',
                                     title: 'Unable to Register Student!',
                                     showConfirmButton: false,
                                     timer: 4000
                                   })
                                   // this.studentForm.reset(); 
                                 }
                               )
                            }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Class!',showConfirmButton: false,timer: 4000 })}
                          }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Year!',showConfirmButton: false,timer: 4000 })}
                        }else{
                         if(this.studentForm.controls.batch.value){
                          console.log('I am IN')
                          console.log(this.email.value)
                          console.log(this.parentEmail.value)
                          console.log(this.studentForm.value)
                           let data=this.studentForm.value;
                           console.log(this.studentForm.controls.department.value)
                           let department=this.studentForm.controls.department.value.id;
                           data['email']=this.email.value;
                           data['parentEmail']=this.parentEmail.value;
                           data['batch']=''
                           if(this.studentForm.controls.batch.value){
                              let batch=this.studentForm.controls.batch.value.batch;
                              data['batch']=batch;
                            }
                           let parent_id=this.studentForm.controls.parent_id.value.id;
                           console.log(department)
                           data['image']=this.croppedImage;
                           data['class']='';
                           if(this.studentForm.controls.class.value){
                             data['class']=this.studentForm.controls.class.value.faculties;
                           }
                           data['year']=''
                           if(this.studentForm.controls.year.value){
                           data['year']=this.studentForm.controls.year.value.batch
                           }
                           data['department']=department;
                           data['parent_id']=parent_id;
                           console.log('i ma data',data)
                           this.service.postStudent(data).subscribe(
                            (response)=>{
                              swal({
                               position: 'top-end',
                               type: 'success',
                               title: 'Sucessfully Registed Student!',
                               showConfirmButton: false,
                               timer: 4000
                              })  
                              this.studentForm.reset();
                             },(error)=>{
                               swal({
                                 position: 'top-end',
                                 type: 'error',
                                 title: 'Unable to Register Student!',
                                 showConfirmButton: false,
                                 timer: 4000
                               })
                               // this.studentForm.reset(); 
                             }
                           )
                        }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Batch!',showConfirmButton: false,timer: 4000 })}} 
                      }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Department!',showConfirmButton: false,timer: 4000 })}
                    }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Parmanent Address!',showConfirmButton: false,timer: 4000})}
                  }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Gender!',showConfirmButton: false,timer: 4000})}
                }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Join Date!',showConfirmButton: false,timer: 4000})}
              }else{ swal({position: 'top-end',type: 'error',title: 'Please Fill Last Name!',showConfirmButton: false,timer: 4000})}
           }else{swal({position: 'top-end',type: 'error',title: 'Please Fill First Name!',showConfirmButton: false,timer: 4000 })}      
         }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Email!',showConfirmButton: false,timer: 4000})}
      }else{swal({position: 'top-end',type: 'error',title: 'Please Fill User Name!',showConfirmButton: false,timer: 4000})     }  
   }  
}
