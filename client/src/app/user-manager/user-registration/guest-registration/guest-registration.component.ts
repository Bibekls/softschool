import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { UserRegistrationService } from '../user-registration.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-guest-registration',
  templateUrl: './guest-registration.component.html',
  styleUrls: ['./guest-registration.component.scss']
})
export class GuestRegistrationComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  cropperReady = false;
  public guestForm:FormGroup;
  constructor(private service:UserRegistrationService) { }
  
  ngOnInit() {
    this.guestForm=new FormGroup({
       userName:new FormControl('',Validators.required),
       email:new FormControl('',Validators.required),
       firstName:new FormControl('',Validators.required),
       middleName:new FormControl(),
       lastName:new FormControl('',Validators.required),
       gender:new FormControl(),
       parmanentAddress:new FormControl('',Validators.required),
       currentAddress:new FormControl('',Validators.required),
       mobileNumber:new FormControl('',Validators.required),
       phoneNumber:new FormControl('',Validators.required),
       discription:new FormControl('',Validators.required),
    })
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    console.log(event)
  }

  imageCropped(image: string) {
    this.croppedImage = image;
    console.log(image)
  }

  imageLoaded() {
    this.cropperReady = true;
  }

  imageLoadFailed() {
    console.log('Load failed');
  }
  submitGuestForm(){
    if(this.guestForm.controls.userName.value){
      if(this.guestForm.controls.email.value){
        if(this.guestForm.controls.firstName.value){
          if(this.guestForm.controls.middleName.value){
            if(this.guestForm.controls.lastName.value){
              if(this.guestForm.controls.gender.value){
                if(this.guestForm.controls.parmanentAddress.value){
                  if(this.guestForm.controls.currentAddress.value){
                    if(this.guestForm.controls.mobileNumber.value){
                       if(this.guestForm.controls.phoneNumber.value){
                         if(this.guestForm.controls.discription.value){
                          console.log(this.guestForm.value)
                          let data=this.guestForm.value;
                          data['image']= this.croppedImage;
                          console.log(data) 
                          this.service.postGuest(data).subscribe(
                            (response)=>{
                              console.log(response)
                              this.guestForm.reset();
                              swal({
                                position: 'top-end',
                                type: 'success',
                                title: 'Sucessfully Registed Guest!',
                                showConfirmButton: false,
                                timer: 4000
                              })
                            },(error)=>{
                               console.log('Unable to Insert');
                               swal({
                                position: 'top-end',
                                type: 'error',
                                title: 'Your work has been saved',
                                showConfirmButton: false,
                                timer: 1500
                              })
                            }
                          )
                         }else{
                          swal({
                            position: 'top-end',
                            type: 'error',
                            title: 'Plsease Insert Description',
                            showConfirmButton: false,
                            timer: 1500
                          })
                         }
                       }else{
                        swal({
                          position: 'top-end',
                          type: 'error',
                          title: 'Plsease Insert Phone Number',
                          showConfirmButton: false,
                          timer: 1500
                        })
                       }
                    }else{                        
                    swal({
                      position: 'top-end',
                      type: 'error',
                      title: 'Plsease Insert Mobile Number',
                      showConfirmButton: false,
                      timer: 1500
                    })
                  }
                  }else{
                    swal({
                      position: 'top-end',
                      type: 'error',
                      title: 'Plsease Insert Current Address',
                      showConfirmButton: false,
                      timer: 1500
                    })
                  }
                }else{
                  swal({
                    position: 'top-end',
                    type: 'error',
                    title: 'Plsease Insert Parmanent Address',
                    showConfirmButton: false,
                    timer: 1500
                  })
                }
              }else{
                swal({
                  position: 'top-end',
                  type: 'error',
                  title: 'Plsease Insert Gender',
                  showConfirmButton: false,
                  timer: 1500
                })
              }
            }else{
              swal({
                position: 'top-end',
                type: 'error',
                title: 'Plsease Insert Last Name',
                showConfirmButton: false,
                timer: 1500
              })
            }
          }else{
            swal({
              position: 'top-end',
              type: 'error',
              title: 'Plsease Insert Middle Name',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }else{
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Plsease Insert First Name',
            showConfirmButton: false,
            timer: 1500
          })
        }
      }else{
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Plsease Insert Email',
          showConfirmButton: false,
          timer: 1500
        })
      }
    }else{
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Plsease Insert User Name',
        showConfirmButton: false,
        timer: 1500
      })
    }





















  //  if(this.guestForm.valid){
  //     console.log(this.guestForm.value)
  //     let data=this.guestForm.value;
  //     data['image']= this.croppedImage;
  //     console.log(data) 
  //     this.service.postGuest(data).subscribe(
  //       (response)=>{
  //         console.log(response)
  //         this.guestForm.reset();
  //         swal({
  //           position: 'top-end',
  //           type: 'success',
  //           title: 'Sucessfully Registed Guest!',
  //           showConfirmButton: false,
  //           timer: 4000
  //         })
  //       },(error)=>{
  //          console.log('Unable to Insert');
  //          swal({
  //           position: 'top-end',
  //           type: 'error',
  //           title: 'Your work has been saved',
  //           showConfirmButton: false,
  //           timer: 1500
  //         })
  //       }
  //     )
  //   }
  }
}
