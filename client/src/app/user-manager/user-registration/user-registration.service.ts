import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'
import { ENV } from "../../env";

@Injectable()
export class UserRegistrationService {

  constructor(private http: Http) { }
  postGuest(data) {
    console.log(ENV.Request_URL)
    console.log('I am Ok')
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/post_guest", data, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  private extractData(res: Response) {
    let response = res.json();
    return response || {};
  }
  private handleError(error: Response | any) {
    return Observable.throw(error);
  }
  postTeacher(data) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/post_teacher", data, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  postStudent(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/post_student", data, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getFaculty(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/faculties", options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getParent(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_parent", options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getBatchYear(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_batch", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   postEmail(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/email/"+data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
}
