import { TestBed, inject } from '@angular/core/testing';

import { TeacherRegistrationService } from './teacher-registration.service';

describe('TeacherRegistrationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeacherRegistrationService]
    });
  });

  it('should be created', inject([TeacherRegistrationService], (service: TeacherRegistrationService) => {
    expect(service).toBeTruthy();
  }));
});
