import { Injectable } from '@angular/core';

@Injectable()
export class EditorOptions {
  ImageName: string;
  ImageUrl?: string;
  ImageType?: string;
  File?: File;
  AspectRatios?: Array<RatioType>;
} 
  export interface NgxAspectRatio {
  value: number;
  text: RatioType;
  }
  export type RatioType = "16:9" | '4:3' | '1:1' | '2:3' | 'Default';  
  export const NGX_DEFAULT_RATIOS: Array<NgxAspectRatio> = [
    {
        value: 16 / 9, text: '16:9'
    },
    {
        value: 4 / 3, text: '4:3'
    },
    {
        value: 1 / 1, text: '1:1'
    },
    {
        value: 2 / 3, text: '2:3'
    },
    {
        value: 0 / 0, text: 'Default'
    }
  ];
 
 let  config: EditorOptions = {
      ImageName: 'Some image',
      AspectRatios: ["4:3", "16:9","1:1","2:3","Default"],
      ImageUrl: 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg',
      ImageType: 'image/jpeg'
  }
export class TeacherRegistrationService {

constructor() { 

  }
getConfig(): EditorOptions{
  return config;
 }
}
// https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg