import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup,Validators } from '@angular/forms';
declare const Cropper: any;
import { EditorOptions } from './teacher-registration.service';
import { NgxImageEditorComponent } from "ngx-image-editor";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
declare var jQuery: any;
declare var $;
import { Observable } from 'rxjs/Observable';
import "rxjs/Rx";
import { NgxCropperOption } from 'ngx-cropper';
import { UserRegistrationService } from '../user-registration.service';
import swal from 'sweetalert2'
@Component({
  selector: 'app-teacher-registration',
  templateUrl: './teacher-registration.component.html',
  styleUrls: ['./teacher-registration.component.scss']
})
export class TeacherRegistrationComponent implements OnInit {
  public ImageSrc;
  public config;
  public closeEvent;
  public removeImg;
  public imageEditor = false;
  public teacherForm: FormGroup;
  public ngxCropperConfig: NgxCropperOption;
  posts = new FormControl();
  toppings = new FormControl();
  email: FormControl;
  constructor(private service:UserRegistrationService,public dialog: MatDialog) {}
  imageChangedEvent: any = '';
  croppedImage: any = '';
  public emailTest=false;
  cropperReady = false;
  public passwordTest=false;
  toppingList = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  toppingArray = ['Principal', 'Co-ordinator', 'Teacher'];
  public facultiesLists=[]; 
   ngOnInit() {
    this.teacherForm = new FormGroup({
      username: new FormControl('',Validators.required),
      email: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required),
      confirmPassword: new FormControl('',Validators.required),
      firstName: new FormControl('',Validators.required),
      middleName: new FormControl(),
      post :new FormControl(),
      department : new FormControl(),
      lastName: new FormControl('',Validators.required),
      parentName: new FormControl('',Validators.required),
      gender: new FormControl('',Validators.required),
      joinDate: new FormControl(),
      leaveDate: new FormControl(),
      parmanentAddress: new FormControl('',Validators.required),
      currentAddress: new FormControl('',Validators.required),
      mobileNumber: new FormControl('',Validators.required),
      phoneNumber: new FormControl('',Validators.required)
    })
    this.teacherForm.controls.confirmPassword.valueChanges.subscribe(
      (term)=>{
          this.passwordCheck();
     })
     this.email = new FormControl();
this.email.valueChanges
.debounceTime(400)
.distinctUntilChanged()
.subscribe(term => {
  if(term){
    console.log(term)
    this.getEmail(term).subscribe();
   }
});
     this.service.getFaculty().subscribe(
       (response)=>{
         console.log(response)
         this.facultiesLists=response.faculties;
       }
     )
  }


  public passwordCheck(){
    console.log('confirm Password',this.teacherForm.controls.confirmPassword.value);
    console.log('Password',this.teacherForm.controls.password.value)
    if(this.teacherForm.controls.confirmPassword.value !== this.teacherForm.controls.password.value){
      this.passwordTest=true;
    }else{
      this.passwordTest=false;
    }
  }
  getEmail(term){
    return new Observable(observer => {
      if(term){
        this.service.postEmail(term).subscribe(
          (response)=>{
            console.log(response)
            for(let x in response.users){
                if(term.toLowerCase() === response.users[x].email){
                    this.emailTest=true;
                }else{
                    this.emailTest=false;
                }
             }
           }
         )
       }
     })
   }

  fileChangeEvent(event: any): void {
      this.imageChangedEvent = event;
      console.log(event)
  }

  imageCropped(image: string) {
      this.croppedImage = image;
      console.log(image)
  }

  imageLoaded() {
    this.cropperReady = true;
  }

  imageLoadFailed () {
    console.log('Load failed');
  }
  public submitTeacher() {
    if(this.teacherForm.controls.username.value){
      if(this.email.value){
        if(this.teacherForm.controls.password.value){
           if(this.teacherForm.controls.confirmPassword.value){
              if(this.teacherForm.controls.firstName.value){
                if(this.teacherForm.controls.lastName.value){
                   if(this.teacherForm.controls.post.value){
                      if(this.teacherForm.controls.department.value){
                        if(this.teacherForm.controls.gender.value){
                           if(this.teacherForm.controls.joinDate.value){
                              if(this.teacherForm.controls.parmanentAddress.value){
                                let data=this.teacherForm.value;
                                console.log('test Department',this.teacherForm.controls.department.value)
                                data['departments']=this.teacherForm.controls.department.value;
                                data['posts']=this.teacherForm.controls.post.value;
                                data['image']=this.croppedImage;
                                data['email']=this.email.value;
                                console.log(data)
                                this.service.postTeacher(data).subscribe(
                                  (response)=>{
                                       console.log(response)
                                       swal({
                                        position: 'top-end',
                                        type: 'success',
                                        title: 'Sucessfully Registed Teacher!',
                                        showConfirmButton: false,
                                        timer: 4000
                                      })
                                      // this.teacherForm.reset();
                                  },(error)=>{
                                        swal({
                                          position: 'top-end',
                                          type: 'error',
                                          title: 'Unable to Register Teacher!',
                                          showConfirmButton: false,
                                          timer: 4000
                                        })
                                      // this.teacherForm.reset();
                                    }
                                 )
                              }else{swal({position: 'top-end',type: 'error',title: 'Please Fill parmanent Address',showConfirmButton: false,timer: 4000})}
                           }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Join Date',showConfirmButton: false,timer: 4000})}
                        }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Gender',showConfirmButton: false,timer: 4000})}
                      }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Departmant',showConfirmButton: false,timer: 4000})}
                   }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Post',showConfirmButton: false,timer: 4000})}
                }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Last Name',showConfirmButton: false,timer: 4000})}
              }else{swal({position: 'top-end',type: 'error',title: 'Please Fill First Name',showConfirmButton: false,timer: 4000})}
           }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Confirm Password',showConfirmButton: false,timer: 4000})} 
        }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Password',showConfirmButton: false,timer: 4000})}
      }else{swal({position: 'top-end',type: 'error',title: 'Please Fill Email',showConfirmButton: false,timer: 4000})}
    }else{swal({position: 'top-end',type: 'error',title: 'Please Fill User Name',showConfirmButton: false,timer: 4000})}
  }
}


