import { Injectable } from '@angular/core';
import { ENV } from "../../env";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'
@Injectable()
export class UserListsService {

constructor(private http:Http) { }
getTeacher(){
  console.log(ENV.Request_URL)
    console.log('I am Ok')
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_teacher", options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  private extractData(res: Response) {
    let response = res.json();
    return response || {};
  }
  private handleError(error: Response | any) {
    return Observable.throw(error);
  }
  getFaculty(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/faculties", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getUsers(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/users", options)
      .map(this.extractData)
      .catch(this.handleError);
   } 
   getGuest(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_guest", options)
      .map(this.extractData)
      .catch(this.handleError);
   } 
   getSchoolStudent(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_school_student", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getStudentForUpdate(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/get_student_for_update",data ,options)
      .map(this.extractData)
      .catch(this.handleError); 
   }
   getBatchYear(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_batch", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   updateStudent(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/update_student",data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getSchoolStudentForUpdate(id){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_school_student_for_update/"+id, options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   updateSchoolStudent(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/update_school_student",data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getTeacherForUpdate(id){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_teacher_for_update/"+id, options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   updateTeacher(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/update_teacher",data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getGuestForEdit(id){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_guest_for_update/"+id, options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   postGuest(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/update_guest",data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
}
