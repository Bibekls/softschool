import { Component, OnInit } from '@angular/core';
import { UserListsService } from './user-lists.service';
import { MatTabChangeEvent } from '@angular/material';
import { Router } from '@angular/router';
declare var jQuery: any;
declare var $;
import swal from 'sweetalert2'
import { FormGroup, FormControl,Validators } from '@angular/forms';

@Component({
  selector: 'app-user-lists',
  templateUrl: './user-lists.component.html',
  styleUrls: ['./user-lists.component.scss']
})
export class UserListsComponent implements OnInit {
  public teacherLists=[];
  public facultyLists=[];
  public studentForm:FormGroup;
  public studentSchoolForm:FormGroup;
  public teacherForm:FormGroup;
  public guestForm:FormGroup;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  cropperReady = false;
  // teachercroppedImage
  teacherimageChangedEvent: any = '';
  teachercroppedImage: any = '';
  teachercropperReady = false;

  public batchLists=[]
  public collegeStudentLists=[];
  public guestLists=[];
  public schoolStudentLists=[];
  public parentId;
  public userId;
  public studentId;
  public batch;
  public editedId;
  public image;
  public studentimage;
  public schoolStudentImage;
  public student_id;
  public studentRecord_id;
  public parent_id;
  public teacherImage;
  public userIdForTeacher;
  public teacherId;
  toppingArray = ['Principal', 'Co-ordinator', 'Teacher'];
  public levelofSchoool
  public idofSchool; 
  public guestImage;
  public guestId;
constructor(private service:UserListsService,private router:Router){ }
ngOnInit() {

  this.studentForm=new FormGroup({
    userName:new FormControl('',Validators.required),
    email:new FormControl('',Validators.required),
    firstName:new FormControl('',Validators.required),
    middleName:new FormControl(),
    lastName:new FormControl('',Validators.required),
    joinDate:new FormControl('',Validators.required),
    gender:new FormControl('',Validators.required),
    parmanentAddress:new FormControl('',Validators.required),
    currentAddress:new FormControl('',Validators.required),
    symbolNumber:new FormControl('',Validators.required),
    registrationNumber:new FormControl('',Validators.required),
    batch:new FormControl('',Validators.required),
    department:new FormControl('',Validators.required),
    mobileNumber:new FormControl('',Validators.required),
    phoneNumber:new FormControl('',Validators.required),
    parentCheck:new FormControl(''),
    parentFirstName:new FormControl(''),
    parentMiddleName:new FormControl(''),
    parentLastName:new FormControl(''),
    parentMobile:new FormControl(''),
    parentEmail:new FormControl(''),
    parent_id:new FormControl(''),
    class:new FormControl(),
    year:new FormControl()
  })

  this.studentSchoolForm=new FormGroup({
    userName:new FormControl('',Validators.required),
    email:new FormControl('',Validators.required),
    firstName:new FormControl('',Validators.required),
    middleName:new FormControl(),
    lastName:new FormControl('',Validators.required),
    Date:new FormControl('',Validators.required),
    gender:new FormControl('',Validators.required),
    parmanentAddress:new FormControl('',Validators.required),
    currentAddress:new FormControl('',Validators.required),
    symbolNumber:new FormControl('',Validators.required),
    registrationNumber:new FormControl('',Validators.required),
    batch:new FormControl('',Validators.required),
    department:new FormControl('',Validators.required),
    mobileNumber:new FormControl('',Validators.required),
    phoneNumber:new FormControl('',Validators.required),
    parentCheck:new FormControl(''),
    parentFirstName:new FormControl(''),
    parentMiddleName:new FormControl(''),
    parentLastName:new FormControl(''),
    parentMobile:new FormControl(''),
    parentEmail:new FormControl(''),
    parent_id:new FormControl(''),
    class:new FormControl(),
    year:new FormControl()
  })

  this.teacherForm = new FormGroup({
    username: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    password: new FormControl('',Validators.required),
    confirmPassword: new FormControl('',Validators.required),
    firstName: new FormControl('',Validators.required),
    middleName: new FormControl(),
    post :new FormControl(),
    department : new FormControl(),
    lastName: new FormControl('',Validators.required),
    parentName: new FormControl('',Validators.required),
    gender: new FormControl('',Validators.required),
    join_Date: new FormControl(),
    leaveDate: new FormControl(),
    parmanentAddress: new FormControl('',Validators.required),
    currentAddress: new FormControl('',Validators.required),
    mobileNumber: new FormControl('',Validators.required),
    phoneNumber: new FormControl('',Validators.required)
  })

  this.guestForm=new FormGroup({
    userName:new FormControl('',Validators.required),
    email:new FormControl('',Validators.required),
    firstName:new FormControl('',Validators.required),
    middleName:new FormControl(),
    lastName:new FormControl('',Validators.required),
    gender:new FormControl(),
    parmanentAddress:new FormControl('',Validators.required),
    currentAddress:new FormControl('',Validators.required),
    mobileNumber:new FormControl('',Validators.required),
    phoneNumber:new FormControl('',Validators.required),
    discription:new FormControl('',Validators.required),
 })

  this.service.getFaculty().subscribe(
    (response)=>{
      console.log(response)
      this.facultyLists=response.faculties;
    }
  )
  this.service.getBatchYear().subscribe(
    (response)=>{
      console.log(response)
      this.batchLists=response.batches;
    }
  )
// this.studentForm.controls.department.setValue('BSc CSIT');  
  // this.service.getTeacher().subscribe(
  //   (response)=>{
  //     console.log(response);
  //     this.teacherLists=response.teachers;
  //   }
  // )
  this.service.getUsers().subscribe(
    (response)=>{
      this.collegeStudentLists=response.response;
      console.log(this.collegeStudentLists)
      }    
   )
}
onLinkClick(event: MatTabChangeEvent) {
    console.log('tab => ', event.tab.textLabel);
    this.croppedImage=null;
    if(event.tab.textLabel.toLowerCase() === 'teacher'){
      this.service.getTeacher().subscribe(
        (response)=>{
          this.teacherLists=response.teachers;
          console.log(response)
        }
      )
    }
    if(event.tab.textLabel.toLowerCase() === 'guest'){
    this.service.getGuest().subscribe(
      (response)=>{
         this.guestLists=response.guests;
         console.log(response)
        }
      )
    }
    if(event.tab.textLabel.toLowerCase() === 'college'){
      this.service.getUsers().subscribe(
        (response)=>{
          this.collegeStudentLists=response.response;
          console.log(this.collegeStudentLists)
          }    
       )
    }
    if(event.tab.textLabel.toLowerCase() === 'school'){
       this.service.getSchoolStudent().subscribe(
         (response)=>{
           console.log(response.students)
           this.schoolStudentLists=response.students;
           console.log(this.schoolStudentLists)
         }
       )  
     }
    }
    fileChangeEvent(event: any): void {
       this.imageChangedEvent = event;
       console.log(event)
    }
  
    imageCropped(image: string) {
      this.croppedImage = image;
      console.log(image)
    }
  
    imageLoaded() {
      this.cropperReady = true;
    }
  
    imageLoadFailed() {
      console.log('Load failed');
    }
  editstudent(batch,id){
    console.log(batch,id);
    let data={};
    this.batch=batch;
    this.editedId=id;
    data['batch']=batch;
    data['id']=id;
    this.service.getStudentForUpdate(data).subscribe(
    (response)=>{
         console.log(response.response)
         console.log(response.response[0].first_name)
         this.parentId=response.response[0].grand_id;
         this.userId=response.response[0].id;
         this.studentId=response.response[0].student_id;

         this.studentForm.controls.userName.setValue(response.response[0].userName);  
         this.studentForm.controls.email.setValue(response.email[0].email);
         this.studentForm.controls.firstName.setValue(response.response[0].first_name)
         this.studentForm.controls.middleName.setValue(response.response[0].middle_name)
         this.studentForm.controls.lastName.setValue(response.response[0].last_name)
         this.studentForm.controls.joinDate.setValue(response.response[0].join_date)
         this.studentForm.controls.gender.setValue(response.response[0].gender)
         this.studentForm.controls.parmanentAddress.setValue(response.response[0].permanent_address)
         this.studentForm.controls.currentAddress.setValue(response.response[0].current_address)
         this.studentForm.controls.symbolNumber.setValue(response.response[0].symbol_number)
         this.studentForm.controls.registrationNumber.setValue(response.response[0].registration_number)
         this.studentForm.controls.batch.setValue(response.response[0].batch)
         this.studentForm.controls.department.setValue(response.response[0].facultyname)
         this.image=response.response[0].image;
         this.studentimage=response.response[0].image;

         this.studentForm.controls.mobileNumber.setValue(response.response[0].mobile_number)
         this.studentForm.controls.phoneNumber.setValue(response.response[0].phone_number)

         this.studentForm.controls.parentFirstName.setValue(response.response[0].firstName)
         this.studentForm.controls.parentMiddleName.setValue(response.response[0].middleName)
         this.studentForm.controls.parentLastName.setValue(response.response[0].lastName)
         this.studentForm.controls.parentMobile.setValue(response.response[0].mobileNumber)
         this.studentForm.controls.parentEmail.setValue(response.response[0].parent_email)

        //  this.studentForm.controls.batch.setValue(response.response[0].batch)
         jQuery('#myModal').modal('show');
        }
      )
    }   
    submitStudentForm(){
        jQuery('#myModal').modal('hide');
        let data={}
        data=this.studentForm.value;
        if(this.studentForm.controls.department.value instanceof Object) {
             data['department']=this.studentForm.controls.department.value.id;
        }else{
        for(let x in this.facultyLists){
          if(this.facultyLists[x].name.toLowerCase() === this.studentForm.controls.department.value.toLowerCase()){
             data['department']=this.facultyLists[x].id;
          }
        }
      }
      if(this.studentForm.controls.batch.value instanceof Object){
        data['batch']=this.studentForm.controls.batch.value.batch;
      }else{
        for(let x in this.batchLists){
           if(this.batchLists[x].batch == this.studentForm.controls.batch.value ){
            data['batch']=this.batchLists[x].batch;
           }
        }
      }
      data['studentId']=this.studentId;
      data['parent_id']=this.parentId;
      if(this.croppedImage){
        data['image']=this.croppedImage;
      }else{
        data['image']=this.image;
      }
      this.service.updateStudent(data).subscribe(
        (response)=>{
          console.log(response)
           swal({
             position: 'top-end',
             type: 'success',
             title: 'Successfully Updated Student!',
             showConfirmButton: false,
             timer: 4000
          })
          this.croppedImage=null;

          for(let y in this.collegeStudentLists){
             for(let x in this.collegeStudentLists[y].batch_list){
               if(this.collegeStudentLists[y].batch_list[x].name == this.batch){
                 for(let i in this.collegeStudentLists[y].batch_list[x].students){
                   if(this.collegeStudentLists[y].batch_list[x].students[i].user_id == this.editedId){
                    this.collegeStudentLists[y].batch_list[x].students[i].first_name=this.studentForm.controls.firstName.value;
                    this.collegeStudentLists[y].batch_list[x].students[i].middle_name=this.studentForm.controls.middleName.value;
                    this.collegeStudentLists[y].batch_list[x].students[i].last_name=this.studentForm.controls.lastName.value;
                   }
                 }
               }
             }
          }
        },(error)=>{
          swal({
             position: 'top-end',
             type: 'error',
             title: 'Unable to Update Student!',
             showConfirmButton: false,
             timer: 4000
          })
        }
      )
    }
  editSchoolStudent(level , id){
    jQuery('#myschoolStudentModal').modal('show');
    console.log(level,id)
    this.levelofSchoool=level;
    this.idofSchool=id;
    this.service.getSchoolStudentForUpdate(id).subscribe(
      (response)=>{
         console.log(response)
         this.studentSchoolForm.controls.userName.setValue(response.email[0].name);  
         this.studentSchoolForm.controls.email.setValue(response.email[0].email);  

         this.studentSchoolForm.controls.firstName.setValue(response.students[0].first_name);  
         this.studentSchoolForm.controls.middleName.setValue(response.students[0].middle_name);  
         this.studentSchoolForm.controls.lastName.setValue(response.students[0].last_name);  
         this.studentSchoolForm.controls.Date.setValue(response.students[0].join_date);  
         this.studentSchoolForm.controls.gender.setValue(response.students[0].gender);  
         this.studentSchoolForm.controls.parmanentAddress.setValue(response.students[0].permanent_address);  
         this.studentSchoolForm.controls.currentAddress.setValue(response.students[0].current_address);  
         this.studentSchoolForm.controls.symbolNumber.setValue(response.students[0].symbol_number);  
         this.studentSchoolForm.controls.registrationNumber.setValue(response.students[0].registration_number);  

         this.schoolStudentImage=response.students[0].image
         this.studentSchoolForm.controls.department.setValue('School');  
         this.studentSchoolForm.controls.mobileNumber.setValue(response.students[0].mobile_number);  
         this.studentSchoolForm.controls.phoneNumber.setValue(response.students[0].phone_number);
      
         this.student_id=response.students[0].student_id;
         this.studentRecord_id=response.students[0].studentRecord_id;
         this.parent_id=response.students[0].parent_id;

         this.studentSchoolForm.controls.parentFirstName.setValue(response.students[0].firstName);  
         this.studentSchoolForm.controls.parentMiddleName.setValue(response.students[0].mobileNumber);  
         this.studentSchoolForm.controls.parentLastName.setValue(response.students[0].lastName);  
         this.studentSchoolForm.controls.parentMobile.setValue(response.students[0].mobileNumber);  
         this.studentSchoolForm.controls.parentEmail.setValue(response.students[0].parent_email);  
         this.studentSchoolForm.controls.class.setValue(response.students[0].class);  
         this.studentSchoolForm.controls.year.setValue(response.students[0].year);  

        }
      )
    }     
    submitSchoolStudentForm(){
      jQuery('#myschoolStudentModal').modal('hide');
      console.log(this.studentSchoolForm.value)
      let data={};
      data['parent_id']=this.parent_id;
      data['studentRecord']=this.studentRecord_id;
      data['student_id']=this.student_id;
      if(this.croppedImage){
        data['image']=this.croppedImage;
      }else{
        data['image']=this.image;
      }
      this.service.updateSchoolStudent(data).subscribe(
        (response)=>{
          console.log(response)
           swal({
              position: 'top-end',
              type: 'success',
              title: 'Successfully Updated Student!',
              showConfirmButton: false,
              timer: 4000
            })
            for(let x in this.schoolStudentLists){
              if(this.schoolStudentLists[x].class === this.levelofSchoool){
                for(let y in this.schoolStudentLists[x].students){
                   if(this.schoolStudentLists[x].students[y].user_id == this.idofSchool){
                     if(this.croppedImage){
                      this.schoolStudentLists[x].students[y].image=this.croppedImage
                     }
                    this.schoolStudentLists[x].students[y].first_name= this.studentSchoolForm.controls.firstName.value
                    this.schoolStudentLists[x].students[y].middle_name= this.studentSchoolForm.controls.middleName.value
                    this.schoolStudentLists[x].students[y].last_name= this.studentSchoolForm.controls.lastName.value 
                   } 
                }
              }   
           }
             this.croppedImage=null;

        },(error)=>{
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Unable to Updated Student!',
            showConfirmButton: false,
            timer: 4000
          })
        }
      )
    }  
    editTeacher(id){
      console.log(id)
      this.service.getTeacherForUpdate(id).subscribe(
        (response)=>{
          console.log(response.teachers)
          this.userIdForTeacher=response.teachers[0].user_id;
          this.teacherId=response.teachers[0].teacher_id;
          this.teacherForm.controls.department.setValue(response.teachers[0].department); 
          this.teacherForm.controls.username.setValue(response.teachers[0].name); 
          this.teacherForm.controls.email.setValue(response.teachers[0].email);
          this.teacherImage=response.teachers[0].image; 

          // this.teacherForm.controls.password.setValue(response.teachers[0].password); 
          // this.teacherForm.controls.confirmPassword.setValue(response.teachers[0].password); 


          this.teacherForm.controls.firstName.setValue(response.teachers[0].first_name); 
          this.teacherForm.controls.middleName.setValue(response.teachers[0].middle_name); 
          this.teacherForm.controls.lastName.setValue(response.teachers[0].last_name); 
          this.teacherForm.controls.gender.setValue(response.teachers[0].gender); 
          this.teacherForm.controls.join_Date.setValue(response.teachers[0].joining_date); 
          this.teacherForm.controls.leaveDate.setValue(response.teachers[0].leaving_date); 

          this.teacherForm.controls.parmanentAddress.setValue(response.teachers[0].permanent_address); 
          this.teacherForm.controls.currentAddress.setValue(response.teachers[0].current_address); 
          this.teacherForm.controls.mobileNumber.setValue(response.teachers[0].mobile_number); 
          this.teacherForm.controls.phoneNumber.setValue(response.teachers[0].phone_number); 
          this.teacherForm.controls.post.setValue(response.teachers[0].post); 
          jQuery('#myTeacherModal').modal('show'); 
        }
      )
    }
    submitTeacher(){
      console.log(this.teacherForm.value)
      jQuery('#myTeacherModal').modal('hide'); 
      let data={};
      data=this.teacherForm.value;
      data['user_id']=this.userIdForTeacher;
      data['teacher_id']=this.teacherId;
      if(this.croppedImage){
        data['image']=this.croppedImage;
      }else{
        data['image']=this.image;
      }
      this.service.updateTeacher(data).subscribe(
        (response)=>{
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Successfully Updated Teacher!',
            showConfirmButton: false,
            timer: 4000
         })
         for(let x in this.teacherLists){
            if(this.teacherLists[x].user_id  ==this.userIdForTeacher){
              this.teacherLists[x].first_name = this.teacherForm.controls.firstName.value
              this.teacherLists[x].middle_name = this.teacherForm.controls.middleName.value
              if(this.croppedImage){
                this.teacherLists[x].image = this.croppedImage
              }
              this.teacherLists[x].last_name = this.teacherForm.controls.lastName.value
            }
          }
         this.croppedImage=null;
        },(error)=>{
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Unable to  Updated Teacher!',
            showConfirmButton: false,
            timer: 4000
         })
        }
      )
    } 
    editGuest(id){
      this.service.getGuestForEdit(id).subscribe(
      (response)=>{
        this.guestForm.controls.userName.setValue(response.guests[0].name)
        this.guestForm.controls.email.setValue(response.guests[0].email)
        this.guestForm.controls.firstName.setValue(response.guests[0].first_name)
        this.guestForm.controls.middleName.setValue(response.guests[0].middle_name)
        this.guestForm.controls.lastName.setValue(response.guests[0].last_name)
        this.guestForm.controls.gender.setValue(response.guests[0].gender)
        this.guestForm.controls.parmanentAddress.setValue(response.guests[0].permanent_address)
        this.guestForm.controls.currentAddress.setValue(response.guests[0].current_address)
        this.guestForm.controls.mobileNumber.setValue(response.guests[0].mobile_number)
        this.guestForm.controls.phoneNumber.setValue(response.guests[0].phone_number)
        this.guestForm.controls.discription.setValue(response.guests[0].description)
        this.guestImage=response.guests[0].image; 
        this.guestId=response.guests[0].user_id;
        jQuery('#myGuestModal').modal('show'); 
        console.log(response)
      }
    )
  }  
  submitGuestForm(){
    let data={};
    jQuery('#myGuestModal').modal('hide'); 
    data=this.guestForm.value;
    data['guest_id']=this.guestId
    if(this.croppedImage){
      data['image']=this.croppedImage;
    }else{
      data['image']=this.image;
    }
    console.log(this.guestForm.value)
    this.service.postGuest(data).subscribe(
      (response)=>{
        
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Successfully Updated Guest!',
          showConfirmButton: false,
          timer: 4000
        })   
        for(let x in this.guestLists){
           if(this.guestLists[x].user_id == this.guestId ){
            if(this.croppedImage){
              this.guestLists[x].image = this.croppedImage
            }
            this.guestLists[x].first_name=this.guestForm.controls.firstName.value
            this.guestLists[x].middle_name=this.guestForm.controls.middleName.value
            this.guestLists[x].last_name=this.guestForm.controls.lastName.value
           }
        }    
        this.croppedImage=null;
       },(error)=>{
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Unable to Updat Guest!',
          showConfirmButton: false,
          timer: 4000
        })      
        }
      ) 
    }  
 }
