import { TestBed, inject } from '@angular/core/testing';

import { UserListsService } from './user-lists.service';

describe('UserListsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserListsService]
    });
  });

  it('should be created', inject([UserListsService], (service: UserListsService) => {
    expect(service).toBeTruthy();
  }));
});
