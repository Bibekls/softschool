import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    public menuItems;
    constructor() { }

    ngOnInit() {
        this.menuItems = [
            {
                menus:
                    {
                        flag: true, path: 'dashboard', title: 'Dashboard', icon: 'dashboard', class: '',
                        submenus: [],
                    }
            },
            {
                menus:
                    {
                        flag: false, path: '', title: 'User Management', icon: 'dashboard', class: '',
                        submenus: [
                            { path: 'user-list', title: 'User List', icon: 'people', class: '' },
                            { path: 'user-registration', title: 'User Registration', icon: 'people', class: '' }
                        ],
                    }
            },
            {
                menus:
                    {
                        flag: true, path: 'subject', title: 'Subject Management', icon: 'subject', class: '',
                        submenus: [],
                    }
            },
            {
                menus:
                    {
                        flag: true, path: 'routine', title: 'Routine Management', icon: 'schedule', class: '',
                        submenus: [],
                    }
            },
            {
                menus:
                    {
                        flag: true, path: 'event', title: 'Event Management', icon: 'event', class: '',
                        submenus: [],
                    }
            },
            {
                menus:
                    {
                        flag: true, path: 'notification', title: 'Notification Management', icon: 'information', class: '',
                        submenus: [],
                    }
            },
            {
                menus:
                    {
                        flag: true, path: 'attendence', title: 'Student Attendence', icon: 'person', class: '',
                        submenus: [],
                    }
            },
            {
                menus:
                    {
                        flag: false, path: '', title: 'Configuration', icon: 'check', class: '',
                        submenus: [
                            { path: 'dashboard-user', title: 'Dashboard User', icon: 'unlock', class: '' },
                            { path: 'batch', title: 'Batch-Level Mapping', icon: 'mapping', class: '' },
                            { path: 'faculty', title: 'Faculty', icon: 'content_paste', class: '' }
                        ]
                    }
            },
        ]
    }
    isMobileMenu() {
        //   if ($(window).width() > 991) {
        //       return false;
        //   }
        //   return true;
    }
}
