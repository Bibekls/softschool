import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CdkTableModule} from '@angular/cdk/table';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { UserListsComponent } from './user-manager/user-lists/user-lists.component';
import { UserRegistrationComponent } from './user-manager/user-registration/user-registration.component';
import { TeacherRegistrationComponent } from './user-manager/user-registration/teacher-registration/teacher-registration.component';
import { StudentRegistrationComponent } from './user-manager/user-registration/student-registration/student-registration.component';
import { GuestRegistrationComponent } from './user-manager/user-registration/guest-registration/guest-registration.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import {
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatRadioModule,
  MatTabsModule,
  MatNativeDateModule,
} from '@angular/material';
import { SubjectManagementComponent } from './subject-management/subject-management.component';
import { RoutineManagementComponent } from './routine-management/routine-management.component';
import { EventManagementComponent } from './event-management/event-management.component';
import { RoutineManagementService } from './routine-management/routine-management.service';
// import {DxSchedulerModule,DxDataGridModule} from 'devextreme-angular';
import { CalendarModule } from 'angular-calendar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NotificationManagementComponent } from './notification-management/notification-management.component';
import { StudentAttendenceComponent } from './student-attendence/student-attendence.component';
import { TreeviewModule } from 'ngx-treeview';
import { TreeModule } from 'angular-tree-component';
import { Daterangepicker } from 'ng2-daterangepicker';
// import {NgxImageEditorModule} from "ngx-image-editor";
import { TeacherRegistrationService } from './user-manager/user-registration/teacher-registration/teacher-registration.service';
import { ConfigurationComponent } from './configuration/configuration.component';
import { DashboardUserComponent } from './configuration/dashboard-user/dashboard-user.component';
import { BatchLevelMappingComponent } from './configuration/batch-level-mapping/batch-level-mapping.component';
import { FacultyComponent } from './configuration/faculty/faculty.component';
import { ImageUploadModule } from "angular2-image-upload";
import { ImageCropperModule } from 'ngx-image-cropper';
import { UserRegistrationService } from './user-manager/user-registration/user-registration.service';
import { UserListsService } from './user-manager/user-lists/user-lists.service';
import { SubjectManagementService } from './subject-management/subject-management.service';
import { EventManagementService } from './event-management/event-management.service';
import { ConfigurationService } from './configuration/configuration.service';
import { NgSelectModule } from '@ng-select/ng-select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AttendenceService } from './student-attendence/attendence.service';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { NotificationService } from './notification-management/notification.service';

@NgModule({
  declarations: [
    AppComponent,
    UserManagerComponent,
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    UserListsComponent,
    UserRegistrationComponent,
    TeacherRegistrationComponent,
    StudentRegistrationComponent,
    GuestRegistrationComponent,
    SubjectManagementComponent,
    RoutineManagementComponent,
    EventManagementComponent,
    NotificationManagementComponent,
    StudentAttendenceComponent,
    ConfigurationComponent,
    DashboardUserComponent,
    BatchLevelMappingComponent,
    FacultyComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpModule,
    RouterModule,
    ReactiveFormsModule,
    AppRoutingModule,
    CdkTableModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatRadioModule,
    MatTabsModule,
    HttpClientModule,
    MatNativeDateModule,
    // DxSchedulerModule,
    CalendarModule.forRoot(),
    NgbModule.forRoot(),
    TreeModule, 
    Daterangepicker,
    // NgxImageEditorModule,
    ImageUploadModule.forRoot(),
    ImageCropperModule,
    NgSelectModule,
    TreeviewModule.forRoot(),
    TreeViewModule
 ],
  providers: [RoutineManagementService,NotificationService,AttendenceService,ConfigurationService,EventManagementService,SubjectManagementService,UserRegistrationService,TeacherRegistrationService,UserListsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
