import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,FormArray,FormBuilder, Validators } from '@angular/forms';
import { ConfigurationService } from '../configuration.service';
import swal from 'sweetalert2'
declare var jQuery:any;

@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.scss']
})
export class FacultyComponent implements OnInit {
 public facultyForm:FormGroup;
 public updatefacultyForm:FormGroup;
 public facultyLists=[];
 public idForEdit;
 public is_Edit=false;
 public indexOfEdit;
 public idForDelete;
 public indexForDelete;
 public tempFaculty=[]
 public updateFacultyLists=[]
 public initUpdateFaculties=[];
  constructor(private service:ConfigurationService,private fb:FormBuilder) { }
  ngOnInit() {
    this.facultyForm=new FormGroup({
      facultyName:new FormControl(),
      facultyWiseClasses:this.fb.array([this.initFaculties()]),
      start_at:new FormControl(),
      end_at:new FormControl()
     })
    this.service.getFaculty().subscribe(
    (response)=>{
        console.log(response.faculties);
        this.facultyLists=response.faculties;
      }) 
  }
  initFaculties() {
    return this.fb.group({
        // list all your form controls here, which belongs to your form array
        faculties: ['']
    });
  }
  addFaculty() {
    // control refers to your formarray
    const control = <FormArray>this.facultyForm.controls['facultyWiseClasses'];
    // add new formgroup
    control.push(this.initFaculties());
    console.log('I am in')
  }
  removeFaculty(index:number){
    // control refers to your formarray
    const control = <FormArray>this.facultyForm.controls['facultyWiseClasses'];
    // remove the chosen row
 control.removeAt(index);
  }
  submitFacultyForm(){
  console.log(this.facultyForm.value)
  let data=this.facultyForm.value;
  data['is_edit']=this.is_Edit;
  data['id']=this.idForEdit;
  this.service.postFaculty(data).subscribe(
    (response)=>{
      console.log(response.faculties)
      if(response.faculties == 1){ 
        this.facultyLists[this.indexOfEdit].name=this.facultyForm.controls.facultyName.value;
        this.facultyLists[this.indexOfEdit].start_at=this.facultyForm.controls.start_at.value;
        this.facultyLists[this.indexOfEdit].end_at=this.facultyForm.controls.end_at.value;
        this.is_Edit=false;
        this.facultyForm.reset()
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Sucessfully Updated Faculty!',
          showConfirmButton: false,
          timer: 4000
        })
      }else{
        this.facultyLists.push(response.faculties);
        this.facultyForm.reset()
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Sucessfully Inserted Faculty!',
          showConfirmButton: false,
          timer: 4000
        })
      }
    },(error)=>{
      this.facultyForm.reset()
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Unable to Insert Faculty!',
          showConfirmButton: false,
          timer: 4000
        })
    }
  )
}
 editFaculty(i){
   let data=[];
   this.idForEdit=this.facultyLists[i].id;
   this.is_Edit=true;
   this.indexOfEdit=i;
   jQuery("#myupdateModal").modal("show");
   this.updateFacultyLists=this.facultyLists[i].faculty_wise_class
  for(let x in this.updateFacultyLists){
    console.log(this.updateFacultyLists[x].faculties)
    this.initUpdateFaculties.push(this.fb.group({
      update_faculties:this.updateFacultyLists[x].faculties
    }));
  }

  this.updatefacultyForm=this.fb.group({
    'updatefacultyName':[null,Validators.maxLength(60)],
     updatefacultyWiseClasses:this.fb.array(this.initUpdateFaculties),
     'update_start_at':[null],
     'update_end_at':[null],
  })

   console.log(this.updatefacultyForm.value)
   data=this.facultyLists[i].faculty_wise_class;  
   this.updatefacultyForm.controls.updatefacultyName.setValue(this.facultyLists[i].name);
   this.updatefacultyForm.controls.update_start_at.setValue(this.facultyLists[i].start_at);
   this.updatefacultyForm.controls.update_end_at.setValue(this.facultyLists[i].end_at);
}
 deleteFaculty(i){
 this.idForDelete =this.facultyLists[i].id;
 this.indexForDelete=i;
 jQuery("#myModal").modal("show");
}
  deleteFacultyModel(){
  jQuery("#myModal").modal("hide");
  this.service.deleteFaculty(this.idForDelete).subscribe(
    (response)=>{
      console.log(response)
      swal({
        position: 'top-end',
        type: 'success',
        title: 'Sucessfully Deleted Faculty!',
        showConfirmButton: false,
        timer: 4000
      })
      this.facultyLists.splice(this.indexForDelete,1);
    },(error)=>{
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Unable to  Delete Faculty!',
        showConfirmButton: false,
        timer: 4000
      })
    }
  )
 }
 updateFacultyForm(){
   console.log(this.updatefacultyForm.value)
 }
 updateremoveFaculty(index){
   this.updateFacultyLists.splice(index,1)
   console.log(index)
 } 
 updateaddFaculty(){
  const control = <FormArray>this.facultyForm.controls['facultyWiseClasses'];
  // add new formgroup
  control.push(this.updateinitFaculties());
    this.updateFacultyLists.push(control)
  }
  updateinitFaculties(){
    return this.fb.group({
      // list all your form controls here, which belongs to your form array
      update_faculties: ['']
  });
  }
}
