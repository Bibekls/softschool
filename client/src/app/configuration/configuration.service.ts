import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'
import { ENV } from "./../env";
@Injectable()
export class ConfigurationService {
constructor(private http:Http) { }
 postFaculty(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/post_faculty",data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   private extractData(res: Response) {
    let response = res.json();
    return response || {};
  }
  private handleError(error: Response | any) {
    return Observable.throw(error);
   }

   getFaculty(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_faculty",options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   deleteFaculty(id){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/delete_faculty/"+id,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getBatch(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_batch", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getStudentFormBatchLevelMapping(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/get_student_form_batch_level_mapping",data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   postPromotedStudent(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/promoted_student",data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getBLM(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_BLM",options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   postBatchLevel(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/batch_level",data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getYear(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/get_year",options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getmap(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/getmap",options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   
}
