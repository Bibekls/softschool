import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchLevelMappingComponent } from './batch-level-mapping.component';

describe('BatchLevelMappingComponent', () => {
  let component: BatchLevelMappingComponent;
  let fixture: ComponentFixture<BatchLevelMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchLevelMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchLevelMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
