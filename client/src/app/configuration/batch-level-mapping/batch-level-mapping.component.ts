import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from '../configuration.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
declare var jQuery: any;
declare var $;
import swal from 'sweetalert2'

@Component({
  selector: 'app-batch-level-mapping',
  templateUrl: './batch-level-mapping.component.html',
  styleUrls: ['./batch-level-mapping.component.scss']
})
export class BatchLevelMappingComponent implements OnInit {
  public facultyList = []
  promoteStudent: FormGroup;
  promoteForm: FormGroup;
  batchMapping: FormGroup;
  public batchLists = [];
  public batchMappingLists = [];
  public levelLists = []
  public studentLists = [];
  public mappingLists = []
  studentPromation: FormGroup;
  public studentPromote = []
  public selectedStudent = []
  public promoteData;
  public tempArray = [];
  public yearLists=[];
  public year;
  public isChecked=false;
  constructor(private service: ConfigurationService, private fb: FormBuilder) { }
  ngOnInit() {
    this.promoteStudent = new FormGroup({
      faculty: new FormControl(),
      batch: new FormControl(),
      level: new FormControl(),
    });
    this.promoteForm = new FormGroup({
      promateYear:new FormControl(),
      promateTo:new FormControl()
    })
    this.service.getYear().subscribe(
      (response)=>{
        console.log(response.year)
        this.yearLists=response.year;
      }
    )
    this.service.getFaculty().subscribe(
      (response) => {
        this.facultyList = response.faculties;
        for(let x in this.facultyList){
          if(this.facultyList[x].name.toLowerCase() === 'school'){
            this.levelLists = this.facultyList[x].faculty_wise_class;
          }
        }
        console.log(response)
      }
    )
    this.service.getBLM().subscribe(
      (response) => {
        console.log(response.blm)
        this.batchMappingLists = response.blm;
          
          let flag=true;   
          let temp; 
          let firstFlag=true;
          for(let x in this.batchMappingLists){
              if(flag){
                  temp=this.batchMappingLists[x].faculty_id;
                  flag=false;
                }
                if(temp == this.batchMappingLists[x].faculty_id){
                this.batchMappingLists[x].created_at='faculty'+x;
                if(firstFlag){
                  this.batchMappingLists[x].updated_at=0;
                  firstFlag=false;
                  }else{
                  this.batchMappingLists[x].updated_at=1;
                 }
                for(let y in this.batchMappingLists[x].faculty_wise_class) {
                  if(this.batchMappingLists[x].faculty_wise_class[y].faculties == this.batchMappingLists[x].level) {
                      this.batchMappingLists[x].faculty_wise_class[y].faculties=1;
                    }else{
                      this.batchMappingLists[x].faculty_wise_class[y].faculties=0;
                  }
                }
              }else{
                this.batchMappingLists[x].created_at='faculty'+x;
                this.batchMappingLists[x].updated_at=0;
                for(let y in this.batchMappingLists[x].faculty_wise_class) {
                  if(this.batchMappingLists[x].faculty_wise_class[y].faculties == this.batchMappingLists[x].level) {
                      this.batchMappingLists[x].faculty_wise_class[y].faculties=1;
                    }else{
                      this.batchMappingLists[x].faculty_wise_class[y].faculties=0;
                  }
                } 
              }
            }
    this.batchMapping = this.fb.group({
      batch_mapping: this.fb.array(this.mappingLists)
    })
        console.log(this.mappingLists);
      }
    )
    this.promoteStudent.controls.faculty.setValue('School')
    this.service.getBatch().subscribe(
      (response) => {
        console.log(response)
        this.batchLists = response.batches;
      }
    )
   this.service.getmap().subscribe(
     (response)=>{
       console.log(response)
     }
   )
    this.promoteStudent.controls.faculty.valueChanges.subscribe(
      (batch) => {
        console.log(batch)
        if (batch) {
          for(let x in this.facultyList){
            if(batch.toLowerCase() === this.facultyList[x].name.toLowerCase()){
              this.levelLists = this.facultyList[x].faculty_wise_class;
            }
          }
          console.log(this.levelLists)
        }
      }
    )
    this.promoteStudent.controls.batch.valueChanges.subscribe(
      (batch) => {
        this.year=parseInt(batch.year)+1
        this.promoteForm.controls.promateYear.setValue(this.year)
      }
    )
  }
  submitPromoteStudent() {
    console.log(this.promoteStudent.value)
    let data = this.promoteStudent.value;
    for(let x in this.facultyList){
      if(this.promoteStudent.controls.faculty.value.toLowerCase() === this.facultyList[x].name.toLowerCase(0)){
        data['faculty'] = this.facultyList[x].id;
      }
    }
    data['batch']= this.promoteStudent.controls.batch.value.year;
    data['level'] = this.promoteStudent.controls.level.value.faculties;
    console.log(data)
    this.service.getStudentFormBatchLevelMapping(data).subscribe(
      (response) => {
        console.log(response)
        this.studentLists = response.students;
        for (let x in this.studentLists) {
          this.studentPromote.push(this.fb.group({
            promote: true,
            first_name: this.studentLists[x].first_name,
            middle_name: this.studentLists[x].middle_name,
            last_name: this.studentLists[x].last_name,
            roll_no:this.studentLists[x].symbol_number,
            id: this.studentLists[x].student_id,
            faculty_id: this.studentLists[x].faculty_id
          }));
        }
        this.promoteForm = this.fb.group({
          promateStudent: this.fb.array(this.studentPromote),
          promateTo: [''],
          promateYear: [this.year]
        })
        console.log(this.studentLists)
        // this.promoteForm.controls.promateYear.setValue(this.year)
      })

  }
  submitPromoteForm() {
    console.log(this.promoteForm.value)
    let data = this.promoteForm.value;
     
    let index =parseInt(this.promoteForm.controls.promateTo.value)-1;

    this.selectedStudent = data.promateStudent.filter(x => x.promote).map(x => { return { id: x.id, faculty_id: x.faculty_id,roll_no:x.roll_no }; });
    console.log(this.selectedStudent);
    console.log(this.levelLists)
    data['promateTo']=this.levelLists[index].faculties;
    data['students'] = this.selectedStudent;
    console.log(data)
    this.promoteData = data;
    jQuery('#myModal').modal('show');

  }
  submitPromoteFormModel() {
    jQuery('#myModal').modal('hide');
    this.service.postPromotedStudent(this.promoteData).subscribe(
      (response) => {
        console.log(response)
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Successfully Promoted Students!',
          showConfirmButton: false,
          timer: 4000
        })
      }, (error) => {
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Unable To Promote Students!',
          showConfirmButton: false,
          timer: 4000
        })
      })
  }
  unChangeBatch(index,batch,faculty_id){
    console.log('I ma Reset'+index,batch,faculty_id);
    let Param={}
    Param['level']=index+1;
    Param['batch']=batch;
    Param['faculty_id']=faculty_id;
    console.log(Param)
    this.service.postBatchLevel(Param).subscribe(
      (response)=>{
        console.log(response)
      }
    )
  }
}
