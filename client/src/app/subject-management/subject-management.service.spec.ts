import { TestBed, inject } from '@angular/core/testing';

import { SubjectManagementService } from './subject-management.service';

describe('SubjectManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubjectManagementService]
    });
  });

  it('should be created', inject([SubjectManagementService], (service: SubjectManagementService) => {
    expect(service).toBeTruthy();
  }));
});
