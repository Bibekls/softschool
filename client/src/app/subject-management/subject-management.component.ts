import { Component, OnInit } from '@angular/core';
import { SubjectManagementService } from './subject-management.service';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2'
import { MatTabChangeEvent } from '@angular/material';
import { HttpHeaderResponse } from '@angular/common/http';
declare var jQuery: any;
declare var $;

@Component({
  selector: 'app-subject-management',
  templateUrl: './subject-management.component.html',
  styleUrls: ['./subject-management.component.scss']
})
export class SubjectManagementComponent implements OnInit {
  panelOpenState: boolean = false;
  public facultiesList = []
  public storeSubject: FormGroup;
  public cloneSubject: FormGroup;
  public batches=[];
  public batchWiseClasses=[]
  public subjectLists=[];
  public subjectOfParticularBatch=[];
  public check=false;
  public levelLists=[]
  public subjectEdit:FormGroup;
  public tab;
  public idForEdit;
  public batch;
  public level;
  public id;
  public arraylists=[]
  public indexForDelete;
  constructor(private service: SubjectManagementService) { }
  states = ['2070', '2071', '2072', '2073', '2074', '2075'];
  menulists = [
    { fa: 'fa fa-dashboard', status: true, link: 'n', name: 'BSc CSIT' },
    { fa: 'fa fa-university', status: true, link: 'm', name: 'Science' },
    { fa: 'fa fa-users', status: true, link: 'o', name: 'Management' },
    { fa: 'fa fa-users', status: true, link: 'q', name: 'BBS' },
    { fa: 'fa fa-users', status: true, link: 'r', name: 'BCA' },
  ]
  batchs = [
    {  batchname: 2070 },
    {  batchname: 2071 },
    {  batchname: 2072 },
    {  batchname: 2073 },
    {  batchname: 2074 },
    {  batchname: 2075 },
    {  batchname: 2076 },
    {  batchname: 2077 },
    {  batchname: 2078 },
    {  batchname: 2079 },
    {  batchname: 2080 },
    {  batchname: 2081 }
  ]
  Lists = [
    { fa: 'fa fa-dashboard', status: false, link: 'n', name: ' Live Visitor', batchname: 2070, listname: 'level1' },
    { fa: 'fa fa-dashboard', status: false, link: 'n', name: ' Live Visitor', batchname: 2070, listname: 'level2' },
    { fa: 'fa fa-dashboard', status: false, link: 'n', name: ' Live Visitor', batchname: 2070, listname: 'level3' },
    { fa: 'fa fa-dashboard', status: false, link: 'n', name: ' Live Visitor', batchname: 2070, listname: 'level4' },
  ]
  ngOnInit() {
    this.getSubject();
    this.cloneSubject = new FormGroup({
      faculty: new FormControl(),
      past_year: new FormControl(),
      year: new FormControl(),
      level: new FormControl()
    });

    this.storeSubject = new FormGroup({
      subjectName: new FormControl(),
      faculty: new FormControl(),
      level: new FormControl(),
      year: new FormControl()
    })
    this.subjectEdit=new FormGroup({
      subjectName:new FormControl()
    })
    this.service.getFaculty().subscribe(
      (response) => {
        console.log(response)
        this.facultiesList = response.faculties;
        console.log(this.facultiesList)
      }
    )
    this.service.getBatchYear().subscribe(
      (response)=>{
        console.log(response.batches)
        this.batches=response.batches;
      }
    )

    this.cloneSubject.controls.faculty.valueChanges.subscribe(
      (term)=>{
         console.log(term)
         if(term){
           if(term.name.toLowerCase() === 'school'){
            this.levelLists=term.faculty_wise_class;
            this.check=true
           }else{
            this.check=false
           }
         }
      }
    )
    
    this.storeSubject.controls.faculty.valueChanges.subscribe(
      (term)=>{
        console.log(term)
        if(term){
          if(term.name.toLowerCase() === "school"){
            this.batchWiseClasses=term.faculty_wise_class;
            // this.batchWiseClasses=term.name.faculty_wise_class;
               }
             }
        if(term){
          console.log(term.id)
          this.service.getLevel(term.id).subscribe(
            (response)=>{
              console.log(response.level);
              this.batchWiseClasses=response.level[0].faculty_wise_class;
              console.log(this.batchWiseClasses)
            }
          )
        }
      }
    )
  }

  getSubject(){
  this.subjectLists=[];
  this.subjectOfParticularBatch=[];
  console.log('Tested 1',this.subjectLists);
  console.log('Tested 2',this.subjectOfParticularBatch)
  this.service.getSubject().subscribe(
    (response)=>{
      console.log(response.subjects)
        this.subjectLists=response.subjects;
          for(let x in this.subjectLists){
            if(this.subjectLists[x].faculty.name === this.facultiesList[0].name){
               this.subjectOfParticularBatch=this.subjectLists[x];
               console.log(this.subjectOfParticularBatch)
             }     
          }
       }
     )
  }
  submitCloneSubject() {
    console.log(this.cloneSubject.value);
    console.log(this.cloneSubject.value);
    let data = this.cloneSubject.value;
    data['faculty']=this.cloneSubject.controls.faculty.value.id;
    data['level']=null;
    if(this.cloneSubject.controls.level.value){
      data['level']=this.cloneSubject.controls.level.value.faculties;
    }
    data['past_year']=this.cloneSubject.controls.past_year.value.batch;
    this.service.postCloneSubject(data).subscribe(
      (response) => {
        console.log(response)
        if(response){
          this.getSubject();
        }
        
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Sucessfully Clone Subject!',
          showConfirmButton: false,
          timer: 4000
        })

        this.cloneSubject.reset();
    },(error)=>{
        
      swal({
          position: 'top-end',
          type: 'error',
          title: 'Unable to Clone Subject!',
          showConfirmButton: false,
          timer: 4000
        })

        this.cloneSubject.reset();
      }
    )
  }
  submitStoreSubject() {
    console.log(this.storeSubject.value);
    let data = this.storeSubject.value
    data['faculty']=this.storeSubject.controls.faculty.value.id;
    data['year']=this.storeSubject.controls.year.value.batchname;
    data['level']=this.storeSubject.controls.level.value.faculties;
    console.log(data)
    this.service.storeSubject(data).subscribe(
      (response) => {

        swal({
          position: 'top-end',
          type: 'success',
          title: 'Sucessfully Add Subject!',
          showConfirmButton: false,
          timer: 4000
        })
        console.log(response)
         if(response){
           this.getSubject();
         }
         this.storeSubject.controls.subjectName.setValue('');
      },(error)=>{
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Unable to  Add Subject!',
          showConfirmButton: false,
          timer: 4000
        })
        this.storeSubject.reset();
      }
    )
  }
  onLinkClick(event: MatTabChangeEvent) {
    console.log('tab => ', event.tab.textLabel);
     this.tab=event.tab.textLabel;
     for(let x in this.subjectLists){
         if(this.subjectLists[x].faculty.name === event.tab.textLabel){
            this.subjectOfParticularBatch=this.subjectLists[x];
            console.log(this.subjectOfParticularBatch)
          }
       }
    }
   editSubject(batch,level,id){
     console.log(batch,level,id)
     this.batch=batch;
     this.level=level;
     this.id=id;
     this.idForEdit=id;
     console.log(this.subjectOfParticularBatch)
     for(let x in this.subjectLists){
      if(this.subjectLists[x].faculty.name === this.tab){
          console.log(this.subjectLists[x].batch);
          for(let y in this.subjectLists[x].batch){
            console.log(this.subjectLists[x].batch[y].batch)
            if(this.subjectLists[x].batch[y].batch == batch){
               for(let z in this.subjectLists[x].batch[y].levels){
                  console.log(this.subjectLists[x].batch[y].levels[z].level)
                  if(this.subjectLists[x].batch[y].levels[z].level === level){
                     for(let i in this.subjectLists[x].batch[y].levels[z].subjects){
                       if(this.subjectLists[x].batch[y].levels[z].subjects[i].id == id){
                         console.log(this.subjectLists[x].batch[y].levels[z].subjects[i].name)
                         this.subjectEdit.controls.subjectName.setValue(this.subjectLists[x].batch[y].levels[z].subjects[i].name)
                       }
                     }
                  }
               }
            }
          }
       }
    }
       jQuery("#myModal").modal("show");
    } 
    submitSubjectEdit(){
      jQuery("#myModal").modal("hide");
     console.log(this.subjectEdit.value)
     let data={}
     data=this.subjectEdit.value;
     data['id']=this.idForEdit;
     console.log(data)
     this.service.editSubject(data).subscribe(
       (response)=>{
         console.log(response)
   
         swal({
          position: 'top-end',
          type: 'success',
          title: 'Sucessfully Edit Subject!',
          showConfirmButton: false,
          timer: 4000
          })

         for(let x in this.subjectLists){
          if(this.subjectLists[x].faculty.name === this.tab){
              console.log(this.subjectLists[x].batch);
              for(let y in this.subjectLists[x].batch){
                console.log(this.subjectLists[x].batch[y].batch)
                if(this.subjectLists[x].batch[y].batch == this.batch){
                   for(let z in this.subjectLists[x].batch[y].levels){
                      console.log(this.subjectLists[x].batch[y].levels[z].level)
                      if(this.subjectLists[x].batch[y].levels[z].level === this.level){
                         for(let i in this.subjectLists[x].batch[y].levels[z].subjects){
                           if(this.subjectLists[x].batch[y].levels[z].subjects[i].id == this.id){
                             console.log(this.subjectLists[x].batch[y].levels[z].subjects[i].name)
                             this.subjectLists[x].batch[y].levels[z].subjects[i].name=this.subjectEdit.controls.subjectName.value;
                           }
                         }
                      }
                   }
                }
              }
           }
         }
       },(error)=>{
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Unable to Edit Subject!',
          showConfirmButton: false,
          timer: 4000
          })
       }
     )
    }
    deleteSubject(i,batch,level,id) {
      jQuery("#deletemyModal").modal("show");
       this.batch=batch;
       this.level=level;
       this.id=id;
       this.indexForDelete=i;
       console.log(i,batch,level,id)
    } 
    deletedSubjectModel(){
      jQuery("#deletemyModal").modal("hide");
      this.service.deleteSubject(this.id).subscribe(
        (response)=>{
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Sucessfully Delete Subject!',
            showConfirmButton: false,
            timer: 4000
            })
          console.log(response)
          for(let x in this.subjectLists){
            if(this.subjectLists[x].faculty.name === this.tab){
                console.log(this.subjectLists[x].batch);
                for(let y in this.subjectLists[x].batch){
                  console.log(this.subjectLists[x].batch[y].batch)
                  if(this.subjectLists[x].batch[y].batch == this.batch){
                     for(let z in this.subjectLists[x].batch[y].levels){
                        console.log(this.subjectLists[x].batch[y].levels[z].level)
                        if(this.subjectLists[x].batch[y].levels[z].level === this.level){
                          this.subjectLists[x].batch[y].levels[z].subjects.splice(this.indexForDelete)
                        }
                     }
                  }
                }
             }
           }
        },(error)=>{
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Unable To Delete Subject!',
            showConfirmButton: false,
            timer: 4000
            })
        }
      )
   }
   public deleteAllSubject(batch,level){
    jQuery("#deleteinblock").modal("show");
    this.batch=batch;
    this.level=level;
     let data={};
       for(let x in this.subjectLists){
          if(this.subjectLists[x].faculty.name === this.tab){
              for(let y in this.subjectLists[x].batch){
                console.log(this.subjectLists[x].batch[y].batch)
                if(this.subjectLists[x].batch[y].batch == batch){
                   for(let z in this.subjectLists[x].batch[y].levels){
                      console.log(this.subjectLists[x].batch[y].levels[z].level)
                      if(this.subjectLists[x].batch[y].levels[z].level === level){
                         for(let i in this.subjectLists[x].batch[y].levels[z].subjects){
                           data={};
                           console.log('id',this.subjectLists[x].batch[y].levels[z].subjects[i].id)
                           data['id']=this.subjectLists[x].batch[y].levels[z].subjects[i].id;
                           data['index']=i;
                           this.arraylists.push(data)
                         }
                      }
                   }
                }
              }
           }
         } 
     }
    public deletedInBlockModel(){
      jQuery("#deleteinblock").modal("hide");
      this.service.deleteInBlockSubject(this.arraylists).subscribe(
        (response)=>{
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Sucessfully Deleted Subject!',
            showConfirmButton: false,
            timer: 4000
            })
          console.log(response)
          for(let x in this.subjectLists){
            if(this.subjectLists[x].faculty.name === this.tab){
                for(let y in this.subjectLists[x].batch){
                  console.log(this.subjectLists[x].batch[y].batch)
                  if(this.subjectLists[x].batch[y].batch == this.batch){
                     for(let z in this.subjectLists[x].batch[y].levels){
                        console.log(this.subjectLists[x].batch[y].levels[z].level)
                        if(this.subjectLists[x].batch[y].levels[z].level === this.level){
                           for(let index in this.arraylists){
                            console.log(this.arraylists[index].index)
                            this.subjectLists[x].batch[y].levels[z].subjects.splice(this.arraylists[index].index)
                           }
                        }
                     }
                   }
                 }
              }
            } 
         },(error)=>{
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Unable To Delete Subject!',
            showConfirmButton: false,
            timer: 4000
            })
         }
      )  
    }
  }