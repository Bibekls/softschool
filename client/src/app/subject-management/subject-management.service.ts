import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'
import { ENV } from "./../env";
@Injectable()
export class SubjectManagementService {

constructor(private http:Http) { }
getFaculty(){
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers, withCredentials: true });
  return this.http.get(ENV.Request_URL + "/mis_api/faculties", options)
    .map(this.extractData)
    .catch(this.handleError);
 }
 private extractData(res: Response) {
  let response = res.json();
  return response || {};
}
private handleError(error: Response | any) {
  return Observable.throw(error);
 }
 postCloneSubject(data){
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers, withCredentials: true });
  return this.http.post(ENV.Request_URL + "/mis_api/clone_subject",data, options)
    .map(this.extractData)
    .catch(this.handleError);
 }
 storeSubject(data){
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers, withCredentials: true });
  return this.http.post(ENV.Request_URL + "/mis_api/store_subject",data, options)
    .map(this.extractData)
    .catch(this.handleError);
 }
 getBatchYear(){
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers, withCredentials: true });
  return this.http.get(ENV.Request_URL + "/mis_api/get_batch", options)
    .map(this.extractData)
    .catch(this.handleError);
 }
 getLevel(id){
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers, withCredentials: true });
  return this.http.get(ENV.Request_URL + "/mis_api/get_level/"+id, options)
    .map(this.extractData)
    .catch(this.handleError);
 }
 getSubject(){
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers, withCredentials: true });
  return this.http.get(ENV.Request_URL + "/mis_api/get_subject", options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  editSubject(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers, withCredentials: true });
  return this.http.post(ENV.Request_URL + "/mis_api/edit_subject", data,options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  deleteSubject(id){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/delete_subject/"+id, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  deleteInBlockSubject(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/delete_in_block", data,options)
      .map(this.extractData)
      .catch(this.handleError);
  }
}
