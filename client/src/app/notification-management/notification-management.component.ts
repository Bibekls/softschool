import { Component, OnInit } from '@angular/core';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { of } from 'rxjs/observable/of';
import { NotificationService } from './notification.service';
import { Subject } from 'rxjs/Subject';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2'

@Component({
  selector: 'app-notification-management',
  templateUrl: './notification-management.component.html',
  styleUrls: ['./notification-management.component.scss']
})

export class NotificationManagementComponent implements OnInit {
  public checkedKeys: any[] = [];
  public checkedKeysOfbatch:any[]=[];
  public checkedKeysForSelectedBatch:any[]=[];
  public batchLists: any[]=[];
  public facultyLists: any[]=[];
  public childbatch;
  notificationForm:FormGroup;
  public hasClildbatch;
  public data: any[] = [
      {
        text: 'Furniture', items: [
          { text: 'Tables & Chairs' },
          { text: 'Sofas' },
          { text: 'Hello' }
        ]
      },
      { text: 'Decor' },
      { text: 'Outdoors' }
  ];

  public children = (dataItem: any) =>of(dataItem.items);
  public hasChildren = (dataItem: any): boolean => !!dataItem.items;
  constructor(private service:NotificationService) { }
  ngOnInit() {
 this.notificationForm=new FormGroup({
   title:new FormControl(),
   description:new FormControl()
 })
 this.service.getFacultyFormNotification().subscribe(
   (response)=>{
         console.log(response)
         this.facultyLists=response.faculties;
       }
     )
     this.service.getNotification().subscribe(
       (response)=>{
         console.log('notification',response)
       }
     )
     this.service.getfacultyAndBatch().subscribe(
      (response)=>{
            console.log(response)
            console.log(response.batches)
            let subject=[]
            for(let x in response.batcheslevel){
              if(response.batcheslevel[x].name.toLowerCase() === 'school'){
                 subject = response.batcheslevel[x].subject_level;
                console.log(subject)
              }
            }
            for(let i in response.batches){
              if(response.batches[i].name.toLowerCase() === 'school'){
                response.batches[i].subject=subject
              }
            }
            this.batchLists=response.batches;
            this.childbatch=(dataItem: any) =>of(dataItem.subject);
            this.hasClildbatch= (dataItem: any): boolean => !!dataItem.subject;
          }
        )
    }
  submitNotificationForm(){
    let lists=[]
    let data=[]
    let checkedLists=[]
    console.log(this.checkedKeysOfbatch);
    console.log(this.checkedKeysForSelectedBatch);
    console.log(this.notificationForm.value)
    let Temp=this.notificationForm.value;
    console.log(this.batchLists);
    for(let x in this.checkedKeysForSelectedBatch){
        data.push(this.checkedKeysForSelectedBatch[x].split('_'));
       }
     for(let x in data){
         if(data[x].length == 2){
            lists.push(data[x])
         }
      }
      console.log(lists)
      for(let i in lists){
          console.log(lists[i][0])
            for(let x in this.batchLists){
               if(parseInt(lists[i][0]) == parseInt(x)){
                 for(let y in this.batchLists[x].subject){
                    if(parseInt(lists[i][1]) == parseInt(y)){
                        checkedLists.push(this.batchLists[x].subject[y])
                    }
                 }   
              }             
           }
       }
       for(let x in this.checkedKeysOfbatch){
           for(let y in this.facultyLists){
             if(parseInt(this.checkedKeysOfbatch[x])== parseInt(y)){
              checkedLists.push(this.facultyLists[y])
             }
           }
        }
       console.log(checkedLists)
       Temp['target_user']=checkedLists;
       this.service.postNotification(Temp).subscribe(
         (response)=>{
           console.log(response)
           swal({
            position: 'top-end',
            type: 'success',
            title: 'Sucessfully Pushed Notification!',
            showConfirmButton: false,
            timer: 4000
          })
          this.notificationForm.reset();
          this.checkedKeysOfbatch=[]
          this.checkedKeysForSelectedBatch=[]
         },(error)=>{
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Unable To Push Notification!',
            showConfirmButton: false,
            timer: 4000
          })
          this.notificationForm.reset();
          this.checkedKeysOfbatch=[]
          this.checkedKeysForSelectedBatch=[]
         }
       )
   }
}
