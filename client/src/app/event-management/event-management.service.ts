import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'
import { ENV } from "./../env";

@Injectable()
export class EventManagementService {

  constructor(private http: Http) { }
  postEvent(data) {
    console.log(data)
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/post_event", data, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  private extractData(res: Response) {
    let response = res.json();
    return response || {};
  }
  private handleError(error: Response | any) {
    return Observable.throw(error);
  }
  getEvent() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_event", options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  futureEvent() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/future_event", options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  pastEvent() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/past_event", options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  deleteEvent(id){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/delete_event/"+id,options)
      .map(this.extractData)
      .catch(this.handleError);
  }
}
