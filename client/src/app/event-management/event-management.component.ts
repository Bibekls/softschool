import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
declare var jQuery: any;
declare var $;
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { FormGroup, FormControl } from '@angular/forms';
import { EventManagementService } from './event-management.service';
import swal from 'sweetalert2'
import { MatTabChangeEvent } from '@angular/material';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};
@Component({
  selector: 'app-event-management',
  templateUrl: './event-management.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./event-management.component.scss']
})
export class EventManagementComponent implements OnInit {
  // @ViewChild('modalContent') modalContent: TemplateRef<any>;
  activeDayIsOpen: boolean = true;
  public eventForm: FormGroup;
  public hideTime = false;
  events: CalendarEvent[];
  public editEvent:FormGroup;
  public upComingLists=[];
  public completeList=[];
  public eventData=[]
  public is_Edit=false;
  public idForEdit;
  public indexForDelete;
  public idForDelete;
  public indexForDeleteCompleted;
  constructor(private modal: NgbModal, private service: EventManagementService) { }
  ngOnInit() {
    this.eventForm = new FormGroup({
      title: new FormControl(),
      discription: new FormControl(),
      joinDate: new FormControl(),
      check: new FormControl(false),
      startAt: new FormControl(),
      endAt: new FormControl(),
    })

    this.editEvent=new FormGroup({
      title: new FormControl(),
      Date: new FormControl(),
    });
    this.eventForm.controls.check.valueChanges.subscribe(
      (term) => {
        this.hasTimeIntervalCheck();
      })
    this.service.getEvent().subscribe(
      (response) => {
        console.log(response.events)
        this.events=response.events;
        this.eventData=response.events;
        for(let x in this.events){
           this.events[x].start=startOfDay(new Date(this.events[x].start));
           this.events[x].color= colors.yellow;
           this.events[x].actions=this.actions;
        }
      }
    )
    this.service.futureEvent().subscribe(
      (response)=>{
         console.log(response.futureEvents)
         this.upComingLists=response.futureEvents;
      }
    )
    this.service.pastEvent().subscribe(
      (response)=>{
        console.log(response.pastEvents)
        this.completeList=response.pastEvents;
      }
    )
  }
  submitEditForm(){
    console.log(this.editEvent.value)
  }
  submitEventForm() {
    console.log(this.eventForm.value)
    let data = this.eventForm.value;
    data['is_Edit']=this.is_Edit;
    data['id']=this.idForEdit;
    this.service.postEvent(data).subscribe(
      (response) => {
        console.log('Testing response date',response)
        if(response.event == 1){
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Sucessfully Updated Event!',
            showConfirmButton: false,
            timer: 4000
          })
          for(let x in this.upComingLists){
            if(this.upComingLists[x].id == this.idForEdit){
              this.upComingLists[x].date=this.eventForm.controls.joinDate.value;
              this.upComingLists[x].description=this.eventForm.controls.discription.value;
              this.upComingLists[x].start_at=this.eventForm.controls.startAt.value;
              this.upComingLists[x].end_at=this.eventForm.controls.endAt.value;
            }
          }
          for(let x in this.events){
            if(this.events[x].id == this.idForEdit){
              this.events[x].start=startOfDay(new Date( this.eventForm.controls.joinDate.value));
              this.events[x].color= colors.yellow;
              this.events[x].actions=this.actions;
            }
          }
          this.eventForm.reset();
          this.is_Edit=false;
        }else{
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Sucessfully Registed Event!',
            showConfirmButton: false,
            timer: 4000
          })
          console.log(response.event)
           response.event.start=startOfDay(new Date(response.event.start));
           response.event.color= colors.yellow;
           response.event.actions=this.actions;
           console.log(response.event)
           this.upComingLists.push(response.event)
           this.events.push(response.event);
          console.log(this.events);
          console.log(this.upComingLists)
        }       
      }
    )
  }
  
  hasTimeIntervalCheck() {
    if (this.eventForm.controls.check.value === true) {
      this.hideTime = true
    } else {
      this.hideTime = false
    }
  }
  view: string = 'month';
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  refresh: Subject<any> = new Subject();

  actions: CalendarEventAction[] = [
    {
      label: '',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];
  
  // events: CalendarEvent[] = [
  //   {
  //     start: subDays(startOfDay(new Date()), 1),
  //     end: addDays(new Date(), 1),
  //     title: 'A 3 day event',
  //     color: colors.red,
  //     actions: this.actions
  //   },
  //   {
  //     start: startOfDay(new Date()),
  //     title: 'An event with no end date',
  //     color: colors.yellow,
  //     actions: this.actions
  //   }
  //   ,
  //   {
  //     start: subDays(endOfMonth(new Date()), 3),
  //     end: addDays(endOfMonth(new Date()), 3),
  //     title: 'A long event that spans 2 months',
  //     color: colors.blue
  //   },
  //   {
  //     start: addHours(startOfDay(new Date()), 2),
  //     end: new Date(),
  //     title: 'A draggable and resizable event',
  //     color: colors.yellow,
  //     actions: this.actions,
  //     resizable: {
  //       beforeStart: true,
  //       afterEnd: true
  //     },
  //     draggable: true
  //   }
  // ];
  handleEvent(action: string, event: CalendarEvent): void {
    console.log(action)
    this.modalData = { event, action };
    jQuery("#modalContent").modal("show");
  }

  eventTimesChanged({ event, newStart, newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }
  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }
  close() {
    jQuery("#modalContent").modal("hide");
  }
  onLinkClick(event: MatTabChangeEvent) {
    console.log('tab => ', event.tab);
  }
  deleteUpComingEvent(index){
    this.indexForDelete=index;
    jQuery('#myModal').modal('show');
  }
  deletedEventModel(){
  jQuery('#myModal').modal('hide');
  let id = this.upComingLists[this.indexForDelete].id;
  this.service.deleteEvent(id).subscribe(
    (response)=>{
      this.upComingLists.splice(this.indexForDelete,-1);
      console.log(response)
      swal({
        position: 'top-end',
        type: 'success',
        title: 'Sucessfully Deleted Event!',
        showConfirmButton: false,
        timer: 4000
      })
    },(error)=>{
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Unable to Delete Event!',
        showConfirmButton: false,
        timer: 4000
      })
    }
  )
  }
  deleteCompleteEvent(index){
    this.indexForDeleteCompleted=index;
    jQuery('#deletemyModal').modal('show');    
  }
  deletedCompletedEventModel(){
    jQuery('#deletemyModal').modal('hide');    
    let id=this.completeList[this.indexForDeleteCompleted].id;
    this.service.deleteEvent(id).subscribe(
       (response)=>{
         console.log(response);
         this.completeList.splice(this.indexForDeleteCompleted,-1);
         swal({
             position: 'top-end',
            type: 'success',
            title: 'Sucessfully Deleted Event!',
            showConfirmButton: false,
            timer: 4000
          })
         },(error)=>{
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Sucessfully Deleted Event!',
           showConfirmButton: false,
           timer: 4000
         })
         }
      )
  }
  editUpComingEvent(id){
    console.log(id)
    this.is_Edit=true;
    this.idForEdit=id;
    for(let x in this.eventData){
      if(this.events[x].id == id){
           this.eventForm.controls.title.setValue(this.eventData[x].title);
           this.eventForm.controls.discription.setValue(this.eventData[x].description)
           this.eventForm.controls.joinDate.setValue(this.eventData[x].date)
           if(this.eventData[x].start_at !== null){
            this.eventForm.controls.check.setValue(true)
            this.eventForm.controls.startAt.setValue(this.eventData[x].start_at)
            this.eventForm.controls.endAt.setValue(this.eventData[x].end_at)
           }
       } 
     }
   }
};
