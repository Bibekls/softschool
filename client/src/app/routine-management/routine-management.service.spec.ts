import { TestBed, inject } from '@angular/core/testing';

import { RoutineManagementService } from './routine-management.service';

describe('RoutineManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RoutineManagementService]
    });
  });

  it('should be created', inject([RoutineManagementService], (service: RoutineManagementService) => {
    expect(service).toBeTruthy();
  }));
});
