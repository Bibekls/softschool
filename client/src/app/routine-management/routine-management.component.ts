import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { RoutineManagementService } from './routine-management.service';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import swal from 'sweetalert2'
import { MatTabChangeEvent } from '@angular/material';
import { LowerCasePipe, getLocaleDateTimeFormat } from '@angular/common';
import { getTime } from 'date-fns';
import * as moment from 'moment';
declare var jQuery: any;
declare var $;

export class User {
  constructor(
     public name: string,
     public updated_at: Date,
     public id: Number, 
     public created_at: Date, 
     public end_at: Date, 
     public start_at: Date) { }
}

@Component({
  selector: 'app-routine-management',
  templateUrl: './routine-management.component.html',
  styleUrls: ['./routine-management.component.scss']
})

export class RoutineManagementComponent implements OnInit {
  currentDate: Date = new Date(2017, 4, 25);
  public teacherLists = [];
  public selectedFaculty = [];
  public faculitiesLists: User[]
  routineForm: FormGroup;
  public tempLevel;
  public tempBatch;
  public tempFaculty;
  public tempFacultyId;
  filteredOptions: Observable<User[]>;
  myControl = new FormControl()
  public batchLists = []
  public subjectLists = []
  public routineLists=[]
  public batchWiseClasses=[]
  public routineEachTab;
  public dayByDayRoutine=[];
  public tempTimeIntervals={}; 
  public timeIntervals=[]
  public startTime;
  public endTime;
  public totalTime;
  public TempArray=[];
  public dayForDelete;
  public indexForDelete;
  public idForDelete;
  public deleteLists=[]
  public startedMinutes;
  constructor(private service: RoutineManagementService) { }
  toppings = new FormControl();
  post = new FormControl();
  days = [
    { id: 1, name: 'Sunday' },
    { id: 2, name: 'Monday' },
    { id: 3, name: 'Tuesday' },
    { id: 4, name: 'Wednesday' },
    { id: 5, name: 'Thursday' },
    { id: 6, name: 'Friday' },
    { id: 7, name: 'Saturday' }
  ];
  lavelLists = [
    { id: 1, level: '1' },
    { id: 2, level: '2' },
    { id: 3, level: '3' },
    { id: 4, level: '4' },
    { id: 5, level: '5' },
    { id: 6, level: '6' },
    { id: 7, level: '7' },
    { id: 7, level: '8' }
  ];
  toppingList = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  ngOnInit() {
    this.routineForm = new FormGroup({
      faculty: new FormControl(),
      teacher: new FormControl(),
      days: new FormControl(),
      batch: new FormControl(),
      level: new FormControl(),
      subject: new FormControl(),
      start_at: new FormControl(),
      end_at: new FormControl()
    });
    this.service.getFaculty().subscribe(
      (response) => {
         console.log(response)
         this.faculitiesLists = response.faculties;
         this.endTime=this.faculitiesLists[0].end_at;
         this.startTime=this.faculitiesLists[0].start_at;

         var start = this.startTime.split(':');
         var end = this.endTime.split(':');
         console.log(start[0],start[1])
         var startMinues=parseInt(start[0])*60 +parseInt(start[1]);
         this.startedMinutes=parseInt(start[0])*60 +parseInt(start[1]);
         var endMinues=parseInt(end[0])*60 +parseInt(end[1]);
         var tempMinutes=endMinues - startMinues;
         console.log(tempMinutes)
         this.totalTime = tempMinutes * 60;
         console.log(this.totalTime)
         console.log(this.endTime,this.startTime)
         var startingTime=moment(this.startTime, 'h:mm', 'Pacific/Auckland');
         var endingTime=moment(this.endTime, ' h:mm', 'Pacific/Auckland');
         console.log(startingTime,endingTime)
         while(startingTime < endingTime){
            this.tempTimeIntervals={}    
            let start=startingTime.format("h:mm");
            this.tempTimeIntervals['val']=(startingTime.format("h:mm") + " - " + startingTime.add(30, "minutes").format("h:mm"));
            console.log(this.tempTimeIntervals['val'])
            var sep_start = start.split(':');
            var tSep_start= parseInt(sep_start[0])* 60+parseInt(sep_start[1])
            this.tempTimeIntervals['startTime']=tSep_start * 60;
            var sep_end = startingTime.format("h:mm").split(':');
            var tSep_end= parseInt(sep_end[0])* 60+parseInt(sep_end[1]);
            this.tempTimeIntervals['endTime']=tSep_end * 60;
            let data=this.tempTimeIntervals;
            this.timeIntervals.push(this.tempTimeIntervals)
          }
         console.log('time',this.timeIntervals)
      })
      this.service.getRoutine().subscribe(
        (response)=>{
          this.routineLists=response.routines;
          for(let x in this.routineLists){
            if(this.routineLists[x].faculty.id == this.faculitiesLists[0].id){
              this.routineEachTab=this.routineLists[x];
              for(let j in this.routineLists[x].levels[0].days){
                let tst;
                let flag=true;
                let count = 0;
                this.TempArray=[];
                for(let i in this.routineLists[x].levels[0].days[j].period){
                    if(flag){
                      // console.log(i);
                      if(this.startedMinutes * 60 != this.routineLists[x].levels[0].days[j].period[i].start_at){
                          let temp=parseInt(this.routineLists[x].levels[0].days[j].period[i].start_at) - (this.startedMinutes * 60);                     
                          let temp_val = {
                           id:this.routineLists[x].levels[0].days[j].period[i].id,
                           style: 2000*temp/this.totalTime,
                           start_at: this.startedMinutes * 60,
                           end_at:this.routineLists[x].levels[0].days[j].period[i].start_at,
                           fname:'',
                           lname:'',
                           subject: 'N/A'
                          }
                          this.TempArray.push(temp_val);
                      }
                      
                      let temp=parseInt(this.routineLists[x].levels[0].days[j].period[i].end_at) - parseInt(this.routineLists[x].levels[0].days[j].period[i].start_at);
                      this.routineLists[x].levels[0].days[j].period[i].style= 2000*temp/this.totalTime ;
                      flag=false;
                      tst=this.routineLists[x].levels[0].days[j].period[i].end_at;
                      this.TempArray.push(this.routineLists[x].levels[0].days[j].period[i])
                    }
                    else{
                       if(tst != this.routineLists[x].levels[0].days[j].period[i].start_at) {
                        let temp=parseInt(this.routineLists[x].levels[0].days[j].period[i].start_at) - parseInt(tst);                     
                           let temp_val = {
                            id:this.routineLists[x].levels[0].days[j].period[i].id,
                            style: 2000*temp/this.totalTime,
                            start_at: tst,
                            end_at:this.routineLists[x].levels[0].days[j].period[i].start_at,
                            fname:'',
                            lname:'',
                            subject: 'break'
                           }

                           console.log('Start_time: ', tst);
                           console.log('End_time: ', this.routineLists[x].levels[0].days[j].period[i].start_at);
                          this.TempArray.push(temp_val);
                           tst=temp_val.end_at;
                           console.log('TST: ', tst);
                           temp=parseInt(this.routineLists[x].levels[0].days[j].period[i].end_at) - parseInt(this.routineLists[x].levels[0].days[j].period[i].start_at);
                           this.routineLists[x].levels[0].days[j].period[i].style=2000*temp/this.totalTime ;                           
                           this.TempArray.push(this.routineLists[x].levels[0].days[j].period[i]);
                      }
                      else{
                        let temp=parseInt(this.routineLists[x].levels[0].days[j].period[i].end_at) - parseInt(this.routineLists[x].levels[0].days[j].period[i].start_at);
                        this.routineLists[x].levels[0].days[j].period[i].style= 2000*temp/this.totalTime ;
                        tst=this.routineLists[x].levels[0].days[j].period[i].end_at;
                        this.TempArray.push(this.routineLists[x].levels[0].days[j].period[i])
                      } 
                   }                          
                } 
                console.log(this.TempArray)
                this.routineLists[x].levels[0].days[j].period=[];
                this.routineLists[x].levels[0].days[j].period=this.TempArray;
                console.log(this.routineLists[x].levels[0].days[j].period)
                this.dayByDayRoutine=this.routineLists[x].levels[0].days;
                console.log('day By day',this.dayByDayRoutine)
              }
            }
          }
          console.log('Console.log',this.TempArray)

        }
     )
    this.service.getBatch().subscribe(
      (response) => {
        this.batchLists = response.batches;
      }
    )
    this.routineForm.controls.batch.valueChanges.subscribe(
      (batch) => {
        if (batch) {
          this.tempBatch = batch.batch;
          this.tempGetSubject();
        }
      }
    )
    this.routineForm.controls.level.valueChanges.subscribe(
      (level) => {
        if (level) {
          this.tempLevel = level.faculties;
          this.tempGetSubject();
        }
      }
    )
    this.routineForm.controls.faculty.valueChanges.subscribe(
      (faculty) => {
        if (faculty) {
          this.tempFaculty = faculty.id;
          this.service.getTeacher(faculty.id).subscribe(
            (response) => {
              this.teacherLists = response.teachers;
            })
          this.service.getLevel(faculty.id).subscribe(
            (response)=>{
              this.batchWiseClasses=response.level[0].faculty_wise_class;
            }
          ) 
          this.tempGetSubject();
        }
      }
    )
  }
  submitRoutineForm() {
    let data = this.routineForm.value;
    console.log(data)
    this.service.addRoutine(data).subscribe(
      (response) => {
         console.log('Test',response)
        if (response.test == 1) {
          swal({
            position: 'top-end',
            type: 'error',
            title: response.time_interval,
            showConfirmButton: false,
            timer: 4000
          })
        }
        if(response.test == 2) {
          swal({
            position: 'top-end',
            type: 'error',
            title: response.time_interval,
            showConfirmButton: false,
            timer: 4000
          })
        }
        if(response.test == 3) {
          swal({
            position: 'top-end',
            type: 'success',
            title: response.time_interval,
            showConfirmButton: false,
            timer: 4000
          })
          console.log('testing console 271',response)
          for(let x in this.dayByDayRoutine){
              if(response.routine[0].day == this.dayByDayRoutine[x].day){
                this.dayByDayRoutine[x].period.push(response.routine[0]);
                console.log(this.dayByDayRoutine[x].period)
            }
          }
        }
      }
    )
  }
  CkeckComtroller() {
    console.log('testing Controller');
  }
  public tempGetSubject() {
    console.log(this.tempBatch,this.tempFaculty,this.tempLevel)
    if (this.tempBatch && this.tempFaculty && this.tempLevel) {
      let data = {}
      data['level'] = this.tempLevel;
      data['batch'] = this.tempBatch;
      data['faculty'] = this.tempFaculty;
      console.log(data)
      this.service.getSubject(data).subscribe(
        (response) => {
          console.log(response)
          this.subjectLists = response.subjects;
        }
      )
    }
  }
  onLinkClickBatch(event: MatTabChangeEvent) {
    console.log(event.tab.textLabel)
    for(let x in this.routineLists){
      if(this.routineLists[x].faculty.name.toLowerCase() === event.tab.textLabel.toLowerCase()){
        console.log(this.routineLists[x]); 
        this.routineEachTab=this.routineLists[x];
        this.dayByDayRoutine=this.routineLists[x].levels[0].days;
        for(let j in this.dayByDayRoutine){
          let tst;
          let flag=true;
          let count = 0;
          this.TempArray=[];
          for(let i in this.dayByDayRoutine[j].period){
            if(flag){
              if((this.startedMinutes * 60) != this.dayByDayRoutine[j].period[i].start_at) {
                  let temp=parseInt(this.dayByDayRoutine[j].period[i].start_at) - (this.startedMinutes * 60);                     
                    let temp_val = {
                     id:this.dayByDayRoutine[j].period[i].id,
                     style: 2000*temp/this.totalTime,
                     start_at: this.startedMinutes * 60,
                     end_at:this.dayByDayRoutine[j].period[i].start_at,
                     fname:'',
                     lname:'',
                     subject: 'N/A'
                    }
                    this.TempArray.push(temp_val);
              }
              flag=false;
              tst=this.dayByDayRoutine[j].period[i].end_at;
              this.TempArray.push(this.dayByDayRoutine[j].period[i])
            }
            else{
              if(tst !=this.dayByDayRoutine[j].period[i].start_at) {
               let temp=parseInt(this.dayByDayRoutine[j].period[i].start_at) - parseInt(tst);  
               let temp_val = {
                id:this.dayByDayRoutine[j].period[i].id,
                style: '',
                start_at: tst,
                end_at:this.dayByDayRoutine[j].period[i].start_at,
                fname:'',
                lname:'',
                subject: 'break'
               }
               this.TempArray.push(temp_val);
               tst=temp_val.end_at;
               console.log('TST: ', tst);
               this.TempArray.push(this.dayByDayRoutine[j].period[i]);
              }else{
                tst=this.dayByDayRoutine[j].period[i].end_at;
                this.TempArray.push(this.dayByDayRoutine[j].period[i])
              }
            }
          }
          this.dayByDayRoutine[j].period=[];         
          this.dayByDayRoutine[j].period=this.TempArray;
        }
        console.log(this.TempArray)
     }
   }
}
  onLinkSubTab(event: MatTabChangeEvent){
    console.log('tab => ', event.tab.textLabel);
      for(let x in this.routineEachTab.levels){
          if(this.routineEachTab.levels[x].level == event.tab.textLabel){
            this.dayByDayRoutine=this.routineEachTab.levels[x].days;
            for(let j in this.dayByDayRoutine){
              let tst;
              let flag=true;
              let count = 0;
              this.TempArray=[];
              for(let i in this.dayByDayRoutine[j].period){
                if(flag){
                  flag=false;
                  tst=this.dayByDayRoutine[j].period[i].end_at;
                  this.TempArray.push(this.dayByDayRoutine[j].period[i])
                }
                else{
                  if(tst !=this.dayByDayRoutine[j].period[i].start_at) {
                   let temp=parseInt(this.dayByDayRoutine[j].period[i].start_at) - parseInt(tst);  
                   let temp_val = {
                    id:this.dayByDayRoutine[j].period[i].id,
                    style: '',
                    start_at: tst,
                    end_at:this.dayByDayRoutine[j].period[i].start_at,
                    fname:'',
                    lname:'',
                    subject: 'break'
                   }
                   this.TempArray.push(temp_val);
                   tst=temp_val.end_at;
                   console.log('TST: ', tst);
                   this.TempArray.push(this.dayByDayRoutine[j].period[i]);
                  }else{
                    tst=this.dayByDayRoutine[j].period[i].end_at;
                    this.TempArray.push(this.dayByDayRoutine[j].period[i])
                  }
                }
              }
              this.dayByDayRoutine[j].period=[];         
              this.dayByDayRoutine[j].period=this.TempArray;
            }
            console.log(this.TempArray)
          }
        }
     }

    progressRoutineDelete(index,id,day){
      console.log(index,id,day)  
      jQuery('#myModal').modal('show');
      this.dayForDelete=day;
      this.indexForDelete=index;
      this.idForDelete=id;
    }

    deletedRoutineModel(){
      jQuery('#myModal').modal('hide');
      console.log(this.dayForDelete,this.idForDelete,this.indexForDelete);
      let data={};
      data['id']=this.idForDelete;
      // data['day']=this.dayForDelete;
      this.service.deleteRoutine(data).subscribe(
        (response)=>{         
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Successfully Deleted Routine!',
            showConfirmButton: false,
            timer: 4000
          })
          for(let x in this.dayByDayRoutine){
            if(this.dayByDayRoutine[x].day == this.dayForDelete){
              console.log(this.dayByDayRoutine[x].period)
              this.dayByDayRoutine[x].period.splice(this.indexForDelete)
            }
          }

          console.log(response)
        },(error)=>{
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Unable To Delete Routine!',
            showConfirmButton: false,
            timer: 4000
          })
        }
      )
      console.log('I am tested')
    }

    progressAllRoutineDelete(day){
    jQuery('#blockMyModal').modal('show');
    console.log(day)
    this.dayForDelete=day;
    this.deleteLists=[]
      let param={}
       for(let x in this.dayByDayRoutine){
         if(this.dayByDayRoutine[x].day == day){
            console.log(this.dayByDayRoutine[x].period);
              for(let i in this.dayByDayRoutine[x].period){
                if(this.dayByDayRoutine[x].period[i].subject !== 'break'){
                  console.log()
                  param={}
                   param['id']=this.dayByDayRoutine[x].period[i].id;
                   this.deleteLists.push(param)
                }
            }
         }
         console.log('Testing',this.deleteLists)
      }
  }

    deletedBlockRoutineModel(){
      jQuery('#blockMyModal').modal('hide');
      this.service.deletedBlockOfRoutine(this.deleteLists).subscribe(
        (response)=>{
          console.log(response)
          for(let x in this.dayByDayRoutine){
            if(this.dayByDayRoutine[x].day == this.dayForDelete){
              this.dayByDayRoutine[x]=[]
            }
          }
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Successfully Deleted Routine!',
            showConfirmButton: false,
            timer: 4000
          })
        },(error)=>{
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Unable To Deleted Routine!',
            showConfirmButton: false,
            timer: 4000
          })
        }
      )
    }  
  }

