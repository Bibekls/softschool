import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'
import { ENV } from "./../env";
@Injectable()

export class RoutineManagementService {
  constructor(private http:Http) { }
getFaculty(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/faculties", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   private extractData(res: Response) {
    let response = res.json();
    return response || {};
  }
  private handleError(error: Response | any) {
    return Observable.throw(error);
   }
   getTeacher(id){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_teacher_form_routine/"+id, options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getSubject(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/get_subject_form_routine",data ,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getBatch(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_batch", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   addRoutine(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/add_routine",data ,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getRoutine(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_routine" ,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getLevel(id){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_level/"+id, options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   deleteRoutine(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/delete_routine",data,options)
    .map(this.extractData)
    .catch(this.handleError);
   }
   deletedBlockOfRoutine(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/delete_block_routine",data,options)
    .map(this.extractData)
    .catch(this.handleError);
   }
  }

