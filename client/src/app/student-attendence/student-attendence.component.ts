
import { Component, OnInit,ViewContainerRef,ViewChild, ComponentFactoryResolver} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
// import { DaterangePickerComponent } from 'ng2-daterange-picker';
import { AttendenceService } from './attendence.service';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-student-attendence',
  templateUrl: './student-attendence.component.html',
  styleUrls: ['./student-attendence.component.scss']
})
export class StudentAttendenceComponent implements OnInit {
  myControl: FormControl = new FormControl();
  options = [
    'One',
    'Two',
    'Three'
  ];

  date = new FormControl(new Date());
  // serializedDate = new FormControl((new Date()).toISOString());
  Date= new FormControl();
  filteredOptions: Observable<string[]>;

  public  datas = [
  {name: "Moroni", age: 50},
  {name: "Tiancum", age: 43},
  {name: "Jacob", age: 27},
  {name: "Nephi", age: 29},
  {name: "Enos", age: 34},
  {name: "Tiancum", age: 43},
  {name: "Jacob", age: 27},
  {name: "Nephi", age: 29},
  {name: "Enos", age: 34},
  {name: "Tiancum", age: 43},
  {name: "Jacob", age: 27},
  {name: "Nephi", age: 29},
  {name: "Enos", age: 34},
  {name: "Tiancum", age: 43},
  {name: "Jacob", age: 27},
  {name: "Nephi", age: 29},
  {name: "Enos", age: 34}
  ];

  public schoolAttendence=[];
  public facultiesLists=[]
  public batchLists=[]
  public levelLists=[]
  public facultyId;
  public batchNo;
  public DateGroup:FormGroup;
  public messageCheck:FormGroup;
  public DateGroupOfSchool:FormGroup;
  public levelId;
  public attendenceLists=[];
  public studentLists=[]
  public dateRangeStart;
  public dateRangeEnd;
  public studentWiseLists=[];
  public attendenceFlag=true;
  public getDayWiseLists=[];
  public dayWiseClassesOfSchool=[];
  public batch;
  public faculty;
  public attendenceFlagSchool=true;
  constructor( private service:AttendenceService,private fb: FormBuilder) { }
  toppings = new FormControl();
  post = new FormControl();
  toppingList = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  public attendenceForm:FormGroup;
  public daterange: any = {};
  private selectedDate(value: any) {
      console.log(value)
      this.dateRangeStart = value.start._d;
      this.dateRangeEnd = value.end._d;
  }
  public calendarCanceled(e:any) {
    console.log(e);
  }

  public calendarApplied(e:any) {
    console.log(e);
  }
  ngOnInit() {
    this.DateGroup=new FormGroup({
      Date:new FormControl()
    });
        this.DateGroupOfSchool=new FormGroup({
      DateSchool:new FormControl()
    });
   this.attendenceForm=new FormGroup({
     batch:new FormControl(),
     level:new FormControl(),
     faculty:new FormControl(),
     student:new FormControl(),
   });
   this.messageCheck = this.fb.group({
      message: this.fb.array([]),
      discription:''
    });
   this.service.getBatch().subscribe(
    (response)=>{
     console.log(response)
     this.batchLists=response.batches;
    })
   this.service.getFaculty().subscribe(
    (response)=>{
     console.log(response)
     this.facultiesLists=response.faculties;
    })
   this.service.getAttendence().subscribe(
     (response)=>{
       console.log(response)
       this.attendenceLists=response.attendence;
       console.log(this.attendenceLists)
      })
   this.attendenceForm.controls.faculty.valueChanges.subscribe(
     (faculty)=>{
       console.log(faculty)
       if(faculty){
         this.facultyId=faculty.name;
         this.getStudent();
         for(let x in this.facultiesLists){
            if(this.facultiesLists[x].name === faculty.name){
               this.levelLists=this.facultiesLists[x].faculty_wise_class;
            }
         }
       }
     })
  this.attendenceForm.controls.batch.valueChanges.subscribe(
    (batch)=>{
      if(batch){
        this.batchNo=batch.batch;
        this.getStudent();
         }
          console.log(batch)
      }
    )
  this.attendenceForm.controls.level.valueChanges.subscribe(
    (faculties)=>{
        if(faculties){
              this.levelId=faculties.faculties;
              this.getStudent();
         }
          console.log(faculties)
    })
  this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }
  filter(val: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }
  public getStudent(){
   console.log('I am in')
   console.log( this.batchNo)
   console.log( this.facultyId)
   let Param={};
    if(this.facultyId && this.batchNo){
       console.log(this.facultyId,this.batchNo)
        Param['faculty']=this.facultyId
        Param['batch']=this.batchNo
        this.service.getStudent(Param).subscribe(
          (response)=>{
            console.log(response)
            this.studentLists=response.students;
          }
        )
     }
   }
  submitAttendenceForm(){
     console.log(this.attendenceForm.value)
     let data=this.attendenceForm.value;
     data['batch']=this.attendenceForm.controls.batch.value.batch;
     data['faculty']=this.attendenceForm.controls.faculty.value.name;
     data['start']=this.dateRangeStart;
     data['end']=this.dateRangeEnd;
     data['level']=this.attendenceForm.controls.level.value.faculties;
     data['student_id']=this.attendenceForm.controls.student.value.student_id;
     console.log(data)
     this.service.studentWise(data).subscribe(
       (response)=>{
          console.log(response)
          this.studentWiseLists=response;
       }
     )
   }  
  submitDate(index,faculty){
  console.log(index)
  console.log(this.DateGroup.controls.Date.value)
  this.batch=index;
  this.faculty=faculty;

  let data={};
  data['date']=this.DateGroup.controls.Date.value;
  data['batch']=index
  data['faculty']=faculty;

  this.service.getDayWise(data).subscribe(
    (response)=>{
           console.log(response)
           this.getDayWiseLists=response;
           this.attendenceFlag=false;
     })
   }
  dayWiseAttendence(){
    this.getDayWiseLists=[];
    let data={};
    data['date']=this.DateGroup.controls.Date.value;
    data['batch']= this.batch;
    data['faculty']=this.faculty;
    this.service.getDayWise(data).subscribe(
      (response)=>{
             console.log(response)
             this.getDayWiseLists=response;
             this.attendenceFlag=false;
       })
   } 
  changeFlag(){
    this.getDayWiseLists=[]
    this.attendenceFlag=true; 
   } 
  onChange(id:number,number:number ,isChecked: boolean) {
    const emailFormArray = <FormArray>this.messageCheck.controls.message;
     console.log(number)
    if(isChecked) {
      emailFormArray.push(new FormControl(number));
     } else {
      let index = emailFormArray.controls.findIndex(x => x.value == number)
      emailFormArray.removeAt(index);
     }
   }
  messageSubmit(){
  // console.log(this.messageCheck.value)
  // console.log(this.messageCheck.controls.message.value)
  }
  onLinkClickBatch(event){
  this.schoolAttendence=[];
  this.attendenceFlagSchool=true;
  console.log(event)
  console.log('text',event.tab.textLabel)
  for(let x in this.facultiesLists){
    if(event.tab.textLabel.toLowerCase() === this.facultiesLists[x].name.toLowerCase()){
      let para={}
      para['faculty_id']=this.facultiesLists[x].id;
      this.service.getAttendenceOfSchool(para).subscribe(
        (response)=>{
          console.log(response)
          console.log(this.schoolAttendence) 
          for(let x in response){
            for(let y in response[x]){
              if(response[x][y].classes.toLowerCase() !=='none'){
                this.schoolAttendence.push(response[x][y]);
              }
            }
          }
        }
      )
    }
  }
}
 dayWiseAttendenceofSchool(classes){
  // this.schoolAttendence=[];
  this.attendenceFlagSchool=false;
  let data={};
  var id;
  console.log(classes)
  for(let x in this.schoolAttendence){
     for(let y in this.schoolAttendence[x].faculty){
        id=this.schoolAttendence[x].faculty[y].id
     }  
  }
  data['faculty_id']=id;
  data['date']=this.DateGroupOfSchool.controls.DateSchool.value;
  data['class']=classes;
  console.log(data)
  this.service.getDatWiseAttendenceOfSchool(data).subscribe(
    (response)=>{
      this.dayWiseClassesOfSchool=response;
      console.log(response)
    })
  }
changeFlagOfSchool(){
  this.attendenceFlagSchool=true;
   console.log('it is true')
  }
}
