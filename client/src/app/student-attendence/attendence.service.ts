import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'
import { ENV } from "./../env";
@Injectable()
export class AttendenceService {

  constructor( private http:Http) { }
  getFaculty(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/faculties", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   private extractData(res: Response) {
    let response = res.json();
    return response || {};
  }
  private handleError(error: Response | any) {
    return Observable.throw(error);
   }
  getBatch(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_batch", options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getStudent(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/get_student", data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getAttendence(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(ENV.Request_URL + "/mis_api/get_attendence",options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   studentWise(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/student_wise", data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getDayWise(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/day_wise", data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getAttendenceOfSchool(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/get_attendence_of_school", data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   getDatWiseAttendenceOfSchool(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(ENV.Request_URL + "/mis_api/day_wise_of_school", data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
}

